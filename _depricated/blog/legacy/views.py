from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.db.models import Q
from django.contrib import auth
from waaagh.multiblog.user import auth_user, create_user, restore_password
from waaagh.multiblog.forms import NewUserForm, LoginForm, AmnesiaForm, PrivateMessageForm, AvatarForm
from waaagh.multiblog.models import  Post, PrivateMessage, UserProfile, Comment, User
from waaagh.multiblog.tools.mail import send_email
from waaagh.multiblog.controllers.online import add_online_user
from waaagh.multiblog.controllers.user import get_user_settings, update_user_avatar
from waaagh.multiblog.controllers.post import get_edit_post_data, get_new_post_data, get_single_post, get_edited_post, get_post
from waaagh.multiblog.controllers.tags import get_tags_list
from waaagh.multiblog.controllers.stable import get_stable_data
from _depricated.blog.consts import *
from waaagh.multiblog.controllers.post_list import *
from _depricated.blog.legacy.messages import new_private_massage_msg, new_private_massage_subj

def login_required(func):
    def login_check(*args, **kwargs):
        if not args[0].user.is_authenticated():
            return HttpResponseRedirect('/login/')
        else:
            add_online_user(args[0].user.username)
        return func(*args, **kwargs)
    return login_check

def staff_required(func):
    def staff_check(*args, **kwargs):
        if not args[0].user.is_staff:
            return HttpResponseRedirect('/')
        return func(*args, **kwargs)
    return staff_check

def post_page(request, page = 0, tag = None, author = None):
    """View for display post list (titles + starting text) page"""
    return render_to_response('main.html', {'stable': get_stable_data(request=request, tag=tag, current_user=author,
                                                    current_blog_author=author),
                                            'posts_list':get_posts_list(int(page), tag,author,
                                                    request.user if request.user.is_authenticated() else None),
                                            'blog': request.user.is_authenticated() and request.user.username == author })

def tags(request):
    return render_to_response('tags.html', {'stable': get_stable_data(request), 'tags': get_tags_list()})

@login_required
def bookmarks(request):
    return render_to_response('main.html', {'stable': get_stable_data(request),
                                            'posts_list': get_bookmarked_posts_list(request.user),
                                            'bookmarks': True })

def articles(request):
    return render_to_response('articles.html', {'stable': get_stable_data(request), 'articles': True })


def post(request, id):
    """View for single post"""
    post_ent = get_post(id)
    if post_ent is None:
        return HttpResponseRedirect('/')
    page_data = {'stable': get_stable_data(request=request, current_user=post_ent.author,
        current_blog_author=post_ent.author)}
    page_data.update(get_single_post(request, post_ent))
    return render_to_response('post.html', page_data)

@login_required
def new_post(request):
    """View for creating new post"""
    return render_to_response('new_post.html', {'stable': get_stable_data(request), 'form': get_new_post_data()})

@login_required
def edit_post(request, id):
    """View for edit existing post"""
    post = get_edited_post(request.user, id)
    if post is None:
        return HttpResponseRedirect('/post/' + id)
    return render_to_response('new_post.html', {'stable': get_stable_data(request), 'form': get_edit_post_data(post)})

@login_required
@staff_required
def delete_post(request, post):
    from waaagh.multiblog.controllers.post import delete_post
    delete_post(Post.objects.get(id=int(post)))
    return HttpResponseRedirect('/')

@login_required
@staff_required
def block_user(request, name):
    from waaagh.multiblog.controllers.user import block_user
    block_user(User.objects.get(username=name))
    return HttpResponseRedirect('/')

@login_required
@staff_required
def unblock_user(request, name):
    from waaagh.multiblog.controllers.user import unblock_user
    unblock_user(User.objects.get(username=name))
    return HttpResponseRedirect('/')



def login(request):
    """View for login user"""
    page_data = {'stable': get_stable_data(request)}
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            auth_user(request, form.cleaned_data)
            return HttpResponseRedirect('/')
    else:
        form = LoginForm()
    page_data.update({'form': form})
    return render_to_response('login.html', page_data)

def amnesia(request):
    page_data = {'stable': get_stable_data(request)}
    if request.method == 'POST':
        form = AmnesiaForm(request.POST)
        if form.is_valid():
            restore_password(form.cleaned_data)
            page_data.update({'success': True})
    else:
        form = AmnesiaForm()
    page_data.update({'form': form})
    return render_to_response('amnesia.html', page_data)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def new_user(request):
    """View for creating user"""
    page_data = {'stable': get_stable_data(request)}
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            create_user(request, form.cleaned_data)
            return HttpResponseRedirect('/')
    else:
        form = NewUserForm()
    page_data.update({'form': form})
    return render_to_response('new_user.html', page_data)

@login_required
def settings(request):
    if request.method == 'POST':
        form = AvatarForm(request.POST, request.FILES)
        if form.is_valid():
            update_user_avatar(request.user, form.cleaned_data)
        page_data = {'form': form, 'avatar_form': True}
    else:
        page_data = {'form': AvatarForm()}
    page_data.update({'stable': get_stable_data(request)})
    page_data.update(get_user_settings(request.user))
    return render_to_response('settings.html', page_data)

def user(request, name):
    """View for creating user"""
    from _depricated.blog.legacy.voting import get_user_vote_limit
    user = User.objects.get(username = name)
    page_data = {'stable': get_stable_data(request),
                 'profile': user.get_profile(),
                 'rate_limit': get_user_vote_limit(user),
                 'posts_count': Post.objects.filter(author=user).count(),
                 'comments_count': Comment.objects.filter(author=user).count(),
                 'blog_count': Post.objects.filter(author=user).count(),
                 'posts': Post.objects.filter(author=user).order_by('-time')[0:user_last_post_limit],
                 'blog': Post.objects.filter(author=user).order_by('-time')[0:user_last_post_limit]}
    return render_to_response('user.html', page_data)

def users(request, page = 0):
    page = int(page)
    page_data = {'stable': get_stable_data(request),
                 'users': UserProfile.objects.all().order_by('-rate')[page * users_per_page_limit: (page + 1) * users_per_page_limit]}
    if page > 0:
        page_data.update({'prev': page - 1})
    if page < ((UserProfile.objects.all().count() - 1) / users_per_page_limit):
        page_data.update({'next': page + 1})
    return render_to_response('users.html', page_data)

def admins(request):
    return render_to_response('users.html', {'stable': get_stable_data(request),
                                             'users': [user.get_profile() for user in User.objects.filter(is_staff=True).order_by('date_joined')]})

@login_required
def edit_comment(request, id):
    from _depricated.blog.legacy.controllers.comment import edit_comment
    comment = Comment.objects.get(id=int(id))
    if comment.author.username != request.user.username and not request.user.is_staff:
        return HttpResponseRedirect('/post/%d/' % comment.post_id)
    if request.method == 'POST':
        edit_comment(
            comment=comment,
            text=request.POST['text'],
            proc_images= 'proc_images' in list(request.POST.keys()),
            proc_ref = 'proc_ref' in list(request.POST.keys()),
            proc_lines ='proc_lines' in list(request.POST.keys())
        )
        return HttpResponseRedirect('/post/%d/' % comment.post_id)
    else:
        page_data = {'stable': get_stable_data(request),
                     'form': {'text': comment.text }}
        return render_to_response('new_comment.html', page_data)

@login_required
def new_comment(request, post, parent=None):
    from _depricated.blog.legacy.controllers.comment import new_comment
    if request.method == 'POST':
        new_comment(
            author=request.user,
            post=Post.objects.get(id=int(post)),
            parent=Comment.objects.get(id=int(parent)) if parent is not None else None,
            text=request.POST['text'],
            proc_images= 'proc_images' in list(request.POST.keys()),
            proc_ref = 'proc_ref' in list(request.POST.keys()),
            proc_lines ='proc_lines' in list(request.POST.keys())
        )
        return HttpResponseRedirect('/post/%s/' % post)
    post = Post.objects.get(id=int(post))
    page_data = {'stable': get_stable_data(request),
                 'form': {'text':
                              quote_text(Comment.objects.get(id=int(parent)).text if parent is not None else post.text)}}
    return render_to_response('new_comment.html', page_data)


# Private Messages
@login_required
def new_message(request, to = None):
    page_data = {'stable': get_stable_data(request)}
    if request.method == 'POST':
        form = PrivateMessageForm(request.POST,current_user=request.user.username)
        if form.is_valid():
            message = PrivateMessage(from_user=request.user, to_user=User.objects.get(username=form.cleaned_data['to_user']),
                subject=form.cleaned_data['subject'], text=form.cleaned_data['text'])
            message.save()
            if message.to_user.get_profile().new_messages_notify:
                send_email(EmailMessage(new_private_massage_subj,
                    new_private_massage_msg % {'username' : message.to_user.username,
                                               'author': message.from_user.username,
                                               'text': message.text}, to=[message.to_user.email]))
            return HttpResponseRedirect('/mail/')
    else:
        form = PrivateMessageForm(initial={'to_user':to})
    page_data.update({'form': form, 'mail': {'zone': {'new': True}}})
    return render_to_response('new_mail.html', page_data)

def get_unread_list(user):
    data = {'inbox': PrivateMessage.objects.filter(to_user=user, receiver_status=PrivateMessage.status_normal,
                read_time=None).count(),
            'outbox': PrivateMessage.objects.filter(from_user=user, sender_status=PrivateMessage.status_normal,
                read_time=None).count(),
            'trash': PrivateMessage.objects.filter(to_user=user,
                read_time=None, receiver_status=PrivateMessage.status_trash).count(),
            'box': PrivateMessage.objects.filter(
                Q(from_user=user, sender_status=PrivateMessage.status_normal)|Q(to_user=user,
                    receiver_status=PrivateMessage.status_normal), read_time=None).count()}
    return data

def get_mail_list(request, condition, zone):
    return render_to_response('mail.html',
            { 'stable': get_stable_data(request),
              'mail': {'zone': { zone: True,
                                 'mail_list' : PrivateMessage.objects.filter(condition).order_by('-send_time')},
                       'unread': get_unread_list(request.user)}})

@login_required
def mail(request):
    return get_mail_list(
        request=request,
        condition = Q(Q(from_user=request.user, sender_status=PrivateMessage.status_normal)|
                      Q(to_user=request.user, receiver_status=PrivateMessage.status_normal)),
        zone = 'box')

@login_required
def mail_inbox(request):
    return get_mail_list(
        request=request,
        condition = Q(to_user=request.user, receiver_status=PrivateMessage.status_normal),
        zone = 'inbox')

@login_required
def mail_outbox(request):
    return get_mail_list(
        request=request,
        condition = Q(from_user=request.user, sender_status=PrivateMessage.status_normal),
        zone = 'outbox')

@login_required
def mail_trash(request):
    return get_mail_list(
        request=request,
        condition = Q(Q(from_user=request.user, sender_status=PrivateMessage.status_trash)|
                      Q(to_user=request.user, receiver_status=PrivateMessage.status_trash)),
        zone = 'trash')

@login_required
def read_mail(request, id):
    message = PrivateMessage.objects.get(id=int(id))
    if message.from_user.username != request.user.username and message.to_user.username != request.user.username:
        raise Exception('Access denied for reading this message')
    if message.to_user.username == request.user.username and message.read_time is None:
        from datetime import datetime
        message.read_time = datetime.now()
        message.save()

    zone = 'box'
    if message.to_user.username == request.user.username:
        if message.receiver_status == PrivateMessage.status_normal:
            zone = 'inbox'
        if message.receiver_status == PrivateMessage.status_trash:
            zone = 'trash'
    elif message.from_user.username == request.user.username:
        if message.sender_status == PrivateMessage.status_normal:
            zone = 'outbox'
        if message.sender_status == PrivateMessage.status_trash:
            zone = 'trash'

    page_data = {'stable': get_stable_data(request),
                 'mail': {'zone': {zone: True, 'message' : message},
                          'unread': get_unread_list(request.user)}}
    return render_to_response('read_mail.html', page_data)

@login_required
def delete_mail(request, id):
    message = PrivateMessage.objects.get(id=int(id))
    if message.from_user.username != request.user.username and message.to_user.username != request.user.username:
        raise Exception('Access denied for deleting this message')
    deleted = False
    if message.to_user.username == request.user.username:
        if message.receiver_status == PrivateMessage.status_normal:
            message.receiver_status = PrivateMessage.status_trash
        elif message.receiver_status == PrivateMessage.status_trash:
            message.receiver_status = PrivateMessage.status_deleted
            deleted = True
        message.save()
    elif message.from_user.username == request.user.username:
        if message.sender_status== PrivateMessage.status_normal:
            message.sender_status = PrivateMessage.status_trash
        elif message.sender_status == PrivateMessage.status_trash:
            message.sender_status = PrivateMessage.status_deleted
            deleted = True
        message.save()
    if message.sender_status == PrivateMessage.status_deleted and message.receiver_status == PrivateMessage.status_deleted:
        message.delete()
    if deleted:
        return HttpResponseRedirect('/mail/')
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

@login_required
def restore_mail(request, id):
    message = PrivateMessage.objects.get(id=int(id))
    if message.from_user.username != request.user.username and message.to_user.username != request.user.username:
        raise Exception('Access denied for restoring this message')
    if message.to_user.username == request.user.username:
        message.receiver_status = PrivateMessage.status_normal
        message.save()
    elif message.from_user.username == request.user.username:
        message.sender_status = PrivateMessage.status_normal
        message.save()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

def quote_text(text):
    lines = text.splitlines()
    result = ''
    for line in lines:
        if len(line) is not 0:
            result += '> ' + line + '\r\n'
        else:
            result += line + '\r\n'
    return result

@login_required
def reply_mail(request, id):
    page_data = {'stable': get_stable_data(request)}
    if request.method == 'POST':
        form = PrivateMessageForm(request.POST,current_user=request.user.username)
        if form.is_valid():
            message = PrivateMessage(from_user=request.user, to_user=User.objects.get(username=form.cleaned_data['to_user']),
                subject=form.cleaned_data['subject'], text=form.cleaned_data['text'])
            message.save()
            if message.to_user.get_profile().new_messages_notify:
                send_email(EmailMessage(new_private_massage_subj,
                    new_private_massage_msg % {'username' : message.to_user.username,
                                               'author': message.from_user.username,
                                               'text': message.text},
                    to=[message.to_user.email]))
            return HttpResponseRedirect('/mail/')
    else:
        message = PrivateMessage.objects.get(id=int(id))
        to = None
        if message.to_user.username == request.user.username:
            to = message.from_user
        elif message.from_user.username == request.user.username:
            to = message.to_user
        form = PrivateMessageForm(initial={'to_user':to,
                                           'subject': 'Re: '+message.subject,
                                           'text': quote_text(message.text)})
    page_data.update({'form': form, 'mail': {'zone': {'new': True}}})
    return render_to_response('new_mail.html', page_data)

def search(request):
    return render_to_response('search.html', {'stable': get_stable_data(request)})

@login_required
def hq(request, system=None, army=None):
    from waaagh.multiblog.hq.roster import get_roster, create_roster
    roster = get_roster(request.user)
    if roster:
        return render_to_response('hq/'+roster.system+'/'+roster.army+'/roster.html', {'stable': get_stable_data(request),
                                                                         'roster': roster})
    if system and army:
        return render_to_response('hq/'+system+'/'+army+'/roster.html', {'stable': get_stable_data(request),
                                                                         'roster': create_roster(request.user, system, army)})
    return render_to_response('hq/hq.html', {'stable': get_stable_data(request)} )

@login_required
def hq_print(request, guid):
    from waaagh.multiblog.hq.roster import get_roster_by_guid
    return render_to_response('hq/wh40k/roster_print.html', {'roster': get_roster_by_guid(request.user, guid)})

@login_required
def hq_reset(request, guid):
    from waaagh.multiblog.hq.roster import reset_roster
    reset_roster(request.user, guid)
    return HttpResponseRedirect('/hq')
