# -*- coding: utf-8 -*-

__author__ = 'dante'

error_required = 'Обязательное поле'
error_invalid = 'Введите корректное значение'
error_invalid_email = 'Введите корректное значение e-mail адресa'
error_passwords_not_match = 'Пароли не совпадают'
error_username_exist = 'Пользователь с таким именем уже существует'
error_email_exist = 'E-mail уже используется'
error_email_not_found = 'E-mail не найден'
error_user_not_found = 'Пользователь не найден'
error_self_mail = 'Вы не можете послать письмо самому себе'
error_access_denied = 'Неверный пароль'
error_max_value = 'Проверьте что данное значение меньше либо равно %(limit_value)s'
error_min_value = 'Проверьте что данное значение больше либо равно %(limit_value)s'
error_date = 'Неверная дата'
error_max_avatar_size = 'Превышен максимальный размер аватара (%d килобайт)'

months = {
    1: 'январь',
    2: 'февраль',
    3: 'март',
    4: 'апрель',
    5: 'май',
    6: 'июнь',
    7: 'июль',
    8: 'август',
    9: 'сентябрь',
    10: 'октябрь',
    11: 'ноябрь',
    12: 'декабрь'}

restore_password_subj = 'Восстановление пароля'
restore_password_msg = 'Здравствуйте\r\nВашему пользователю www.DaBunker.com: %(name)s\r\nбыл установлен новый пароль: %(password)s'

new_comment_subj = 'Новый комментарий'
new_comment_msg = 'В теме "%(title)s" за которой Вы следите www.DaBunker.com/post/%(post_id)d пользователь %(author)s оставил комментарий следующего содержания:\r\n%(text)s'

new_private_massage_subj = 'Новое личное сообщение'
new_private_massage_msg = 'Вашему пользователю: %(username)s, пришло новое личное сообщения от пользователя: %(author)s, следующего содержания:\r\n %(text)s'
