function scrollToElement(el) {
    $("html, body").animate({ scrollTop: el.offset().top - 50 }, 400);
}

function isDigit(c) {
    return c >= 48 && c <= 57
}

function digitFilter(obj, event) {
    return isDigit(event.charCode);
}

function isValidEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var AjaxTransport = function(url) {
    var self = this;
    self.url = url;
    self.sending = ko.observable(false);

    self.send = function(data, success, fail)
    {
        if (self.sending())
            return false;
        self.sending(true);
        $.ajax({
            url:self.url,
            type: 'POST',
            data: {'data': JSON.stringify(data)},
            dataType : "json",
            success: function (data) {
                if (data.redirect)
                    document.location.href = data.redirect;
                else if (data.error)
                    document.modalInfo.error(data.error);
                else if (data.success)
                    document.modalInfo.success(data.success);
                if (success)
                    success(data);
                self.sending(false);
            },
            error: function () {
                if (fail)
                    fail();
                document.modalInfo.error("Server connection error");
                self.sending(false);
            }
        });
        return true;
    }
};

var Info = function(id, show) {
    var self = this;
    this.confirmFlag = ko.observable(false);
    this.type = ko.observable('info');
    this.header = ko.observable('');
    this.message = ko.observable('');

    this.onYesClick = function() { self.onYes(); };

    this.show = function(state, head, message, onYes) {
        self.confirmFlag(onYes != undefined);
        self.message(message);
        self.header(head);
        self.type(state);
        self.onYes = onYes || function() { alert('Info.onYes() not defined'); };
        show();
    };

    this.error = function(message, onYes) { self.show('danger', 'Error!', message, onYes); };
    this.info = function(message, onYes) { self.show('info', 'Information', message, onYes); };
    this.warning = function(message, onYes) { self.show('warning', 'Warning!', message, onYes); };
    this.success = function(message, onYes) { self.show('success', 'Success!', message, onYes); };

    ko.applyBindings(this, document.getElementById(id));
};
