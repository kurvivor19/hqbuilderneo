/**
 * Created by dromanow on 4/25/14.
 */


var OptionalSubUnit = function(opt) {
    var self = this;
    this.type = 'optional_sub_unit';
    this.setupNode(opt.parent, opt.data);
    this.name = opt.data.name;
    this.options = new OptionsList({
        parent: self,
        data: opt.data.options
    });
    this.sub_units = ko.observableArray( opt.data.units.map(function(unit) {
        if (unit.type == 'sub_unit') {
            var su = new SubUnit({parent: self, data: unit});
            su.updateUnit = function(obj) {
                self.parent.update({id: self.id, units: [{id: su.id, unit: obj}]})
            };
            return su;
        }
        if (unit.type == 'unit_list') {
            var ul = new UnitList({parent: self, data: unit});
            ul.updateUnit = function(obj) {
                self.parent.update({id: self.id, units: [{id: ul.id, units: [obj]}]})
            }
            ul.addUnit = function(unit) {
                self.parent.update({id: self.id, units: [{id: ul.id, add: unit.id}]});
            };
            ul.deleteUnit = function(unit) {
                self.parent.update({id: self.id, units: [{id: ul.id, 'delete': unit.id}]});
            };
            ul.splitUnit = function(unit) {
                self.parent.update({id: self.id, units: [{id: ul.id, split: unit.id}]});
            };
            return ul;
        }
    }));
};

OptionalSubUnit.prototype = new Node();

OptionalSubUnit.prototype.update = function(obj) {
    this.parent.update({id: this.id, options: obj})
};

OptionalSubUnit.prototype.pushChanges = function(data) {
    var self = this;
    this.genericPush(data);
    if (data.options)
        this.options.pushChanges(data.options);
    if (data.units)
        $.each(data.units, function(n, unit_data) {
            $.each(self.sub_units(), function(n, unit) {
                if (unit.id == unit_data.id)
                    unit.pushChanges(unit_data);
            });
        });
};
