/**
 * Created by dromanow on 4/29/14.
 */


var OptionsBlock = function(parent, options) {
    var options_types = {
        one_of: OneOf,
        options_list: OptionsList,
        count: GenericCount,
        model_count: SkipCount,
        sub_unit: SubUnit,
        sub_roster: SubRoster,
        formation: Formation,
        unit_list: UnitList,
        optional_sub_unit: OptionalSubUnit
    };
    var self = this;

    this.options = ko.observableArray(options.reduce(function(val, item) {
        if (item.type)
            val.push(new options_types[item.type]({ parent: parent, data: item }));
        return val;
    }, []));

    this.visible = ko.computed(function() { return self.hasVisible(); })
};

OptionsBlock.prototype.hasVisible = function() {
    return this.options().reduce(function(val, item){ return val || item.visible(); }, false)
};

OptionsBlock.prototype.pushChanges = function(data) {
    var self = this;
    $.each(data, function(n, option_data){
        $.each(self.options(), function(n, option) {
            if (option.id == option_data.id)
                option.pushChanges(option_data);
        });
    });
};

OptionsBlock.prototype.getType = function(option) {
    return option.type;
};
