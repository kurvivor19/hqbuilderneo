__author__ = 'Denis Romanov'

from datetime import datetime
from flask import url_for
from db.models import Roster, db


class ReloadRosterError(Exception):
    """
    Used to request reload of page if it is needed
    """
    pass


class Builder(object):

    class RosterCount(object):
        def __init__(self):
            self._count = None

        @property
        def count(self):
            if self._count is None:
                self.update()
            return self._count

        def update(self):
            self._count = Roster.query.count()

        def inc(self):
            self.update()

    def __init__(self, debug, logger):
        super(Builder, self).__init__()
        from . import games
        self.games = games.Games(debug, logger)
        self.roster_count = self.RosterCount()

    @staticmethod
    def _get_roster(roster_id):
        # TODO: maybe remove somehow?
        if str(roster_id).isdigit():
            roster = Roster.query.filter_by(id=int(roster_id)).first_or_404()
        else:
            # TODO: this is temporary roster link with mongodb key
            roster = Roster.query.filter_by(legacy_key=roster_id).first_or_404()
        return roster

    def _build_header(self, roster_data):
        _game_name, _army_name, army_tags = self.games.get_roster_name(roster_data['army_id'])

        class Header(object):
            army_name = _army_name
            game_name = _game_name
            name = roster_data.get('name', army_name)
            points = roster_data.get('points', 0)
            limit = roster_data.get('limit', None)
            description = roster_data.get('description', '')
            tags = army_tags + roster_data.get('tags', [])

            title = '{name} {army_name}({points}{limit}pt.)'.format(
                name=name,
                points=points,
                limit='/' + str(limit) if limit else '',
                army_name='[{0}] '.format(army_name.encode()) if army_name != name else ''
            )

        return Header

    def _build_roster(self, owner_id, data):
        roster = Roster(ow=int(owner_id), data=data)
        db.session.add(roster)
        db.session.commit()
        self.roster_count.inc()
        return roster.id

    def build_header(self, roster_object):
        return self._build_header(roster_object.data)

    def build_description(self, roster_object):
        return self.games.load_roster(roster_object.id, roster_object.data).dump()

    def create_roster(self, owner_id, army):
        return self._build_roster(owner_id, self.games.get_roster_object(army).save())

    def get_roster_dump(self, roster_id, owner_id):
        roster_object = self._get_roster(roster_id)
        roster = self.games.load_roster(roster_object.id, roster_object.data).dump()
        roster['read_only'] = not owner_id or (roster_object.owner_id != int(owner_id))
        return roster

    def get_user_rosters(self, user_id):
        res = []
        for roster in Roster.query.filter_by(owner_id=user_id,
                                             state=Roster.State.PRIVATE).order_by(Roster.last_update.desc()):
            header = self._build_header(roster.data)
            res.append({
                'id': roster.id,
                'url': url_for('builder.edit_roster', roster_id=roster.id),
                'type_name': header.army_name,
                'game': header.game_name,
                'name': header.name,
                'points': header.points,
                'limit': header.limit,
                'description': header.description,
                'tags': header.tags
            })
        return res

    def process_roster(self, roster_id, owner_id, data):
        roster_object = self._get_roster(roster_id)
        if not roster_object or roster_object.owner_id != int(owner_id):
            return
        roster = self.games.load_roster(roster_object.id, roster_object.data)
        reload = False
        try:
            roster.update(data)
        except ReloadRosterError:
            reload = True
        roster.check_rules_chain()
        delta = roster.delta
        roster_object.data = roster.save()
        roster_object.last_update = datetime.utcnow()
        db.session.commit()
        if reload:
            raise ReloadRosterError
        return {'roster': delta}

    @staticmethod
    def delete_rosters(owner, ids):
        db.session.query(Roster).filter(Roster.id.in_(ids)).filter(Roster.owner_id == int(owner)).\
            update({
                Roster.state: Roster.State.DELETED,
                Roster.last_update: datetime.utcnow()
            }, synchronize_session=False)
        db.session.commit()

    def clone_roster(self, roster_id, owner_id):
        return self._build_roster(owner_id, self._get_roster(roster_id).data)
