from builder.core import id_filter
from builder.core.options import GenericOption
from builder.core import node_load

__author__ = 'Denis Romanov'


class SubUnit(GenericOption):
    is_sub_unit = True

    def __init__(self, id, unit):
        GenericOption.__init__(self, id, unit.name)
        self._unit = self.observable_object('unit', unit)

    def dump(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'type': 'sub_unit',
                'unit': self._unit.get().dump(),
            }
        )
        return res

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'unit': self._unit.get().dump_save(),
            }
        )
        return res

    @node_load
    def load(self, data):
        self._unit.get().load(data['unit'])

    @id_filter
    def update(self, data):
        if 'unit' in list(data.keys()):
            self._unit.get().update(data['unit'])

    def get_unit(self):
        return self._unit.get()

    def get_selected(self):
        if not self.is_used():
            return None
        return {'sub_unit': self._unit.get().description.get(), 'count': self._unit.get().get_count()}

    def points(self):
        if not self.is_used():
            return 0
        return self._unit.get().points.get()

    def get_count(self):
        if not self.is_used():
            return 0
        return self._unit.get().get_count()

    def get_errors(self):
        return self.get_unit().get_errors()

    def set_active(self, val):
        GenericOption.set_active(self, val)
        GenericOption.set_visible(self, val)
