from builder.core import id_filter, node_load
from builder.core.options import GenericOption, norm_counts
from builder.core.options.count import Count
from functools import reduce

__author__ = 'Denis Romanov'


class UnitList(GenericOption):
    def __init__(self, id, name, unit_class, min, max):
        GenericOption.__init__(self, id, name)
        self.unit_class = unit_class
        self.min = min
        self.max = max
        self._can_add = self.observable('can_add', True)
        self._can_delete = self.observable('can_delete', False)
        self._can_split = self.observable('can_split', [])
        self._deleted_unit = self.observable('deleted_unit', '')
        self._new_unit = self.observable('new_unit', {})
        self._units = self.observable_array('units', [unit_class()])

    def get_count(self):
        if not self.is_used():
            return 0
        return reduce(lambda val, u: val + u.get_count(), self._units.get(), 0)

    def dump(self):
        res = GenericOption.dump(self)
        res.update({
            'type': 'unit_list',
            'id': self.id,
            'can_add': self._can_add.get(),
            'can_delete': self._can_delete.get(),
            'can_split': self._can_split.get(),
            'units': [unit.dump() for unit in self._units.get()],
        })
        return res

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'can_add': self._can_add.get(),
                'can_delete': self._can_delete.get(),
                'can_split': self._can_split.get(),
                'units': [unit.dump_save() for unit in self._units.get()],
            }
        )
        return res

    @node_load
    def load(self, data):
        self._can_add.set(data['can_add'])
        self._can_delete.set(data['can_delete'])
        self._can_split.set(data.get('can_split', [])),
        self._units.get()[:] = []
        for unit_data in data['units']:
            unit = self.unit_class()
            unit.load(unit_data)
            self._units.get().append(unit)

    def add_unit(self):
        if self.get_count() >= self.max:
            return
        unit = self.unit_class()
        unit.check_rules_chain()
        self._units.get().append(unit)
        self._new_unit.set(unit.dump())

    def delete_unit(self, id):
        for u in self._units.get():
            if u.id != id:
                continue
            self._units.get().remove(u)
            self._deleted_unit.set(id)
        self.update_range(self.min, self.max)

    def split_unit(self, unit_id):
        for u in self._units.get():
            if u.id != unit_id:
                continue
            if not isinstance(u.count, Count) and u.count.get() <= 1:
                print('Error: can\'t split unit')
            new_unit = u.clone()
            u.count.set(u.count.get() - 1)
            new_unit.count.set(1)
            u.check_rules_chain()
            new_unit.check_rules_chain()
            self._units.get().append(new_unit)
            self._new_unit.set(new_unit.dump())

    @id_filter
    def update(self, data):
        if 'add' in list(data.keys()):
            self.add_unit()
        if 'delete' in list(data.keys()):
            self.delete_unit(data['delete'])
        if 'split' in list(data.keys()):
            self.split_unit(data['split'])
        if 'units' in list(data.keys()):
            for unit_data in data['units']:
                for unit in self._units.get():
                    unit.update(unit_data)
        self.update_actions_status()

    def get_selected(self):
        if not self.is_used():
            return None
        self.check_units_rules()
        return [{'sub_unit': u.description.get(), 'count': u.get_count()} for u in self._units.get()]

    def points(self):
        if not self.is_used():
            return 0
        self.check_units_rules()
        return reduce(lambda val, u: val + u.points.get(), self._units.get(), 0)

    def update_range(self, min_limit=None, max_limit=None):
        if max_limit and max_limit < len(self._units.get()):
            return
        if min_limit:
            self.min = min_limit
        if max_limit:
            self.max = max_limit
        norm_counts(self.min, self.max, [m.count for m in self._units.get() if isinstance(m.count, Count)], 1)
        self.update_actions_status()

    def get_units(self):
        return self._units.get()

    def update_actions_status(self):
        self._can_delete.set(len(self._units.get()) > 1)
        self._can_add.set(self.get_count() < self.max)
        self._can_split.set([unit.id for unit in (unit for unit in self._units.get() if unit.get_count() > 1)])

    def check_units_rules(self):
        for u in self.get_units():
            u.reset_errors()
            u.check_rules()

    def get_errors(self):
        return sum((unit.get_errors() for unit in self.get_units()), [])

    def set_active(self, val):
        GenericOption.set_active(self, val)
        GenericOption.set_visible(self, val)
