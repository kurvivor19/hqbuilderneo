from builder.core import id_filter, build_id, build_full_name, node_load
from builder.core.options import GenericOption
from functools import reduce

__author__ = 'Denis Romanov'


class OptionsList(GenericOption):

    class SingleOption(GenericOption):
        def __init__(self, option_id, name, title, points, value):
            GenericOption.__init__(self, option_id, name)
            self.title = title
            self._points = points
            self._value = self.observable('value', value)
            self.strong = False

        def set(self, val):
            self._value.set(val)

        def get(self):
            return self.is_used() and self._value.get()

        def points(self):
            return self.cost() if self.get() and self.is_used() else 0

        def cost(self):
            return self._points

        @id_filter
        def update(self, data):
            self.set(data['value'])

        def dump(self):
            res = GenericOption.dump(self)
            res.update({
                'name': self.name,
                'value': self.get(),
            })
            return res

        def dump_save(self):
            res = GenericOption.dump(self)
            res.update({
                'val': self.get(),
            })
            return res

        @node_load
        def load(self, data):
            self.set(data.get('val', False))

        def set_strong_active(self, val):
            self.set_active(val)
            if not val:
                self.strong = True

    def __init__(self, id, name, variants, limit=None, points_limit=None):
        GenericOption.__init__(self, id, name)
        self._variants = self.observable_array('options', [self.SingleOption(
            option_id=v[2] if len(v) > 2 else build_id(v[0]),
            name=build_full_name(v[0], v[1]),
            title=v[0],
            points=v[1],
            value=False) for v in variants])
        self.variants_dict = {}
        for v in self._variants.get():
            self.variants_dict[v.id] = v
        self.limit = limit
        self.points_limit = points_limit

    @id_filter
    def update(self, data):
        for variant in self._variants.get():
            for option in data['options']:
                variant.update(option)
        self.update_limit()

    def set_active_options(self, ids, val):
        for opt_id in ids:
            self.variants_dict[opt_id].set_strong_active(val)
            if val is False:
                self.variants_dict[opt_id].set(False)
        self.update_limit()

    def set_visible_options(self, ids, val):
        for opt_id in ids:
            self.variants_dict[opt_id].set_visible(val)
            if val is False:
                self.variants_dict[opt_id].set(False)
        if val:
            self.update_limit()

    def set_active_all(self, val):
        for v in self._variants.get():
            v.set_active(val)
            if val is False:
                v.set(False)
        if val:
            self.update_limit()

    def points(self):
        if not self.is_used():
            return 0
        return reduce(lambda val, i: val + i.points(), self._variants.get(), 0)

    def get_selected(self):
        if not self.is_used():
            return
        res = []
        for v in self._variants.get():
            if v.get():
                res.append(v.title)
        return res

    def get(self, id):
        if id in self.variants_dict:
            return self.variants_dict[id].get()
        else:
            return False

    def get_all(self):
        if not self.is_used():
            return []
        return [v.id for v in self._variants.get() if v.get()]

    def get_all_ids(self):
        return [i.id for i in self._variants.get()]

    def set(self, id, val):
        self.variants_dict[id].set(val)
        self.update_limit()

    def dump(self):
        res = GenericOption.dump(self)
        res.update({
            'type': 'options_list',
            'name': self.name,
            'options': [v.dump() for v in self._variants.get()]
        })
        return res

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update({
            'options': [v.dump_save() for v in self._variants.get()]
        })
        return res

    @node_load
    def load(self, data):
        for variant_data in data.get('options', []):
            if variant_data['id'] in list(self.variants_dict.keys()):
                self.variants_dict[variant_data['id']].load(variant_data)

    def update_limit(self):
        #self.update_count_limit()
        #self.update_points_limit()
        if self.limit is None and self.points_limit is None:
            return
        if self.limit is None:
            mcount = len(self._variants.get()) + 1
        else:
            mcount = self.limit
        if self.points_limit is None:
            rest = sum((v.cost() for v in self._variants.get()))
        else:
            rest = self.points_limit - self.points()
        allc = reduce(lambda val, v: val + 1 if v.get() else val, self._variants.get(), 0)
        if allc > mcount:
            for v in self._variants.get():
                v.set(False)
            allc = 0
        for v in self._variants.get():
            if v.strong and not v.is_active():
                continue
            v.set_active(v.get() or (allc < mcount and v.cost() <= rest))

    def update_count_limit(self):
        if self.limit is None:
            return
        all = reduce(lambda val, v: val + 1 if v.get() else val, self._variants.get(), 0)
        if all > self.limit:
            for v in self._variants.get():
                v.set(False)
        for v in self._variants.get():
            v.set_active(v.get() or all < self.limit)

    def set_limit(self, limit=None, points_limit=None):
        self.limit = limit
        self.points_limit = points_limit
        self.update_limit()

    def update_points_limit(self):
        if self.points_limit is None:
            return
        rest = self.points_limit - self.points()
        for v in self._variants.get():
            v.set_active(v.get() or v.cost() <= rest)

    def is_selected(self, ids):
        if isinstance(ids, str):
            ids = [ids]
        return next((True for i in ids if self.get(i)), False)

    def is_any_selected(self):
        return self.is_used() and any((v.get() for v in self._variants.get()))
