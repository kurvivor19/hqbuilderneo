__author__ = 'Denis Romanov'

from builder.core import id_filter
from builder.core.options import GenericOption
from builder.core.options.options_list import OptionsList
from builder.core.options.sub_unit import SubUnit
from builder.core import node_load, build_id


class OptionalSubUnit(GenericOption):

    def __init__(self, id, name, units):
        GenericOption.__init__(self, id, name)
        if not isinstance(units, (list, tuple)):
            units = [units]
        self._options = self.observable_object('options', OptionsList('osn_ol_' + id, name, [[u.name, u.base_points, build_id(u.name)] for u in units], limit=1))
        self._units = self.observable_array('units', [SubUnit(unit=u, id=build_id(u.name)) for u in units])
        self.unit = None

    def dump(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'type': 'optional_sub_unit',
                'options': self._options.get().dump(),
                'units': [unit.dump() for unit in self._units.get()]
            }
        )
        return res

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'options': self._options.get().dump_save(),
                'units': [unit.dump_save() for unit in self._units.get()]
            }
        )
        return res

    @node_load
    def load(self, data):
        self._options.get().load(data['options'])
        for unit_data in data['units']:
            next(o for o in self._units.get() if o.id == unit_data['id']).load(unit_data)

    @id_filter
    def update(self, data):
        if 'options' in list(data.keys()):
            self._options.get().update(data['options'])
        if 'units' in list(data.keys()):
            for unit_data in data['units']:
                next(o for o in self._units.get() if o.id == unit_data['id']).update(unit_data)
        self.check_rules()

    def check_rules(self):
        selected = self._options.get().get_all()
        if not selected:
            self.unit = None
        for u in self._units.get():
            sel = u.id in selected
            if sel:
                self.unit = u
            u.set_active(sel)

    def check_rules_chain(self):
        GenericOption.check_rules_chain(self)
        self.check_rules()

    def get_count(self):
        if not self.is_used() or not self.unit:
            return 0
        return self.unit.get_count()

    def get(self, name):
        return self._options.get().get(build_id(name))

    def get_unit(self):
        return self.unit

    def get_options(self):
        return self._options.get()

    def get_selected(self):
        if not self.is_used() or not self.unit:
            return None
        return {'sub_unit': self.unit.get_unit().description.get(), 'count': self.unit.get_count()}

    def set_active_options(self, names, val):
        self._options.get().set_active_options([build_id(name) for name in names], val)
        self.check_rules()

    def set_visible_options(self, names, val):
        self._options.get().set_visible_options([build_id(name) for name in names], val)
        self.check_rules()

    def points(self):
        if not self.is_used() or not self.unit:
            return 0
        return self.unit.points()

    def get_errors(self):
        if self.get_unit():
            return self.get_unit().get_unit().get_errors()
        return []

    def set_active(self, val):
        GenericOption.set_active(self, val)
        GenericOption.set_visible(self, val)
