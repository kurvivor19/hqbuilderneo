from builder.core.section import Section

__author__ = 'Ivan Truskov'


class PercentSection(Section):
    def __init__(self, name, types, min=0, max=100, roster=None):
        Section.__init__(self, name, min, max, types, roster)

    def check_limits(self):
        pass

    def count_choices(self, key, group):
        return len([item for item in group])

    def get_limits_error(self):
        if not self.roster.limit:
            return
        current_percent = 100.0 * float(self.get_points()) / self.roster.limit
        if self.max < current_percent:
            return 'Section {0} is more then {1}% of total points'.format(self.name, self.max)
        elif self.min > current_percent:
            return 'Section {0} is less then {1}% of total points'.format(self.name, self.min)
