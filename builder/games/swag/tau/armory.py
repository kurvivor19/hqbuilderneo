__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Photon grenades', 10, var_dicts=[
            {'name': 'Weapon reload', 'points': 5}
        ])


class Basic(OptionsList):
    def __init__(self, parent, syren=False):
        super(Basic, self).__init__(parent, "Basic Weapons")
        self.variants("Pulse carbine", 30, var_dicts=[
            {'name': 'Weapon reload', 'points': 15}
        ])


class Pistols(OptionsList):
    def __init__(self, parent, syren=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Pulse pistol", 30, var_dicts=[
            {'name': 'Weapon reload', 'points': 15}
        ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Clip harness', 10)
        self.variant('Photo-visor', 15)
        self.variant('Markerlight', 15)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        self.variants("Ion rifle", 100, var_dicts=[
            {'name': 'Weapon reload', 'points': 50}
        ])
        self.variants("Rail rifle", 120, var_dicts=[
            {'name': 'Weapon reload', 'points': 60}
        ])
