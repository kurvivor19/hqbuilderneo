__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Shasui(Unit):
    type_name = "Pathfinder Shas'ui"
    type_id = 'tau_shasui_v1'

    def __init__(self, parent):
        super(Shasui, self).__init__(parent, points=140, gear=[
            Gear('Combat blade'), Gear('Recon armour')
        ])
        Pistols(self)
        Basic(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Pathfinder(Unit):
    type_name = 'Pathfinder'
    type_id = 'tau_pathfinder_v1'

    def __init__(self, parent):
        super(Pathfinder, self).__init__(parent, points=60, gear=[
            Gear('Combat blade'), Gear('Recon armour')
        ])
        Pistols(self)
        Basic(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Cadet(Unit):
    type_name = 'Pathfinder Cadet'
    type_id = 'tau_cadet_v1'

    def __init__(self, parent):
        super(Cadet, self).__init__(parent, points=50, gear=[
            Gear('Combat blade'), Gear('Recon armour')
        ])
        Pistols(self)
        Basic(self)
        Misc(self)


class Specialist(Unit):
    type_name = 'Pathfinder Specialist'
    type_id = 'tau_specialist_v1'

    def __init__(self, parent):
        super(Specialist, self).__init__(parent, points=60, gear=[
            Gear('Combat blade'), Gear('Recon armour')
        ])
        Pistols(self)
        Special(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Recon(Unit):
    type_name = 'MB3 Recon Drone'
    type_id = 'tau_recon_v1'

    def __init__(self, parent):
        super(Recon, self).__init__(parent, 'Recon Drone', 110, [
            Gear('Burst cannon'), Gear('Scanning array'),
            Gear('Combat blade'), Gear('Drone caparace')
        ], static=True)


class Grav(Unit):
    type_name = 'MV33 Grav-Inhibitor Drone'
    type_id = 'tau_grav_v1'

    def __init__(self, parent):
        super(Grav, self).__init__(parent, 'Grav-Inhibitor Drone', 50, [
            Gear('Gravity wave projector'),
            Gear('Combat blade'), Gear('Drone caparace')
        ], static=True)


class Pulse(Unit):
    type_name = 'MV31 Pulse Accelerator Drone'
    type_id = 'tau_pulse_v1'

    def __init__(self, parent):
        super(Pulse, self).__init__(parent, 'Pulse Accelerator Drone', 50, [
            Gear('Pulse accelerator'),
            Gear('Combat blade'), Gear('Drone caparace')
        ], static=True)
