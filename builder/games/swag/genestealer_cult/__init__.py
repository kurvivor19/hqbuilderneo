__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import NLeader, Hybrid, Initiate, NHeavy


class GCLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(GCLeader, self).__init__(*args, **kwargs)
        UnitType(self, NLeader)


class GCTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(GCTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Hybrid)


class GCNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(GCNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Initiate)


class GCSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(GCSpecialists, self).__init__(*args, max_limit=3, **kwargs)
        UnitType(self, NHeavy)


class GenestealerCultKillTeam(SWAGRoster):
    army_name = 'Genestealer Cult Hybrid Kill Team'
    army_id = 'gcult_swag_v1'

    def __init__(self):
        super(GenestealerCultKillTeam, self).__init__(
            GCLeader, GCTroopers, GCSpecialists, GCNewRecruits, max_limit=15)
