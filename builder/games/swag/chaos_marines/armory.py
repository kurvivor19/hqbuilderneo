__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Mark(OneOf):
    def __init__(self, parent):
        super(Mark, self).__init__(parent, 'Mark of Chaos')
        self.variant('Mark of Chaos Undivided')
        self.variant('Mark of Khorne')
        self.tzeentch = self.variant('Mark of Tzeentch')
        self.nurgle = self.variant('Mark of Nurgle')
        self.variant('Mark of Slaanesh')


class Grenades(OptionsList):
    def __init__(self, parent, mark, champion=False):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.mark = mark
        self.variants('Frag grenades', 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])
        if champion:
            self.variants('Melta bombs', 30, var_dicts=[
                {'name': 'Weapon reload', 'points': 15}
            ])
        self.blight = self.variants('Blight grenades', 35, var_dicts=[
            {'name': 'Weapon reload', 'points': 18}
        ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])

    def check_rules_chain(self):
        self.blight.used = self.blight.visible = self.mark.cur == self.mark.nurgle
        super(Grenades, self).check_rules_chain()


class MarineH2H(OptionsList):
    def __init__(self, parent, champion=False):
        super(MarineH2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant("Assault blade", 15)
        self.variant("Chainsword", 25)
        if champion:
            self.variant("Power sword", 50)
            self.variant('Power fist', 85)


class CultH2H(OptionsList):
    def __init__(self, parent):
        super(CultH2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant('Flail', 10)
        self.variant("Assault blade", 15)
        self.variant('Axe', 15)
        self.variant('Bludgeon', 15)


class MarinePistols(OptionsList):
    def __init__(self, parent, mark, champion=False):
        super(MarinePistols, self).__init__(parent, "Pistols")
        self.mark = mark
        self.bp = self.variants("Bolt Pistol", 25, var_dicts=[
            {'name':'Inferno bolts', 'points':25},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Weapon reload', 'points':13}
        ])
        # should be available from list
        self.ib = self.options[-3]
        if champion:
            self.variants("Plasma pistol", 50, var_dicts=[
                {'name':'Red-dot laser sight', 'points':20},
                {'name':'Weapon reload', 'points':25}
            ])

    def check_rules(self):
        super(MarinePistols, self).check_rules()
        #presuming that it is executed after rules chain for options
        if not self.mark.cur == self.mark.tzeentch:
            self.ib.used = self.ib.visible = False


class CultPistol(OptionsList):
    def __init__(self, parent):
        super(CultPistol, self).__init__(parent, 'Pistols')
        self.variants('Autopistol', 15, var_dicts=[
            {'name': 'Weapon reload', 'points': 8}
        ])


class MarineBasic(OptionsList):
    def __init__(self, parent, mark):
        super(MarineBasic, self).__init__(parent, 'Basic weapons')
        self.mark = mark
        self.variants("Boltgun", 35, var_dicts=[
            {'name': 'Inferno bolts', 'points': 25},
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 18}
        ])
        self.ib = self.options[1]

    def check_rules(self):
        super(MarineBasic, self).check_rules()
        #presuming that it is executed after rules chain for options
        if not self.mark.cur == self.mark.tzeentch:
            self.ib.used = self.ib.visible = False


class CultBasic(OptionsList):
    def __init__(self, parent):
        super(CultBasic, self).__init__(parent, 'Basic weapons')
        self.variants('Autogun', 20, var_dicts=[
            {'name': 'Weapon reload', 'points': 10}
        ])
        self.variants('Shotgun', 20, var_dicts=[
            {'name': 'Weapon reload', 'points': 10}
        ])


class Heavy(OptionsList):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent, 'Heavy weapons')
        ac = self.variants('Autocannon', 150, var_dicts=[{'name': 'Weapon reload', 'points': 75}])
        hb = self.variants('Heavy bolter', 180, var_dicts=[{'name': 'Weapon reload', 'points': 90}])
        ml1 = self.variants('Missile launcher (frag missiles)', 175,
                            var_dicts=[{'name': 'Weapon reload', 'points': 88}])
        ml2 = self.variants('Missile launcher (super krak missiles)', 190,
                            var_dicts=[{'name': 'Weapon reload', 'points': 95}])
        ml3 = self.variants('Missile launcher (frag and super krak missiles)', 225,
                            var_dicts=[{'name': 'Weapon reload', 'points': 113}])
        lc = self.variants('Lascannon', 250, var_dicts=[{'name': 'Weapon reload', 'points': 125}])
        self.weapons = [ac, hb, ml1, ml2, ml3, lc]

    def check_rules(self):
        super(Heavy, self).check_rules()
        OptionsList.process_limit(self.weapons, 1)


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Camo gear', 5)
        self.variant('Clip harness', 10)
        self.variant('Photo-visor', 15)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        opts = [{'name': 'Red-dot laser sight', 'points': 20},
                {'name': 'Telescopic sight', 'points': 20}]
        self.variants('Flamer', 40, var_dicts=[{'name': 'Weapon reload', 'points': 20}])
        self.variants('Plasma gun', 80, var_dicts=opts + [{'name': 'Weapon reload', 'points': 40}])
        self.variants('Meltagun', 95, var_dicts=opts + [{'name': 'Weapon reload', 'points': 48}])
