__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Sergeant, Veteran, Guardsman, Operative


class AMLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(AMLeader, self).__init__(*args, **kwargs)
        UnitType(self, Sergeant)


class AMTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(AMTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Veteran)


class AMNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(AMNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Guardsman)


class AMSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(AMSpecialists, self).__init__(*args, max_limit=3, **kwargs)
        UnitType(self, Operative)


class AstraMilitarumKillTeam(SWAGRoster):
    army_name = 'Astra Militarum Veteran Kill Team'
    army_id = 'astra_militarum_swag_v1'

    def __init__(self):
        super(AstraMilitarumKillTeam, self).__init__(
            AMLeader, AMTroopers, AMSpecialists, AMNewRecruits)
