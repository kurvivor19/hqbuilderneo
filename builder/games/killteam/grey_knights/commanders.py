__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from . import points


class BrotherhoodChampion(KTCommander):
    desc = points.BrotherhoodChampion
    type_id = 'brotherhood_champion_v1'
    specs = ['Fortitude', 'Leadership', 'Melee', 'Psyker', 'Shooting', 'Strength']
    gears = [points.NemesisForceSword, points.StormBolter,
             points.PsykOutGrenade, points.FragGrenade, points.KrakGrenade]
