__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.core2.options import OneOf
from . import points


class NecronWarrior(KTModel):
    desc = points.NecronWarrior
    type_id = 'nwarrior_v1'
    gears = [points.GaussFlayer]
    specs = ['Leader', 'Comms', 'Veteran']


class Immortal(KTModel):
    desc = points.Immortal
    type_id = 'immortal_v1'
    gears = []
    specs = ['Leader', 'Comms', 'Veteran', 'Zealot']

    class Gun(OneOf):
        def __init__(self, parent):
            super(Immortal.Gun, self).__init__(parent, 'Gun')
            self.variant(*points.GaussBlaster)
            self.variant(*points.TeslaCarbine)

    def __init__(self, parent):
        super(Immortal, self).__init__(parent)
        self.Gun(self)


class FlayedOne(KTModel):
    desc = points.FlayedOne
    type_id = 'flayed_v1'
    gears = [points.FlayerClaws]
    specs = ['Leader', 'Combat', 'Veteran', 'Zealot']


class Deathmark(KTModel):
    desc = points.Deathmark
    type_id = 'deathmark_v1'
    gears = [points.SynapticDisintegrator]
    specs = ['Leader', 'Comms', 'Veteran', 'Scout', 'Sniper']
