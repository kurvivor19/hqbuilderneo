__author__ = 'Ivan Truskov'

# units
BurnaBoy = ('Burna Boy', 12)
BurnaSpanner = ('Burna Spanner', 10)
Gretchin = ('Gretchin', 3)
Kommando = ('Kommando', 8)
KommandoBossNob = ('Kommando Boss Nob', 12)
Loota = ('Loota', 12)
LootaSpanner = ('Loota Spanner', 10)
OrkBoy = ('Ork Boy', 6)
OrkBoyGunner = ('Ork Boy Gunner', 7)
BossNob = ('Boss Nob', 10)

Warboss = ('Warboss', [62, 82, 102, 127])
BigMek = ('Big Mek', [20, 25, 40, 60])
Painboy = ('Painboy', [20, 25, 40, 60])

# Ranged
BigShoota = ('Big shoota', 0)
Burna = ('Burna', 0)
Deffgun = ('Deffgun', 0)
GrotBlasta = ('Grot blasta', 0)
KombiRokkitLauncha = ('Kombi-weapon with rokkit launcha', 3)
KombiSkorcha = ('Kombi-weapon with skorcha', 4)
KustomMegaBlasta = ('Kustom mega-blasta', 0)
RokkitLauncha = ('Rokkit launcha', 3)
Shoota = ('Shoota', 0)
Slugga = ('Slugga', 0)
Stikkbomb = ('Stikkbomb', 0)

WKombiRokkit = ('Kombi-weapon with rokkit launcha', 5)
WKombiSkorcha = ('Kombi-weapon with skorcha', 8)
KustomShoota = ('Kustom shoota', 0)
SAG = ('Shokk attack gin', 27)
KustomMegaSlugga = ('Kustom mega-slugga', 4)

# Melee
BigChoppa = ('Big choppa', 2)
Choppa = ('Choppa', 0)
PowerKlaw = ('Power klaw', 4)

WBigChoppa = ('Big choppa', 0)
WPowerKlaw = ('Power klaw', 13)
PPowerKlaw = ('Power klaw', 0)
Syringe = ("'Urty syringe", 0)
# Gear
Squig = ('Attack Squig', 6)
KFF = ('Kustom Force Field', 0)
