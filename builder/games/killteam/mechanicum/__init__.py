__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import SkitariiRanger, RangerGunner, RangerAlpha, SkitariiVanguard,\
    VanguardGunner, VanguardAlpha, Ruststalker, RuststalkerPrinceps,\
    Infiltrator, InfiltratorPrinceps, UR025
from .commanders import Manipulus, Enginseer, Dominus
from builder.games.killteam.astra_militarum import Eisenhorn


class AdeptusMechanicumKT(KTRoster):
    army_name = 'Adeptus Mechanicum Kill Team'
    army_id = 'adeptus_mechanicum_kt_v1'
    unit_types = [SkitariiRanger, RangerGunner, RangerAlpha,
                  SkitariiVanguard, VanguardGunner, VanguardAlpha, Ruststalker,
                  RuststalkerPrinceps, Infiltrator, InfiltratorPrinceps, UR025]


class ComAdeptusMechanicumKT(KTCommanderRoster, AdeptusMechanicumKT):
    army_name = 'Adeptus Mechanicum Kill Team'
    army_id = 'adeptus_mechanicum_kt_v2_com'
    commander_types = [Manipulus, Enginseer, Dominus, Eisenhorn]
