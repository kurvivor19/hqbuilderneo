__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class ExaltedChampion(KTCommander):
    desc = points.ExaltedChampion
    type_id = 'exalted_champion_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Stealth', 'Strategist', 'Strength']
    gears = [points.FragGrenade, points.KrakGrenade]

    class Pistol(OneOf):
        def __init__(self, parent):
            super(ExaltedChampion.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.BoltPistol)
            self.variant(*points.ExPlasmaPistol)

    class Gun(OptionsList):
        def __init__(self, parent):
            super(ExaltedChampion.Gun, self).__init__(parent, 'Gun')
            self.variant(*points.Boltgun)

    class Sword(OneOf):
        def __init__(self, parent):
            super(ExaltedChampion.Sword, self).__init__(parent, 'Sword')
            self.variant(*points.Chainsword)
            self.variant(*points.ExPowerAxe)
            self.variant(*points.ExPowerFist)
            self.variant(*points.ExPowerSword)

    def __init__(self, parent):
        super(ExaltedChampion, self).__init__(parent)
        self.Pistol(self)
        self.Gun(self)
        self.Sword(self)


class Sorcerer(KTCommander):
    desc = points.Sorcerer
    type_id = 'sorcerer_v1'
    specs = ['Fortitude', 'Melee', 'Psyker', 'Shooting', 'Strength']
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Sorcerer.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.ForceSword)
            self.variant(*points.ForceStave)

    def __init__(self, parent):
        super(Sorcerer, self).__init__(parent)
        self.Weapon(self)
