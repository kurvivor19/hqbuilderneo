__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Poxwalker, PlagueMarine, PlagueMarineGunner,\
    PlagueMarineFighter, PlagueChampion
from .commanders import FoulBlightspawn, Tallyman, BiologusPutrifier,\
    PlagueSurgeon

class DeathGuardKT(KTRoster):
    army_name = 'Death Guard Kill Team'
    army_id = 'death_guard_kt_v1'
    unit_types = [Poxwalker, PlagueMarine, PlagueMarineGunner,
                  PlagueMarineFighter, PlagueChampion]


class ComDeathGuardKT(KTCommanderRoster, DeathGuardKT):
    army_name = 'Death Guard Kill Team'
    army_id = 'death_guard_kt_v2_com'
    commander_types = [FoulBlightspawn, Tallyman, BiologusPutrifier,
                       PlagueSurgeon]
