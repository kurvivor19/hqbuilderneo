__author__ = 'Ivan Truskov'

from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Bloodletter(KTModel):
    desc = points.Bloodletter
    type_id = 'bloodletter_v1'
    gears = [points.Hellblade]
    specs = ['Combat', 'Veteran']


class BloodletterIconBearer(KTModel):
    desc = points.BloodletterIconBearer
    datasheet = 'Bloodletter'
    type_id = 'bloodletter_icon_v1'
    gears = [points.Hellblade, points.IconOfKhorne]
    specs = ['Comms', 'Combat', 'Veteran']
    limit = 1


class BloodletterHornblower(KTModel):
    desc = points.BloodletterHornblower
    datasheet = 'Bloodletter'
    type_id = 'bloodletter_horn_v1'
    gears = [points.Hellblade, points.InstrumentOfKhorne]
    specs = ['Comms', 'Combat', 'Veteran']
    limit = 1


class Bloodreaper(KTModel):
    desc = points.Bloodreaper
    datasheet = 'Bloodletter'
    type_id = 'bloodreaper_v1'
    gears = [points.Hellblade]
    specs = ['Leader', 'Combat', 'Veteran']
    limit = 1


class Daemonette(KTModel):
    desc = points.Daemonette
    type_id = 'daemonette_v1'
    gears = [points.PiercingClaws]
    specs = ['Combat', 'Veteran', 'Scout']


class DaemonetteIconBearer(KTModel):
    desc = points.DaemonetteIconBearer
    datasheet = 'Daemonette'
    type_id = 'daemonette_icon_v1'
    gears = [points.PiercingClaws, points.IconOfSlaanesh]
    specs = ['Comms', 'Combat', 'Veteran', 'Scout']
    limit = 1


class DaemonetteHornblower(KTModel):
    desc = points.DaemonetteHornblower
    datasheet = 'Daemonette'
    type_id = 'daemonette_horn_v1'
    gears = [points.PiercingClaws, points.InstrumentOfSlaanesh]
    specs = ['Comms', 'Combat', 'Veteran', 'Scout']
    limit = 1


class Alluress(KTModel):
    desc = points.Alluress
    datasheet = 'Daemonette'
    type_id = 'alluress_v1'
    gears = [points.PiercingClaws]
    specs = ['Leader', 'Combat', 'Veteran', 'Scout']
    limit = 1


class Horror(KTModel):
    desc = points.Horror
    datasheet = 'Horror'
    type_id = 'horror_v1'
    gears = [points.CoruscatingFlames]
    specs = ['Demolitions', 'Veteran']


class HorrorIconBearer(KTModel):
    desc = points.HorrorIconBearer
    datasheet = 'Horror'
    type_id = 'horror_icon_v1'
    gears = [points.CoruscatingFlames, points.IconOfTzeentch]
    specs = ['Comms', 'Demolitions', 'Veteran']
    limit = 1


class HorrorHornblower(KTModel):
    desc = points.HorrorHornblower
    datasheet = 'Horror'
    type_id = 'horror_horn_v1'
    gears = [points.CoruscatingFlames, points.InstrumentOfTzeentch]
    specs = ['Comms', 'Demolitions', 'Veteran']
    limit = 1


class IridescentHorror(KTModel):
    desc = points.IridescentHorror
    datasheet = 'Horror'
    type_id = 'iridiscent_horror_v1'
    gears = [points.CoruscatingFlames]
    specs = ['Leader', 'Demolitions', 'Veteran']
    limit = 1


class Plaguebearer(KTModel):
    desc = points.Plaguebearer
    type_id = 'plaguebearer_v1'
    gears = [points.Plaguesword]
    specs = ['Combat', 'Veteran']


class PlaguebearerIconBearer(KTModel):
    desc = points.PlaguebearerIconBearer
    datasheet = 'Plaguebearer'
    type_id = 'plaguebearer_icon_v1'
    gears = [points.Plaguesword, points.IconOfNurgle]
    specs = ['Comms', 'Combat', 'Veteran']
    limit = 1


class PlaguebearerHornblower(KTModel):
    desc = points.PlaguebearerHornblower
    datasheet = 'Plaguebearer'
    type_id = 'plaguebearer_horn_v1'
    gears = [points.Plaguesword, points.InstrumentOfNurgle]
    specs = ['Comms', 'Combat', 'Veteran']
    limit = 1


class Plagueridden(KTModel):
    desc = points.Plagueridden
    datasheet = 'Plaguebearer'
    type_id = 'plagueridden_v1'
    gears = [points.Plaguesword]
    specs = ['Leader', 'Combat', 'Veteran']
    limit = 1
