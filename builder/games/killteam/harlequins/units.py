__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Player(KTModel):
    desc = points.Player
    type_id = 'player_v1'
    gears = [points.PlasmaGrenade]
    specs = ['Leader', 'Combat', 'Medic', 'Scout', 'Veteran', 'Zealot']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Player.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.ShurikenPistol)
            self.variant(*points.NeuroDisruptor)
            self.variant(*points.FusionPistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(Player.Sword, self).__init__(parent, 'Close combat weapon')
            self.variant(*points.HarlequinBlade)
            self.variant(*points.HarlequinEmbrace)
            self.variant(*points.HarlequinCaress)
            self.variant(*points.HarlequinKiss)

    def __init__(self, parent):
        super(Player, self).__init__(parent)
        self.Pistol(self)
        self.Sword(self)
