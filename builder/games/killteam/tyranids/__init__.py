__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Termagant, Hormagaunt, Lictor,\
    TWarrior, TGunner, Genestealer
from .commanders import TPrime, Broodlord


class TyranidKT(KTRoster):
    army_name = 'Tyranid Kill Team'
    army_id = 'tyranid_kt_v1'
    unit_types = [Termagant, Hormagaunt, Lictor, TWarrior, TGunner,
                  Genestealer]


class ComTyranidKT(KTCommanderRoster, TyranidKT):
    army_name = 'Tyranid Kill Team'
    army_id = 'tyranid_kt_v2_com'
    commander_types = [TPrime, Broodlord]
