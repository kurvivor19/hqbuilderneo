__author__ = 'Ivan Truskov'

# units

KabaliteWarrior = ('Kabalite Warrior', 7)
KabaliteGunner = ('Kabalite Gunner', 8)
Sybarite = ('Sybarite', 8)
Wych = ('Wych', 8)
WychFighter = ('Wych Fighter', 9)
Hekatrix = ('Hekatrix', 9)

Archon = ('Archon', [56, 71, 86, 111])
Succubus = ('Succubus', [48, 63, 78, 103])
Haemonculus = ('Haemonculus', [30, 35, 50, 70])

# Ranged
BlastPistol = ('Blast pistol', 2)
Blaster = ('Blaster', 3)
DarkLance = ('Dark lance', 4)
PhantasmGrenadeLauncher = ('Phantasm grenade launcher', 1)
PlasmaGrenade = ('Plasma grenade', 0)
Shredder = ('Shredder', 1)
SplinterCannon = ('Splinter cannon', 3)
SplinterPistol = ('Splinter pistol', 0)
SplinterRifle = ('Splinter rifle', 0)

# Melee
Agoniser = ('Agoniser', 2)
HekatariiBlade = ('Hekatarii blade', 0)
HydraGauntlets = ('Hydra gauntlets', 2)
PowerSword = ('Power sword', 2)
Razorflails = ('Razorflails', 2)
ShardnetAndImpaler = ('Shardnet and impaler', 2)

# commander gear
CAgoniser = ('Agoniser', 0)
CBlastPistol = ('Blast pistol', 10)
CHuskblade = ('Huskblade', 0)
CPowerSword = ('Power sword', 0)
CSplinterPistol = ('Splinter pistol', 0)
CVenomBlade = ('Venom blade', 0)
ArchiteGlaive = ('Archite glaive', 0)
HaemonculusTools = ('Haemonculus tools', 0)
IchorInjector = ('Ichor injector', 5)
StingerPistol = ('Stinger pistol', 0)
