__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class DeathwatchVeteran(KTModel):
    desc = points.DeathwatchVeteran
    type_id = 'deathwatch_veteran_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Combat', 'Comms', 'Demolitions', 'Sniper', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent, veteran=False, gunner=False):
            super(DeathwatchVeteran.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.CombiMelta)
            self.variant(*points.CombiPlasma)
            self.variant(*points.StalkerPatternBoltgun)
            self.variant(*points.PowerSword)
            self.variant(*points.PowerMaul)
            self.variant(*points.StormShield)

            if veteran:
                self.variant(*points.DeathwatchShotgun)
                self.variant(*points.HeavyThunderHammer)

            if gunner:
                self.variant(*points.DeathwatchFragCannon)
                self.variant(*points.InfernusHeavyBolter)


    class Optional(OptionsList):
        def __init__(self, parent, sergeant=False):
            super(DeathwatchVeteran.Optional, self).__init__(parent, 'Weapon')
            self.variant(*points.PowerSword)
            self.variant(*points.PowerMaul)
            if sergeant:
                self.variant(*points.XenophaseBlade)

    def __init__(self, parent):
        super(DeathwatchVeteran, self).__init__(parent)
        self.Weapon(self, veteran=True)
        self.Optional(self)


class DeathwatchVeteranSergeant(KTModel):
    desc = points.DeathwatchWatchSergeant
    datasheet = 'Deathwatch Veteran'
    type_id = 'dw_sergeant_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Demolitions', 'Sniper', 'Veteran', 'Zealot']
    limit = 1

    def __init__(self, parent):
        super(DeathwatchVeteranSergeant, self).__init__(parent)
        DeathwatchVeteran.Weapon(self)
        DeathwatchVeteran.Optional(self, sergeant=True)


class DeathwatchVeteranGunner(KTModel):
    desc = points.DeathwatchVeteranGunner
    datasheet = 'Deathwatch Veteran'
    type_id = 'dw_gunner_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Heavy', 'Combat', 'Comms', 'Demolitions', 'Veteran', 'Zealot']
    limit = 4


    def __init__(self, parent):
        super(DeathwatchVeteranGunner, self).__init__(parent)
        DeathwatchVeteran.Weapon(self, gunner=True)
        DeathwatchVeteran.Optional(self)


class DeathwatchBlackShield(KTModel):
    desc = points.DeathwatchBlackShield
    datasheet = 'Deathwatch Veteran'
    type_id = 'dw_black_shield_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Combat', 'Comms', 'Demolitions', 'Veteran', 'Zealot']
    limit = 1


    def __init__(self, parent):
        super(DeathwatchBlackShield, self).__init__(parent)
        DeathwatchVeteran.Weapon(self)
        DeathwatchVeteran.Optional(self)


class DWReiver(KTModel):
    desc = points.Reiver
    type_id = 'dw_reiver_v1'
    gears = [points.HeavyBoltPistol, points.FragGrenade, points.KrakGrenade, points.ShockGrenade]
    specs = ['Combat', 'Comms', 'Demolitions', 'Scout', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DWReiver.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.BoltCarbine)
            self.variant(*points.CombatKnife)

    class Optionnal(OptionsList):
        def __init__(self, parent):
            super(DWReiver.Optionnal, self).__init__(parent, 'Optionnal')
            self.variant(*points.GravChute)
            self.variant(*points.GrapnelLauncher)

    def __init__(self, parent):
        super(DWReiver, self).__init__(parent)
        self.Weapon(self)
        self.Optionnal(self)


class DWReiverSergeant(KTModel):
    desc = points.ReiverSergeant
    datasheet = 'Reiver'
    type_id = 'reiver_sergeant_v1'
    gears = [points.FragGrenade, points.KrakGrenade, points.ShockGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Demolitions', 'Scout', 'Veteran']
    limit = 1

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWReiverSergeant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*points.BoltCarbine)
            self.variant(*points.CombatKnife)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWReiverSergeant.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*points.HeavyBoltPistol)
            self.variant(*points.CombatKnife)

    def __init__(self, parent):
        super(DWReiverSergeant, self).__init__(parent)
        self.Weapon1(self)
        self.Weapon2(self)
        DWReiver.Optionnal(self)


class DWIntercessor(KTModel):
    desc = points.Intercessor
    type_id = 'dw_intercessor_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Combat', 'Comms', 'Sniper', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DWIntercessor.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.BoltRifle)
            self.variant(*points.AutoBoltRifle)
            self.variant(*points.StalkerBoltRifle)

    def __init__(self, parent):
        super(DWIntercessor, self).__init__(parent)
        self.Weapon(self)


class DWIntercessorSergeant(KTModel):
    desc = points.IntercessorSergeant
    datasheet = 'Intercessor'
    type_id = 'dw_intercessor_sergeant_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Sniper', 'Veteran']
    limit = 1

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(DWIntercessorSergeant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*points.PowerSword)
            self.variant(*points.Chainsword)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWIntercessorSergeant.Weapon2, self).__init__(parent, 'Weapon')
            self.rifle = self.variant(*points.BoltRifle)
            self.variant(*points.AutoBoltRifle)
            self.variant(*points.StalkerBoltRifle)
            self.melee = [
                self.variant(*points.PowerSword),
                self.variant(*points.Chainsword)
            ]

    def __init__(self, parent):
        super(DWIntercessorSergeant, self).__init__(parent)
        self.gun = self.Weapon2(self)
        self.sword = self.Weapon1(self)

    def check_rules(self):
        super(DWIntercessorSergeant, self).check_rules()
        self.sword.used = self.sword.visible = self.gun.cur not in self.gun.melee


class DWIntercessorGunner(KTModel):
    desc = points.IntercessorGunner
    datasheet = 'Intercessor'

    type_id = 'dw_intercessor_gunner_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Demolitions', 'Combat', 'Comms', 'Sniper', 'Veteran']

    class Optional(OptionsList):
        def __init__(self, parent):
            super(DWIntercessorGunner.Optional, self).__init__(parent, 'Weapon')
            self.variant(*points.AuxiliaryGrenadeLauncher)

    def __init__(self, parent):
        super(DWIntercessorGunner, self).__init__(parent)
        self.Optional(self)
