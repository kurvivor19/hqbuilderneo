__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Voidsman(KTModel):
    desc = points.Voidsman
    type_id = 'voidsman_v1'
    gears = [points.Lasgun, points.Laspistol, points.ConcussionGrenades]
    specs = ['Demolitions', 'Veteran']


class VoidsmanGunner(KTModel):
    desc = points.VoidsmanGunner
    datasheet = 'Voidsman'
    type_id = 'voidsman_gunner_v1'
    gears = [points.RotorCannon, points.Laspistol, points.ConcussionGrenades]
    specs = ['Heavy', 'Demolitions', 'Veteran']
    limit = 1


class VoidmasterNitsch(KTModel):
    desc = points.VoidmasterNitsch
    datasheet = 'Voidsman'
    type_id = 'voidmaster_v1'
    gears = [points.ArtificerShotgun, points.Laspistol, points.ConcussionGrenades]
    specs = ['Leader', 'Demolitions', 'Veteran']
    limit = 1


class Aximillion(KTModel):
    desc = points.Aximillion
    datasheet = 'Voidsman'
    type_id = 'star_doggo_v1'
    gears = [points.ViciousBite]
    specs = ['Scout', 'Demolitions', 'Veteran']
    limit = 1


class KnossoPrond(KTModel):
    desc = points.KnossoPrond
    force_spec = True
    type_id = 'knosso_prond_v1'
    gears = [points.DeathCultPowerBlade, points.Dartmask, points.ConcussionGrenades]
    specs = ['Combat']
    limit = 1


class Larsen(KTModel):
    desc = points.Larsen
    force_spec = True
    type_id = 'larsen_v1'
    gears = [points.VoltaicPistol, points.ConcussionGrenades]
    specs = ['Comms']
    limit = 1


class SanistasiaMinst(KTModel):
    desc = points.SanistasiaMinst
    force_spec = True
    type_id = 'sanistasia_v1'
    gears = [points.ScalpelClaw, points.Laspistol, points.ConcussionGrenades]
    specs = ['Medic']
    limit = 1
