__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class BlackLegionnaire(KTModel):
    desc = points.BlackLegionnaire
    type_id = 'black_legionnaire_v1'
    gears = [points.Boltgun, points.BoltPistol, points.FragGrenades, points.KrakGrenades]
    specs = ['Leader', 'Combat', 'Sniper', 'Veteran', 'Zealot']


class RoguePsyker(KTModel):
    desc = points.RoguePsyker
    type_id = 'rogue_psyker_v1'
    gears = [points.Laspistol, points.ChaosStave]
    specs = ['Leader', 'Comms', 'Veteran', 'Zealot']


class NegavoltCultist(KTModel):
    desc = points.NegavoltCultist
    type_id = 'negavolt_cultist_v1'
    gears = [points.ElectroGoads]
    specs = ['Leader', 'Combat', 'Veteran', 'Zealot']


class ChaosBeastman(KTModel):
    desc = points.ChaosBeastman
    type_id = 'chaos_beastman_v1'
    gears = [points.FragGrenades]
    specs = ['Leader', 'Combat', 'Demolitions', 'Veteran', 'Zealot']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(ChaosBeastman.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.Laspistol)
            self.variant(*points.Autopistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(ChaosBeastman.Sword, self).__init__(parent, 'Sword')
            self.variant(*points.Chainsword)
            self.variant(*points.BrutalAssaultWeapon)

    def __init__(self, parent):
        super(ChaosBeastman, self).__init__(parent)
        self.Pistol(self)
        self.Sword(self)


class TraitorGuardsman(KTModel):
    desc = points.TraitorGuardsman
    type_id = 'traitor_guardsman_v1'
    gears = [points.FragGrenades]
    specs = ['Demolitions', 'Scout', 'Sniper', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TraitorGuardsman.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Lasgun)
            self.variant('Autopistol and brutal assault weapon',
                         points_price(0, points.Autopistol, points.BrutalAssaultWeapon),
                         gear=create_gears(points.Autopistol, points.BrutalAssaultWeapon))
            self.variant('Laspitol and brutal assault weapon',
                         points_price(0, points.Laspistol, points.BrutalAssaultWeapon),
                         gear=create_gears(points.Laspistol, points.BrutalAssaultWeapon))

    def __init__(self, parent):
        super(TraitorGuardsman, self).__init__(parent)
        self.Weapon(self)


class TraitorGuardsmanGunner(KTModel):
    desc = points.TraitorGuardsmanGunner
    datasheet = 'Traitor Guardsman'
    type_id = 'traitor_guardsman_gunner_v1'
    gears = [points.FragGrenades]
    specs = ['Heavy', 'Demolitions', 'Scout', 'Sniper', 'Veteran']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TraitorGuardsmanGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Lasgun)
            self.variant(*points.Flamer)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TraitorGuardsmanGunner.Options, self).__init__(parent, '')
            self.variant(*points.KrakGrenades)

    def __init__(self, parent):
        super(TraitorGuardsmanGunner, self).__init__(parent)
        self.Weapon(self)
        self.Options(self)


class TraitorGuardsmanSergeant(KTModel):
    desc = points.TraitorSergeant
    datasheet = 'Traitor Guardsman'
    type_id = 'traitor_guardsman_sergeant_v1'
    gears = [points.Laspistol, points.Chainsword, points.FragGrenades]
    specs = ['Leader', 'Demolitions', 'Scout', 'Sniper', 'Veteran']
    limit = 1
