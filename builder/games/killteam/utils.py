__author__ = 'Ivan Truskov'


from builder.core2.options import Gear


def get_name(item):
    return item[0]


def get_cost(item):
    return item[1]


def points_price(price, *gears):
    for el in gears:
        price += el[1]
    return price


def create_gears(*gears):
    result = []
    for el in gears:
        result.append(Gear(el[0]))
    return result
