__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Vulgar(KTCommander):
    desc = points.VulgarThriceCursed
    type_id = 'vulgar_v1'
    gears = [points.BellyFlamer, points.FleshripperClaws]
    specs = ['Strength', 'Combat', 'Demolitions', 'Veteran', 'Zealot']

    class ExtraTraits(OptionsList):
        def __init__(self, parent):
            super(Vulgar.ExtraTraits, self).__init__(parent, 'Extra traits', order=18)
            self.variant('Twisted Brilliance', 10)
            self.variant('Master of Vermin', 15)

    def __init__(self, parent):
        super(Vulgar, self).__init__(parent)
        self.ExtraTraits(self)
