__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from . import points


class Patriarch(KTCommander):
    desc = points.Patriarch
    type_id = 'patriarch_v1'
    specs = ['Ferocity', 'Fortitude', 'Melee', 'Psyker', 'Stealth', 'Strength']
    gears = [points.MonstrousRendingClaws]


class Magus(KTCommander):
    desc = points.Magus
    type_id = 'magus_v1'
    specs = ['Leadership', 'Logistics', 'Psyker', 'Stealth']
    gears = [points.Autopistol, points.ForceStave]


class Primus(KTCommander):
    desc = points.Primus
    type_id = 'primus_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Stealth', 'Strategist', 'Strength']
    gears = [points.NeedlePistol, points.PBonesword, points.ToxinInjector, points.BlastingCharge]


class AcolyteIconward(KTCommander):
    desc = points.Iconward
    type_id = 'iconward_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Melee', 'Stealth']
    gears = [points.Autopistol, points.RendingClaw, points.BlastingCharge]
