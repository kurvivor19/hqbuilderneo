__author__ = 'Ivan Truskov'

from .units import *

unit_types = [
    Judicitators,
    Liberators,
    Prime,
    Azyros,
    Heraldor,
    Venator,
    Vexillor,
    Castellant,
    Celestant,
    CelestantDracoth,
    Relictor,
    Decimators,
    GryphHounds,
    HammerProsecutors,
    JavelinProsecutors,
    Protectors,
    Retributors
]
