__author__ = 'Ivan Truskov'


from builder.core2 import OptionsList, OneOf, Gear,\
    ListSubUnit, UnitList, UnitDescription, Count
from .fast import Squadron
from .hq import Covenant
from builder.games.wh40k.unit import Unit


class LemanRuss(ListSubUnit):
    type_name = 'Leman Russ'

    class Type(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Type, self).__init__(parent=parent, name='Type')
            self.variant('Leman Russ Battle Tank', 140, gear=[Gear('Battle cannon')])
            self.variant('Leman Russ Exterminator', 120, gear=[Gear('Exterminator autocannon')])
            self.variant('Leman Russ Conqueror', 110, gear=[Gear('Conqueror cannon'), Gear('Storm bolter')])
            self.variant('Leman Russ Annihilator', 120, gear=[Gear('Twin-linked lascannon')])
            self.vanq = self.variant('Leman Russ Vanquisher', 125, gear=[Gear('Vanquisher battle cannon')])
            self.variant('Leman Russ Eradicator', 110, gear=[Gear('Eradicator nova cannon')])
            self.variant('Leman Russ Punisher', 130, gear=[Gear('Punisher gatling cannon')])
            self.variant('Leman Russ Executioner', 145, gear=[Gear('Executioner plasma cannon')])
            self.variant('Leman Russ Demolisher', 160, gear=[Gear('Demolisher siege cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.lascannon = self.variant('Lascannon', 10)

    class CoAxial(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.CoAxial, self).__init__(parent, 'Co-axial weapon', limit=1)
            self.variant('Storm bolter', 10)
            self.variant('Heavy stubber', 10)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy flamers', 10, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Multi-meltas', 20, gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 30, gear=[Gear('Plasma cannon', count=2)])

    class Vehicle(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Vehicle, self).__init__(parent, 'Options')
            self.mt = self.variant('Militia training', 10)
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)
            self.variant('Hunter-killer missile', 10)
            self.dozen = self.variant('Dozer blade', 5)
            self.variant('Extra armour', 10)
            self.variant('Mine plough', 15)

        def check_rules(self):
            super(LemanRuss.Vehicle, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    def __init__(self, parent):
        super(LemanRuss, self).__init__(
            parent=parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.type = self.Type(self)
        self.Weapon(self)
        self.coax = self.CoAxial(self)
        self.Sponsons(self)
        self.Vehicle(self)

    def check_rules(self):
        super(LemanRuss, self).check_rules()
        self.coax.used = self.coax.visible = self.type.cur == self.type.vanq

    def build_description(self):
        desc = super(LemanRuss, self).build_description()
        desc.name = self.type.cur.title
        return desc


class TankSquadron(Squadron):
    type_name = 'Renegade Tank Squadron'
    type_id = 'r_tanks_v1'
    unit_class = LemanRuss


class StrikeTank(ListSubUnit):
    type_name = 'Renegade Griffon'

    class Type(OneOf):
        def __init__(self, parent):
            super(StrikeTank.Type, self).__init__(parent, 'Type')
            self.variant('Renegade Griffon', 35, gear=[Gear('Heavy mortar')])
            self.variant('Renegade Wyvern', 55, gear=[Gear('Twin-linked stormshard mortar', count=2)])

    class Vehicle(OptionsList):
        def __init__(self, parent, pintle=10):
            super(StrikeTank.Vehicle, self).__init__(parent, 'Options')
            self.variant('Enclosed crew compartment', 15)
            self.sb = self.variant('Storm bolter', pintle)
            self.hs = self.variant('Heavy stubber', pintle)
            self.variant('Hunter-killer missile', 10)
            self.dozen = self.variant('Dozer blade', 5)
            self.variant('Extra armour', 10)

        def check_rules(self):
            super(StrikeTank.Vehicle, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(StrikeTank.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)

    def __init__(self, parent):
        super(StrikeTank, self).__init__(parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.type = self.Type(self)
        self.Weapon(self)
        self.Vehicle(self)

    def build_points(self):
        return super(StrikeTank, self).build_points() +\
            self.root_unit.opt.points

    def build_description(self):
        res = super(StrikeTank, self).build_description()
        res.name = self.type.cur.title
        res.add(self.root_unit.opt.description)
        return res


class ArtilleryTank(ListSubUnit):
    type_name = 'Renegade Basilisk'

    class Type(OneOf):
        def __init__(self, parent):
            super(ArtilleryTank.Type, self).__init__(parent, 'Type')
            self.variant('Renegade Basilisk', 115, gear=[Gear('Earthshaker cannon')])
            self.medusa = self.variant('Renegade Medusa', 125, gear=[Gear('Medusa siege cannon')])

    class Vehicle(StrikeTank.Vehicle):
        def __init__(self, parent):
            super(ArtilleryTank.Vehicle, self).__init__(parent,)
            self.shells = self.variant('Breacher shells', 5)

        def check_rules(self):
            super(ArtilleryTank.Vehicle, self).check_rules()
            self.shells.used = self.shells.visible = self.parent.type.cur == self.parent.type.medusa

    def __init__(self, parent):
        super(ArtilleryTank, self).__init__(parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.type = self.Type(self)
        StrikeTank.Weapon(self)
        self.Vehicle(self)

    def build_points(self):
        return super(ArtilleryTank, self).build_points() +\
            self.root_unit.opt.points

    def build_description(self):
        res = super(ArtilleryTank, self).build_description()
        res.name = self.type.cur.title
        res.add(self.root_unit.opt.description)
        return res


class ArtilleryBattery(Squadron):
    type_name = 'Renegade Artillery Battery'
    type_id = 'r_artillery_v1'

    class Options(OptionsList):
        def __init__(self, parent, cost):
            super(ArtilleryBattery.Options, self).__init__(parent, 'Battery options')
            self.mt = self.variant('Militia training', cost, per_model=True)

        def check_rules(self):
            super(ArtilleryBattery.Options, self).check_rules()
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    unit_class = ArtilleryTank

    def __init__(self, parent):
        super(ArtilleryBattery, self).__init__(parent)
        self.opt = self.Options(self, 10)

    def build_points(self):
        return self.tanks.points

    def build_description(self):
        return UnitDescription(self.name, self.points, self.get_count(),
                               options=self.tanks.description)


class StrikeBattery(Squadron):
    type_name = 'Renegade Strike Battery'
    type_id = 'r_strike_v1'

    unit_class = StrikeTank
    max_unit = 5

    def __init__(self, parent):
        super(StrikeBattery, self).__init__(parent)
        self.opt = ArtilleryBattery.Options(self, 10)

    def build_points(self):
        return self.tanks.points

    def build_description(self):
        return UnitDescription(self.name, self.points, self.get_count(),
                               options=self.tanks.description)


class Hydra(ListSubUnit):
    type_name = 'Renegade Hydra'

    def __init__(self, parent):
        super(Hydra, self).__init__(parent, points=60,
                                    gear=[Gear('Searchlight'), Gear('Smoke launchers'),
                                          Gear('Twin-linked Hydra autocannon', count=2)])
        StrikeTank.Weapon(self)
        StrikeTank.Vehicle(self)

    def build_points(self):
        return super(Hydra, self).build_points() +\
            self.root_unit.opt.points

    def build_description(self):
        res = super(Hydra, self).build_description()
        res.add(self.root_unit.opt.description)
        return res


class HydraBattery(Squadron):
    type_name = 'Renegade Hydra Battery'
    type_id = 'r_hydra_v1'

    unit_class = Hydra

    def __init__(self, parent):
        super(HydraBattery, self).__init__(parent)
        self.opt = ArtilleryBattery.Options(self, 10)

    def build_points(self):
        return self.tanks.points

    def build_description(self):
        return UnitDescription(self.name, self.points, self.get_count(),
                               options=self.tanks.description)


class Colossus(ListSubUnit):
    type_name = 'Renegade Colossus'

    class Vehicle(StrikeTank.Vehicle):
        def __init__(self, parent):
            super(Colossus.Vehicle, self).__init__(parent, pintle=5)
            self.mt = self.variant('Militia training', 10)

        def check_rules(self):
            super(Colossus.Vehicle, self).check_rules()
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    def __init__(self, parent):
        super(Colossus, self).__init__(parent, points=145,
                                    gear=[Gear('Searchlight'), Gear('Smoke launchers'),
                                          Gear('Colossus siege bombard')])
        StrikeTank.Weapon(self)
        self.Vehicle(self)


class BombardBattery(Squadron):
    type_name = 'Renegade Bombard Battery'
    type_id = 'r_bombard_v3'
    unit_class = Colossus


class SupportSquad(Unit):
    type_name = 'Renegade Support Squad'
    type_id = 'r_support_v1'

    class SquadOptions(OptionsList):
        def __init__(self, parent, krak=True):
            super(SupportSquad.SquadOptions, self).__init__(parent, 'Crew options')
            self.fnp = self.variant('Feel No Pain (6+)', 10)
            self.flak = self.variant('Flak armour', 10, gear=[])
            self.mt = self.variant('Militia training', 10)
            self.sflak = self.variant('Sub-flak armour', 5, gear=[])
            self.krak = krak and self.variant('Krak grenades', 10, gear=[])

        def check_rules(self):
            super(SupportSquad.SquadOptions, self).check_rules()
            self.fnp.used = self.fnp.visible = self.roster.demagogue.is_heretek()
            if self.roster.demagogue.is_reaver():
                self.flak.used = self.flak.visible = True
                self.mt.value = True
                self.mt.active = False
            else:
                self.flak.used = self.flak.visible = False
                self.mt.active = True

    class WeaponsTeam(ListSubUnit):
        type_name = 'Renegade Weapons Team'

        class HeavyWeapon(OneOf):

            def __init__(self, parent):
                super(SupportSquad.WeaponsTeam.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
                self.variant('Heavy stubber', 0)
                self.variant('Heavy bolter', 5)
                self.variant('Autocannon', 10)
                self.ml = self.variant('Missile launcher', 10)
                self.variant('Lascannon', 15)
                self.variant('Mortar', 5)

        def __init__(self, parent):
            super(SupportSquad.WeaponsTeam, self).__init__(parent, points=10,
                                                           gear=[Gear('Close combat weapon'),
                                                                 Gear('Frag grenades')])
            self.wep = self.HeavyWeapon(self)

        def build_description(self):
            res = UnitDescription(self.name, self.points, self.get_count())
            if self.root_unit.opt.krak.value:
                res.add(Gear('Krak grenades'))
            if self.root_unit.opt.sflak.value:
                res.add(Gear('Sub-flak armour'))
            if self.root_unit.opt.flak.used and self.root_unit.opt.flak.value:
                res.add(Gear('Flak armour'))
            res.add(self.wep.description)
            return res

    def __init__(self, parent):
        super(SupportSquad, self).__init__(parent, points=25 - 3 * 10)
        self.opt = self.SquadOptions(self)
        self.teams = UnitList(self, self.WeaponsTeam, 3, 6)
        self.cov = Covenant(self, 20)

    def get_count(self):
        return self.teams.count

    def not_nurgle(self):
        return self.cov.not_nurgle()


class OrdnanceBattery(Unit):
    type_name = 'Renegade Heavy Ordnance Battery'
    type_id = 'r_ordnance_v1'

    class GunOptions(OneOf):
        def __init__(self, parent):
            super(OrdnanceBattery.GunOptions, self).__init__(parent, 'Cannon')
            self.variant('Earthshaker cannon', 0, per_model=True)
            self.medusa = self.variant('Medusa siege gun', 25, per_model=True)

    class Carriage(ListSubUnit):
        type_name = 'Artillery Carriage'

        class Options(OptionsList):
            def __init__(self, parent):
                super(OrdnanceBattery.Carriage.Options, self).__init__(parent, 'Shells')
                self.variant('Breacher shells', 5)

        def __init__(self, parent):
            super(OrdnanceBattery.Carriage, self).__init__(parent, points=55)
            self.shells = self.Options(self)
            self.crew = Count(self, 'Additional crew', 0, 4, 3, per_model=True)

        def build_points(self):
            return super(OrdnanceBattery.Carriage, self).build_points() + self.root_unit.opt.points

        def check_rules(self):
            super(OrdnanceBattery.Carriage, self).check_rules()
            self.shells.used = self.shells.visible = self.root_unit.opt.cur == self.root_unit.opt.medusa

        def build_description(self):
            res = UnitDescription(self.name, self.points, self.get_count(),
                                  options=self.root_unit.opt.description)
            res.add(self.shells.description)
            crewmember = UnitDescription('Renegade Crew', options=[
                Gear('Close combat weapon'), Gear('Frag grenades'), Gear('Lasgun')
            ])
            if self.root_unit.squad_options.sflak.value:
                crewmember.add(Gear('Sub-flak armour'))
            if self.root_unit.squad_options.flak.value and\
               self.root_unit.squad_options.flak.used:
                crewmember.add(Gear('Flak armour'))
            crewmember.set_count(4 + self.crew.cur)
            res.add(crewmember)
            return res

    def __init__(self, parent):
        super(OrdnanceBattery, self).__init__(parent)
        self.squad_options = SupportSquad.SquadOptions(self, krak=False)
        self.opt = self.GunOptions(self)
        self.guns = UnitList(self, self.Carriage, 1, 3)

    def get_count(self):
        return self.guns.count

    def build_points(self):
        return self.squad_options.points + self.guns.points

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count(),
                              options=self.squad_options.description)
        res.add(self.guns.description)
        return res

    def build_statistics(self):
        return {'Models': sum(u.models.cur * (5 + u.crew.cur) for u in self.guns.units),
                'Units': 1}


class Rapiers(Unit):
    type_name = 'Renegade Rapier Laser Destroyer Battery'
    type_id = 'r_rapiers_v1'

    def __init__(self, parent):
        super(Rapiers, self).__init__(parent)
        self.squad_options = SupportSquad.SquadOptions(self, krak=False)
        self.rap = Count(self, 'Rapier', 1, 3, 20, True,
                         gear=UnitDescription('Rapier', 20,
                                              options=[Gear('Laser destroyer array')]))
        self.crew = Count(self, 'Additional Renegade Crewman', 0, 1, 3, True)
        self.crewwep = [
            Count(self, 'Shotgun', 0, 1),
            Count(self, 'Autogun', 0, 1)
        ]

    def check_rules(self):
        self.crew.max = self.rap.cur
        crewlimit = self.rap.cur + self.crew.cur
        Count.norm_counts(0, crewlimit, self.crewwep)

    def get_count(self):
        return self.rap.cur

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count(),
                              self.squad_options.description)
        res.add(self.rap.description)
        lascount = self.rap.cur + self.crew.cur
        crew = UnitDescription('Renegade Crew', options=[
            Gear('Close combat weapon'), Gear('Krak grenades')
        ])
        if self.squad_options.sflak.value:
            crew.add(Gear('Sub-flak armour'))
        if self.squad_options.flak.value and\
           self.squad_options.flak.used:
            crew.add(Gear('Flak armour'))
        for wep in self.crewwep:
            if wep.cur:
                res.add(crew.clone().add(wep.gear).set_count(wep.cur))
                lascount -= wep.cur
        if lascount:
            res.add(crew.clone().add(Gear('Lasgun')).set_count(lascount))
        return res

    def build_statistics(self):
        return {'Models': self.rap.cur * 2 + self.crew.cur,
                'Units': 1}


class FieldBattery(Unit):
    type_name = 'Renegade Field Artillery Battery'
    type_id = 'r_field_v1'

    class Gun(ListSubUnit):
        type_name = 'Artillery Piece'

        class Type(OneOf):
            def __init__(self, parent):
                super(FieldBattery.Gun.Type, self).__init__(parent, 'Type')
                self.variant('Heavy quad launcher')
                self.variant('Heavy mortar')

        def __init__(self, parent):
            super(FieldBattery.Gun, self).__init__(parent, points=30)
            self.type = self.Type(self)
            self.crew = Count(self, 'Additional Renegade crewman', 0, 2, 3, True)

        def build_description(self):
            res = UnitDescription(name=self.type.cur.title, points=self.points, count=self.get_count())
            crew = UnitDescription('Renegade Crew', options=[
                Gear('Close combat weapon'), Gear('Krak grenades'), Gear('Lasgun')
            ])
            if self.root_unit.squad_options.sflak.value:
                crew.add(Gear('Sub-flak armour'))
            if self.root_unit.squad_options.flak.value and\
               self.root_unit.squad_options.flak.used:
                crew.add(Gear('Flak armour'))
            res.add(crew.set_count(3 + self.crew.cur))
            return res

    def __init__(self, parent):
        super(FieldBattery, self).__init__(parent)
        self.squad_options = SupportSquad.SquadOptions(self, krak=False)
        self.guns = UnitList(self, self.Gun, 1, 4)

    def get_count(self):
        return self.guns.count

    def build_statistics(self):
        return {'Models': sum(u.models.cur * (4 + u.crew.cur) for u in self.guns.units),
                'Units': 1}
