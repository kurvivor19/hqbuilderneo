__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OneOf
from builder.games.wh40k.unit import Unit
from builder.games.wh40k.imperial_armour.volume1 import Baneblade


class Macharius(Unit):
    type_name = 'Death Korps Macharius heavy tank'
    type_id = 'macharius_krieg_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Macharius.Options, self).__init__(parent, name='Options')
            self.variant('Hunter-killer missile', 10)
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)

        def check_rules(self):
            super(Macharius.Options, self).check_rules()
            self.process_limit([self.sb, self.hs], 1)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Macharius.Sponsons, self).__init__(parent, 'Sponson weapons')
            self.variant('Heavy stubbers', gear=[Gear('Heavy stubber', count=2)])
            self.variant('Heavy bolters', 10, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy flamers', 10, gear=[Gear('Heavy bolter', count=2)])

    def __init__(self, parent):
        super(Macharius, self).__init__(parent, 'Macharius',
                                        points=325, gear=[
                                            Gear('Macharius battle cannon'),
                                            Gear('Twin-linked heavy stubber')
                                        ])
        self.Options(self)
        self.Sponsons(self)


class MachariusVulcan(Unit):
    type_name = 'Death Korps Macharius Vulcan'
    type_id = 'mvulcan_krieg_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MachariusVulcan, self).__init__(parent, 'Macharius Vulcan',
                                              points=405, gear=[
                                                  Gear('Vulcan mega-bolter'),
                                                  Gear('Twin-linked heavy stubber')
                                              ])
        Macharius.Options(self)
        Macharius.Sponsons(self)


class MachariusVanquisher(Unit):
    type_name = 'Death Korps Macharius Vanquisher'
    type_id = 'mvanquisher_krieg_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MachariusVanquisher, self).__init__(parent, 'Macharius Vanquisher',
                                                  points=375, gear=[
                                                      Gear('Macharius Vanquisher cannon'),
                                                      Gear('Twin-linked heavy stubber')
                                                  ])
        Baneblade.Options(self)
        Macharius.Sponsons(self)
