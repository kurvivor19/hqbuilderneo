__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.wh40k.section import LordsOfWarSection, ElitesSection,\
    FastSection, HeavySection
from .tyranids import Dimachaeron, ScythedHierodule, MeioticSpores,\
    MalanthropeBrood, SCCarnifexBrood


class AnphelionElites(ElitesSection):
    def __init__(self, parent):
        super(AnphelionElites, self).__init__(parent)
        UnitType(self, MalanthropeBrood)


class AnphelionFast(FastSection):
    def __init__(self, parent):
        super(AnphelionFast, self).__init__(parent)
        UnitType(self, Dimachaeron)
        UnitType(self, MeioticSpores)


class AnphelionLords(LordsOfWarSection):
    def __init__(self, parent):
        super(AnphelionLords, self).__init__(parent)
        UnitType(self, ScythedHierodule)


class AnphelionHeavy(HeavySection):
    def __init__(self, parent):
        super(AnphelionHeavy, self).__init__(parent)
        UnitType(self, SCCarnifexBrood)
