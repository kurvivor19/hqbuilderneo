__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, ListSubUnit
from builder.games.wh40k.unit import Squadron


class VehicleOptions(OptionsList):
    def __init__(self, parent, light=False, netting=False):
        super(VehicleOptions, self).__init__(parent, 'Options')
        if light:
            self.variant('Searchlight', light)
        self.sb = self.variant('Storm bolter', 10)
        self.hs = self.variant('Heavy stubber', 10)
        self.variant('Hunter-killer missile', 10)
        self.dozen = self.variant('Dozer blade', 10)
        self.variant('Extra armour', 15)
        if netting:
            self.variant('Camo netting', netting)

    def check_rules(self):
        super(VehicleOptions, self).check_rules()
        self.process_limit([self.hs, self.sb], 1)


class CamoSquadron(Squadron):
    clear_name = ''
    launchers = False
    netting = 20

    class SquadronOptions(OptionsList):
        def __init__(self, parent):
            super(CamoSquadron.SquadronOptions, self).__init__(parent, 'Options')
            self.variant('Camo netting', parent.netting, per_model=True)
            if parent.launchers:
                self.variant('Smoke launchers', parent.launchers, per_model=True)

    def __init__(self, parent):
        super(CamoSquadron, self).__init__(parent, name=self.clear_name)
        self.opt = self.SquadronOptions(self)

    def get_subclass(self):
        class VehicleSubclass(self.unit_class, ListSubUnit):
            def build_points(self):
                return super(VehicleSubclass, self).build_points() +\
                    self.root_unit.opt.points
        return VehicleSubclass

    def build_points(self):
        return self.vehicles.points
