__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, Gear
from builder.games.wh40k.roster import Unit


class Caster(OptionsList):
    def __init__(self, parent):
        super(Caster, self).__init__(parent, 'Options')
        self.variant('Dirge Caster', 5)


class Dedication(OptionsList):
    def __init__(self, parent):
        super(Dedication, self).__init__(parent, '', limit=1)
        self.khorne = self.variant('Daemon Knight of Khorne', 50)
        self.nurgle = self.variant('Daemon Knight of Nurgle', 75)
        self.tzeentch = self.variant('Daemon Knight of Tzeentch', 65)
        self.slaanesh = self.variant('Daemon Knight of Slaanesh', 50)


class KhorneKnight(Unit):
    def __init__(self, *args, **kwargs):
        super(KhorneKnight, self).__init__(*args, **kwargs)
        self.dedication.khorne.value = True
        self.dedication.visible = False


class ChaosErrant(Unit):
    type_name = 'Chaos Knight Errant (Dataslate)'
    type_id = 'chaos_errant_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(ChaosErrant, self).__init__(
            parent, name='Chaos Knight Errant', points=370, gear=[
                Gear('Heavy stubber'), Gear('Thermal cannon'),
                Gear('Foe-reaper chainsword'), Gear('Ion shield')
            ])
        Caster(self)
        self.dedication = Dedication(self)

    def not_nurgle(self):
        return self.dedication.any and not self.dedication.nurgle.value


class ChaosPaladin(Unit):
    type_name = 'Chaos Knight Paladin (Dataslate)'
    type_id = 'chaos_paladin_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(ChaosPaladin, self).__init__(
            parent, name='Chaos Knight Paladin', points=375, gear=[
                Gear('Heavy stubber', count=2),
                Gear('Rapid-fire battle cannon'),
                Gear('Foe-reaper chainsword'),
                Gear('Ion shield')
            ])
        Caster(self)
        self.dedication = Dedication(self)

    def not_nurgle(self):
        return self.dedication.any and not self.dedication.nurgle.value


class KhorneErrant(KhorneKnight, ChaosErrant):
    pass


class KhornePaladin(KhorneKnight, ChaosPaladin):
    pass


class Kytan(Unit):
    type_name = 'Kytan Daemon Engine of Khorne (Dataslate)'
    type_id = 'kytan_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Kytan, self).__init__(parent, 'Kytan Daemon Engine', 525, [
            Gear('Kytan gatling cannon'), Gear('Great Cleaver of Khorne')
        ], static=True)
