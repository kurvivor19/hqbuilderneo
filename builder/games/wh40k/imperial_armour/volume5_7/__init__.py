__author__ = 'Ivan Truskov'

from .characters import HectorRex, Necrosius, Zhufor, Arkos, Mamon,\
    Uraka
from builder.games.wh40k.section import UnitType, HQSection
from builder.games.wh40k.chaos_marines_v2.elites import Terminators


class InquisitorCharacters(HQSection):
    def __init__(self, parent):
        super(InquisitorCharacters, self).__init__(parent)
        UnitType(self, HectorRex)


class ChaosMarinesCharacters(HQSection):
    def __init__(self, parent):
        super(ChaosMarinesCharacters, self).__init__(parent)
        self.necrosius = UnitType(self, Necrosius)
        self.zhufor = UnitType(self, Zhufor)
        UnitType(self, Arkos)
        self.bodyguards = UnitType(self, Terminators, slot=0)

    def check_rules(self):
        super(ChaosMarinesCharacters, self).check_rules()
        self.bodyguards.active = self.zhufor.count
        if self.bodyguards.count > self.zhufor.count:
            self.error('Only one unit of Chaos Terminators may be taken as bodyguards for Zhufor')


class RenegadeCharacters(HQSection):
    def __init__(self, parent):
        super(RenegadeCharacters, self).__init__(parent)
        UnitType(self, Mamon)
        self.uraka = UnitType(self, Uraka)
