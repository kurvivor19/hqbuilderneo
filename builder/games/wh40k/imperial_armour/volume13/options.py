__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, ListSubUnit
from builder.core2.node import Node
from builder.games.wh40k.unit import Unit, Squadron
import sys


class ChaosSpaceMarinesLegaciesNode(Node):

    @property
    def ia_enabled(self):
        try:
            return self.root.imperial_armour_on
        except Exception:
            return False

    def count_ruin_legacies(self):
        return 0

    @staticmethod
    def root_check(roster):
        leg_sum = 0
        for sec in roster.sections:
            for unit in sec.units:
                if hasattr(unit, 'sub_roster') and\
                   isinstance(unit.sub_roster.roster, ChaosSpaceMarinesLegaciesNode):
                    leg_sum += unit.sub_roster.roster.count_ruin_legacies()
        leg_limit = roster.points / 1000
        if leg_limit < leg_sum:
            roster.error("No more then 1 Legacy of Ruin per 1000 points may be taken; taken: {}".format(leg_sum))


class CommonOption(OptionsList):
    def __init__(self, parent, armour=None,
                 ceramite=None, malefic=None,
                 posession=15):
        super(CommonOption, self).__init__(parent, 'Options')
        if armour:
            self.variant('Extra Armour', armour)
        if ceramite:
            self.variant('Armoured Ceramite', ceramite)
        self.variant('Dirge caster', 5)
        self.variant('Dozer blade', 5)
        self.variant('Warpflame Gargoyles', 5)
        self.variant('Havok launcher', 12)
        self.variant('Destroyer blades', 15)
        if posession:
            self.variant('Daemonic posession', posession)
        if malefic:
            self.variant('Malefic ammunition', malefic)


class LegaciesOfRuin(OptionsList):
    def __init__(self, parent, super_heavy=False, walker=False, tank=False,
                 transport=False, order=sys.maxsize - 1):
        super(LegaciesOfRuin, self).__init__(parent, 'Legacies of Ruin',
                                             node_id='legacies_of_ruin',
                                             order=order)
        if tank:
            self.variant('Veteran of the Scouring', 90 if super_heavy else 45)
            self.variant('War within the Eye', 40 if super_heavy else 20)
            self.variant('Blood of Mackan', 40 if super_heavy else 30)
            self.variant('Badab Uprising', 30 if super_heavy else 20)
            self.variant('Scourge of the Greenskin', 25)
        else:
            if walker:
                self.variant('Veteran of the Scouring', 80 if super_heavy else 40)
                self.variant('War within the Eye', 40 if super_heavy else 20)
                if not super_heavy:
                    self.variant('Badab Uprising', 20)
        self.variant('Maelstorm Raider', 50 if super_heavy else 25)
        self.variant('Death of Kasyr Lutien', 25 if super_heavy else 15)
        self.variant('Siege of Vraks', 20 if super_heavy else 15)
        self.variant('Fourth Quadrant Rebellion', 30 if super_heavy else 15)
        self.variant('Screams of Lugganath', 30 if super_heavy else 25)
        self.variant('Perdus Rift Anomaly', 40 if super_heavy else 20)
        self.variant('1st War of Armageddon', 35 if super_heavy else 25)
        self.variant('Vessel of Akashneth of the Boiling Brass', 20 if super_heavy else 15)
        self.variant('Vessel of Shyak the Seeker', 20 if super_heavy else 15)
        self.variant('Vessel of Dhornurgh the Reborn', 15 if super_heavy else 10)
        self.variant('Vessel of Tzenakh the Occluder', 20 if super_heavy else 10)
        self.variant('Auloth the Primordial Iterator', 80 if super_heavy else 40)
        if transport:
            self.variant('Last of the Forge', 20)

    def check_rules(self):
        super(LegaciesOfRuin, self).check_rules()
        # in future, all Wh40k base roster classes will implement
        # Wh40kImperial, but for now we swallow errors
        try:
            self.used = self.visible = self.root.imperial_armour_on
        except Exception:
            pass


class ChaosSpaceMarinesBaseVehicle(Unit):
    def __init__(self, parent, *args, **kwargs):
        super_heavy = kwargs.pop('super_heavy', False)
        walker = kwargs.pop('walker', False)
        tank = kwargs.pop('tank', False)
        transport = kwargs.pop('transport', False)

        super(ChaosSpaceMarinesBaseVehicle, self).__init__(parent, *args, **kwargs)

        self.legacies_of_ruin = LegaciesOfRuin(self, super_heavy, walker, tank, transport)

    def get_unique_gear(self):
        if self.legacies_of_ruin.used:
            return self.legacies_of_ruin.description
        return []

    def count_ruin_legacies(self):
        if self.legacies_of_ruin.used:
            return sum(opt.value for opt in self.legacies_of_ruin.options)
        else:
            return 0


class ChaosSpaceMarinesBaseSquadron(Squadron):

    def get_subclass(self):
        class VehicleSubclass(self.unit_class, ListSubUnit):
            @ListSubUnit.count_unique
            def get_uniq_gear(self):
                return self.get_unique_gear()

        return VehicleSubclass

    def get_unique_gear(self):
        return sum((u.get_uniq_gear() for u in self.vehicles.units), [])

    def count_ruin_legacies(self):
        return sum(u.count_ruin_legacies() * u.models.cur for u in self.vehicles.units)
