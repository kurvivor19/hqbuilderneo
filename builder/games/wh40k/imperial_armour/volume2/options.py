from builder.core2 import OptionsList, ListSubUnit
from builder.games.wh40k.unit import Unit, Squadron
import sys

__author__ = 'dante'


class LegaciesOfGlory(OptionsList):
    def __init__(self, parent, super_heavy=False, walker=False, tank=False,
                 deep_strike=False, thunder_hawk=False,
                 transport=False, order=sys.maxsize):
        super(LegaciesOfGlory, self).__init__(parent, 'Legacies of Glory', node_id='legacies_of_glory',
                                              order=order)
        self.variant('Battle of Keylek', 35 if not super_heavy else 75)
        if tank or walker:
            self.variant('Ullanor crusade', 50 if super_heavy else 25 if tank else 20)
        self.variant('War of Murder', 20 if not super_heavy else 40)
        self.variant('Battle of Sarosh', 30 if not super_heavy else 60)
        self.variant('Isstvan III', 30 if not super_heavy else 60)
        self.variant('Burning of Prospero', 20 if not super_heavy else 40)
        if deep_strike:
            self.variant('Istvaan V Dropsite Massacre ', 20)
        self.variant('Battle of Signus Prime', 35 if not super_heavy else 70)
        if thunder_hawk:
            self.variant('Battle of the Phall System', 35 if not super_heavy else 70)
        self.variant('Thramas Crusade', 40 if not super_heavy else 80)
        if tank:
            self.variant('Shism of Mars', 25 if not super_heavy else 50)
        self.variant('Battle of Terra', 40 if not super_heavy else 80)
        self.variant('Icon of Glory', 50 if not super_heavy else 100)
        self.variant('Shrouded Provenance', 30 if not super_heavy else 60)
        if transport:
            self.variant('Ancient Mariner', 10)

    def check_rules(self):
        super(LegaciesOfGlory, self).check_rules()
        # in future, all Wh40k base roster classes will implement
        # Wh40kImperial, but for now we swallow errors
        try:
            self.used = self.visible = self.root.imperial_armour_on
        except Exception:
            pass


class SpaceMarinesBaseVehicle(Unit):
    def __init__(self, parent, *args, **kwargs):
        super_heavy = kwargs.pop('super_heavy', False)
        walker = kwargs.pop('walker', False)
        tank = kwargs.pop('tank', False)
        deep_strike = kwargs.pop('deep_strike', False)
        thunder_hawk = kwargs.pop('thunder_hawk', False) or kwargs.pop('storm_eagle', False)
        transport = kwargs.pop('transport', False)

        super(SpaceMarinesBaseVehicle, self).__init__(parent, *args, **kwargs)

        self.legacies_of_glory = LegaciesOfGlory(self, super_heavy, walker, tank, deep_strike, thunder_hawk, transport)

    def get_unique_gear(self):
        if self.legacies_of_glory.used:
            return self.legacies_of_glory.description
        return []

    def count_glory_legacies(self):
        if self.legacies_of_glory.used:
            return sum(opt.value for opt in self.legacies_of_glory.options)
        else:
            return 0


class SpaceMarinesBaseSquadron(Squadron):

    def get_subclass(self):
        class VehicleSubclass(self.unit_class, ListSubUnit):
            @ListSubUnit.count_unique
            def get_uniq_gear(self):
                return self.get_unique_gear()

        return VehicleSubclass

    def get_unique_gear(self):
        return sum((u.get_uniq_gear() for u in self.vehicles.units), [])

    def count_glory_legacies(self):
        return sum(u.count_glory_legacies() * u.models.cur for u in self.vehicles.units)
