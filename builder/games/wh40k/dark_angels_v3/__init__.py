__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = ['Codex Dark Angels 7th edition', 'Dark Angels Librarius Conclave']

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PrimaryDetachment, PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation, LordsOfWarSection,\
    FlyerSection
from builder.games.wh40k.fortifications import Fort

from builder.games.wh40k.escalation.space_marines import LordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.imperial_armour.dataslates.space_marines import Deredeo, Xiphon,\
    Leviathan
from builder.games.wh40k.imperial_armour.aeronautika.space_marines import HyperiosBattery

from .hq import CompanyMaster, InterrogatorChaplain, Chaplain, Techmarine,\
    Belial, Sammael, Sableclaw, Asmodai, Ezekiel, Librarian
from .elites import CompanyVeteransSquad, CommandSquad, DeathwingTerminatorSquad,\
    DeathwingCommandSquad, DeathwingKnights, RavenwingCommandSquad, Dreadnoughts,\
    VenDreadnoughts
from .troops import TacticalSquad, ScoutSquad
from .fast import AssaultSquad, RavenwingBikeSquad, RavenwingAttackBikeSquad,\
    RavenwingBlackKnights, RavenwingDarkShroud, RavenwingDarkTalon, RavenwingSpeeders,\
    NephilimJetfighter, DarkTalons, Jetfighters
from .heavy import Devastators, Predators, Whirlwinds, Vindicators, LandSpeederVengeance,\
    DeimosVindicator
from .transport import Rhino, DropPod, Razorback, LandRaider, LandRaiderRedeemer,\
    LandRaiderCrusader
from builder.games.wh40k.dataslates.command_tanks import SpaceMarineCommandTanks
from .lords import Azrael
from itertools import chain


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.ezekiel = UnitType(self, Ezekiel)
        self.asmodai = UnitType(self, Asmodai)
        self.belial = UnitType(self, Belial)
        self.sammael = UnitType(self, Sammael)
        UnitType(self, Sableclaw)
        self.company_master = UnitType(self, CompanyMaster)
        self.librarian = UnitType(self, Librarian)
        self.interrogator = UnitType(self, InterrogatorChaplain)
        self.chaplain = UnitType(self, Chaplain)
        self.tech = UnitType(self, Techmarine)
        UnitType(self, SpaceMarineCommandTanks)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.company_veterans_squad = UnitType(self, CompanyVeteransSquad)
        self.deathwing_terminator_squad = UnitType(self, DeathwingTerminatorSquad)
        self.deathwing_knights = UnitType(self, DeathwingKnights)
        self.dreadnought = UnitType(self, Dreadnoughts)
        UnitType(self, VenDreadnoughts)
        UnitType(self, CommandSquad)
        UnitType(self, DeathwingCommandSquad)
        UnitType(self, RavenwingCommandSquad)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.tactical_squad = UnitType(self, TacticalSquad)
        self.scout_squad = UnitType(self, ScoutSquad)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        UnitType(self, DropPod)
        self.assault_squad = UnitType(self, AssaultSquad)
        UnitType(self, RavenwingBikeSquad)
        UnitType(self, RavenwingAttackBikeSquad)
        self.ravenwing_support_squad = UnitType(self, RavenwingSpeeders)
        self.ravenwing_black_knights = UnitType(self, RavenwingBlackKnights)
        self.ravenwing_dark_shroud = UnitType(self, RavenwingDarkShroud)
        fighter = UnitType(self, NephilimJetfighter)
        fighter.visible = False
        UnitType(self, Jetfighters)
        talon = UnitType(self, RavenwingDarkTalon)
        talon.visible = False
        UnitType(self, DarkTalons)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.devastators = UnitType(self, Devastators)
        self.predator = UnitType(self, Predators)
        self.whirlwind = UnitType(self, Whirlwinds)
        self.vindicator = UnitType(self, Vindicators)
        self.land_raider = UnitType(self, LandRaider)
        self.land_raider_crusader = UnitType(self, LandRaiderCrusader)
        self.land_raider_redeemer = UnitType(self, LandRaiderRedeemer)
        self.land_speeder_vengeance = UnitType(self, LandSpeederVengeance)


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        UnitType(self, Azrael)


class HQ(volume2.SpaceMarineHQ, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, HyperiosBattery)
        UnitType(self, Xiphon)


class Elites(volume2.DarkAngelsElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.relics.append(UnitType(self, Deredeo))
        UnitType(self, DeimosVindicator)
        self.relics.append(UnitType(self, Leviathan))


class Lords(volume2.SpaceMarineLordsOfWar, Titans, LordsOfWar, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Jetfighters, DarkTalons])


class DarkAngelsV3Base(volume2.SpaceMarinesRelicRoster):
    army_id = 'dark_angels_v3_base'
    army_name = 'Dark Angels'
    # set IA enabled by default

    def __init__(self):
        super(DarkAngelsV3Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=Lords(parent=self)
        )

    def has_keeper(self):
        return self.hq.interrogator.count > 0


class DemiCompany(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_demi'
    army_name = 'Battle Demi-Company (Dark Angels)'
    discount = False

    def __init__(self):
        super(DemiCompany, self).__init__()
        self.cap = UnitType(self, CompanyMaster)
        self.chap = UnitType(self, Chaplain)
        self.add_type_restriction([self.cap, self.chap], 1, 1)
        UnitType(self, CommandSquad, max_limit=1)
        UnitType(self, CompanyVeteransSquad, max_limit=1)
        UnitType(self, TacticalSquad, min_limit=3, max_limit=3)
        UnitType(self, AssaultSquad, min_limit=1, max_limit=1)
        UnitType(self, Devastators, min_limit=1, max_limit=1)
        UnitType(self, Dreadnoughts, max_limit=1)

    def has_cap(self):
        return self.cap.count

    def has_chap(self):
        return self.chap.count


class DeathwingRedemption(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_redemption'
    army_name = 'Deathwing Redemption Force'

    def __init__(self):
        super(DeathwingRedemption, self).__init__()
        bel = UnitType(self, Belial)
        self.chars = [
            UnitType(self, CompanyMaster),
            UnitType(self, InterrogatorChaplain),
            UnitType(self, Librarian)
        ]
        self.add_type_restriction(self.chars + [bel], 1, 1)
        UnitType(self, DeathwingTerminatorSquad, min_limit=2, max_limit=5)
        UnitType(self, DeathwingCommandSquad, max_limit=1)
        UnitType(self, DeathwingKnights, max_limit=1)
        self.vendred = UnitType(self, VenDreadnoughts)

    def check_rules(self):
        super(DeathwingRedemption, self).check_rules()
        for com in chain(*[utype.units for utype in self.chars]):
            if not com.armour.is_tda():
                self.error('All Independent characters must have Terminator armour')
        for dredunit in self.vendred.units:
            if not dredunit.transport.drop.visible:
                self.error('Venerable Dreadnought must have a Drop Pod as a dedicated transport')


class RavenwingAttack(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_attack'
    army_name = 'Ravenwing attack squadron'

    def __init__(self):
        super(RavenwingAttack, self).__init__()
        bikes = UnitType(self, RavenwingBikeSquad)
        abikes = UnitType(self, RavenwingAttackBikeSquad)
        self.add_type_restriction([bikes, abikes], 1, 1)
        self.speeders = UnitType(self, RavenwingSpeeders)
        veng = UnitType(self, LandSpeederVengeance)
        self.add_type_restriction([self.speeders, veng], 1, 1)

    def check_rules(self):
        super(RavenwingAttack, self).check_rules()
        for sun in self.speeders.units:
            if sun.count > 1:
                self.error('Ravenwing Land Speeders may only include one model')


class RavenwingSupport(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_support'
    army_name = 'Ravenwing support squadron'

    def __init__(self):
        super(RavenwingSupport, self).__init__()
        self.speeders = UnitType(self, RavenwingSpeeders, min_limit=1, max_limit=1)
        veng = UnitType(self, LandSpeederVengeance)
        shroud = UnitType(self, RavenwingDarkShroud)
        self.add_type_restriction([veng, shroud], 1, 1)

    def check_rules(self):
        super(RavenwingSupport, self).check_rules()
        for sun in self.speeders.units:
            if sun.count < 3:
                self.error('Ravenwing Land Speeders must include at least 3 models')


class RavenwingSilence(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_silence'
    army_name = 'Ravenwing Silence squadron'

    def __init__(self):
        super(RavenwingSilence, self).__init__()
        UnitType(self, NephilimJetfighter, min_limit=2, max_limit=2)
        UnitType(self, RavenwingDarkTalon, min_limit=1, max_limit=1)


class RavenwingAbductor(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_abductor'
    army_name = 'Ravenwing Abductor squadron'

    def __init__(self):
        super(RavenwingAbductor, self).__init__()
        UnitType(self, NephilimJetfighter, min_limit=2, max_limit=2)
        UnitType(self, RavenwingDarkTalon, min_limit=1, max_limit=1)


class Hammer(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'dark_angels_v3_hammer'
    army_name = 'The Hammer of Caliban'

    def __init__(self):
        super(Hammer, self).__init__()
        UnitType(self, Techmarine, min_limit=1, max_limit=1)
        raiders = [
            UnitType(self, LandRaider),
            UnitType(self, LandRaiderCrusader),
            UnitType(self, LandRaiderRedeemer)
        ]
        self.add_type_restriction(raiders, 1, 1)
        self.tanks = [
            UnitType(self, Predators),
            UnitType(self, Whirlwinds),
            UnitType(self, Vindicators)
        ]
        self.add_type_restriction(self.tanks, 1, 1)

    def check_rules(self):
        super(Hammer, self).check_rules()
        for sun in chain(*[utype.units for utype in self.tanks]):
            if sun.count < 3:
                self.error('Tank units must include 3 models')


class LionBlade(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'dark_angels_v3_lion_blade'
    army_name = 'Lion\'s Blade Strike Force'

    class Core(Detachment):

        def __init__(self, parent):

            super(LionBlade.Core, self).__init__(parent, 'core', 'Core',
                                               [DemiCompany], 1, None)
            self.dctype = self.types[0]

        def is_full(self):
            if self.dctype.count != 2:
                return False
            chap_cnt = sum([unit.sub_roster.roster.has_chap() for unit in
                            self.dctype.units])
            cap_cnt = sum([unit.sub_roster.roster.has_cap() for unit in
                           self.dctype.units])
            return chap_cnt == 1 and cap_cnt == 1

    class InnerCircle(Formation):
        army_id = 'inner_circle'
        army_name = 'Inner circle'

        def __init__(self):
            super(LionBlade.InnerCircle, self).__init__()
            self.caps = [
                UnitType(self, Azrael),
                UnitType(self, Belial),
                UnitType(self, Sammael),
                UnitType(self, Sableclaw),
                UnitType(self, CompanyMaster),
                UnitType(self, InterrogatorChaplain),
                UnitType(self, Asmodai),
                UnitType(self, Librarian),
                UnitType(self, Ezekiel)
            ]
            self.add_type_restriction(self.caps, 1, 1)

    class CommandSection(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(LionBlade.CommandSection, self).__init__(parent, 'command', 'Command',
                                                           [LionBlade.InnerCircle], 0, 1)

        def check_limits(self):
            self.max = max([1, len(self.core.units)])
            return super(LionBlade.CommandSection, self).check_limits()

    class TenthCompany(Formation):
        army_id = '10th_company'
        army_name = '10th company support'

        def __init__(self):
            super(LionBlade.TenthCompany, self).__init__()
            UnitType(self, ScoutSquad, min_limit=1, max_limit=5)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(LionBlade.Auxilary, self).__init__(parent, 'aux',
                                                   'Auxilary', [
                                                        RavenwingAttack,
                                                        RavenwingSupport,
                                                        RavenwingSilence,
                                                        LionBlade.TenthCompany,
                                                        DeathwingRedemption,
                                                        Hammer
                                                   ], 1, None)

    def __init__(self):
        self.core = self.Core(self)
        command = self.CommandSection(self, self.core)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        discount = self.core.is_full()
        for unit in self.core.units:
            if unit.sub_roster.roster.discount != discount:
                unit.sub_roster.roster.discount = discount
                unit.check_rules_chain()


class DarkAngelsV3CAD(DarkAngelsV3Base, CombinedArmsDetachment):
    army_id = 'dark_angels_v3_cad'
    army_name = 'Dark Angels (Combined arms detachment)'


class DarkAngelsV3AD(DarkAngelsV3Base, AlliedDetachment):
    army_id = 'dark_angels_v3_ad'
    army_name = 'Dark Angels (Allied detachment)'


class DarkAngelsV3PA(DarkAngelsV3Base, PlanetstrikeAttacker):
    army_id = 'dark_angels_v3_pa'
    army_name = 'Dark Angels (Planetstrike attacker detachment)'


class DarkAngelsV3PD(DarkAngelsV3Base, PlanetstrikeDefender):
    army_id = 'dark_angels_v3_pd'
    army_name = 'Dark Angels (Planetstrike defender detachment)'


class DarkAngelsV3SA(DarkAngelsV3Base, SiegeAttacker):
    army_id = 'dark_angels_v3_sa'
    army_name = 'Dark Angels (Siege War attacker detachment)'


class DarkAngelsV3SD(DarkAngelsV3Base, SiegeDefender):
    army_id = 'dark_angels_v3_sd'
    army_name = 'Dark Angels (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'dark_angels_v3_asd'
    army_name = 'Dark Angels (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class DeathwingStrikeForce(Wh40kBase, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'dark_angels_v3_deathwing_strike'
    army_name = 'Deathwing Strike Force'

    class DeathwingHQ(HQSection):
        def __init__(self, parent):
            super(DeathwingStrikeForce.DeathwingHQ, self).__init__(parent, max_limit=3)
            UnitType(self, Belial)
            UnitType(self, InterrogatorChaplain)
            UnitType(self, Asmodai)
            UnitType(self, Librarian)
            UnitType(self, Ezekiel)
            UnitType(self, CompanyMaster)

    class DeathwingElite(ElitesSection):
        class DeathwingVenDread(VenDreadnoughts):
            def check_rules(self):
                super(DeathwingStrikeForce.DeathwingElite.DeathwingVenDread, self).check_rules()
                if not self.transport.drop.visible:
                    self.error('Venerable Dreadnought must be given a Drop Pod')

        def __init__(self, parent):
            super(DeathwingStrikeForce.DeathwingElite, self).__init__(
                parent, min_limit=2, max_limit=12)
            UnitType(self, self.DeathwingVenDread)
            UnitType(self, DeathwingTerminatorSquad)
            UnitType(self, DeathwingCommandSquad)
            UnitType(self, DeathwingKnights)

    def __init__(self):
        super(DeathwingStrikeForce, self).__init__(
            ections=[self.DeathwingHQ(parent=self),
                     self.DeathwingElite(parent=self)]
        )


class RavenwingStrikeForce(Wh40kBase, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'dark_angels_v3_ravenwing_strike'
    army_name = 'Ravenwing Strike Force'

    class RavenwingHQ(HQSection):
        def __init__(self, parent):
            super(RavenwingStrikeForce.RavenwingHQ, self).__init__(parent, max_limit=3)
            UnitType(self, Sammael)
            UnitType(self, Sableclaw)
            self.bikers = [
                UnitType(self, InterrogatorChaplain),
                UnitType(self, Librarian),
                UnitType(self, Chaplain),
                UnitType(self, Techmarine)
            ]

    class RavenwingElite(ElitesSection):

        def __init__(self, parent):
            super(RavenwingStrikeForce.RavenwingElite, self).__init__(
                parent, max_limit=1)
            UnitType(self, RavenwingCommandSquad)

    class RavenwingFastAttack(FastSection):
        def __init__(self, parent):
            super(RavenwingStrikeForce.RavenwingFastAttack, self).__init__(
                parent, min_limit=2, max_limit=12)
            UnitType(self, RavenwingBikeSquad)
            UnitType(self, RavenwingAttackBikeSquad)
            UnitType(self, RavenwingSpeeders)
            UnitType(self, RavenwingDarkShroud)
            UnitType(self, NephilimJetfighter)
            UnitType(self, RavenwingDarkTalon)
            UnitType(self, RavenwingBlackKnights)

    class RavenwingHeavy(HeavySection):

        def __init__(self, parent):
            super(RavenwingStrikeForce.RavenwingHeavy, self).__init__(parent)
            UnitType(self, LandSpeederVengeance)

    def __init__(self):
        self.hq = self.RavenwingHQ(parent=self)
        super(RavenwingStrikeForce, self).__init__(
            sections=[
                self.hq,
                self.RavenwingElite(parent=self),
                self.RavenwingFastAttack(parent=self),
                self.RavenwingHeavy(parent=self)
            ]
        )

    def check_rules(self):
        super(RavenwingStrikeForce, self).check_rules()
        for utype in self.hq.bikers:
            for unit in utype.units:
                if not unit.has_bike():
                    self.error('{} must be mounted on bike'.format(unit.name))


class DarkAngelsV3KillTeam(Wh40kKillTeam):
    army_id = 'dark_angels_v3_kt'
    army_name = 'Dark Angels'
    ia_enabled = False

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(DarkAngelsV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [TacticalSquad], lambda u: [u.transport.drop])
            self.scout_squad = UnitType(self, ScoutSquad)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(DarkAngelsV3KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                Rhino, Razorback, RavenwingSpeeders, RavenwingDarkShroud
            ])
            Wh40kKillTeam.process_transported_units(self, [AssaultSquad], lambda u: [u.transport.drop])
            UnitType(self, RavenwingBikeSquad)
            UnitType(self, RavenwingAttackBikeSquad)
            self.ravenwing_black_knights = UnitType(self, RavenwingBlackKnights)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(DarkAngelsV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                CompanyVeteransSquad, CommandSquad
            ], lambda u: [u.transport.drop])
            UnitType(self, RavenwingCommandSquad)

    def __init__(self):
        super(DarkAngelsV3KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFastAttack(self)
        )


class Conclave(Formation):
    army_id = 'dark_angels_v3_conclave'
    army_name = 'Dark Angels Librarius Conclave'

    def __init__(self):
        super(Conclave, self).__init__()
        UnitType(self, Ezekiel, min_limit=1, max_limit=1)
        UnitType(self, Librarian, min_limit=2, max_limit=4)


faction = 'Dark Angels'


class DarkAngelsV3(Wh40k7ed, Wh40kImperial):
    army_id = 'dark_angels_v3'
    army_name = 'Dark Angels'
    faction = faction
    # development = True

    def __init__(self):
        super(DarkAngelsV3, self).__init__([
            DarkAngelsV3CAD,
            DeathwingStrikeForce,
            RavenwingStrikeForce,
            FlierDetachment,
            LionBlade,
            DemiCompany,
            DeathwingRedemption,
            RavenwingAttack,
            RavenwingSupport,
            RavenwingSilence,
            RavenwingAbductor,
            Hammer,
            Conclave
        ])


class DarkAngelsV3Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'dark_angels_v3_mis'
    army_name = 'Dark Angels'
    faction = faction
    # development = True

    def __init__(self):
        super(DarkAngelsV3Missions, self).__init__([
            DarkAngelsV3CAD,
            DarkAngelsV3PA,
            DarkAngelsV3PD,
            DarkAngelsV3SA,
            DarkAngelsV3SD,
            DeathwingStrikeForce,
            RavenwingStrikeForce,
            FlierDetachment,
            LionBlade,
            DemiCompany,
            DeathwingRedemption,
            RavenwingAttack,
            RavenwingSupport,
            RavenwingSilence,
            RavenwingAbductor,
            Hammer,
            Conclave
        ])


detachments = [
    DarkAngelsV3CAD,
    DarkAngelsV3AD,
    DarkAngelsV3PA,
    DarkAngelsV3PD,
    DarkAngelsV3SA,
    DarkAngelsV3SD,
    DeathwingStrikeForce,
    RavenwingStrikeForce,
    FlierDetachment,
    LionBlade,
    DeathwingRedemption,
    RavenwingAttack,
    RavenwingSupport,
    RavenwingSilence,
    RavenwingAbductor,
    Hammer,
    Conclave
]


unit_types = [
    CompanyMaster, InterrogatorChaplain, Chaplain, Techmarine,
    Belial, Sammael, Sableclaw, Asmodai, Ezekiel, Librarian,
    CompanyVeteransSquad, CommandSquad, DeathwingTerminatorSquad,
    DeathwingCommandSquad, DeathwingKnights, RavenwingCommandSquad, Dreadnoughts,
    VenDreadnoughts,
    TacticalSquad, ScoutSquad,
    AssaultSquad, RavenwingBikeSquad, RavenwingAttackBikeSquad,
    RavenwingBlackKnights, RavenwingDarkShroud, RavenwingDarkTalon, RavenwingSpeeders,
    NephilimJetfighter, DarkTalons, Jetfighters,
    Devastators, Predators, Whirlwinds, Vindicators, LandSpeederVengeance,
    Rhino, DropPod, Razorback, LandRaider, LandRaiderRedeemer,
    LandRaiderCrusader, Azrael
]
