__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import Gear, SubUnit, Count, OptionsList, UnitDescription
from .transport import Transport
from .armory import Weapon, Heavy, Special
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from builder.games.wh40k.roster import Unit


class TacticalSquad(IATransportedUnit):
    type_name = 'Tactical Squad'
    type_id = 'tactical_squad_v3'
    model_points = 14
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(TacticalSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(TacticalSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * TacticalSquad.model_points,
                gear=TacticalSquad.model_gear
            )
            self.wep1 = Weapon(self, 'Weapon', bolt_gun=True, melee=True)
            self.wep2 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(TacticalSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    def __init__(self, parent):
        super(TacticalSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.special = Special(parent=self, bolt_gun=True, name='Special weapon')
        self.heavy = Heavy(parent=self, bolt_gun=True, name='Heavy weapon')
        self.transport = Transport(parent=self)

    def check_rules(self):
        self.special.visible = self.special.used = self.heavy.cur == self.heavy.boltgun or self.get_count() == 10
        self.heavy.visible = self.heavy.used = self.special.cur == self.special.boltgun or self.get_count() == 10

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=TacticalSquad.model_gear + [Gear('Bolt pistol')],
            points=TacticalSquad.model_points
        )
        count = self.marines.cur
        for o in [self.heavy, self.special]:
            if o.used and o.cur != o.boltgun:
                spec_marine = marine.clone()
                spec_marine.points += o.points
                if o == self.heavy:
                    spec_marine.points += o.flakk.points
                spec_marine.add(o.description)
                desc.add(spec_marine)
                count -= 1
        marine.add(Gear('Boltgun'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class ScoutSquad(Unit):
    type_name = 'Scout Squad'
    type_id = 'scout_squad_v3'

    model_points = 11
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):
        class Weapon(Weapon):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon', bolt_gun=True,
                                                                 melee=True, ranged=True)
                self.sniperrifle = self.variant('Sniper rifle', 1)
                self.shotgun = self.variant('Shotgun', 0)
                self.combatknife = self.variant('Combat knife', 0)

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(ScoutSquad.Sergeant, self).__init__(
                name='Scout Sergeant',
                parent=parent, points=55 - 4 * ScoutSquad.model_points,
                gear=TacticalSquad.model_gear
            )
            self.wep1 = self.Weapon(self)
            self.wep2 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(ScoutSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Scout Sergeant'
            if self.parent.parent.opt.any:
                desc.add(Gear('Camo cloak')).add_points(2)
            return desc

    class Heavy(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Heavy, self).__init__(parent=parent, name='Heavy weapon')

            self.hbgun = self.variant('Heavy bolter', 8)
            self.hellfire = self.variant('Hellfire shells', 5, visible=False)
            self.mlaunch = self.variant('Missile launcher', 15)
            self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
            self.heavy = [self.hbgun, self.mlaunch]

        def check_rules(self):
            super(ScoutSquad.Heavy, self).check_rules()
            self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
            self.hellfire.visible = self.hellfire.used = self.hbgun.value
            self.process_limit(self.heavy, 1)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Options, self).__init__(parent=parent, name='Options')
            self.cloak = self.variant('Camo cloaks', 2, per_model=True)

    def __init__(self, parent):
        super(ScoutSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Scout', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.sniper = Count(parent=self, name='Sniper rifle', min_limit=0, max_limit=4, points=1)
        self.shotgun = Count(parent=self, name='Space Marine shotgun', min_limit=0, max_limit=4, points=0)
        self.knife = Count(parent=self, name='Combat knife', min_limit=0, max_limit=4, points=0)

        self.heavy = self.Heavy(parent=self)
        self.opt = self.Options(parent=self)

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        super(ScoutSquad, self).check_rules()
        max_wep = self.marines.cur
        if self.heavy.any:
            max_wep -= 1
        Count.norm_counts(0, max_wep, [self.sniper, self.shotgun, self.knife])

    def build_points(self):
        return self.sergeant.points + self.marines.points + self.heavy.points\
            + self.sniper.points + self.opt.points * self.get_count()

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count(),
                               self.sergeant.description)
        scout = UnitDescription('Scout', self.model_points,
                                options=self.model_gear + [Gear('Bolt pistol')])
        if self.opt.any:
            scout.add(Gear('Camo cloak')).add_points(2)
        count = self.marines.cur
        if self.heavy.any:
            spec_weapon = scout.clone().add(self.heavy.description)
            spec_weapon.points += self.heavy.points
            desc.add(spec_weapon)
            count -= 1
        for o in [self.knife, self.shotgun, self.sniper]:
            if o.cur:
                desc.add(scout.clone().add(Gear(o.name))
                         .add_points(o.option_points)
                         .set_count(o.cur))
                count -= o.cur
        if count:
            scout.count = count
            desc.add(scout.add(Gear('Boltgun')))
        return desc
