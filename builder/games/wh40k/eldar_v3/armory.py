__author__ = 'dvarh'


from builder.core2.options import Option, OptionsList, OneOf
from builder.games.wh40k.ynnari.armory import YnnariArtefacts,\
    YnnariBlade, YnnariPistol


class RelicWeapon(Option):
    def __init__(self, *args, **kwargs):
        psyker = kwargs.pop('psyker', False)
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.relic_options = [
            self.variant("Kurnous' Bow", 10),
            self.variant("Uldanorethi Long Rifle", 25),
            self.variant('Firesabre', 30),
            self.variant('Shard of Anaris', 40)
        ]

        self.gifts = [
            self.variant('The Celestial Lance', 35),
            self.variant('Soulshrive', 30)
        ]
        if psyker:
            self.gifts.append(self.variant('Spear of Teuthlas', 15))

    def get_unique(self):
        if isinstance(self, OneOf):
            if self.cur in (self.relic_options + self.gifts):
                return self.description
        if isinstance(self, OptionsList):
            if any(opt.value for opt in (self.relic_options + self.gifts)):
                desc = []
                for o in self.relic_options:
                    if o.value:
                        desc.append(o.gear)
                return desc
        return []

    @property
    def has_gifts(self):
        if isinstance(self, OneOf):
            return self.cur in self.gifts
        if isinstance(self, OptionsList):
            return any(opt.value for opt in self.gifts)

    @property
    def has_remnants(self):
        if isinstance(self, OneOf):
            return self.cur in self.relic_options
        if isinstance(self, OptionsList):
            return any(opt.value for opt in self.relic_options)

    def activate_gifts(self, val):
        for o in self.gifts:
            o.active = val

    def activate_remnants(self, val):
        for o in self.relic_options:
            o.active = val

    def show_gifts(self, val):
        for o in self.gifts:
            o.visible = val
            o.used = val and o.active


class Pistol(OneOf):
    def __init__(self, parent):
        super(Pistol, self).__init__(parent, 'Pistol')
        self.variant('Shuriken pistol')


class RelicBlade(YnnariBlade, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicBlade, self).__init__(*args, **kwargs)
        self.relic_options.append(self.hblade)


class RelicPistol(YnnariPistol, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicPistol, self).__init__(*args, **kwargs)
        self.relic_options.append(self.ysong)


class Relics(OptionsList):
    def __init__(self, parent, seer=True):
        super(Relics, self).__init__(parent, name='Remnants of Glory', limit=1)
        self.remnants = []
        if seer:
            self.remnants.append(self.variant("Spirit Stone of Anath'Lan", 15))

        self.remnants.extend([
            self.variant("The Phoenix Gem", 25),
            self.variant("Faolchu's Wing", 30)
        ])

        self.gifts = [
            self.variant('The Wraithforge Stone', 30),
            self.variant('Guardian Helm of Xellethon', 15)
        ]

    def get_unique(self):
        if self.any:
            return self.description
        return []

    def activate_gifts(self, val):
        for o in self.gifts:
            o.active = val
            o.used = val and o.visible

    def activate_remnants(self, val):
        for o in self.remnants:
            o.active = val
            o.used = val and o.visible

    def show_gifts(self, val):
        for o in self.gifts:
            o.visible = val
            o.used = val and o.active

    @property
    def has_gifts(self):
        return any(o.value for o in self.gifts if o.used)

    @property
    def has_remnants(self):
        return any(o.value for o in self.remnants)


class YnnariRelics(YnnariArtefacts, Relics):
    pass


class VehicleEquipment(OptionsList):
    def __init__(self, parent, warwalker=False, squadron=True):
        super(VehicleEquipment, self).__init__(parent, name='Eldar Vehicle Equipment')
        if not warwalker:
            self.variant('Holo-fields', 15)
        if not squadron:
            self.variant('Ghostwalk matrix', 10)
        self.variant('Spirit stones', 10)
        self.variant('Star engines', 15)
        self.variant('Vectored engines', 15)
        self.variant('Crystal targeting matrix', 25)


class Ghostwalk(OptionsList):
    def __init__(self, parent):
        super(Ghostwalk, self).__init__(parent=parent, name='Eldar Vehicle Equipment')
        self.ghostwalk = self.variant('Ghostwalk matrix', 10, gear=[], per_model=True)
