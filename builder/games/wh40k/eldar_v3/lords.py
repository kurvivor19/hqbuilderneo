__author__ = 'dvarh'

from builder.core2 import Gear, OneOf, Count
from builder.games.wh40k.roster import Unit


class Wraithknight(Unit):
    type_name = 'Wraithknight'
    type_id = 'wraithknight_v3'

    class ArmWeapon(OneOf):
        def __init__(self, parent):
            super(Wraithknight.ArmWeapon, self).__init__(parent=parent, name='Arm weapons')
            self.variant('Two heavy wraithcannon', 0)
            self.variant('Ghostglave and scattershield', 0)
            self.variant('Suncannon and scattershield', 0)

    def __init__(self, parent):
        super(Wraithknight, self).__init__(parent=parent, points=295)
        self.wep1 = self.ArmWeapon(self)
        self.scatter = Count(self, 'Scatter laser', 0, 2, points=15)
        self.shuriken = Count(self, 'Shuriken cannon', 0, 2, points=15)
        self.star = Count(self, 'Starcannon', 0, 2, points=15)

    def check_rules(self):
        super(Wraithknight, self).check_rules()
        Count.norm_counts(0, 2, [self.scatter, self.shuriken, self.star])


class Avatar(Unit):
    type_name = 'Avatar of Khaine'
    type_id = 'avatar_v3'

    def __init__(self, parent):
        super(Avatar, self).__init__(parent=parent, name='Avatar of Khaine',
                                      points=195, unique=True, gear=[
                                          Gear('Wailing Doom')
                                      ], static=True)
