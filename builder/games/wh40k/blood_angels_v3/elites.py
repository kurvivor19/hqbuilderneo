__author__ = 'Denis Romanov'

from builder.core2 import UnitList, ListSubUnit, OptionsList,\
    UnitDescription, Gear, OneOf, SubUnit
from .transport import Transport, TerminatorTransport, DreadnoughtTransport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from .armory import CommonRanged, Melee, BoltPistol, Chainsword, Special,\
    Armour, Boltgun, ThunderHammer, PowerFist, PowerWeapon, RelicBlade, Combi,\
    LightningClaw, BaseWeapon, Heavy
from itertools import chain
from .hq import TearerDreadnought
from builder.games.wh40k.roster import Unit


class CommandSquad(IATransportedUnit):
    type_id = 'command_squad_v3'
    type_name = 'Command Squad'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Command-Squad')

    model_points = 20

    class Veteran(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Options, self).__init__(parent=parent, name='Options')

                self.mbomb = self.variant('Melta bombs', 5)
                self.ss = self.variant('Storm Shield', 10)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Banners, self).__init__(parent=parent, name='Banner')

                self.comstd = self.variant('Company Standard', 15)

        class Weapon1(Special, CommonRanged, Melee, Boltgun, Chainsword):
            pass

        class Weapon2(Melee, Special, CommonRanged, Boltgun, BoltPistol):
            pass

        def __init__(self, parent):
            super(CommandSquad.Veteran, self).__init__(
                parent=parent, name='Veteran', points=CommandSquad.model_points, gear=Armour.power_armour_set)

            self.weapon1 = self.Weapon1(self, 'Weapon')
            self.weapon2 = self.Weapon2(self, '')
            self.opt = self.Options(self)
            self.banners = self.Banners(self)

        def check_rules(self):
            super(CommandSquad.Veteran, self).check_rules()
            self.gear = Armour.power_armour_set + self.root_unit.opt.description

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.SquadOptions, self).__init__(parent, 'Options')
            self.variant('Jump pack', 25)

    def __init__(self, parent):
        # point cost for company champion and novitiate
        super(CommandSquad, self).__init__(parent=parent, points=40)
        self.veterans = UnitList(self, self.Veteran, min_limit=3, max_limit=3)
        self.opt = self.SquadOptions(self)
        self.transport = Transport(self)

    def get_count(self):
        return 5

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count, self.veterans.description)
        sang = UnitDescription('Sanguinary Novitiate', 20)
        sang.add(Armour.power_armour_set + self.opt.description + [Gear('Bolt pistol'), Gear('Chainsword'), Gear('Narthecium')])
        champ = UnitDescription('Company Champion', 20)
        champ.add(Armour.power_armour_set + self.opt.description + [Gear('Bolt pistol'), Gear('Power sword'), Gear('Combat shield')])
        desc.add(sang).add(champ).add(self.transport.description)
        return desc


class DeathCompany(IATransportedUnit):
    type_name = 'Death Company'
    type_id = 'death_company_v1'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Death-Company-Squad')

    class JumpPack(OptionsList):
        def __init__(self, parent):
            super(DeathCompany.JumpPack, self).__init__(parent=parent, name='Options')
            self.variant('Jump packs', 3, per_model=True, gear=[])

        @property
        def points(self):
            return 0

    class Pistol(BoltPistol):
        def __init__(self, parent, name):
            super(DeathCompany.Pistol, self).__init__(parent=parent, name=name)
            self.variant('Hand flamer', 10)
            self.variant('Inferno pistol', 15)
            self.variant('Plasma pistol', 15)

    class Weapon1(ThunderHammer, PowerFist, PowerWeapon, Boltgun, Pistol):
        pass

    class Weapon2(ThunderHammer, PowerFist, PowerWeapon, Chainsword):
        pass

    class Marine(ListSubUnit):
        type_name = 'Death Company'

        def __init__(self, parent):
            super(DeathCompany.Marine, self).__init__(parent=parent, points=20,
                                                      gear=Armour.power_armour_set)
            DeathCompany.Weapon1(self, name='Weapon')
            DeathCompany.Weapon2(self, name='')

        def check_rules(self):
            self.gear = Armour.power_armour_set +\
                ([Gear('Jump pack')] if self.root_unit.opt.any else [])

        def build_points(self):
            return super(DeathCompany.Marine, self).build_points()\
                + (3 if self.root_unit.opt.any else 0)

    def get_transport(self):
        return Transport(self)

    def __init__(self, parent):
        super(DeathCompany, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 5, 15)
        self.opt = self.JumpPack(self)
        self.transport = self.get_transport()

    def check_rules(self):
        super(DeathCompany, self).check_rules()
        self.transport.visible = self.transport.used = not self.opt.any


class Lemartes(Unit):
    type_name = 'Lemartes, Guardian of the Lost'
    type_id = 'lemartes_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Lemartes')

    def __init__(self, parent):
        super(Lemartes, self).__init__(parent, 'Lemartes', 130, gear=[
            Gear('Power armour'), Gear('Bolt pistol'),
            Gear('The Blood Crozius'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear("Jump Pack"), Gear('Rosarius')],
            unique=True, static=True)


class SanguinaryGuards(Unit):
    type_name = 'Sanguinary Guard'
    type_id = 'sanguinaryguard_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Sanguinary-Guard')

    class SanguinaryGuard(ListSubUnit):
        def __init__(self, parent):
            super(SanguinaryGuards.SanguinaryGuard, self).__init__(
                parent=parent, name='Sanguinary Guard', points=33,
                gear=[Gear("Artificer Armour"), Gear('Frag and Krak Grenades'), Gear('Jump Pack')]
            )
            self.base_gear = self.gear
            self.Weapon1(self)
            self.Weapon2(self)
            self.banner = self.Standard(self)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(SanguinaryGuards.SanguinaryGuard.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.angelusboltgun = self.variant('Angelus Boltgun', 0)
                self.plasmapistol = self.variant('Plasma Pistol', 10)
                self.infernuspistol = self.variant('Infernus Pistol', 10)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(SanguinaryGuards.SanguinaryGuard.Weapon2, self).__init__(parent=parent, name='')
                self.sword = self.variant('Encarmine Sword', 0)
                self.axe = self.variant('Encarmine Axe', 0)
                self.powerfist = self.variant('Power Fist', 10)

        class Standard(OptionsList):
            def __init__(self, parent):
                super(SanguinaryGuards.SanguinaryGuard.Standard, self).__init__(parent=parent, name='Banner', limit=1)
                self.chapterbanner = self.variant('Chapter Banner', 25)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banner.any

        def check_rules(self):
            super(SanguinaryGuards.SanguinaryGuard, self).check_rules()
            self.gear = self.base_gear + ([Gear('Deathmask')] if self.root_unit.opt.any else [])

        def build_points(self):
            return super(SanguinaryGuards.SanguinaryGuard, self).build_points()\
                + (1 if self.root_unit.opt.any else 0)

    def __init__(self, parent):
        super(SanguinaryGuards, self).__init__(parent=parent, points=0, gear=[])
        self.guards = UnitList(self, self.SanguinaryGuard, 5, 10)
        self.opt = self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(SanguinaryGuards.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.deathmasks = self.variant('Death Masks', 1, per_model=True, gear=[])

        @property
        def points(self):
            return 0

    def get_count(self):
        return self.guards.count

    def check_rules(self):
        super(SanguinaryGuards, self).check_rules()
        flag = sum(c.count_banners() for c in self.guards.units)
        if flag > 1:
            self.error('Only one Sanguinary Guard can take a banner (taken: {})'.format(flag))

    # def get_unique_gear(self):
    #     return sum((unit.banner.description for unit in self.guards.units), [])


class Dreadnought(TearerDreadnought):
    type_name = 'Dreadnought'
    type_id = 'dread_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Dreadnought')

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(Dreadnought.RangedWeapon, self).__init__(parent=parent, name='Ranged weapon')
            self.variant('Multi-melta', 0)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 5)
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 5)
            self.twinlinkedheavyflamer = self.variant('Twin-linked heavy flamer', 5)
            self.plasmacannon = self.variant('Plasma cannon', 10)
            self.assaultcannon = self.variant('Assault cannon', 20)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent, name='')
            self.variant('Built-in Storm bolter', 0, gear=[Gear('Storm bolter')])
            self.variant('Built-in Heavy flamer', 10, gear=[Gear('Heavy flamer')])

        def check_rules(self):
            super(Dreadnought.BuildIn, self).check_rules()
            self.visible = self.used = not self.parent.ccw.is_ranged()

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(Dreadnought.MeleeWeapon, self).__init__(parent=parent, name='Melee weapon')
            self.fist = self.variant('Power fist', 0)
            self.variant('Missile launcher', 10)
            self.variant('Twin-linked autocannon', 15)

        def is_ranged(self):
            return self.cur != self.fist

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options')
            self.extraarmour = self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent=parent, points=100, walker=True, gear=[Gear('Smoke launchers'), Gear('Searchlight')])
        self.ccw = self.MeleeWeapon(self)
        self.build_in = self.BuildIn(self)
        self.rng = self.RangedWeapon(self)
        self.opt = self.Options(self)
        self.transport = DreadnoughtTransport(self)


class DeathCompanyDreadnought(TearerDreadnought):
    type_name = 'Death Company Dreadnought'
    type_id = 'deathcompanydreadnought_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Death-Company-Dreadnought')

    def __init__(self, parent):
        super(DeathCompanyDreadnought, self).__init__(
            parent=parent, points=125,
            gear=[Gear('Searchlight')],
            walker=True)
        self.Ccw(self)
        self.BuildIn1(self)
        self.BuildIn2(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class BuildIn1(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.BuildIn1, self).__init__(parent=parent, name='')

            self.builtinstormbolter = self.variant('Built-in Storm bolter', 0,
                                                   gear=[Gear('Storm bolter')])
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 10,
                                                   gear=[Gear('Heavy flamer')])

    class BuildIn2(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.BuildIn2, self).__init__(parent=parent, name='')

            self.builtinstormbolter = self.variant('Built-in meltagun', 0,
                                                   gear=[Gear('Meltagun')])
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 0,
                                                   gear=[Gear('Heavy flamer')])

    class Ccw(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.Ccw, self).__init__(parent=parent, name='Weapon')
            self.variant('Power fists', 0, gear=[Gear('Power fist', count=2)])
            self.variant('Blood talons', 10, gear=[Gear('Blood talon', count=2)])

    class Options(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.Options, self).__init__(parent=parent, name='Options')
            self.smoke = self.variant('Smoke launchers', 0)
            self.magnagrapple = self.variant('Magna-grapple', 0)


class FuriosoDreadnought(TearerDreadnought):
    type_name = 'Furioso Dreadnought'
    type_id = 'furioso_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Furioso-Dreadnought')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(FuriosoDreadnought.Weapon, self).__init__(parent=parent, name='')
            self.fist = self.variant('Power fist', 0, gear=[Gear('Power fist')])
            self.cannon = self.variant('Frag cannon', 5)

        def check_rules(self):
            super(FuriosoDreadnought.Weapon, self).check_rules()
            self.visible = self.used = not self.parent.claws.any

        @property
        def is_dccw(self):
            return self.cur == self.fist

    class BuildIn1(OneOf):
        def __init__(self, parent, mount):
            super(FuriosoDreadnought.BuildIn1, self).__init__(parent, name='')
            self.variant('Built-in Storm bolter', 0, gear=[Gear('Storm bolter')])
            self.variant('Built-in Heavy flamer', 10, gear=[Gear('Heavy flamer')])
            self.mount = mount

        def check_rules(self):
            super(FuriosoDreadnought.BuildIn1, self).check_rules()
            self.visible = self.used = self.parent.claws.any or self.mount.is_dccw

    class BuildIn2(OneOf):
        def __init__(self, parent, mount):
            super(FuriosoDreadnought.BuildIn2, self).__init__(parent, name='')
            self.variant('Built-in meltagun', 0, gear=[Gear('Meltagun')])
            self.variant('Built-in Heavy flamer', 0, gear=[Gear('Heavy flamer')])
            self.mount = mount

        def check_rules(self):
            super(FuriosoDreadnought.BuildIn2, self).check_rules()
            self.visible = self.used = self.parent.claws.any or self.mount.is_dccw

    class Claws(OptionsList):
        def __init__(self, parent):
            super(FuriosoDreadnought.Claws, self).__init__(parent=parent, name='Weapon')
            self.variant('Blood talons', 0, gear=[Gear('Blood talon', count=2), Gear('Meltagun')])

    def __init__(self, parent):
        super(FuriosoDreadnought, self).__init__(parent=parent, points=125, walker=True, gear=[Gear('Searchlight')])
        self.claws = self.Claws(self)
        self.wep1 = self.Weapon(self)
        self.build_in1 = self.BuildIn1(self, self.wep1)
        self.wep2 = self.Weapon(self)
        self.build_in2 = self.BuildIn2(self, self.wep2)
        self.opt = DeathCompanyDreadnought.Options(self)
        self.transport = DreadnoughtTransport(self)


class TerminatorSquad(IATransportedUnit):
    type_name = 'Terminator Squad'
    type_id = 'terminator_squad_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Terminator-Squad')

    model_points = 40

    class Terminator(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Terminator.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 0)
                self.chain_fist = self.variant('Chainfist', 5)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Terminator.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 0)
                self.hf = self.variant('Heavy flamer', 5)
                self.ac = self.variant('Assault Cannon', 20)

            def enable_spec(self, value):
                self.hf.active = self.ac.active = value

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(TerminatorSquad.Terminator.Weapon, self).__init__(parent=parent, name='')
                self.cyc = self.variant('Cyclone missile launcher', 25)

        def __init__(self, parent):
            super(TerminatorSquad.Terminator, self).__init__(
                parent=parent, name='Terminator', points=TerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)

        def check_rules(self):
            super(TerminatorSquad.Terminator, self).check_rules()
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

    class Sergeant(Unit):
        class Sword(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Sergeant.Sword, self).__init__(parent, ' Melee Weapon')
                self.variant('Power sword', 0)
                self.edge = self.variant('Archangel\'s edge', 5)

        def __init__(self, parent):
            super(TerminatorSquad.Sergeant, self).__init__(parent, 'Terminator Sergeant',
                                                           200 - 4 * TerminatorSquad.model_points,
                                                           gear=[Gear('Terminator armour'), Gear('Storm bolter')])
            self.wep = self.Sword(self)

        def get_unique_gear(self):
            return self.wep.description if self.wep.cur == self.wep.edge else []

    def __init__(self, parent):
        super(TerminatorSquad, self).__init__(parent=parent, points=self.model_points)
        aflag = self.roster.supplement == 'archangel'
        if aflag:
            self.leader = SubUnit(self, self.Sergeant(parent=None))
        else:
            self.gear = [UnitDescription(
                'Terminator Sergeant', 200 - 4 * self.model_points,
                options=[Gear('Terminator armour'), Gear('Storm bolter'), Gear('Power sword')]
            )]
        self.terms = UnitList(self, self.Terminator, min_limit=4, max_limit=9)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def get_unique_gear(self):
        if self.roster.supplement == 'archangel':
            return self.leader.unit.get_unique_gear()
        else:
            return []


class TerminatorAssaultSquad(IATransportedUnit):
    type_name = 'Terminator Assault Squad'
    type_id = 'terminatorassaultsquad_v1'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Terminator-Assault-Squad')

    model_points = 40

    class Sergeant(Unit):

        def __init__(self, parent):
            super(TerminatorAssaultSquad.Sergeant, self).__init__(
                name='Terminator Sergeant', parent=parent, points=200 - 4 * TerminatorAssaultSquad.model_points,
                gear=[Gear('Terminator armour')]
            )
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(TerminatorAssaultSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon')
                self.lightningclaws = self.variant('Lightning claws', 0, gear=[Gear('Lightning claw', count=2)])
                self.thunderhammerandstormshield = self.variant(
                    'Thunder hammer and storm shield', 5, gear=[Gear('Thunder hammer'), Gear('Storm shield')])

    class StandardBearer(ListSubUnit):
        class Standard(OptionsList):
            def __init__(self, parent):
                super(TerminatorAssaultSquad.StandardBearer.Standard, self).__init__(parent, 'Standard', limit=1)
                self.variant('Company standard', 5)
                if self.roster.supplement == 'archangel':
                    self.archangel = self.variant('Banner of the Archangel Host', 30)

        def __init__(self, parent):
            super(TerminatorAssaultSquad.StandardBearer, self).__init__(
                parent, 'Terminator', TerminatorAssaultSquad.model_points,
                gear=[Gear('Terminator armour')])
            self.weapon = TerminatorAssaultSquad.Sergeant.Weapon(self)
            self.standard = self.Standard(self)

        def check_rules(self):
            self.weapon.lightningclaws.gear = [Gear('Lightning Claw',
                                                    count=1 if self.standard.any else 2)]
            self.weapon.thunderhammerandstormshield.gear = [Gear('Thunder hammer')] + \
                                                           ([] if self.standard.any else [Gear('Storm shield')])

        @ListSubUnit.count_gear
        def has_banner(self):
            return self.standard.any

    def __init__(self, parent):
        super(TerminatorAssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.terminators = UnitList(self, self.StandardBearer, 4, 9)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terminators.count + 1

    def check_rules(self):
        super(TerminatorAssaultSquad, self).check_rules()
        spec = sum(c.has_banner() for c in self.terminators.units)
        if spec > 1:
            self.error('Only one standard or banner may be carried in squad')

    def get_unique_gear(self):
        if self.roster.supplement == 'archangel':
            return chain(u.standard.description[0] for u in self.terminators.units if u.standard.archangel.value)
        else:
            return []


class SquadAlphaeus(Unit):
    type_name = 'Squad Alphaeus'
    type_id = 'alphaeus_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Terminator-Squad')

    def __init__(self, parent):
        super(SquadAlphaeus, self).__init__(parent, points=215, static=True,
                                            unique=True, gear=[
                                                UnitDescription(name='Alphaeus', options=[Gear('Terminator armour'), Gear('Storm bolter'), Gear('Power sword')]),
                                                UnitDescription(name='Terminator', options=[Gear('Terminator armour'), Gear('Heavy flamer'), Gear('Power fist')]),
                                                UnitDescription(name='Terminator', options=[Gear('Terminator armour'), Gear('Storm bolter'), Gear('Chainfist')]),
                                                UnitDescription(name='Terminator', options=[Gear('Terminator armour'), Gear('Storm bolter'), Gear('Power fist')], count=2)])

    def build_statistics(self):
        return {'Models': 5, 'Units': 1}


class VanguardVeteranSquad(IATransportedUnit):
    type_name = 'Vanguard Veteran Squad'
    type_id = 'vanguard_veteran_squad_v3'
    free_power = False

    model_points = 19

    class JumpPack(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.JumpPack, self).__init__(parent=parent, name='Options')
            self.variant('Jump packs', 3, per_model=True, gear=[])

        @property
        def points(self):
            return 0

    class Pistols(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(VanguardVeteranSquad.Pistols, self).__init__(*args, **kwargs)
            self.variant('Hand flamer', 10)
            self.variant('Grav-pistol', 15)
            self.variant('Inferno pistol', 15)
            self.variant('Plasma pistol', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Options, self).__init__(
                parent, 'Options')
            self.variant('Melta bombs', 5)
            self.variant('Storm shield', 10)

    class Weapon1(Melee, Pistols, BoltPistol):
        pass

    class Weapon2(Pistols, Melee):
        pass

    class SergeantWeapon1(Melee, RelicBlade, Pistols, BoltPistol):
        pass

    class SergeantWeapon2(Pistols, RelicBlade, Melee):
        pass

    class Marine(ListSubUnit):

        def __init__(self, parent):
            super(VanguardVeteranSquad.Marine, self).__init__(
                parent=parent, name='Veteran', points=VanguardVeteranSquad.model_points, gear=Armour.power_armour_set)
            self.wep1 = VanguardVeteranSquad.Weapon1(self, 'Weapon')
            self.wep2 = VanguardVeteranSquad.Weapon2(self, '')
            VanguardVeteranSquad.Options(self)

        def check_rules(self):
            self.gear = Armour.power_armour_set\
                        + ([Gear('Jump pack')] if self.root_unit.opt.any else [])

        def build_points(self):
            val = super(VanguardVeteranSquad.Marine, self).build_points()\
                + (3 if self.root_unit.opt.any else 0)
            if self.root_unit.free_power:
                if self.wep1.cur in [self.wep1.pwr, self.wep1.claw]:
                    val = val - self.wep1.points
                else:
                    if self.wep2.cur in [self.wep2.pwr, self.wep2.claw]:
                        val = val - self.wep2.points
            return val

    class Sergeant(Unit):

        def __init__(self, parent):
            super(VanguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, name='Veteran Sergeant', points=VanguardVeteranSquad.model_points, gear=Armour.power_armour_set)
            self.wep1 = VanguardVeteranSquad.SergeantWeapon1(self, 'Weapon')
            self.wep2 = VanguardVeteranSquad.SergeantWeapon2(self, '')
            VanguardVeteranSquad.Options(self)

        def check_rules(self):
            self.gear = Armour.power_armour_set\
                        + ([Gear('Jump pack')] if self.parent.parent.opt.any else [])

        def build_points(self):
            val = super(VanguardVeteranSquad.Sergeant, self).build_points()\
                + (3 if self.parent.parent.opt.any else 0)
            # remember: we are wrapped in SubUnit
            # also: we must access instance field, not class one
            if self.parent.parent.free_power:
                if self.wep1.cur in [self.wep1.pwr, self.wep1.claw]:
                    val = val - self.wep1.points
                else:
                    if self.wep2.cur in [self.wep2.pwr, self.wep2.claw]:
                        val = val - self.wep2.points
            return val

    def __init__(self, parent):
        super(VanguardVeteranSquad, self).__init__(parent, self.type_name)
        self.sgt = SubUnit(self, self.Sergeant(parent=None))
        self.marines = UnitList(self, self.Marine, 4, 9)
        self.opt = self.JumpPack(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.marines.count + 1

    def check_rules(self):
        super(VanguardVeteranSquad, self).check_rules()
        self.transport.visible = self.transport.used = not self.opt.any


class SternguardVeteranSquad(IATransportedUnit):
    type_name = 'Sternguard Veteran Squad'
    type_id = 'sternguard_veteran_squad_v3'
    free_ranged = False

    model_points = 22

    model_gear = Armour.power_armour_set + [Gear('Special issue ammunition')]

    class Options(OptionsList):
        def __init__(self, parent):
            super(SternguardVeteranSquad.Options, self).__init__(parent=parent, name='Options')
            self.meltabombs = self.variant('Melta bombs', 5)

    class Pistol(BaseWeapon):
        def __init__(self, parent, name):
            super(SternguardVeteranSquad.Pistol, self).__init__(parent=parent, name=name)
            self.gravpistol = self.variant('Grav pistol', 15)
            self.plaspistol = self.variant('Plasma pistol', 15)

    class SergeantWeapon1(PowerFist, PowerWeapon, LightningClaw, Pistol, Chainsword, BoltPistol):
        pass

    class SergeantWeapon2(PowerFist, PowerWeapon, LightningClaw, Pistol, Chainsword, Combi, Boltgun):
        pass

    class VeteranWeapon(Special, Heavy, Combi, Boltgun):
        pass

    class Veteran(ListSubUnit):
        type_name = 'Veteran'

        def __init__(self, parent):
            super(SternguardVeteranSquad.Veteran, self).__init__(
                parent=parent, points=SternguardVeteranSquad.model_points,
                gear=SternguardVeteranSquad.model_gear + [Gear('Bolt pistol')]
            )
            self.wep = SternguardVeteranSquad.VeteranWeapon(self, name='Weapon')

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep.is_heavy() or self.wep.is_spec()

        def build_points(self):
            val = super(SternguardVeteranSquad.Veteran, self).build_points()
            if self.root_unit.free_ranged:
                if self.wep.cur in [self.wep.stormbolter] + self.wep.comby_weapons:
                    val = val - self.wep.points
            return val

    class Sergeant(Unit):
        type_name = 'Veteran Sergeant'

        def __init__(self, parent):
            super(SternguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=110 - SternguardVeteranSquad.model_points * 4,
                gear=SternguardVeteranSquad.model_gear
            )
            SternguardVeteranSquad.SergeantWeapon1(self, name='Weapon')
            self.bwep = SternguardVeteranSquad.SergeantWeapon2(self, name='')
            SternguardVeteranSquad.Options(self)

        def build_points(self):
            val = super(SternguardVeteranSquad.Sergeant, self).build_points()
            # remember, we are wrapped in a SubUnit
            if self.parent.parent.free_ranged:
                if self.bwep.cur in [self.bwep.stormbolter] + self.bwep.comby_weapons:
                    val = val - self.bwep.points
            return val

    def __init__(self, parent):
        super(SternguardVeteranSquad, self).__init__(parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.marines = UnitList(self, self.Veteran, 4, 9)
        self.transport = Transport(self)

    def get_count(self):
        return self.marines.count + 1

    def check_rules(self):
        super(SternguardVeteranSquad, self).check_rules()
        spec = sum(c.count_spec() for c in self.marines.units)
        if spec > 2:
            self.error('Veterans may take only {0} special weapon (taken {1})'.format(2, spec))
