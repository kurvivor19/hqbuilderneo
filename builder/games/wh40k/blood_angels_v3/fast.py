__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionsList, OneOf, SubUnit,\
    ListSubUnit, UnitList, Count, UnitDescription
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle,\
    SpaceMarinesBaseSquadron
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from .transport import Transport
from .armory import Chainsword, Melee, CommonRanged, BoltPistol, Special
from builder.games.wh40k.roster import Unit


class LandSpeeder(SpaceMarinesBaseVehicle):
    class BaseWeapon(OneOf):
        def __init__(self, parent):
            super(LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy Bolter', 0)
            self.heavyflamer = self.variant('Heavy Flamer', 0)
            self.multimelta = self.variant('Multi-melta', 10)

    class UpWeapon(OptionsList):
        def __init__(self, parent):
            super(LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade', limit=1)
            self.heavybolter = self.variant('Heavy Bolter', 10)
            self.heavyflamer = self.variant('Heavy Flamer', 10)
            self.multimelta = self.variant('Multi-melta', 20)
            self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 25)
            self.assaultcannon = self.variant('Assault Cannon', 30)

    def __init__(self, parent):
        super(LandSpeeder, self).__init__(parent=parent, points=50, name="Land Speeder")
        self.wep = self.BaseWeapon(self)
        self.up = self.UpWeapon(self)


class LandSpeederSquadron(SpaceMarinesBaseSquadron):
    type_name = "Land Speeder Squadron"
    type_id = "land_speeder_squadron_v3"
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Land-Speeder-Squadron')

    unit_class = LandSpeeder


class OldAssaultSquad(IATransportedUnit):
    type_name = 'Assault squad'
    type_id = 'assaultsquad_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Assault-Squad')

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 17
    model = UnitDescription('Space Marine', points=model_points, options=model_gear + [Gear('Chainsword')])

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [OldAssaultSquad.model.clone().
                    add([Gear('Bolt pistol')] + ([] if self.parent.transport.any else [Gear('Jump pack')])).set_count(self.cur - self.normalizer())]

    class MarineWeapon(BoltPistol):
        def __init__(self, parent):
            super(OldAssaultSquad.MarineWeapon, self).__init__(parent, 'Assault weapon')
            self.variant(name='Flamer', points=5),
            self.variant(name='Hand flamer', points=10),
            self.variant(name='Meltagun', points=10),
            self.variant(name='Plasma pistol', points=15),
            self.variant(name='Plasma gun', points=15),
            self.variant(name='Infernus pistol', points=15),

    class SpaceMarineSergeant(Unit):
        type_name = 'Space Marine Sergeant'
        type_id = 'spacemarinesergeant_v3'

        class Weapon1(CommonRanged, Melee):
            pass

        class Weapon2(Melee, CommonRanged, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(OldAssaultSquad.SpaceMarineSergeant.Options, self).__init__(parent=parent, name='Options')

                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshield = self.variant('Combat shield', 5)
                self.up = self.variant('Upgrade to Veteran Sergeant', 10, gear=[])

        def __init__(self, parent):
            super(OldAssaultSquad.SpaceMarineSergeant, self).__init__(
                parent=parent, name=self.type_name, points=OldAssaultSquad.model_points,
                gear=OldAssaultSquad.model_gear)
            self.Weapon1(self, 'Weapon')
            self.Weapon2(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            super(OldAssaultSquad.SpaceMarineSergeant, self).check_rules()
            self.name = 'Veteran Sergeant' if self.opt.up.value else self.type_name
            self.gear = OldAssaultSquad.model_gear\
                        + ([] if self.parent.parent.transport.any else [Gear('Jump pack')])

    def __init__(self, parent):
        super(OldAssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.SpaceMarineSergeant(None))
        self.spacemarine = self.MarineCount(self, 'Space Marine', min_limit=4, max_limit=9, points=self.model_points)
        wep1 = self.MarineWeapon(self)
        wep2 = self.MarineWeapon(self)
        self.spec = [wep1, wep2]
        self.spacemarine.normalizer = lambda: sum(c.cur != c.boltpistol for c in self.spec)
        self.transport = Transport(self, off=True)

    def get_count(self):
        return self.spacemarine.cur + 1

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count, self.sergeant.description + self.spacemarine.description)
        for wep in self.spec:
            if wep.cur != wep.boltpistol:
                desc.add_dup(self.model.clone().add(wep.description).add_points(wep.points))
        desc.add(self.transport.description)
        return desc


class AssaultSquad(IATransportedUnit):
    type_name = 'Assault squad'
    type_id = 'assaultsquad_v3_5'

    model_points = 14
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):

        class Weapon1(Melee, CommonRanged, BoltPistol):
            pass

        class Weapon2(CommonRanged, Melee, Chainsword):
            def __init__(self, *args, **kwargs):
                super(AssaultSquad.Sergeant.Weapon2, self).__init__(*args, **kwargs)
                self.evs = self.variant('Eviscerator', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshiled = self.variant('Combat shield', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(AssaultSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep2 = self.Weapon2(self, 'Weapon')
            self.wep1 = self.Weapon1(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            super(AssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

        def build_description(self):
            desc = super(AssaultSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            if self.parent.parent.pack.used and self.parent.parent.pack.any:
                desc.add(Gear('Jump pack'))
                desc.add_points(3)
            return desc

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(AssaultSquad.Jumppack, self).__init__(parent=parent, name='')
            self.jumppack = self.variant('Jump Packs', 3, gear=[], per_model=True)
            self.jumppack.value = True

    def build_points(self):
        return super(AssaultSquad, self).build_points() +\
            self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(AssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.spec = [Count(self, 'Flamer', 0, 2, 5),
                     Count(self, 'Meltagun', 0, 2, 10),
                     Count(self, 'Hand flamer', 0, 2, 10),
                     Count(self, 'Plasmagun', 0, 2, 15),
                     Count(self, 'Inferno pistol', 0, 2, 15),
                     Count(self, 'Plasma pistol', 0, 2, 15)]
        self.evs = Count(self, 'Eviscerator', 0, 2, 25)
        self.pack = self.Jumppack(parent=self)
        self.transport = Transport(parent=self)

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, self.spec)
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max
        self.pack.used = self.pack.visible = not self.transport.any

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear,
            points=AssaultSquad.model_points
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     add_points(25).set_count(self.evs.cur))
        marine.add(Gear('Chainsword'))
        count = self.marines.cur - self.evs.cur
        for o in self.spec:
            if o.cur > 0:
                desc.add_dup(marine.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur

        desc.add(marine.add(Gear('Bolt pistol')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class SpaceMarineBikers(Unit):
    type_name = 'Space Marine Bike Squad'
    type_id = 'spacemarinebikesquad_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Bike-Squad')

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Space Marine bike'),
                  Gear('Twin-linked boltgun')]
    model_points = 21

    class BikerSergeant(Unit):
        type_name = 'Biker Sergeant'
        type_id = 'bikersergeant_v1'

        class Weapon(Melee, CommonRanged, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(SpaceMarineBikers.BikerSergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.up = self.variant('Upgrade to Biker Veteran Sergeant', 10)

        def __init__(self, parent):
            super(SpaceMarineBikers.BikerSergeant, self).__init__(
                parent=parent, name='Biker Sergeant', points=SpaceMarineBikers.model_points,
                gear=SpaceMarineBikers.model_gear
            )
            self.Weapon(self, 'Weapon')
            self.opt = self.Options(self)

        def check_rules(self):
            self.name = 'Biker Veteran Sergeant' if self.opt.up.value else self.type_name

    class AttackBike(OptionsList):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(SpaceMarineBikers.AttackBike.Weapon, self).__init__(parent=parent, name='', used=False)
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        def __init__(self, parent):
            super(SpaceMarineBikers.AttackBike, self).__init__(parent=parent, name='Options')
            self.bike = self.variant('Attack bike', 40)
            self.weapon = self.Weapon(parent)

        def check_rules(self):
            self.weapon.visible = self.bike.value

        @property
        def description(self):
            if self.bike.value:
                return UnitDescription(
                    'Attack Bike', points=40,
                    options=SpaceMarineBikers.model_gear + [Gear('Bolt pistol')])\
                    .add(self.weapon.description).add_points(self.weapon.points)
            return []

        @property
        def points(self):
            return super(SpaceMarineBikers.AttackBike, self).points + self.weapon.points

    class Biker(ListSubUnit):
        class BikerWeapon(Special, Chainsword, BoltPistol):
            pass

        def __init__(self, parent):
            super(SpaceMarineBikers.Biker, self).__init__(
                parent, 'Space Marine Biker', SpaceMarineBikers.model_points,
                SpaceMarineBikers.model_gear)
            self.wep = self.BikerWeapon(self, 'Weapon')

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep.is_spec()

    def __init__(self, parent):
        super(SpaceMarineBikers, self).__init__(parent=parent)

        self.sergeant = SubUnit(self, self.BikerSergeant(None))
        self.bikers = UnitList(self, self.Biker, 2, 7)
        self.attack = self.AttackBike(self)

    def check_rules(self):
        super(SpaceMarineBikers, self).check_rules()
        spec = sum(c.count_spec() for c in self.bikers.units)
        if spec > 2:
            self.error('Only 2 Bikers can take a special weapon (taken: {})'.format(spec))

    def get_count(self):
        return self.bikers.count + 1 + self.attack.bike.value


class AttackBikeSquad(Unit):
    type_name = 'Attack Bike Squad'
    type_id = 'attackbike_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Attack-Bike-Squad')

    model_points = 45
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Bolt pistol'),
                  Gear('Space Marine bike'), Gear('Twin-linked boltgun')]
    model_name = 'Attack Bike'
    model = UnitDescription(name=model_name, options=model_gear, points=model_points)

    class Count(Count):
        @property
        def description(self):
            return AttackBikeSquad.model.clone().add(Gear('Heavy bolter')).set_count(self.cur - self.parent.melta.cur)

    def __init__(self, parent):
        super(AttackBikeSquad, self).__init__(parent)
        self.bikers = self.Count(self, self.model_name, 1, 3, self.model_points, per_model=True)
        self.melta = Count(
            self, 'Multi-melta', 0, 1, 10, per_model=True,
            gear=self.model.clone().add(Gear('Multi-melta')).add_points(10)
        )

    def check_rules(self):
        super(AttackBikeSquad, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur


class ScoutBikers(Unit):
    type_name = 'Scout Bike Squad'
    type_id = 'scoutbikesquad_v1'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Scout-Bike-Squad')

    model_gear = [Gear('Scout armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Space Marine bike'),
                  Gear('Shotgun')]
    model_points = 18
    model = UnitDescription('Scout Biker', points=model_points, options=model_gear)

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [ScoutBikers.model.clone().add([Gear('Bolt pistol'), Gear('Twin-linked boltgun')]).
                    set_count(self.cur - self.normalizer())]

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutBikers.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.clustermines = self.variant('Cluster mines', 20)

    class BikerSergeant(Unit):
        class Weapon(Melee, CommonRanged, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutBikers.BikerSergeant.Options, self).__init__(parent=parent, name='Options')

                self.meltabombs = self.variant('Melta bombs', 5)
                self.locatorbeacon = self.variant('Locator beacon', 10)
                self.up = self.variant('Upgrade to Scout Biker Veteran Sergeant', 10, gear=[])

        def __init__(self, parent):
            super(ScoutBikers.BikerSergeant, self).__init__(name='Scout Biker Sergeant', parent=parent,
                                                            points=ScoutBikers.model_points,
                                                            gear=ScoutBikers.model_gear + [Gear('Twin-linked boltgun')])
            self.Weapon(self, 'Weapon', scout=True)
            self.opt = self.Options(self)

        def check_rules(self):
            super(ScoutBikers.BikerSergeant, self).check_rules()
            self.name = 'Scout Biker Veteran Sergeant' if self.opt.up.value else 'Scout Biker Sergeant'

    def __init__(self, parent):
        super(ScoutBikers, self).__init__(parent=parent)

        self.sergeant = SubUnit(self, self.BikerSergeant(None))
        self.spacemarine = self.MarineCount(self, 'Scout Biker', min_limit=2, max_limit=9,
                                            points=self.model_points)
        grenade_name = 'Astartes grenade launcher'
        grenade_points = 5
        self.grenade = Count(
            self, name=grenade_name, min_limit=0, max_limit=3, points=grenade_points,
            gear=ScoutBikers.model.clone().add(Gear('Bolt pistol')).
            add(Gear(grenade_name)).add_points(grenade_points)
        )

        self.spacemarine.normalizer = lambda: self.grenade.cur
        self.mines = self.Options(self)

    def check_rules(self):
        super(ScoutBikers, self).check_rules()
        self.grenade.max = min(3, self.spacemarine.cur)

    def get_count(self):
        return self.spacemarine.cur + 1


class BaalPredator(SpaceMarinesBaseVehicle):
    type_name = 'Baal Predator'
    type_id = 'baalpredator_v1'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Baal-Predator')

    def __init__(self, parent):
        super(BaalPredator, self).__init__(parent=parent, points=115, gear=[Gear('Smoke launchers')], tank=True)
        self.Turret(self)
        self.Sponsons(self)
        self.Options(self)

    class Turret(OneOf):
        def __init__(self, parent):
            super(BaalPredator.Turret, self).__init__(parent=parent, name='Weapon')
            self.twinlinkedassaultcannon = self.variant('Twin-linked assault cannon', 0)
            self.flamestormlascannon = self.variant('Flamestorm lascannon', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Sponsons, self).__init__(parent=parent, name='Side sponsons', limit=1)
            self.heavyflamers = self.variant('Heavy flamers', 25, gear=[Gear('Heavy flamer', count=2)])
            self.heavybolters = self.variant('Heavy bolters', 30, gear=[Gear('Heavy bolter', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Options, self).__init__(parent=parent, name='Options')
            self.stormbolter = self.variant('Storm bolter', 10)
            self.hunterkillermissile = self.variant('Hunter-killer missile', 10)
            self.dozerblade = self.variant('Dozer blade', 5)
            self.extraarmour = self.variant('Extra armour', 15)
            self.searchlight = self.variant('Searchlight', 1)
