from builder.games.wh40k.genestealers import GoliathTruck

__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription, OneOf, OptionsList, Count, ListSubUnit, UnitList, OptionalSubUnit, \
    SubUnit
from builder.games.wh40k.roster import Unit


class Purestrain(Unit):
    type_name = 'The Purestrain Princelings'
    type_id = 'gs_purestrain_v1'

    def __init__(self, parent):
        super(Purestrain, self).__init__(parent, points=30, gear=[
            UnitDescription('Purestrain Genestealers', options=[
                Gear('Rending claws')], count=2)
        ], static=True)

    def get_count(self):
        return 2


class BrothersAberrants(Unit):
    type_name = 'The Brothers Aberrant'
    type_id = 'gs_aberrants_v1'

    def __init__(self, parent):
        super(BrothersAberrants, self).__init__(parent, points=120, gear=[
            UnitDescription('Aberrant', options=[
                Gear('Rending claws'), Gear('Power hammer')], count=2),
            UnitDescription('Aberrant', options=[
                Gear('Rending claws'), Gear('Power pick')], count=2),
        ], static=True)

    def get_count(self):
        return 4


class PurestrainGenestealers(Unit):
    type_name = 'Purestrain Genestealers'
    type_id = 'gs_purestrain_genestealers_v3'
    model_points = 14
    model_gear = [Gear('Rending claws')]

    def __init__(self, parent):
        super(PurestrainGenestealers, self).__init__(parent, self.type_name)
        self.gens = Count(self, 'Purestrain Genestealer', min_limit=5, max_limit=20,
                             points=PurestrainGenestealers.model_points)
        self.talons = Count(parent=self, name='Scything talons', min_limit=0, max_limit=5, points=3, per_model=True)

    def check_rules(self):
        super(PurestrainGenestealers, self).check_rules()
        max_wep = self.gens.cur
        Count.norm_counts(0, max_wep, [self.talons, ])

    def get_count(self):
        return self.gens.cur


class Aberrants(Unit):
    type_name = 'Aberrants'
    type_id = 'gs_aberrants_v1'

    class Aberrant(ListSubUnit):
        type_name = 'Aberrant'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Aberrants.Aberrant.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Power pick')
                self.variant('Power hammer')

        def __init__(self, parent):
            super(Aberrants.Aberrant, self).__init__(parent, points=30,
                                              gear=[Gear('Rending claws')])
            self.wep = self.Weapon(self)

    def __init__(self, parent):
        super(Aberrants, self).__init__(parent)
        self.models = UnitList(self, self.Aberrant, 4, 8)

    def get_count(self):
        return self.models.count


class HybridMetamorphs(Unit):
    type_name = 'Hybrid Metamorphs'
    type_id = 'gs_hybrid_metamorphs_v1'

    model_points = 9

    class Metamorph(ListSubUnit):

        class Claws(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Claws, self).__init__(parent, 'Claws')
                self.variant('Rending claws')
                self.talon = self.variant('Metamorph talon')

            def is_talon(self):
                return self.cur == self.talon

        class Talons(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Talons, self).__init__(parent, 'Talons')
                self.talons = self.variant('Metamorph talon')
                self.variant('Metamorph claw', 2)
                self.variant('Metamorph whip', 2)

        class Pistols(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Pistols, self).__init__(parent, 'Pistols')
                self.variant('Autopistol', 0)
                self.whip = self.variant('Hand flamer', 5)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Banners, self).__init__(parent, "Icon", limit=1)
                self.variant('Cult Icon', 10)

        def __init__(self, parent):
            super(HybridMetamorphs.Metamorph, self).__init__(
                parent=parent, points=HybridMetamorphs.model_points, name='Metamorph',
                gear=[Gear('Blasting charges'), ]
            )
            self.claws = self.Claws(self)
            self.talons = self.Talons(self)
            self.pistols = self.Pistols(self)
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

        def check_rules(self):
            super(HybridMetamorphs.Metamorph, self).check_rules()
            if self.claws.is_talon():
                self.talons.active = False
                self.talons.cur = self.talons.talons

    class MetamorphLeader(Unit):

        class Bonesword(OptionsList):
            def __init__(self, parent):
                super(HybridMetamorphs.MetamorphLeader.Bonesword, self).__init__(parent, '')
                self.variant('Bonesword', 20)

        class Pistols(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.MetamorphLeader.Pistols, self).__init__(parent, 'Pistols')
                self.variant('Autopistol', 0)
                self.whip = self.variant('Hand flamer', 5)

        def __init__(self, parent):
            super(HybridMetamorphs.MetamorphLeader, self).__init__(parent, name='Hybrid Leader', points=9 + 10,
                                                                   gear=[Gear('Blasting charges'), ])
            self.ccw = self.Bonesword(self)
            self.pistols = self.Pistols(self)
            self.claws = HybridMetamorphs.Metamorph.Claws(self)
            self.talons = HybridMetamorphs.Metamorph.Talons(self)
            self.ccw.order = 10
            self.pistols.order = 11

        def check_rules(self):
            super(HybridMetamorphs.MetamorphLeader, self).check_rules()
            if self.claws.is_talon():
                self.talons.active = False
                self.talons.cur = self.talons.talons

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(HybridMetamorphs.Leader, self).__init__(parent=parent, name='Hybrid Leader', limit=1)
            SubUnit(self, HybridMetamorphs.MetamorphLeader(parent=None))

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(HybridMetamorphs.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, GoliathTruck(parent=None)),
            ]

    def __init__(self, parent):
        super(HybridMetamorphs, self).__init__(parent)
        self.leader = self.Leader(self)
        self.metamorphs = UnitList(self, self.Metamorph, 5, 10)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        flags = sum(pal.count_banners() for pal in self.metamorphs.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

        self.metamorphs.update_range(
            5 - self.leader.count,
            10 - self.leader.count
        )

    def get_count(self):
        return self.leader.count + self.metamorphs.count

    def build_statistics(self):
        return self.note_transport(super(HybridMetamorphs, self).build_statistics())
