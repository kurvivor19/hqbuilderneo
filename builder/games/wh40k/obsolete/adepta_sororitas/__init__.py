__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.adepta_sororitas.hq import *
from builder.games.wh40k.obsolete.adepta_sororitas.elites import *
from builder.games.wh40k.obsolete.adepta_sororitas.troops import *
from builder.games.wh40k.obsolete.adepta_sororitas.fast import *
from builder.games.wh40k.obsolete.adepta_sororitas.heavy import *


class AdeptaSororitas(LegacyWh40k):
    army_id = 'ba5541e24748476087f88fdbe95a4fd5'
    army_name = 'Adepta Sororitas'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[Celestine, Jacobus, Canoness, {'unit': CommandSquad, 'active': False}, Priest,
                {'unit': Conclave, 'active': False}],
            elites=[Celestians, Repentia],
            troops=[Sisters],
            fast=[Seraphims, Dominions],
            heavy=[Retributors, Exorcist, PenitentEngine],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.SOB)

        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([CommandSquad, Conclave, Priest])
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        can = self.hq.count_unit(Canoness)
        self.hq.set_active_types([CommandSquad], can > 0)
        if can < self.hq.count_units([CommandSquad]):
            self.error("You can't have more Sororitas Command Squads then Canonesses.")

        conf = self.hq.count_units([Jacobus, Priest])
        self.hq.set_active_types([Conclave], conf > 0)
        if conf < self.hq.count_units([Conclave]):
            self.error("You can't have more Battle Conclaves then Confessors (including Uriah Jacobus).")

        if self.hq.count_unit(Priest) > 5:
            self.error('You can\'t take more then 5 Preachers.')
