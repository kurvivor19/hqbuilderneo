__author__ = 'dromanow'

from builder.core import get_ids

melee = [
    ['Chainsword', 0, 'csw'],
    ['Power weapon', 15, 'psw'],
]

canoness_melee = melee + [
    ['Evisecrator', 30, 'evsc']
]

ranged = [
    ['Storm Bolter', 5, 'sbgun'],
    ['Combi-melta', 10, 'cmelta'],
    ['Combi-flamer', 10, 'cflame'],
    ['Combi-plasma', 10, 'cplasma'],
    ['Condemnor boltgun', 15, 'cbgun'],
    ['Plasma pistol', 15, 'ppist'],
]

priest_ranged = [
    ['Shotgun', 1, 'sg']
] + ranged

canoness_ranged = ranged + [
    ['Inferno pistol', 15, 'ipist'],
]

relic = [
    ['The Book of St. Lucius', 5, 'book'],
    ['The Litanies of Faith', 15, 'lit'],
    ['The Cloak of St. Aspira', 20, 'cl'],
    ['The Mantle of Ophelia', 25, 'man'],
]

priest_relic = relic + [
    ['The Mace of Valaan', 25, 'mace']
]

weapon_relic = [
    ['The Blade of Admonition', 30, 'blade']
]
weapon_relic_ids = get_ids(weapon_relic)

special = [
    ['Storm bolter', 5, 'sbgun'],
    ['Flamer', 5, 'flame'],
    ['Meltagun', 10, 'mgun'],
]

heavy = [
    ['Heavy bolter', 10, 'hbgun'],
    ['Multi-melta', 10, 'mmgun'],
    ['Heavy flamer', 10, 'hflame']
]

vehicles = [
    ['Storm bolter', 5, 'sbgun'],
    ['Dozer blade', 5, 'dblade'],
    ['Extra armour', 10, 'exarm'],
    ['Hunter-killer missile', 10, 'hkm'],
    ['Laud hailer', 10, 'laud'],
]

