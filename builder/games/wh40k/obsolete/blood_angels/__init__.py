
__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.blood_angels.hq import *
from builder.games.wh40k.obsolete.blood_angels.elites import *
from builder.games.wh40k.obsolete.blood_angels.troops import *
from builder.games.wh40k.obsolete.blood_angels.fast import *
from builder.games.wh40k.obsolete.blood_angels.heavy import *
from functools import reduce


class BloodAngels(LegacyWh40k):
    army_id = 'ed357ab206594a84b0718b9e8224828f'
    army_name = 'Blood Angels'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(self,
            hq=[Dante, Gabriel, Astropath, Sanguinor, Mephiston, Tycho, Librarian, Reclusiarch, Captian, HonourGuards],
            elites=[Chaplain, SanguinaryGuards, FuriosoDreadnought, TerminatorSquad, TerminatorAssaultSquad,
                      SternguardVeteranSquad, Techmarine, SanguinaryPriest, Corbulo],
            troops=[TacticalSquad, DeathCompany, DeathCompanyDreadnought, ScoutSquad, AssaultSquad,
                    {'unit': SanguinaryGuards, 'active': False}],
            fast=[VanguardVeteranSquad, LandSpeederSquadron, BaalPredator, AttackBikeSquad, SpaceMarineBikers,
                  ScoutBikers],
            heavy=[Dreadnought, StormravenGunship, Predator, Devastators, Whirlwind, Vindicator],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.BA)
        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_unit(HonourGuards)
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

        def check_elite_limit():
            count = self.elites.count_unit(Corbulo) + self.elites.count_unit(SanguinaryPriest)
            slots = int(count / 3) + (1 if count % 3 else 0)
            return self.elites.min <= len(self.elites.get_units()) - count + slots <= self.elites.max
        self.elites.check_limits = check_elite_limit

    def check_rules(self):
        dante = self.hq.count_unit(Dante) > 0
        self.troops.set_active_types([SanguinaryGuards], dante)
        self.elites.set_active_types([SanguinaryGuards], not dante)
        if not dante and self.troops.count_unit(SanguinaryGuards) > 0:
            self.error("You can't have Sanguinary Guards in Troops without Commander Dante in HQ.")

        hg = self.hq.count_unit(HonourGuards)
        if hg > len(self.hq.get_units()) - hg:
            self.error("You can't have more Honour Guards units then other units in HQ.")

        astropath = self.hq.count_unit(Astropath) > 0
        if not astropath and self.troops.count_unit(DeathCompany) > 1:
            self.error("You can't have more then one Death Company without Astorath the Grim in HQ.")

        dc_dred_limit = int(reduce(lambda val, u: val + u.get_count(), self.troops.get_units(DeathCompany), 0) / 5)
        self.troops.set_active_types([DeathCompanyDreadnought], dc_dred_limit > 0)
        dc_dred_count = self.troops.count_unit(DeathCompanyDreadnought)
        if dc_dred_limit and dc_dred_count > dc_dred_limit:
            self.error("You can't have more then {0} Death Company Dreadnought.".format(dc_dred_limit))
        elif dc_dred_limit == 0 and dc_dred_count > 0:
            self.error("You can't have Death Company Dreadnought without at least 5 Death Company.")
