__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import Formation, UnitType
from builder.games.wh40k.roster import Wh40k7ed, Wh40kImperial
from .elites import CustodianSquad


class GoldenLegion(Formation):
    army_name = 'Golden Legion Task Force'
    army_id = 'custodians_v1_task'

    def __init__(self):
        super(GoldenLegion, self).__init__()
        UnitType(self, CustodianSquad, min_limit=1, max_limit=3)


faction = 'Custodian Guard'


class CustodianGuard(Wh40kImperial, Wh40k7ed):
    army_id = 'custodians_v1'
    army_name = 'Custodian Guard'
    faction = faction
    obsolete = True

    def __init__(self):
        super(CustodianGuard, self).__init__([GoldenLegion])

detachments = [GoldenLegion]
