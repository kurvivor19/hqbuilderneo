__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.core import get_ids


class Falcon(Unit):
    name = 'Corsair Falcon'
    base_points = 130
    gear = ['Pulse Laser']

    def __init__(self):
        Unit.__init__(self)
        self.main = self.opt_one_of('Main weapon', [
            ['Shuriken Cannon', 5, 'scan'],
            ['Scatter laser', 15, 'scl'],
            ['Eldar Missile Launcher', 20, 'eml'],
            ['Starcannon', 25, 'starcan'],
            ['Bright lance', 30, 'blnc']
        ])
        self.sec = self.opt_one_of('Secondary weapon', [
            ['Twin-linked Shuriuken catapult', 0, 'tlscat'],
            ['Shuriken Cannon', 10, 'scan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Holo-field', 35, 'hlf'],
            ['Vectored Engines', 20, 'veng'],
            ['Star Engines', 15, 'seng'],
            ['Spirit Stones', 10, 'spst']
        ])


class Venom(Unit):
    name = 'Corsair Venom'
    base_points = 45
    gear = ['Twin-linked Shuriuken catapults']

    def __init__(self):
        Unit.__init__(self)
        self.main = self.opt_one_of('Weapon', [
            ['Shuriken Cannon', 10, 'scan'],
            ['Scatter laser', 10, 'scl'],
            ['Eldar Missile Launcher', 15, 'eml'],
            ['Starcannon', 20, 'starcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Holo-field', 35, 'hlf'],
            ['Vectored Engines', 20, 'veng'],
            ['Star Engines', 15, 'seng'],
            ['Spirit Stones', 10, 'spst']
        ])


class Retinue(Unit):
    name = 'Corsair Blade Sworn Retinue'

    class BladeSworn(ListSubUnit):
        name = 'Blade Sworn'
        base_points = 14
        has_pack = False
        wep_list = [
            ['Lasblaster', 2, 'lbl'],
            ['Shuriken catapult', 2, 'shc'],
            ['Fusion pistol', 10, 'fpist'],
            ['Power weapon', 10, 'pwep']
        ]
        wep_ids = get_ids(wep_list)

        def __init__(self):
            ListSubUnit.__init__(self, min_models=2, max_models=5)
            self.wep1 = self.opt_one_of('Weapon', [['Shuriken pistol', 0, 'spist']] + self.wep_list)
            self.wep2 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + self.wep_list)

        def update_unit(self, jp):
            self.wep2.set_active_options(self.wep_ids, self.wep1.get_cur() == 'spist')
            self.wep1.set_active_options(self.wep_ids, self.wep2.get_cur() == 'ccw')
            self.gear = ['Plasma grenades'] + (['Corsair Jet Pack'] if jp.get('jp') else [])
            ListSubUnit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.wars = self.opt_units_list('Blade Sworns', self.BladeSworn, 2, 5)
        self.jp = self.opt_options_list('Options', [['Corsair Jet Pack', 20, 'jp']])
        self.trans = self.opt_optional_sub_unit('Transport', [Falcon(), Venom()])

    def check_rules(self):
        self.wars.update_range()
        self.trans.set_active(not self.jp.get('jp'))
        self.jp.set_active(self.trans.get_count() == 0)
        self.trans.set_active_options([Venom.name], self.wars.get_count() < 5)
        for unit in self.wars.get_units():
            unit.update_unit(self.jp)
        Unit.check_rules(self)

    def get_jp(self):
        return self.jp.get('jp')


class Prince(Unit):
    name = 'Corsair Prince'
    base_points = 100
    gear = ['Plasma grenades']
    wep_list = [
        ['Harlequin\'s kiss', 5, 'hks'],
        ['Lasblaster', 2, 'lbl'],
        ['Shuriken catapult', 2, 'shc'],
        ['Fusion pistol', 10, 'fpist'],
        ['Power weapon', 10, 'pwep'],
        ['Void sabre', 15, 'vs']
    ]

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [['Shuriken pistol', 0, 'spist']] + self.wep_list)
        self.wep2 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + self.wep_list)
        self.opt = self.opt_options_list('Options', [
            ['Haywire grenades', 5, 'hwg'],
            ['Meltabombs', 5, 'mbmb'],
            ['Balelight', 10, 'blght'],
            ['Corsair Jet Pack', 20, 'jpck']
        ])
        self.fld = self.opt_options_list('Protective field', [
            ['Shimmershield', 5, 'shmshld'],
            ['Force shield', 20, 'fshld'],
            ['Shadowshield', 35, 'shwshld']
        ], 1)

    def get_jp(self):
        return self.opt.get('jpck')


class Dreamer(Unit):
    name = 'Corsair Void Dreamer'
    base_points = 70
    gear = ['Rune armour']
    wep_list = [
        ['Neural Shredder', 15, 'nshr'],
        ['Power weapon', 10, 'pwep'],
        ['Void Sabre', 15, 'vsbr'],
        ['Witchblade', 15, 'wbl'],
    ]

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [['Shuriken pistol', 0, 'spist']] + self.wep_list)
        self.wep2 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + self.wep_list)
        self.opt = self.opt_options_list('Options', [
            ['Haywire grenades', 5, 'hwgr'],
            ['Corsair Jet Pack', 20, 'cjpck'],
            ['A Gyrinx', 5, 'cat'],
            ['Spirit Stones', 20, 'spst'],
            ['Balelight', 10, 'bl'],
            ['Webway Portal', 35, 'wwp']
        ])

    def check_rules(self):
        self.wep2.set_active_options(get_ids(self.wep_list), self.wep1.get_cur() == 'spist')
        self.wep1.set_active_options(get_ids(self.wep_list), self.wep2.get_cur() == 'ccw')
        Unit.check_rules(self)
