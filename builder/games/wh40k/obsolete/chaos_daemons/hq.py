__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
from builder.core.options import norm_points


class Scarbrand(StaticUnit):
    name = 'Skarbrand'
    base_points = 225
    gear = ['Warp-forged Armour', 'Slaughter and Carnage']


class Fateweaver(StaticUnit):
    name = "Kairos Fateweaver"
    base_points = 300
    gear = ['Staff of Tomorrow']


class Kugath(StaticUnit):
    name = 'Ku\'gath Plaguefather'
    base_points = 260
    gear = ['Necrotic Missiles']


class Bloodthirster(Unit):
    name = 'Bloodthirster'
    base_points = 250
    gear = ['Warp-forged Armour', 'Lash of Khorne', 'Axe of Khorne']

    def __init__(self):
        Unit.__init__(self)
        self.les = self.opt_count("Lesser Rewards", 0, 5, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 2, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(50, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class LordOfChange(Unit):
    name = 'Lord of Change'
    base_points = 230

    def __init__(self):
        Unit.__init__(self)
        self.mastery = self.opt_one_of("Psyker", [
            ["Mastery level 2", 0, 'ml2'],
            ["Mastery level 3", 25, 'ml3']
        ])
        self.les = self.opt_count("Lesser Rewards", 0, 5, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 2, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(50, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class Unclean(Unit):
    name = 'Great Unclean One'
    base_points = 190

    def __init__(self):
        Unit.__init__(self)
        self.mastery = self.opt_one_of("Psyker", [
            ["Mastery level 1", 0, 'ml1'],
            ["Mastery level 2", 25, 'ml2'],
            ["Mastery level 3", 50, 'ml3']
        ])
        self.les = self.opt_count("Lesser Rewards", 0, 5, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 2, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(50, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class Keeper(Unit):
    name = 'Keeper of Secrets'
    base_points = 170

    def __init__(self):
        Unit.__init__(self)
        self.mastery = self.opt_one_of("Psyker", [
            ["Mastery level 1", 0, 'ml1'],
            ["Mastery level 2", 25, 'ml2'],
            ["Mastery level 3", 50, 'ml3']
        ])
        self.les = self.opt_count("Lesser Rewards", 0, 5, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 2, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(50, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class DaemonPrince(Unit):
    name = "Daemon Prince"
    base_points = 145

    def __init__(self):
        Unit.__init__(self)
        self.god = self.opt_one_of('', [
            ['Daemon of Khorne', 15, 'kh'],
            ['Daemon of Tzeench', 25, 'tz'],
            ['Daemon of Nurgle', 15, 'ng'],
            ['Daemon of Slaanesh', 10, 'sl']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Daemonic flight', 40, 'wng'],
            ['Warp-forged armour', 20, 'pwr']
        ])
        self.mastery = self.opt_options_list("Psyker", [
            ["Mastery level 1", 25, 'ml1'],
            ["Mastery level 2", 50, 'ml2'],
            ["Mastery level 3", 75, 'ml3']
        ], 1)
        self.les = self.opt_count("Lesser Rewards", 0, 5, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 2, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        self.mastery.set_active(not self.god.get_cur() == 'kh')
        norm_points(50, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class Skulltaker(Unit):
    name = "Skulltaker"
    base_points = 100
    gear = ['Lesser Locus of Abjuration', 'Cloak of Sculls', 'The Slayer Sword']

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride', [["Juggernaut of Khorne", 45, 'jug']])

    def get_unique(self):
        return self.name


class Karanak(StaticUnit):
    name = "Karanak"
    base_points = 120
    gear = ['Greater Locus of Fury', 'Brass Collar of Bloody Vengeance']


class HKhorne(Unit):
    name = "Herald of Khorne"
    base_points = 55
    gear = ['Hellblade']

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride', [
            ['Juggernaut of Khorne', 45, 'jug'],
            ['Blood Throne of Khorne', 75, 'chair']
        ], 1)
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Abjuration', 10, 'abj'],
            ['Greater Locus of Fury', 20, 'fury'],
            ['Exalted Locus of Wrath', 25, 'wrath']
        ], 1)
        self.les = self.opt_count("Lesser Rewards", 0, 3, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 1, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(30, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class Changeling(StaticUnit):
    name = "The Changeling"
    base_points = 75
    gear = ["Lesser Locus of Transmogrification"]


class BlueScribes(StaticUnit):
    name = "The Blue Scribes"
    base_points = 81
    gear = ['Scrolls of Sorcery']


class HTzeentch(Unit):
    name = "Herald of Tzeentch"
    base_points = 45

    class BurningChariot(Unit):
        name = "Burning Chariot of Tzeentch"
        base_points = 50

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list("Options", [['Blue Horrors crew', 10, 'crew']])

    class Disc(StaticUnit):
        name = "Disc of Tzeentch"
        base_points = 25

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_optional_sub_unit("Ride", [self.BurningChariot(), self.Disc()])
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Transmogrification', 10, 'trmg'],
            ['Greater Locus of Change', 20, 'chng'],
            ['Exalted Locus of Conjuration', 25, 'conj']
        ], 1)

        self.mastery = self.opt_one_of("Psyker", [
            ["Mastery level 1", 0, 'ml1'],
            ["Mastery level 2", 25, 'ml2'],
            ["Mastery level 3", 50, 'ml3']
        ])
        self.les = self.opt_count("Lesser Rewards", 0, 3, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 1, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(30, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class Epidemius(StaticUnit):
    name = "Epidemius"
    base_points = 110
    gear = ['Lesser Locus of Virulence', 'Plaguesword']


class HNurgle(Unit):
    name = "Herald of Nurgle"
    base_points = 45
    gear = ['Plaguesword']

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list("Ride", [["Palanquin of Nurgle", 40, 'pal']])
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Virulence', 10, 'vir'],
            ['Greater Locus of Fecundity', 25, 'fec'],
            ['Exalted Locus of Contagion', 25, 'con']
        ], 1)
        self.mastery = self.opt_options_list("Psyker", [
            ["Mastery level 1", 25, 'ml2'],
            ["Mastery level 2", 50, 'ml3']
        ], 1)
        self.les = self.opt_count("Lesser Rewards", 0, 3, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 1, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(30, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)


class Masque(StaticUnit):
    name = "The Masque of Slaanesh"
    base_points = 75


class HSlaanesh(Unit):
    name = "Herald of Slaanesh"
    base_points = 45

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride', [
            ['Steed of Slaanesh', 15, 'mnt'],
            ['Seeker Chariot', 30, 'char'],
            ['Exalted Seeker Chariot', 80, 'echar']
        ], 1)
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Grace', 10, 'grc'],
            ['Greater Locus of Swiftness', 20, 'swf'],
            ['Exalted Locus of Beguilement', 30, 'bgl']
        ], 1)
        self.mastery = self.opt_options_list("Psyker", [
            ["Mastery level 1", 25, 'ml2'],
            ["Mastery level 2", 50, 'ml3']
        ], 1)
        self.les = self.opt_count("Lesser Rewards", 0, 3, 10)
        self.grt = self.opt_count("Greater Rewards", 0, 1, 20)
        self.exlt = self.opt_count("Exalted Rewards", 0, 1, 30)

    def check_rules(self):
        norm_points(30, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)
