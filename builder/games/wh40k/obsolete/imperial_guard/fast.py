__author__ = 'Denis Romanow'

from builder.core.unit import Unit, ListSubUnit, OptionsList, ListUnit


class Vehicle(ListSubUnit):
    def __init__(self):
        ListSubUnit.__init__(self, max_models=3)
        self.add_opts = None

    def add_options(self, opt):
        self.add_opts = opt

    def check_rules(self):
        single_points = self.build_points(exclude=[self.count.id], count=1) + (self.add_opts.points() if
                                                                               self.add_opts else 0)
        self.points.set(single_points * self.get_count())
        self.build_description(points=single_points, count=1, exclude=[self.count.id])
        if self.add_opts:
            self.description.add(self.add_opts.get_selected())


class VehicleSquad(Unit):
    def __init__(self):
        Unit.__init__(self)
        self.units = None
        self.opt = None

    def check_rules(self):
        self.units.update_range()
        for unit in self.units.get_units():
            unit.add_options(self.opt)
            unit.check_rules()
        self.points.set(self.build_points(count=1, exclude=[self.opt.id]))
        self.build_description(count=1, exclude=[self.opt.id])


class ScoutSentinelSquad(VehicleSquad):
    name = 'Scout Sentinel Squad'

    class ScoutSentinel(Vehicle):
        name = 'Scout Sentinel'
        base_points = 35

        def __init__(self):
            Vehicle.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Multi-laser', 0],
                ['Autocannon', 5],
                ['Heavy flamer', 5],
                ['Missile launcher', 10],
                ['Lascannon', 15],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Searchlight', 1],
                ['Hunter-killer missile', 10],
            ])

    def __init__(self):
        VehicleSquad.__init__(self)
        opts = OptionsList('opts', 'Options', [
            ['Smoke launchers', 5, 'sl'],
            ['Camo netting', 10, 'cn'],
        ])
        self.units = self.opt_units_list('', self.ScoutSentinel, 1, 3)
        self.append_opt(opts)
        self.opt = opts


class ArmouredSentinelSquad(VehicleSquad):
    name = 'Armoured Sentinel Squad'

    class ArmouredSentinel(Vehicle):
        name = 'Armoured Sentinel'
        base_points = 55
        gear = ['Extra armour']

        def __init__(self, opts):
            Vehicle.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Multi-laser', 0],
                ['Autocannon', 5],
                ['Heavy flamer', 5],
                ['Missile launcher', 10],
                ['Lascannon', 15],
                ['Plasma cannon', 20],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Searchlight', 1],
                ['Hunter-killer missile', 10],
            ])

    def __init__(self):
        VehicleSquad.__init__(self)
        opts = OptionsList('opts', 'Options', [
            ['Smoke launchers', 5, 'sl'],
            ['Camo netting', 10, 'cn'],
        ])
        self.units = self.opt_units_list('', lambda: self.ArmouredSentinel(opts), 1, 3)
        self.append_opt(opts)
        self.opt = opts


class Riders(Unit):
    name = 'Rough Rider Squad'

    class Mogul(Unit):
        base_points = 55
        name = 'Mogul Kamir'
        gear = ['Flak armour', 'Hunting lance', 'Cyber-steed', 'Bolt pistol', 'Frag grenades', 'Krak grenades',
                'Close combat weapon']
        static = True

    class Commander(Unit):
        name = 'Rough Rider Sergeant'
        base_points = 15
        gear = ['Flak armour', 'Hunting lance', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            wep = [
                ['Close combat weapon', 0],
                ['Laspistol', 0],
                ['Power weapon', 10],
                ['Plasma pistol', 10],
            ]
            self.wep = self.opt_one_of('Weapon', wep)
            self.opt = self.opt_options_list('', [['Melta bombs', 5]])

    class Rider(ListSubUnit):
        name = 'Rough Rider'
        gear = ['Flak armour', 'Frag grenades', 'Krak grenades']
        base_points = 10

        def __init__(self):
            ListSubUnit.__init__(self, max_models=9)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0],
                ['Laspistol', 0],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Hunting lance', 0, 'hl'],
                ['Flamer', 5],
                ['Grenade launcher', 5],
                ['Melta gun', 10],
                ['Plasma gun', 15],
            ])

        def get_spec(self):
            return self.count.get() if self.wep2.get_cur() != 'hl' else 0

    def __init__(self):
        Unit.__init__(self)
        self.commander = self.opt_sub_unit(self.Commander())
        self.unique_commanders = self.opt_optional_sub_unit('', [self.Mogul()])
        self.warriors = self.opt_units_list('', lambda: self.Rider(), 4, 9)

    def get_count(self):
        return self.warriors.get_count() + 1

    def check_rules(self):
        self.commander.set_active(self.unique_commanders.get_count() == 0)
        spec = sum((u.get_spec() for u in self.warriors.get_units()))
        if spec > 2:
            self.error('Rough Rider Squad may take not more then 2 special weapon (taken: {0})'.format(spec))
        self.warriors.update_range()
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_unique(self):
        if self.unique_commanders.get_count() > 0:
            return self.Mogul.name


class Hellhounds(VehicleSquad):
    name = 'Hellhound Squadron'

    class Hellhound(Vehicle):
        name = 'Hellhound'
        guns = dict(
            hh='Inferno cannon',
            dd='Melta cannon',
            bw='Chem cannon'
        )

        def __init__(self):
            Vehicle.__init__(self)

            self.type = self.opt_one_of('Type', [
                ['Hellhound', 130, 'hh'],
                ['Devil dog', 120, 'dd'],
                ['Banewolf', 130, 'bw'],
            ])

            self.wep1 = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0],
                ['Heavy flamer', 0],
                ['Multy melta', 15],
            ])

            self.wep3 = self.opt_options_list('', [
                ['Pintle-mounted storm bolter', 10],
                ['Pintle-mounted heavy stubber', 10],
            ], limit=1)

            self.opt = self.opt_options_list('Options', [
                ['Searchlight', 1],
                ['Dozen blade', 10],
                ['Extra armor', 15],
                ['Hunter-killer missile', 15],
            ])

        def check_rules(self):
            single_points = self.build_points(exclude=[self.count.id], count=1) + + (self.add_opts.points() if
                                                                                     self.add_opts else 0)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.type.id],
                                   gear=[self.guns[self.type.get_cur()]], name=self.type.get_selected())
            if self.add_opts:
                self.description.add(self.add_opts.get_selected())

    def __init__(self):
        VehicleSquad.__init__(self)
        opts = OptionsList('opts', 'Options', [
            ['Smoke launchers', 5, 'sl'],
            ['Camo netting', 20, 'cn'],
        ])
        self.units = self.opt_units_list('', self.Hellhound, 1, 3)
        self.append_opt(opts)
        self.opt = opts


class Valkyries(ListUnit):
    name = 'Valkyrie Assault Carrier Squadron'

    class Valkyrie(ListSubUnit):
        name = 'Valkyrie'
        base_points = 100
        gear = ['Searchlight', 'Extra armour']
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Multi-laser', 0],
                ['Lascannon', 15]
            ])
            self.wep2 = self.opt_one_of('', [
                ['Hellstrike missiles', 0, 'mis'],
                ['Multiple rocket pods', 30, 'rp']
            ])
            self.wep3 = self.opt_options_list('', [
                ['Pair of sponsons with heavy bolters', 10, 'hb']
            ])

        def check_rules(self):
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.wep2.id, self.wep3.id])
            if self.wep2.get_cur() == 'mis':
                self.description.add('Hellstrike missile', 2)
            elif self.wep2.get_cur() == 'rp':
                self.description.add('Multiple rocket pod', 2)
            if self.wep3.get('hb'):
                self.description.add('Heavy bolter', 2)

    unit_class = Valkyrie


class Vendetta(ListUnit):
    name = 'Vendetta Gunship Squadron'

    class Vendetta(ListSubUnit):
        name = 'Vendetta'
        base_points = 130
        gear = ['Searchlight', 'Extra armour', 'Twin-linked lascannon']
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Two twin-linked lascannons', 0, 'lc'],
                ['Two hellfury missiles', 0, 'mis']
            ])
            self.wep3 = self.opt_options_list('', [
                ['Pair of sponsons with heavy bolters', 10, 'hb']
            ])

        def check_rules(self):
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.wep1.id, self.wep3.id])
            if self.wep1.get_cur() == 'mis':
                self.description.add('Hellfury missile', 2)
            elif self.wep1.get_cur() == 'lc':
                self.description.add('Twin-linked lascannon', 2)
            if self.wep3.get('hb'):
                self.description.add('Heavy bolter', 2)

    unit_class = Vendetta
