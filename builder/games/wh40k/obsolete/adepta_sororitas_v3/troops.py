__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription, OneOf,\
    OptionsList, SubUnit, Count, OptionalSubUnit
from .armory import VehicleOptions, Boltgun, BoltPistol,\
    Melee, Ranged, SpecialWeapon, HeavyWeapon
from builder.games.wh40k.roster import Unit


class Rhino(Unit):
    type_name = "Rhino"

    type_id = 'rhino_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Sororitas-Rhino')

    gear = []

    def __init__(self, parent):
        super(Rhino, self).__init__(parent, points=40, gear=[
            Gear('Storm bolter'), Gear('Smoke launchers'), Gear('Searchlight')
        ])
        self.opt = VehicleOptions(self)


class Immolator(Unit):
    type_name = "Immolator"
    type_id = 'immolator_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Immolator')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Immolator.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked heavy flamer', 0)
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked multi-melta', 0)

    def __init__(self, parent):
        super(Immolator, self).__init__(parent, points=60, gear=[
            Gear('Smoke launchers'), Gear('Searchlight')
        ]) 
        self.wep = self.Weapon(self)
        self.opt = VehicleOptions(self)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent, 'Transport', limit=1)
        SubUnit(self, Rhino(parent=None))
        SubUnit(self, Immolator(parent=None))


class Sisters(Unit):
    type_name = 'Battle Sister Squad'
    type_id = 'sisters_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Battle-Sister-Squad')

    common_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 12

    class Superior(Unit):
        name = 'Sister Superior'

        class Weapon1(Ranged, Melee, Boltgun):
            pass
        
        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Sisters.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.vet = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(Sisters.Superior, self).__init__(parent, 'Sister Superior',
                                                   Sisters.model_points, Sisters.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(Sisters.Superior, self).check_rules()
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

        def build_description(self):
            res = super(Sisters.Superior, self).build_description()
            if self.opt.vet.value:
                res.name = 'Veteran Sister Superior'
            return res

    class MixWeapon(HeavyWeapon, SpecialWeapon):
        pass

    class Simulacrum(OptionsList):
        def __init__(self, parent):
            super(Sisters.Simulacrum, self).__init__(parent, 'Icon of Faith')
            self.variant('Simulacrum imperialis', 10)

    def __init__(self, parent):
        super(Sisters, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=None))
        self.squad = Count(self, 'Battle Sister', 4, 19, self.model_points)
        self.opt = self.Simulacrum(self)
        self.wep1 = SpecialWeapon(self)
        self.wep2 = self.MixWeapon(self, name='Special or Heavy weapon')
        self.transport = Transport(self)

    def get_count(self):
        return 1 + self.squad.cur

    def build_statistics(self):
        return self.note_transport(super(Sisters, self).build_statistics())

    def build_description(self):
        res = UnitDescription(self.name, self.build_points(), self.get_count())
        res.add(self.sup.description)
        sister = UnitDescription('Battle Sister', self.model_points, options=self.common_gear + [Gear('Bolt Pistol')])
        count = self.squad.cur
        if self.opt.any:
            res.add(sister.clone().add(Gear('Boltgun')).add(self.opt.description).add_points(self.opt.points))
            count -=1
        for wep in [self.wep1, self.wep2]:
            if wep.any:
                res.add_dup(sister.clone().add(wep.description).add_points(wep.points))
                count -= 1
        if count:
            res.add(sister.clone().add(Gear('Boltgun')).set_count(count))
        res.add(self.transport.description)
        return res

