from builder.games.wh40k.obsolete.grey_knights import elites

__author__ = 'Denis Romanov'

from builder.core2 import *

Techmarine = Unit.norm_core1_unit(elites.Techmarine)
Purifiers = Unit.norm_core1_unit(elites.Purifiers)
VenDred = Unit.norm_core1_unit(elites.VenDred)
Paladins = Unit.norm_core1_unit(elites.Paladins)
Callidus = Unit.norm_core1_unit(elites.Callidus)
Eversor = Unit.norm_core1_unit(elites.Eversor)
Culexus = Unit.norm_core1_unit(elites.Culexus)
Vindicare = Unit.norm_core1_unit(elites.Vindicare)
Warband = Unit.norm_core1_unit(elites.Warband)
