__author__ = 'Denis Romanow'
from builder.core.unit import Unit
from builder.games.wh40k.obsolete.black_templars2005.armory import vehicle


class LandRaider(Unit):
    name = "Land Raider"
    base_points = 250
    gear = ['Power of the Machine Spirit', 'Twin-linked heavy bolter', 'Twin-linked lascannon', 'Twin-linked lascannon']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Dozer blade', 5, 'db'],
            ['Extra armour', 5, 'ea'],
            ['Hunter-killer missile', 15, 'hkm'],
            ['Pintle-mounted storm bolter', 10, 'psb'],
            ['Searchlight', 1, 'scl'],
            ['Smoke launchers', 3, 'sls'],
        ])


class LandRaiderCrusader(Unit):
    name = "Land Raider Crusader"
    base_points = 265
    gear = ['Power of the Machine Spirit', 'Extra armour', 'Twin-linked assault cannon', 'Hurricane bolter',
            'Hurricane bolter', 'Multimelta', 'Frag Assault Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Blessed Hull', 25, 'bh'],
            ['Dozer blade', 5, 'db'],
            ['Hunter-killer missile', 15, 'hkm'],
            ['Pintle-mounted storm bolter', 10, 'psb'],
            ['Searchlight', 1, 'scl'],
            ['Smoke launchers', 3, 'sls'],
        ])


class Predator(Unit):
    name = "Predator Annihilator"
    base_points = 120
    gear = ['Twin-linked lascannon']

    def __init__(self):
        Unit.__init__(self)
        self.side = self.opt_options_list('Side sponsons', [
            ['Sponsons with heavy bolters', 10, 'hbgun'],
            ['Sponsons with lascannons', 25, 'lcan']
        ], limit=1)
        self.opt = self.opt_options_list('Options', vehicle)


class Destructor(Unit):
    name = "Predator Destructor"
    base_points = 100
    gear = ['Autocannon']

    def __init__(self):
        Unit.__init__(self)
        self.side = self.opt_options_list('Side sponsons', [
            ['Sponsons with heavy bolters', 10, 'hbgun'],
            ['Sponsons with lascannons', 25, 'lcan']
        ], limit=1)
        self.opt = self.opt_options_list('Options', vehicle)


class Vindicator(Unit):
    name = "Vindicator"
    base_points = 125
    gear = ['Demolisher cannon', 'Storm bolter']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle)
