from builder.games.wh40k.obsolete.chaos_daemons import heavy, fast, troops, elites, hq
from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment, AlliedDetachment, HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.dataslates.belakor import *
from builder.games.wh40k.imperial_armour.apocalypse import chaos as ia_apoc_chaos

from builder.games.wh40k.escalation.chaos import LordsOfWar as DaemonLordsOfWar


__author__ = 'Denis Romanov'

Kugath = Unit.norm_core1_unit(hq.Kugath)
Fateweaver = Unit.norm_core1_unit(hq.Fateweaver)
Scarbrand = Unit.norm_core1_unit(hq.Scarbrand)
Keeper = Unit.norm_core1_unit(hq.Keeper)
Unclean = Unit.norm_core1_unit(hq.Unclean)
Bloodthirster = Unit.norm_core1_unit(hq.Bloodthirster)
LordOfChange = Unit.norm_core1_unit(hq.LordOfChange)
DaemonPrince = Unit.norm_core1_unit(hq.DaemonPrince)
Masque = Unit.norm_core1_unit(hq.Masque)
Karanak = Unit.norm_core1_unit(hq.Karanak)
Epidemius = Unit.norm_core1_unit(hq.Epidemius)
BlueScribes = Unit.norm_core1_unit(hq.BlueScribes)
Changeling = Unit.norm_core1_unit(hq.Changeling)
Skulltaker = Unit.norm_core1_unit(hq.Skulltaker)
HKhorne = Unit.norm_core1_unit(hq.HKhorne)
HTzeentch = Unit.norm_core1_unit(hq.HTzeentch)
HNurgle = Unit.norm_core1_unit(hq.HNurgle)
HSlaanesh = Unit.norm_core1_unit(hq.HSlaanesh)


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.kugath = UnitType(self, Kugath)
        self.fateweaver = UnitType(self, Fateweaver)
        self.scarbrand = UnitType(self, Scarbrand)
        self.keeper = UnitType(self, Keeper)
        self.unclean = UnitType(self, Unclean)
        self.bloodthirster = UnitType(self, Bloodthirster)
        self.lordofchange = UnitType(self, LordOfChange)
        self.prince = UnitType(self, DaemonPrince)
        self.masque = UnitType(self, Masque)
        self.karanak = UnitType(self, Karanak, slot=0.25)
        self.epidemius = UnitType(self, Epidemius, slot=0.25)
        self.bluescribes = UnitType(self, BlueScribes)
        self.changeling = UnitType(self, Changeling, slot=0.25)
        self.skulltaker = UnitType(self, Skulltaker, slot=0.25)
        self.hkhorne = UnitType(self, HKhorne, slot=0.25)
        self.htzeentch = UnitType(self, HTzeentch, slot=0.25)
        self.hnurgle = UnitType(self, HNurgle, slot=0.25)
        self.hslaanesh = UnitType(self, HSlaanesh, slot=0.25)
        UnitType(self, Belakor)


class HQ(ia_apoc_chaos.DaemonsHQ, BaseHQ):
    pass

    def check_limits(self):
        return float(self.min) <= sum(float(t.count) for t in self.types) and \
            sum(float(t.count) * float(t.slot) for t in self.types) <= float(self.max)

Fiends = Unit.norm_core1_unit(elites.Fiends)
Flamers = Unit.norm_core1_unit(elites.Flamers)
Crushers = Unit.norm_core1_unit(elites.Crushers)
Beasts = Unit.norm_core1_unit(elites.Beasts)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, Fiends)
        UnitType(self, Flamers)
        UnitType(self, Crushers)
        UnitType(self, Beasts)


class Elites(ia_apoc_chaos.DaemonsElites, BaseElites):
    pass


Letters = Unit.norm_core1_unit(troops.Letters)
Daemonettes = Unit.norm_core1_unit(troops.Daemonettes)
Bearers = Unit.norm_core1_unit(troops.Bearers)
Horrors = Unit.norm_core1_unit(troops.Horrors)
Nurglings = Unit.norm_core1_unit(troops.Nurglings)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Letters)
        UnitType(self, Daemonettes)
        UnitType(self, Bearers)
        UnitType(self, Horrors)
        UnitType(self, Nurglings)


Hounds = Unit.norm_core1_unit(fast.Hounds)
Seekers = Unit.norm_core1_unit(fast.Seekers)
Drones = Unit.norm_core1_unit(fast.Drones)
Screamers = Unit.norm_core1_unit(fast.Screamers)
Fury = Unit.norm_core1_unit(fast.Fury)
Hellflayer = Unit.norm_core1_unit(fast.Hellflayer)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Hounds)
        UnitType(self, Seekers)
        UnitType(self, Drones)
        UnitType(self, Screamers)
        UnitType(self, Fury)
        UnitType(self, Hellflayer)


class FastAttack(ia_apoc_chaos.DaemonFastAttack, BaseFastAttack):
    pass


SoulGrinder = Unit.norm_core1_unit(heavy.SoulGrinder)
SkullCannon = Unit.norm_core1_unit(heavy.SkullCannon)
BurningChariot = Unit.norm_core1_unit(heavy.BurningChariot)
SeekerCavalcade = Unit.norm_core1_unit(heavy.SeekerCavalcade)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, SoulGrinder)
        UnitType(self, SkullCannon)
        UnitType(self, BurningChariot)
        UnitType(self, SeekerCavalcade)
        self.prince = UnitType(self, DaemonPrince)


class HeavySupport(ia_apoc_chaos.DaemonsHeavy, BaseHeavySupport):
    pass


class LordsOfWar(ia_apoc_chaos.DaemonLordsOfWar, DaemonLordsOfWar):
    pass


#class ChaosDaemonsV2(Wh40k):
#    army_id = 'chaos_daemons_v2'
#    army_name = 'Chaos Daemons'
#
#    def __init__(self, secondary=False):
#        self.hq = HQ(parent=self)
#        self.elites = Elites(parent=self)
#        self.troops = Troops(parent=self)
#        self.fast = FastAttack(parent=self)
#        self.heavy = HeavySupport(parent=self)
#
#        super(ChaosDaemonsV2, self).__init__(
#            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
#            fort=Fort(parent=self),
#            lords=LordsOfWar(parent=self),
#            ally=Ally(parent=self, ally_type=Ally.CD),
#            secondary=secondary
#        )
#
#    def check_rules(self):
#        super(ChaosDaemonsV2, self).check_rules()
#        if sum(u.count for u in [self.hq.skulltaker, self.hq.epidemius, self.hq.changeling, self.hq.karanak,
#                                 self.hq.hkhorne, self.hq.hnurgle, self.hq.hslaanesh, self.hq.htzeentch]) > 4:
#            self.error("More then 4 Heralds cannot be taken in the primary detachment")
#
#        godList = {
#            'kh': 'Khorne',
#            'tz': 'Tzeentch',
#            'ng': 'Nurgle',
#            'sl': 'Slaanesh'
#        }
#        aglist = []
#
#        if self.hq.scarbrand.count + self.hq.bloodthirster.count > 0:
#            aglist.append('kh')
#        if self.hq.fateweaver.count + self.hq.lordofchange.count > 0:
#            aglist.append('tz')
#        if self.hq.kugath.count + self.hq.unclean.count > 0:
#            aglist.append('ng')
#        if self.hq.keeper.count > 0:
#            aglist.append('sl')
#        self.heavy.prince.active = len(aglist) > 0
#        if len(aglist) == 0:
#            if self.heavy.prince.count:
#                self.error("Daemon princes can only be taken for Heavy Support if Greater Daemon of the corresponding "
#                           "Chaos god is in HQ.")
#        else:
#            for gid in aglist:
#                if any(prince.god.get_cur() == gid for prince in self.hq.prince.units):
#                    self.error("Daemon princes of " + godList[gid] + " cannot be taken as HQ.")
#            for gid in [item for item in godList.keys() if item not in aglist]:
#                if any(prince.god.get_cur() == gid for prince in self.heavy.prince.units):
#                    self.error("Daemon princes of " + godList[gid] + " cannot be taken as Heavy Support.")

class ChaosDaemonsV2Base(Wh40kBase):
    army_id = 'chaos_daemons_v2'
    army_name = 'Chaos Daemons'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)

        super(ChaosDaemonsV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        super(ChaosDaemonsV2Base, self).check_rules()
        if sum(u.count for u in [self.hq.skulltaker, self.hq.epidemius, self.hq.changeling, self.hq.karanak,
                                 self.hq.hkhorne, self.hq.hnurgle, self.hq.hslaanesh, self.hq.htzeentch]) > 4:
            self.error("More then 4 Heralds cannot be taken in the primary detachment")

        godList = {
            'kh': 'Khorne',
            'tz': 'Tzeentch',
            'ng': 'Nurgle',
            'sl': 'Slaanesh'
        }
        aglist = []

        if self.hq.scarbrand.count + self.hq.bloodthirster.count > 0:
            aglist.append('kh')
        if self.hq.fateweaver.count + self.hq.lordofchange.count > 0:
            aglist.append('tz')
        if self.hq.kugath.count + self.hq.unclean.count > 0:
            aglist.append('ng')
        if self.hq.keeper.count > 0:
            aglist.append('sl')
        self.heavy.prince.active = len(aglist) > 0
        if len(aglist) == 0:
            if self.heavy.prince.count:
                self.error("Daemon princes can only be taken for Heavy Support if Greater Daemon of the corresponding "
                           "Chaos god is in HQ.")
        else:
            for gid in aglist:
                if any(prince.god.get_cur() == gid for prince in self.hq.prince.units):
                    self.error("Daemon princes of " + godList[gid] + " cannot be taken as HQ.")
            for gid in [item for item in list(godList.keys()) if item not in aglist]:
                if any(prince.god.get_cur() == gid for prince in self.heavy.prince.units):
                    self.error("Daemon princes of " + godList[gid] + " cannot be taken as Heavy Support.")


class ChaosDaemonsV2CAD(ChaosDaemonsV2Base, CombinedArmsDetachment):
    army_id = 'chaos_daemons_v2_cad'
    army_name = 'Chaos Daemons (Combined arms detachment)'


class ChaosDaemonsV2AD(ChaosDaemonsV2Base, AlliedDetachment):
    army_id = 'chaos_daemons_v2_ad'
    army_name = 'Chaos Daemons (Allied detachment)'

faction = 'Chaos_Daemons'


class ChaosDaemonsV2(Wh40k7ed):
    army_id = 'chaos_daemons_v2'
    army_name = 'Chaos Daemons'
    faction = faction
    obsolete = True

    def __init__(self):
        super(ChaosDaemonsV2, self).__init__([ChaosDaemonsV2CAD])

detachments = [ChaosDaemonsV2CAD, ChaosDaemonsV2AD]
