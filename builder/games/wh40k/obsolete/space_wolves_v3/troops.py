__author__ = 'Denis Romanov'

from builder.core2 import Unit, OptionsList, ListSubUnit,\
    Gear, UnitList, OptionalSubUnit, OneOf
from builder.games.wh40k.imperial_armour.volume2.transport\
    import IATransportedUnit
from .fast import WolfTransport, DropPod, Rhino, Razorback,\
    Stormwolf
from .armory import Special, Chainsword, BoltPistol, PlasmaPistol,\
    PowerFist, PowerWeapon, Boltgun
from .heavy import WolfGuard


class Lucas(Unit):
    type_name = "Lucas the Trickster, the Jackalwolf"
    type_id = 'lucas_v3'

    def __init__(self, parent):
        super(Lucas, self).__init__(parent, "Lucas the Trickster", 80,
                                    unique=True, static=True, gear=[
                                        Gear('Power Armour'),
                                        Gear('Wolf Claw'),
                                        Gear('Plasma Pistol'),
                                        Gear('Pelt of the Doppegangrel'),
                                        Gear('Frag grenades'),
                                        Gear('Krak grenades')
                                    ])


class BloodClaws(IATransportedUnit):
    type_name = "Blood Claws"
    type_id = "blood_claws_v3"

    class Marine(ListSubUnit):

        class Ranged(PlasmaPistol, BoltPistol):
            pass

        class Melee(Special, PowerFist, PowerWeapon, Chainsword):
            pass

        def __init__(self, parent):
            super(BloodClaws.Marine, self).__init__(
                parent=parent, name='Blood Claw', points=12,
                gear=[
                    Gear('Power Armour'),
                    Gear('Frag Grenades'),
                    Gear('Krak Grenades')
                ]
            )
            self.wep1 = self.Melee(self, name="Weapon")
            self.wep2 = self.Ranged(self, name="")

        @ListSubUnit.count_gear
        def has_power(self):
            return self.wep1.cur == self.wep1.fist or\
                self.wep1.cur == self.wep1.pwr_weapon

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.is_spec()

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.wep2.cur == self.wep2.ppist

    class BloodGuard(WolfGuard, Marine):
        @ListSubUnit.count_gear
        def is_guard(self):
            return self.up.any

    def __init__(self, parent):
        super(BloodClaws, self).__init__(parent)
        self.marines = UnitList(self, self.BloodGuard, 5, 12)
        self.transport = WolfTransport(self, transport_types=[
            Rhino, Razorback, DropPod, Stormwolf
        ])

    def check_rules(self):
        super(BloodClaws, self).check_rules()

        guards = sum(u.is_guard() for u in self.marines.units)
        if guards > 1:
            self.error("Only one model can be upgraded to Wolf Guard Pack Leader")

        if sum(u.has_pistol() for u in self.marines.units) > 1:
            self.error('Only one Blood Claw may upgrade his Bolt Pistol')
        if sum(u.has_power() for u in self.marines.units) > 1:
            self.error('Only one Blood Claw may upgrade his Chainsword to other close combat weapon')
        spec_limit = 1 if self.get_count() < 15 else 2
        if sum(u.has_spec() for u in self.marines.units) > spec_limit:
            self.error('Only {} Blood Claws may have special weapon'.format(spec_limit))

    def get_count(self):
        return self.marines.count


class GreyHunters(IATransportedUnit):
    type_name = "Grey Hunters Pack"
    type_id = "grey_hunters_pack_v1"

    class Marine(ListSubUnit):

        class Weapon1(Special, PowerFist, PowerWeapon, PlasmaPistol, Boltgun):
            pass

        class Weapon2(PowerFist, PowerWeapon, PlasmaPistol, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(GreyHunters.Marine.Options, self).__init__(parent, 'Options')
                self.variant('Close combat weapon', 2)
                self.wolfstandard = self.variant('Wolf standard', 25)

        def __init__(self, parent):
            super(GreyHunters.Marine, self).__init__(
                parent=parent, name='Grey Hunter', points=14,
                gear=[
                    Gear('Power Armour'),
                    Gear('Frag Grenades'),
                    Gear('Krak Grenades')
                ]
            )
            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def has_free(self):
            return self.opt.free.value and self.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.is_spec()

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.wep1.cur == self.wep1.ppist or\
                self.wep2.cur == self.wep2.ppist

        @ListSubUnit.count_gear
        def has_power(self):
            return self.wep1.cur == self.wep1.fist or\
                self.wep1.cur == self.wep1.pwr_weapon or\
                self.wep2.cur == self.wep2.fist or\
                self.wep2.cur == self.wep2.pwr_weapon

        @ListSubUnit.count_gear
        def has_banner(self):
            return self.opt.wolfstandard.value

    class GreyGuard(WolfGuard, Marine):
        @ListSubUnit.count_gear
        def is_guard(self):
            return self.up.any

    def __init__(self, parent):
        super(GreyHunters, self).__init__(parent)
        self.marines = UnitList(self, self.GreyGuard, 5, 10)
        self.transport = WolfTransport(self, transport_types=[
            Rhino, Razorback, DropPod, Stormwolf
        ])

    def check_rules(self):
        super(GreyHunters, self).check_rules()

        guards = sum(u.is_guard() for u in self.marines.units)
        if guards > 1:
            self.error("Only one model can be upgraded to Wolf Guard Pack Leader")

        if sum(u.has_pistol() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunter may upgrade his Bolt Pistol')
        if sum(u.has_power() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunter may take close power weapon or powe fist')
        spec_limit = 1 if self.get_count() < 10 else 2
        if sum(u.has_spec() for u in self.marines.units) > spec_limit:
            self.error('Only {} Grey Hunters may take a special weapon'.format(spec_limit))
        if sum(u.has_banner() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunter may have Wolf Standard')

    def has_banner(self):
        return sum(u.has_banner() for u in self.marines.units) > 0

    def get_count(self):
        return self.marines.count
