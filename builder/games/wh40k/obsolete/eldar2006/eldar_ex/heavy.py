__author__ = 'dromanow'

from builder.core.unit import Unit


class WHunter(Unit):
    name = 'Warp Hunter'
    base_points = 125
    gear = ['D-cannon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked Shuriken Catapult', 0, 'tlshc'],
            ['Shuriken Cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vectored Engines', 20, 'veng'],
            ['Star Engines', 15, 'seng'],
            ['Holo-fields', 35, 'hf'],
            ['Spirit Stones', 10, 'ss']
        ])
