from builder.core2 import *
from .armory import Vehicle
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle


__author__ = 'dromanow'


class Transport(OptionalSubUnit):
    def __init__(self, parent, crusader=False, dreadnought=False, drop_only=False):
        from builder.games.wh40k.imperial_armour.volume2.transport import LuciusDropPod, InfernumRazorback
        from .heavy import LandRaiderCrusader
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        squad = crusader or (not dreadnought and not drop_only)
        self.rhino = squad and SubUnit(self, Rhino(parent=None))
        self.razor = squad and SubUnit(self, Razorback(parent=None))
        self.drop = SubUnit(self, DropPod(parent=None))
        self.lrc = crusader and SubUnit(self, LandRaiderCrusader(parent=None))
        self.infernum = squad and SubUnit(self, InfernumRazorback(parent=None))
        self.lucius = dreadnought and SubUnit(self, LuciusDropPod(parent=None))

    def get_unique_gear(self):
        return sum((u.unit.get_unique_gear() for u in
                    [self.rhino, self.razor, self.drop, self.lrc, self.infernum, self.lucius] if u), [])

    def check_rules(self):
        self.options.set_visible([self.infernum, self.lucius], self.roster.ia_enabled)


class TerminatorTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(TerminatorTransport, self).__init__(parent=parent, name='Transport', limit=1)
        from .heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
        self.lr = SubUnit(self, LandRaider(parent=None))
        self.lrc = SubUnit(self, LandRaiderCrusader(parent=None))
        self.lrr = SubUnit(self, LandRaiderRedeemer(parent=None))

    def get_unique_gear(self):
        return sum((u.unit.get_unique_gear() for u in [self.lrc, self.lr, self.lrr] if u), [])


class Rhino(SpaceMarinesBaseVehicle):
    type_id = 'rhino_v1'
    type_name = "Rhino"

    def __init__(self, parent, points=35):
        super(Rhino, self).__init__(parent=parent, points=points, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self)


class Razorback(SpaceMarinesBaseVehicle):
    type_id = 'razorback_v1'
    type_name = 'Razorback'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Twin-linked heavy bolter', 0)
            self.tlhflame = self.variant('Twin-linked heavy flamer', 0)
            self.tlasscan = self.variant('Twin-linked assault cannon', 20)
            self.tllcannon = self.variant('Twin-linked lascannon', 20)
            self.lascplasgun = self.variant('Lascannon and twin-linked plasma gun', 20, gear=[
                Gear('Lascannon'), Gear('Twin-linked plasma gun')
            ])

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=55, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.weapon = Razorback.Weapon(self)
        self.opt = Vehicle(self)


class DropPod(SpaceMarinesBaseVehicle):
    type_id = 'drop_pod_v1'
    type_name = 'Drop Pod'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant('Storm bolter', 0)
            self.dwind = self.variant('Deathwind missile launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent, 'Options')
            self.locator = self.variant('Locator beacon', 10)

    def __init__(self, parent, points=35):
        super(DropPod, self).__init__(parent=parent, points=points, deep_strike=True, transport=True)
        self.weapon = DropPod.Weapon(self)
        self.opt = DropPod.Options(self)
