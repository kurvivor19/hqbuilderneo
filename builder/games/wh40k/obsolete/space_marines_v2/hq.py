__author__ = 'Denis Romanov'

from builder.core2 import *
from .transport import Transport
from .armory import *


class Calgar(Unit):
    type_id = 'marneus_valgar_v1'
    type_name = 'Marneus Calgar'

    class Armour(OneOf):
        def __init__(self, parent):
            super(Calgar.Armour, self).__init__(parent, 'Armour')
            self.powerarmour = self.variant('Artificer armour', 0, gear=Armour.artificer_armour_set)
            self.armourofantilochus = self.variant('Armour of Antilochus', 10)

    def __init__(self, parent):
        super(Calgar, self).__init__(parent=parent, points=275, unique=True, gear=[
            Gear('Gauntlets of Ultramar'),
            Gear('Power sword'),
            Gear('Iron Halo'),
        ])
        self.armour = self.Armour(parent=self)


class Sicarius(Unit):
    type_id = 'captain_sicarius_v1'
    type_name = 'Captain Sicarius'

    def __init__(self, parent):
        super(Sicarius, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Mantle of the Suzerain'),
            Gear('Talassarian Tempest Blade'),
            Gear('Plasma pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
        ])


class Tigurius(Unit):
    type_id = 'tigurius_v1'
    type_name = 'Chief Librarian Tigurius'

    def __init__(self, parent):
        super(Tigurius, self).__init__(parent=parent, points=165, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Hood of Hellfire'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Rod of Tigurius'),
        ])


class Cassius(Unit):
    type_id = 'cassius_v1'
    type_name = 'Chaplain Cassius'

    def __init__(self, parent):
        super(Cassius, self).__init__(parent=parent, points=130, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Crozius Arcanum'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Rosarius'),
            Gear('Infernus'),
        ])


class Khan(Unit):
    type_id = 'khan_v1'
    type_name = 'Kor\'sarro Khan'

    class Bike(OptionsList):
        def __init__(self, parent):
            super(Khan.Bike, self).__init__(parent, 'Options')
            self.moondrakkan = self.variant('Moondrakkan', 25)

    def __init__(self, parent):
        super(Khan, self).__init__(parent=parent, points=125, unique=True, gear=[
            Gear('Power armour'),
            Gear('Bolt Pistol'),
            Gear('Moonfang'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
        ])
        self.bike = self.Bike(parent=self)

    def has_bike(self):
        return self.bike.moondrakkan.value


class Vulkan(Unit):
    type_id = 'vulkan_v1'
    type_name = 'Vulkan He\'Stan'

    def __init__(self, parent):
        super(Vulkan, self).__init__(parent=parent, points=190, unique=True, static=True, gear=[
            Gear('Artificer armour'),
            Gear('Bolt Pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Kesare\'s Mantle'),
            Gear('The Spear of Vulkan'),
            Gear('The Gauntlet of the Forge'),
        ])


class Shrike(Unit):
    type_id = 'shrike_v1'
    type_name = 'Shadow Captain Shrike'

    def __init__(self, parent):
        super(Shrike, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Bolt Pistol'),
            Gear('The Raven\'s Talons'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Jump pack'),
            Gear('Iron Halo'),
        ])


class Lysander(Unit):
    type_id = 'lysander_v1'
    type_name = 'Captain Lysander'

    def __init__(self, parent):
        super(Lysander, self).__init__(parent=parent, points=230, unique=True, static=True, gear=[
            Gear('Terminator armour'),
            Gear('The Fist of Dorn'),
            Gear('Storm shield'),
            Gear('Iron Halo'),
        ])


class Cantor(Unit):
    type_id = 'cantor_v1'
    type_name = 'Pedro Cantor'

    def __init__(self, parent):
        super(Cantor, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Dorn\'s arrow'),
            Gear('Power fist'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
        ])


class Helbrecht(Unit):
    type_id = 'helbrecht_v1'
    type_name = 'High Marshall Helbrecht'

    def __init__(self, parent):
        super(Helbrecht, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Artificer armour'),
            Gear('Iron halo'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Combi-melta'),
            Gear('Sword of High Marshals'),
        ])


class Champion(Unit):
    type_id = 'champion_v1'
    type_name = 'The Emperors\'s Champion'

    def __init__(self, parent):
        super(Champion, self).__init__(parent=parent, points=140, unique=True, static=True, gear=[
            Gear('Black Sword'),
            Gear('The Armour of Faith'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ])


class Grimaldus(Unit):
    type_id = 'grimaldus_v1'
    type_name = 'Chaplain Grimaldus'

    class Serv(OptionsList):
        def __init__(self, parent):
            super(Grimaldus.Serv, self).__init__(parent, 'Options', used=False)
            self.cenobyteservitors = self.variant('Cenobyte Servitors', 30)

    def __init__(self, parent):
        super(Grimaldus, self).__init__(parent=parent, points=180, unique=True, gear=[
            Gear('Crozius arcanum'),
            Gear('Rosarius'),
            Gear('Power armour'),
            Gear('Master-crafter plasma pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ])
        self.serv = self.Serv(parent=self)
        self.serv_units = Count(self, name='Servitors', min_limit=3, max_limit=5, points=10, per_model=True,
                                gear=UnitDescription(name='Cenobyte Servitor', points=10))

    def check_rules(self):
        super(Grimaldus, self).check_rules()
        self.serv_units.visible = self.serv_units.used = self.serv.cenobyteservitors.value


class ChapterMaster(Unit):
    type_id = 'chapter_master_v1'
    type_name = 'Chapter Master'

    def __init__(self, parent):
        super(ChapterMaster, self).__init__(parent=parent, points=130, gear=Gear('Iron halo'))

        self.armour = Armour(self, art=20, tda=40)

        class Weapon1(RelicWeapon, TerminatorMelee, Ranged, Melee, Boltgun, BoltPistol):
            pass

        class Weapon2(RelicWeapon, TerminatorRanged, Ranged, Melee, RelicBlade, Chainsword):
            pass

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.relic = Relic(self, armour=self.armour)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True, storm_shield=True)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()

    def has_bike(self):
        return self.opt.has_bike()


class Captain(Unit):
    type_id = 'captain_v1'
    type_name = 'Captain'

    def __init__(self, parent):
        super(Captain, self).__init__(parent=parent, points=90, gear=Gear('Iron halo'))

        self.armour = Armour(self, art=20)

        class Weapon1(RelicWeapon, Ranged, Melee, Boltgun, BoltPistol):
            pass

        class Weapon2(RelicWeapon, Ranged, Melee, RelicBlade, Chainsword):
            pass

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.relic = Relic(self, armour=self.armour)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True, storm_shield=True)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()

    def has_bike(self):
        return self.opt.has_bike()


class TerminatorCaptain(Unit):
    type_name = 'Terminator Captain'
    type_id = 'terminator_captain_v1'

    def __init__(self, parent):
        super(TerminatorCaptain, self).__init__(parent=parent, points=120,
                                                gear=[Gear('Iron halo'), Gear('Terminator armour')])

        class Weapon1(RelicWeapon, TerminatorMelee):
            pass

        class Weapon2(RelicWeapon, TerminatorRanged):
            pass

        self.weapon1 = Weapon1(self, 'Weapon')
        self.weapon2 = Weapon2(self, '')
        self.opt = Options(self)
        self.relics = Relic(self)

    def check_rules(self):
        self.relics.used = self.relics.visible = not self.roster.is_base_codex()
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique()


class Librarian(Unit):
    type_name = 'Librarian'
    type_id = 'librarian_v1'

    class TdaWeapon(OptionsList):
        def __init__(self, parent, armour):
            super(Librarian.TdaWeapon, self).__init__(parent=parent, name='', limit=1)
            self.armour = armour
            self.stormbolter = self.variant('Storm Bolter', 5)
            self.combimelta = self.variant('Combi-melta', 10)
            self.combiflamer = self.variant('Combi-flamer', 10)
            self.combiplasma = self.variant('Combi-plasma', 10)
            self.stormshield = self.variant('Storm shield', 10)

        def check_rules(self):
            super(Librarian.TdaWeapon, self).check_rules()
            self.visible = self.used = self.armour.is_tda()

    class Level(OneOf):
        def __init__(self, parent):
            super(Librarian.Level, self).__init__(parent, 'Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

    def __init__(self, parent):
        super(Librarian, self).__init__(parent=parent, points=65, gear=Gear('Psychic hood'))

        self.armour = Armour(self, tda=25)

        class Weapon1(RelicWeapon, ForceWeapon):
            pass

        class Weapon2(RelicWeapon, Ranged, Boltgun, BoltPistol):
            pass

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour, librarian=True)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.tda_weapon = self.TdaWeapon(self, armour=self.armour)
        self.relic = Relic(self, armour=self.armour, librarian=True)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)
        self.psy = self.Level(self)

    def check_rules(self):
        super(Librarian, self).check_rules()
        self.weapon2.visible = self.weapon2.used = not self.armour.is_tda()
        if self.weapon2.used:
            for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
                r2.active = not self.weapon1.cur == r1
                r1.active = not self.weapon2.cur == r2
        else:
            for r in self.weapon1.relic_weapon:
                r.active = True

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class Chaplain(Unit):
    type_name = 'Chaplain'
    type_id = 'chaplain_v1'

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent=parent, points=90, gear=Gear('Rozarius'))

        self.armour = Armour(self, tda=30)

        class Weapon1(RelicWeapon, Crozius):
            pass

        class Weapon2(RelicWeapon, TerminatorComby, Ranged, PowerFist, Boltgun, BoltPistol):
            pass

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour, chaplain=True)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.relic = Relic(self, armour=self.armour)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class ForgeMaster(Unit):
    type_name = 'Master of the Forge'
    type_id = 'forge_master_v1'

    class Special(OneOf):
        def __init__(self, parent):
            super(ForgeMaster.Special, self).__init__(parent, 'Special')
            self.servoharness = self.variant('Servo-harness', 0)
            self.conversionbeamer = self.variant('Conversion beamer', 20)

    def __init__(self, parent):
        super(ForgeMaster, self).__init__(parent=parent, points=90, gear=Armour.artificer_armour_set)
        self.special = self.Special(self)

        class Weapon1(RelicWeapon, Ranged, Melee, PowerAxe, BoltPistol):
            pass

        class Weapon2(RelicWeapon, PowerAxe, Boltgun):
            pass

        self.weapon1 = Weapon1(self, 'Weapon')
        self.weapon2 = Weapon2(self, '')
        self.relic = Relic(self)
        self.opt = Options(self, bike=True)

    def check_rules(self):
        self.weapon2.visible = self.weapon2.used = not self.special.cur == self.special.conversionbeamer
        if self.weapon2.used:
            for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
                r2.active = not self.weapon1.cur == r1
                r1.active = not self.weapon2.cur == r2
        else:
            for r in self.weapon1.relic_weapon:
                r.active = True

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class Techmarine(Unit):
    type_name = 'Techmarine'
    type_id = 'techmarine_v1'

    class Special(OneOf):
        def __init__(self, parent):
            super(Techmarine.Special, self).__init__(parent, 'Special')
            self.servoarm = self.variant('Servo-arm', 0)
            self.servoharness = self.variant('Servo-harness', 25)

    def __init__(self, parent):
        super(Techmarine, self).__init__(parent=parent, points=50, gear=Armour.artificer_armour_set)
        self.special = self.Special(self)

        class Weapon1(Ranged, Melee, PowerAxe, BoltPistol):
            pass

        class Weapon2(PowerAxe, Boltgun):
            pass

        self.weapon1 = Weapon1(self, 'Weapon')
        self.weapon2 = Weapon2(self, '')
        self.opt = Options(self, bike=True)


class Servitors(Unit):
    type_name = "Servitors"
    type_id = 'servitors_v1'
    model_points = 10

    def __init__(self, parent):
        super(Servitors, self).__init__(parent=parent)
        self.models = Count(self, self.name, points=self.model_points, min_limit=1, max_limit=5, per_model=True)
        self.hw = [
            Count(self, 'Heavy bolter', 0, 2, 10),
            Count(self, 'Multi-melta', 0, 2, 10),
            Count(self, 'Plasma cannon', 0, 2, 20)
        ]

    def check_rules(self):
        Count.norm_counts(0, min(2, self.get_count()), self.hw)

    def build_description(self):
        desc = UnitDescription(self.name, points=self.points, count=self.get_count())
        serv = UnitDescription('Servitor', points=10)
        arms_count = self.get_count()
        for wep in self.hw:
            hw_serv = serv.clone()
            hw_serv.add(Gear(wep.name))
            hw_serv.points += wep.option_points
            hw_serv.count = wep.cur
            desc.add(hw_serv)
            arms_count -= wep.cur
        serv.add(Gear('Servo-Arm'))
        serv.count = arms_count
        desc.add(serv)
        return desc

    def get_count(self):
        return self.models.cur


class HonourGuardSquad(Unit):
    type_name = 'Honour Guard'
    type_id = 'honour_guard_v1'

    model_points = 25

    class HonourGuard(ListSubUnit):

        class Weapon(OneOf):
            def __init__(self, parent):
                super(HonourGuardSquad.HonourGuard.Weapon, self).__init__(parent=parent, name='Weapon')

                self.power = self.variant('Power weapon', 0)
                self.relic = self.variant('Relic blade', 15)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(HonourGuardSquad.HonourGuard.Banners, self).__init__(parent=parent, name='Banners', limit=1)

                self.cb = self.variant('Chapter Banner', 25,)
                self.std = self.variant('Standard of the Emperor Ascendant', 65)

            def get_unique(self):
                return self.description

        def __init__(self, parent):
            super(HonourGuardSquad.HonourGuard, self).__init__(
                parent=parent, points=HonourGuardSquad.model_points, name='Honour Guard',
                gear=Armour.artificer_armour_set + [Gear('Bolt pistol'), Gear('Boltgun')]
            )
            self.weapon = self.Weapon(self)
            self.banners = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    class ChapterChampion(Unit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(HonourGuardSquad.ChapterChampion.Weapon1, self).__init__(parent=parent, name='Weapon')

                self.power = self.variant('Boltgun', 0)
                self.relic = self.variant('Close combat weapon', 0)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(HonourGuardSquad.ChapterChampion.Weapon2, self).__init__(parent=parent, name='')

                self.power = self.variant('Power weapon', 0)
                self.relic = self.variant('Thunder hammer', 15)

        def __init__(self, parent):
            super(HonourGuardSquad.ChapterChampion, self).__init__(
                parent=parent, name='Chapter Champion', points=85 - 2 * HonourGuardSquad.model_points,
                gear=Armour.artificer_armour_set + [Gear('Bolt pistol')]
            )
            self.weapon1 = self.Weapon1(self)
            self.weapon2 = self.Weapon2(self)

    def __init__(self, parent):
        super(HonourGuardSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.ChapterChampion(None))
        self.veterans = UnitList(self, self.HonourGuard, min_limit=2, max_limit=9)
        self.transport = Transport(self)

    def get_count(self):
        return self.veterans.count + 1

    def check_rules(self):
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Honour Guard can take a banner (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), []) + self.transport.get_unique_gear()


class CommandSquad(Unit):
    type_id = 'command_squad_v1'
    type_name = 'Command Squad'

    model_points = 20

    class Veteran(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Options, self).__init__(parent=parent, name='Options')

                self.mbomb = self.variant('Melta bombs', 5)
                self.ss = self.variant('Storm Shield', 10)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Banners, self).__init__(parent=parent, name='Banners', limit=1)

                self.comstd = self.variant('Company Standard', 15)
                self.std = self.variant('Standard of the Emperor Ascendant', 65)

                # Sentinels
                self.staganda = self.variant('The Banner of Staganda', 25)

            def check_rules(self):
                self.staganda.visible = self.staganda.used = self.parent.roster.is_sentinels()

            def get_unique(self):
                if self.std.value:
                    return self.description
                return []

        def __init__(self, parent):
            super(CommandSquad.Veteran, self).__init__(
                parent=parent, name='Veteran', points=CommandSquad.model_points, gear=Armour.power_armour_set)

            class Weapon1(Special, Ranged, Melee, Boltgun, Chainsword):
                pass

            class Weapon2(Special, Ranged, Melee, Boltgun, BoltPistol):
                pass

            self.weapon1 = Weapon1(self, 'Weapon')
            self.weapon2 = Weapon2(self, '')
            self.opt = CommandSquad.Veteran.Options(self)
            self.banners = CommandSquad.Veteran.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    class SpecModels(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.SpecModels, self).__init__(parent, 'Options')
            cham_points = CommandSquad.model_points + 15
            medic_points = CommandSquad.model_points + 15

            self.ch = self.variant('Company Champion', cham_points, gear=UnitDescription(
                name='Company Champion',
                points=cham_points,
                options=Armour.power_armour_set + [
                    Gear('Power weapon'),
                    Gear('Combat Shield'),
                    Gear('Bolt pistol'),
                ]
            ))
            self.ap = self.variant('Apothecary', medic_points, gear=UnitDescription(
                name='Apothecary',
                points=medic_points,
                options=Armour.power_armour_set + [
                    Gear('Chainsword'),
                    Gear('Narthecium'),
                ]
            ))
            self.bikes = self.variant('Bikes', 35)

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=1, max_limit=5, start_value=5)
        self.up = CommandSquad.SpecModels(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.veterans.count + (self.up.count - self.up.bikes.value)

    def check_rules(self):
        self.transport.visible = self.transport.used = not self.up.bikes.value
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Apothecary and Company champion) '
                       '(taken: {})'.format(self.get_count()))
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), []) + self.transport.get_unique_gear()
