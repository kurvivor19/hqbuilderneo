__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.games.wh40k.obsolete.space_marines2008.troops import Rhino,DropPod,Razorback
from builder.core.options import norm_counts
from functools import reduce

class AssaultSquad(Unit):
    name = 'Assault squad'
    gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Chainsword']

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']
        base_points = 100 - 18 * 4

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Storm shield',15,'ssh'],
                ['Plasma pistol',15,'ppist'],
                ['Power weapon',15,'psw'],
                ['Lightning claw',15,'lclaw'],
                ['Power fist',25,'pfist'],
                ['Thunder hammer',30,'ham']
                ]
            self.wep1 = self.opt_one_of('Weapon',
                [['Bolt pistol', 0, 'bpist' ]] + weaponlist)
            self.wep2 = self.opt_one_of('',
                [['Chainsword',0,'chsw']] + weaponlist)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ['Combat shield', 5,'csh']
                ])

    def get_count(self):
        return self.marines.get() + 1

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4, 9, 18)
        self.flame = self.opt_count('Flamer', 0, 1, 0)
        self.pp = self.opt_count('Plasma pistol', 0, 1, 15)
        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())
        self.transport = self.opt_one_of('Options', [
            ['Jump Pack', 0, 'jp'],
            [DropPod.name, 0, 'dp'],
            [Rhino.name, 0, 'rh'],
        ])
        rh = Rhino()
        rh.base_points = 0
        self.rh = self.opt_sub_unit(rh)

        dp = DropPod()
        dp.base_points = 0
        self.dp = self.opt_sub_unit(dp)

    def check_rules(self):
        spec_limit = int(self.get_count() / 5)
        norm_counts(0, spec_limit, [self.flame, self.pp])

        self.rh.set_active(self.transport.get_cur() == 'rh')
        self.dp.set_active(self.transport.get_cur() == 'dp')
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.transport.id])
        self.description.add('Bolt pistol', self.marines.get() - self.flame.get() - self.pp.get())
        if self.transport.get_cur() == 'jp':
            self.description.add(self.transport.get_selected(), self.get_count())


class VanguardVeteranSquad(Unit):
    name = 'Vanguard Veteran Squad'

    class Veteran(ListSubUnit):
        name = 'Veteran'
        base_points = 20
        min = 1
        max = 9
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            ListSubUnit.__init__(self)

            weaponlist = [
                ['Storm shield', 15, 'ss'],
                ['Plasma pistol',15,'ppist'],
                ['Power weapon',15,'psw'],
                ['Lightning claw',15,'lclaw'],
                ['Power fist',25,'pfist'],
                ['Thunder hammer',30,'ham']
                    ]
            self.wep1 = self.opt_one_of('Weapon', [['Chainsword',0,'csw']] + weaponlist)
            self.wep2 = self.opt_one_of('', [['Bolt pistol',0,'bpist']] + weaponlist)
            self.opt = self.opt_options_list('Options',[['Melta bombs',5,'mbomb']])

    class Sergeant(Unit):
        name = 'Space Marine Sergeant'
        base_points = 125 - 4 * 20
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Storm shield', 15, 'ss'],
                ['Plasma pistol',15,'ppist'],
                ['Power weapon',15,'psw'],
                ['Lightning claw',15,'lclaw'],
                ['Power fist',25,'pfist'],
                ['Thunder hammer',30,'ham']
                    ]
            self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol',0,'bpist']] + weaponlist)
            self.wep2 = self.opt_one_of('',[
                ['Power sword',0,'psw'],
                ['Lightning claw',0,'lclaw'],
                ['Power fist',10,'pfist'],
                ['Thunder hammer',15,'ham'],
                ['Relic blade',15,'rsw']
                ])
            self.opt = self.opt_options_list('Options',[['Melta bombs',5,'mbomb']])

    def __init__(self):
        Unit.__init__(self)
        self.vets = self.opt_units_list(self.Veteran.name,self.Veteran,4,9)
        self.leader = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])
        self.ride = self.opt_options_list('Options',[['Jump pack',10,'jp']])

    def get_count(self):
        return self.vets.get_count() + 1

    def check_rules(self):
        self.vets.update_range()
        self.transport.set_active(not self.ride.get('jp'))
        self.set_points(self.build_points(count=1, exclude=[self.ride.id]) + self.ride.get('jp') * self.get_count())
        self.build_description(exclude=[self.ride.id])
        if self.ride.get('jp'):
            self.description.add('Jump Pack', self.get_count())

class LandSpeederSquadron(ListUnit):
    name = "Land Speeder Squadron"

    class LandSpeeder(ListSubUnit):
        name = "Land Speeder"
        base_points = 50
        min = 1
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Heavy Bolter", 0, "bolt"],
                ["Heavy Flamer", 0, "flame"],
                ["Multi-melta", 10, "melta"],
                ])
            self.up = self.opt_options_list('Upgrade', [
                ["Heavy Bolter", 10, "bolt"],
                ["Heavy Flamer", 10, "flame"],
                ["Multi-melta", 20, "melta"],
                ["Assault Cannon", 40, "assault"],
                ["Typhoon Missile Launcher", 40, "tml"],
                ], limit = 1)

        def check_rules(self):
            self.name = "Land Speeder"
            if self.up.get('tml'):
                self.name += ' Typhoon'
            elif self.up.get("bolt") or self.up.get("flame") or self.up.get("melta") or self.up.get("assault"):
                self.name += ' Tornado'
            ListSubUnit.check_rules(self)

    unit_class = LandSpeeder

class LandSpeederStorm(Unit):
    name = "Land Speeder Storm"
    base_points = 50
    gear = ['Jammimg beacon','Cerberus launcher']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Heavy bolter',0,'hbgun'],
            ['Heavy flamer',10,'hflame'],
            ['Multi-melta',15,'mmelta'],
            ['Assault cannon',35,'asscan']
        ])


class AttackBikeSquad(ListUnit):
    name = "Attack Bike squad"

    class AttackBike(ListSubUnit):
        name = "Attack bike"
        base_points = 40
        gear = ['Power armour','Bolt pistol','Frag grenades','Krak grenades','Space Marine bike']
        min = 1
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter',0,'hbgun'],
                ['Multi-melta',10,'mmelta']
            ])

    unit_class = AttackBike


class SpaceMarineBikers(Unit):
    name = 'Space Marine Bike squad'
    gear = ['Power armour', 'Frag grenades', 'Krak grenades','Space Marine bike']

    class BikerSergeant(Unit):
        name = 'Biker Sergeant'
        gear = ['Power armour', 'Frag grenades', 'Krak grenades','Space Marine bike']
        base_points = 90 - 25 * 2
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Bolt pistol', 0, 'bpist' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
                ])
            self.opt = self.opt_options_list('Options',[["Melta bombs", 5, 'mbomb']])

    class AttackBike(Unit):
        name = "Attack bike"
        base_points = 40
        gear = ['Power armour','Bolt pistol','Frag grenades','Krak grenades','Space Marine bike']
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter',0,'hbgun'],
                ['Multi-melta',10,'mmelta']
            ])
        # def get_count(self):
            # TODO: this case point error for attak bike price
            #attack bike is counted as 2 models for the purposes of dividing into combat squads
            # return 2
    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Space Marine Biker',2,7,25)
        self.flame = self.opt_count('Flamer', 0, 2, 0)
        self.melta = self.opt_count('Meltagun', 0, 2, 5)
        self.plasma = self.opt_count('Plasma gun', 0, 2, 10)

        self.sergeant = self.opt_sub_unit(self.BikerSergeant())
        self.abike = self.opt_optional_sub_unit('', [self.AttackBike()])
        self.spec = [self.flame, self.plasma, self.melta]

    def get_count(self):
        return self.bikers.get() + self.abike.get_count() + self.sergeant.get_count()

    def check_rules(self):
        norm_counts(0, 2, self.spec)
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.bikers.id], count=self.bikers.get())
        self.description.add('Bolt pistol', self.bikers.get() - reduce(lambda val, c: val + c.get(), self.spec, 0))

class ScoutBikers(Unit):
    name = 'Scout Bike squad'
    gear = ['Scout armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades','Space Marine bike']

    class BikerSergeant(Unit):
        name = 'Scout Biker Sergeant'
        gear = ['Scout armour', 'Frag grenades', 'Krak grenades','Space Marine bike']
        base_points = 70 - 2 * 20

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Bolt pistol', 0, 'bpist' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
                ])
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ['Locator beacon',25,'lbcn']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Scout Biker',2, 9, 20)
        self.grenades = self.opt_count('Astartes grenade launcher', 0, 3, 10)
        self.opt = self.opt_options_list('Options',[['Cluster mines',10,'cmines']])
        self.sergeant = self.opt_sub_unit(self.BikerSergeant())

    def check_rules(self):
        norm_counts(0, min(self.bikers.get(), 3), [self.grenades])
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.bikers.id, self.opt.id], count=self.bikers.get())
        self.description.add(self.opt.get_selected())


class Stormtalon(Unit):
    name = 'Stormtalon Gunship'
    base_points = 110
    gear = ['Ceramite plating','Twin-linked assault cannon']
    def __init__(self):
        Unit.__init__(self)
        self.weapon = self.opt_one_of('Weapon',[
            ['Twin-linked heavy bolter',0,'tlhb'],
            ['Skyhammer missile launcher',15,'shml'],
            ['Twin-linked lascannon',30,'tllcan'],
            ['Typhoon missile launcher',35,'tml']
        ])
