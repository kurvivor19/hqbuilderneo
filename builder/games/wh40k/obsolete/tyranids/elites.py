__author__ = 'dante'

from builder.core.unit import Unit, StaticUnit


class MyceticSpore(Unit):
    name = "Mycetic Spore"
    base_points = 40
    gear = ['Hardened carapace', 'Lash whip', 'Ripper tentacles']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ["Stinger salvo", 10],
            ["Cluster spines", 10],
            ['Twin-linked Deathspitter', 10],
            ['Barbed strangler', 15],
            ['Venom cannon', 20],
        ], limit=1)


class HiveGuard(Unit):
    name = 'Hive Guard Brood'
    gear = ['Claws and teeth', 'Impaler cannon', 'Hardened carapace']
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Guards', 1, 3, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class Lictor(Unit):
    name = 'Lictor Brood'
    gear = ['Chameleonic skin', 'Flesh hooks', 'Reinforced chitin', 'Rending claws', 'Scything Talons']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Lictor', 1, 3, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class Deathleaper(StaticUnit):
    name = 'Deathleaper'
    base_points = 140
    gear = ['Chameleonic skin', 'Flesh hooks','Reinforced chitin', 'Scything Talons', 'Rending claws']


class Venomethrope(Unit):
    name = 'Venomethrope Brood'
    base_points = 55
    gear = ['Lash whips', 'Reinforced chitin', 'Toxin miasma']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Venomethrope', 1, 3, self.base_points)
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore_unit.set_active(self.spore.get('ms'))
        self.points.set(self.count.points() + self.spore_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class Zoanthrope(Unit):
    name = 'Zoanthrope Brood'
    base_points = 60
    gear = ['Claws and teeth', 'Reinforced chitin']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Zoanthrope', 1, 3, self.base_points)
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())
    def check_rules(self):
        self.spore_unit.set_active(self.spore.get('ms'))
        self.points.set(self.count.points() + self.spore_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class Pyrovore(Unit):
    name = 'Pyrovore Brood'
    base_points = 45
    gear = ['Acid maw', 'Acid blood', 'Flamespurt', 'Claws and teeth', 'Hardened carapace']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Pyrovore', 1, 3, self.base_points)
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore_unit.set_active(self.spore.get('ms'))
        self.points.set(self.count.points() + self.spore_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class TheDoom(Unit):
    name = "The Doom of Malan'tai"
    base_points = 90
    gear = ['Claws and teeth', 'Reinforced chitin']

    def __init__(self):
        Unit.__init__(self)
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore_unit.set_active(self.spore.get('ms'))
        self.points.set(self.base_points + self.spore_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear)
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class Ymgarl(Unit):
    name = 'Ymgarl Genestealer Brood'
    gear = ['Rending claws', 'Hardened carapace']
    base_points = 23

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Genestealer', 5, 10, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])
