__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.chaos_marines.armory import melee, ranged, ranged_ids, tda_melee, tda_ranged
from builder.games.wh40k.obsolete.chaos_marines.troops import Rhino, IconBearer, VeteranOfLongWar
from builder.games.wh40k.obsolete.chaos_marines.heavy import LandRaider
from builder.core.model_descriptor import ModelDescriptor


class Chosen(Unit):
    name = 'Chosen'

    class SingleChosen(IconBearer):
        name = 'Chosen'
        base_points = 18
        spec_list = ['flame', 'mgun', 'pgun']
        combi_list = ['cbgun', 'cflame', 'cpgun', 'cmgun']
        pistol_list = ['ppist']
        heavy_list = ['acan', 'mlaunch', 'lcannon', 'hbgun']
        ccw_list = ['pwep', 'lclaw', 'pfist']

        def __init__(self):
            IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=35, vengeance=25)
            self.count = self.opt_count(self.name, 1, 9, self.base_points)
            self.opt = self.opt_options_list('Special weapon', [['Pair of lightning claws', 30, 'lclawp']])
            self.rng = self.opt_one_of('', [
                ['Boltgun', 0, 'bgun'],
                ['Combi-bolter', 3, 'cbgun'],
                ['Flamer', 5, 'flame'],
                ['Combi-melta', 10, 'cmgun'],
                ['Combi-flamer', 10, 'cflame'],
                ['Combi-plasma', 10, 'cpgun'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun'],
            ])
            self.side = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist'],
                ['Plasma pistol', 15, 'ppist']
            ])
            self.ccw = self.opt_one_of('', [
                ['Close combat weapon', 0, 'ccw'],
                ['Power weapon', 15, 'pwep'],
                ['Lightning claw', 15, 'lclaw'],
                ['Power fist', 25, 'pfist']
            ])
            self.heavy = self.opt_one_of('Heavy weapon', [
                ['Boltgun', 0, 'bgun'],
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun'],
                ['Heavy bolter', 10, 'hbgun'],
                ['Autocannon', 10, 'acan'],
                ['Missile launcher', 15, 'mlaunch'],
                ['Lascannon', 20, 'lcannon']
            ])
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades']

        @staticmethod
        def is_default(opt, opt_id):
            return opt.get_cur() == opt_id or not opt.is_used()

        def has_heavy(self):
            return self.count.get() if self.heavy.get_cur() in self.spec_list + self.heavy_list else 0

        def has_spec(self):
            def check_spec():
                count = 0
                if self.rng.get_cur() in self.spec_list + self.combi_list:
                    count += 1
                if self.ccw.get_cur() in self.ccw_list:
                    count += 1
                if self.side.get_cur() in self.pistol_list:
                    count += 1
                if self.opt.get('lclawp'):
                    count += 1
                return count
            return self.count.get() * check_spec()

        def check_rules(self):
            has_claws = self.opt.get('lclawp')
            can_use_heavy = self.is_default(self.rng, 'bgun') and not has_claws

            self.heavy.set_visible(can_use_heavy)
            self.opt.set_active(not self.has_heavy() and self.is_default(self.rng, 'bgun') and
                                self.is_default(self.ccw, 'ccw') and self.is_default(self.side, 'bpist'))
            self.rng.set_visible(not self.has_heavy() and not has_claws)
            self.ccw.set_visible(not has_claws)
            self.side.set_visible(not has_claws)

            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            exclude = [self.count.id]
            if self.heavy.get_cur() == 'bgun' and self.rng.get_cur() == 'bgun':
                exclude.append(self.heavy.id)
            self.build_description(points=single_points, count=1, exclude=exclude)

    class Champ(IconBearer):
        name = 'Chosen Champion'
        base_points = 90 - 18 * 4
        ranged_ids = [item[2] for item in ranged]

        def __init__(self):
            IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=35, vengeance=25)
            self.wep1 = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.wep3 = self.opt_one_of('', [['Boltgun', 0, 'bgun']] + ranged + [
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun'],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift']
            ])
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades']

        def has_spec(self):
            return 1 if self.wep3.get_cur() in ['flame', 'mgun', 'pgun'] else 0

        def check_rules(self):
            IconBearer.check_rules(self)
            #any weapon can be switched for ranged, but oly single ranged weapon can be taken
            ranged_flag1 = self.wep1.get_cur() in self.ranged_ids
            ranged_flag2 = self.wep2.get_cur() in self.ranged_ids
            ranged_flag3 = self.wep3.get_cur() in self.ranged_ids
            self.wep1.set_active_options(self.ranged_ids, not(ranged_flag2 or ranged_flag3))
            self.wep2.set_active_options(self.ranged_ids, not(ranged_flag1 or ranged_flag3))
            self.wep3.set_active_options(self.ranged_ids, not(ranged_flag2 or ranged_flag1))
            IconBearer.check_rules(self)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(self.Champ())
        self.warriors = self.opt_units_list(self.SingleChosen.name, self.SingleChosen, 4, 9)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 2, 'lwvet']
            ])
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne',  2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 3, 'ng'],
            ['Mark of Slaanesh', 2, 'sl']
        ], limit=1)
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def get_count(self):
        return self.warriors.get_count() + 1

    def check_rules(self):
        self.warriors.update_range()
        icon = sum([u.set_icon(self.marks) for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Chosen can't take more then 1 icon (taken: {0}).".format(icon))

        spec = sum((u.has_spec() for u in [self.ldr.get_unit()] + self.warriors.get_units()))
        if spec > 4:
            self.error("Only 4 models (chosen and champion) can take special weapons (taken: {0}).".format(spec))
        heavy = sum((u.has_heavy() for u in self.warriors.get_units()))
        if heavy > 1:
            self.error("Only one chosen can exchange his bolter for heavy weapon.")

        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        ((self.opt.points() if not self.black_legion else 2) + self.marks.points()) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')


class Possessed(IconBearer):
    name = "Possessed"
    base_gear = ['Power armour', 'Close combat weapon']
    base_points = 26

    class Champ(IconBearer):
        name = "Possessed Champion"
        base_points = 130 - 26 * 4

        def __init__(self):
            IconBearer.__init__(self, wrath=15, flame=5, despair=5, excess=35, vengeance=5)
            self.gifts = self.opt_count('Gift of mutation', 0, 2, 10)
            self.gear = ['Close combat weapon', 'Power armour']

    def __init__(self, black_legion=False):
        self.black_legion = black_legion
        IconBearer.__init__(self, wrath=15, flame=5, despair=5, excess=35, vengeance=5)
        self.count = self.opt_count(self.name, 4, 19, self.base_points)
        self.ldr = self.opt_sub_unit(self.Champ())
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 2, 'lwvet']
            ])
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 3, 'kh'],
            ['Mark of Tzeentch', 5, 'tz'],
            ['Mark of Nurgle', 4, 'ng'],
            ['Mark of Slaanesh', 3, 'sl']
        ], limit=1)
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def check_rules(self):
        soldier_icons = (1 if self.set_icon(self.marks) > 1 else 0)
        icon = self.ldr.get_unit().set_icon(self.marks) + soldier_icons
        if icon > 1:
            self.error("Possessed can't take more then 1 icon (taken: {0}).".format(icon))
        self.set_points(self.base_points * self.count.get() +
                        self.icons.points() + self.ldr.points() + self.transport.points() +
                        ((self.opt.points() if not self.black_legion else 2) + self.marks.points()) * self.get_count())
        self.build_description(options=[self.ldr, self.marks], count=1)
        poss = ModelDescriptor(self.name, points=self.base_points, gear=self.base_gear)
        count = self.count.get()
        if soldier_icons:
            self.description.add(poss.clone().add_gear_opt(self.icons).build(1))
            count -= 1
        self.description.add(poss.build(count=count))
        self.description.add(self.transport.get_selected())
        self.description.add('Veterans of the Long War' if self.black_legion else self.opt.get_selected())

    def get_count(self):
        return self.count.get() + 1


class Terminators(Unit):
    name = 'Chaos Terminators'

    class Terminator(IconBearer):
        name = 'Chaos Terminator'
        base_points = 31

        def __init__(self):
            IconBearer.__init__(self, wrath=25, flame=15, despair=10, excess=40, vengeance=35)
            self.gear = ['Terminator armour']
            self.count = self.opt_count(self.name, 1, 9, self.base_points)
            self.ccw = self.opt_one_of('Weapon', [
                ['Power weapon', 0, 'pwep'],
                ['Lightning claw', 3, 'lclaw'],
                ['Power fist', 7, 'pfist'],
                ['Chainfist', 12, 'chfist']
            ])
            self.rng = self.opt_one_of('', [
                ['Combi-bolter', 0, 'cbgun'],
                ['Combi-melta', 5, 'cmgun'],
                ['Combi-flamer', 5, 'cflame'],
                ['Combi-plasma', 5, 'cpgun'],
                ['Heavy flamer', 10, 'hflame'],
                ['Reaper autocannon', 25, 'rpr']
            ])
            self.opt = self.opt_options_list('', [
                ['Pair of lightning claws', 7, 'lclawp']
            ])
            self.ccw_ids = ['lclaw', 'pfist', 'chfist']
            self.rng_ids = ['cmgun', 'cflame', 'cpgun']

        def has_spec(self):
            return self.count.get() if self.rng.get_cur() in ['hflame', 'rpr'] else 0

        def check_rules(self):
            self.opt.set_active(self.ccw.get_cur() not in self.ccw_ids and self.rng.get_cur() not in self.rng_ids)
            self.rng.set_visible(not self.opt.get('lclawp'))
            self.ccw.set_visible(not self.opt.get('lclawp'))

            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])

    class Champion(IconBearer, VeteranOfLongWar):
        name = 'Terminator Champion'
        base_points = 95 - 31 * 2

        def __init__(self):
            IconBearer.__init__(self, wrath=25, flame=15, despair=10, excess=40, vengeance=35)
            self.gear = ['Terminator armour']
            self.ccw = self.opt_one_of('Weapon', [['Power weapon', 0, 'pwep']] + tda_melee)
            self.rng = self.opt_one_of('', [['Combi-bolter', 0, 'cbgun']] + tda_ranged)
            self.opt = self.opt_options_list('Options', [
                ['Gift of mutation', 10, 'gift']
            ])

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(self.Champion())
        self.warriors = self.opt_units_list(self.Terminator.name, self.Terminator, 2, 9)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 3, 'lwvet']
            ])
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 3, 'kh'],
            ['Mark of Tzeentch', 5, 'tz'],
            ['Mark of Nurgle', 6, 'ng'],
            ['Mark of Slaanesh', 4, 'sl']
        ], limit=1)
        if self.black_legion:
            self.desp = self.opt_options_list('', [
                ['The Bringers of Despair', 6, 'bod']
            ])
        self.transport = self.opt_optional_sub_unit('Transport', LandRaider())

    def get_count(self):
        return self.warriors.get_count() + 1

    def check_rules(self):
        self.warriors.update_range()

        if self.black_legion:
            self.desp.set_active(self.get_roster().has_abaddon())

        icon = sum([u.set_icon(self.marks) for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Chaos Terminators can't take more then 1 icon (taken: {0}).".format(icon))

        spec_lim = int(self.get_count() / 5)
        taken = sum((u.has_spec() for u in self.warriors.get_units()))
        if taken > spec_lim:
            self.error("Chaos Terminators can't have more then 1 special weapon for 5 terminators (taken {}).".
                       format(taken))
        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        ((self.opt.points() if not self.black_legion else 2) + self.marks.points()) * self.get_count() +
                        (self.desp.points() if self.black_legion else 0) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_unique(self):
        if self.black_legion:
            return self.desp.get_selected()


class Hellbrute(Unit):
    name = 'Helbrute'
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        power_list = [
            ['Power fist', 0, 'pfist'],
            ['Thunder hammer', 5, 'tham'],
            ['Power scourge', 10, 'psc'],
            ['Missile launcher', 10, 'mis']
        ]
        self.wep1 = self.opt_one_of('Weapon', power_list)
        self.opt1 = self.opt_options_list('Built-in weapon', [
            ['Combi-bolter', 5, 'cbgun'],
            ['Heavy flamer', 15, 'hflame']
        ], limit=1)
        self.wep2 = self.opt_one_of('', [['Multi-melta', 0, 'mmgun']] + power_list + [
            ['Twin-linked heavy bolter', 5, 'tlhbgun'],
            ['Reaper autocannon', 5, 'racan'],
            ['Plasma cannon', 10, 'pcan'],
            ['Twin-linked lascannon', 25, 'tllcan']
        ])
        self.opt2 = self.opt_options_list('Built-in weapon', [
            ['Combi-bolter', 5, 'cbgun'],
            ['Heavy flamer', 15, 'hflame']
        ], limit=1)

    def check_rules(self):
        self.opt1.set_visible(self.wep1.get_cur() == 'pfist')
        self.opt2.set_visible(self.wep2.get_cur() == 'pfist')
        self.wep1.set_active_options(['mis'], self.wep2.get_cur() != 'mis')
        self.wep2.set_active_options(['mis'], self.wep1.get_cur() != 'mis')
        Unit.check_rules(self)


class Mutilators(Unit):
    name = "Mutilators"
    base_gear = ['Fleshmetal']
    base_points = 55

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.count = self.opt_count('Mutilator', 1, 3, self.base_points)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 5, 'kh'],
            ['Mark of Tzeentch', 8, 'tz'],
            ['Mark of Nurgle', 6, 'ng'],
            ['Mark of Slaanesh', 6, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 4, 'lwvet']
            ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]) + (4 * self.get_count() if self.black_legion else 0))
        self.build_description(exclude=[self.count.id], count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')
        self.description.add(ModelDescriptor('Mutilator', self.get_count(), self.base_points, self.base_gear).build())


class Berzerks(Unit):
    name = 'Khorne Berzerkers'
    gear = ['Mark of Khorne']

    class Berserk(ListSubUnit):
        name = 'Khorne Berzerker'
        base_points = 19

        def __init__(self):
            ListSubUnit.__init__(self)
            self.icons = self.opt_options_list('Chaos Icon', [['Icon of wrath', 15, 'wrath']])
            self.opt = self.opt_options_list('Weapon', [['Chainaxe', 3, 'chaxe']])
            self.side = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist'],
                ['Plasma pistol', 15, 'ppist']
            ])
            self.gear = ['Power armour', 'Close combat weapon', 'Frag grenades', 'Krak grenades']

        def has_icon(self):
            return self.get_count() if self.icons.get('wrath') else 0

        def has_spec(self):
            return self.get_count() if self.side.get_cur() == 'ppist' else 0

    class Champion(Unit):
        name = 'Berzerker Champion'
        base_points = 105 - 19 * 4

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.wep3 = self.opt_options_list('', [['Chainaxe', 3, 'chaxe']])
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift'],
                ['Icon of wrath', 15, 'wrath']
            ])
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades']

        def has_icon(self):
            return self.get_count() if self.opt.get('wrath') else 0

        def check_rules(self):
            self.wep1.set_active_options(ranged_ids, self.wep2.get_cur() not in ranged_ids)
            self.wep2.set_active_options(ranged_ids, self.wep1.get_cur() not in ranged_ids)
            Unit.check_rules(self)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(self.Champion())
        self.warriors = self.opt_units_list(self.Berserk.name, self.Berserk, 4, 19)
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 1, 'lwvet']
            ])

    def check_rules(self):
        self.warriors.update_range()
        taken = sum((it.has_spec() for it in self.warriors.get_units()))
        if taken > 2:
            self.error("Khorne Berzerkers can't have more then 2 plasma pistols in unit (taken: {0}).".format(taken))
        icon = sum([u.has_icon() for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Khorne Berzerkers can't take more then 1 icon (taken: {0}).".format(icon))
        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        (self.opt.points() if not self.black_legion else 1) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1


class ThousandSons(Unit):
    name = "Thousand Sons"
    gear = ['Veterans of the Long War', 'Mark of Tzeentch']
    base_gear = ['Power armour', 'Boltgun', 'Aura of Dark Glory', 'Infernno bolts']
    base_points = 23

    class Champ(Unit):
        name = "Aspiring Sorcerer"
        gear = ['Power armour', 'Bolt pistol', 'Force weapon', 'Aura of Dark Glory', 'Infernno bolts']
        base_points = 150 - 23 * 4

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift'],
                ['Icon of Flame', 15, 'flame']
            ])

        def has_icon(self):
            return 1 if self.opt.get('flame') else 0

    def __init__(self):
        Unit.__init__(self)
        self.ldr = self.opt_sub_unit(self.Champ())
        self.warriors = self.opt_count('Thousand Son', 4, 19, 23)
        self.opt = self.opt_options_list('Options', [['Icon of Flame', 15, 'flame']])
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def check_rules(self):
        if self.ldr.get_unit().has_icon() and self.opt.get('flame'):
            self.error("Thousand Sons can't take more then 1 icon.")
        self.set_points(self.ldr.points() + self.warriors.points() + self.opt.points() + self.transport.points())
        self.build_description(options=[self.ldr], count=1)
        ts = ModelDescriptor("Thousand Son", points=self.base_points, gear=self.base_gear)
        count = self.warriors.get()
        if self.opt.get('flame'):
            self.description.add(ts.clone().add_gear_opt(self.opt).build(1))
            count -= 1
        self.description.add(ts.build(count=count))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return 1 + self.warriors.get()


class PlagueMarines(Unit):
    name = 'Plague Marines'
    gear = ['Mark of Nurgle']

    class Fatty(ListSubUnit):
        name = 'Plague Marine'
        base_points = 24

        def __init__(self):
            ListSubUnit.__init__(self)
            self.side = self.opt_one_of('Weapon', [
                ['Bolt pistol',  0, 'bpist'],
                ['Plasma pistol', 15, 'ppist']
            ])
            self.wep = self.opt_one_of('', [
                ['Boltgun', 0, 'bgun'],
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun']
            ])
            self.icons = self.opt_options_list('Chaos Icon', [['Icon of despair', 10, 'despair']])
            self.gear = ['Power armour', 'Plague knife', 'Blight grenades', 'Krak grenades']

        def has_spec(self):
            return self.get_count() if self.side.get_cur() != 'bpist' or self.wep.get_cur() != 'bgun' else 0

        def has_icon(self):
            return self.get_count() if self.icons.get('despair') else 0

        def check_rules(self):
            self.side.set_active_options(['ppist'], self.wep.get_cur() == 'bgun')
            self.wep.set_active_options(['flame', 'mgun', 'pgun'], self.side.get_cur() == 'bpist')
            ListSubUnit.check_rules(self)

    class Champion(Unit):
        name = 'Plague Champion'
        base_points = 120 - 24 * 4

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Plague knife', 0, 'ccw']] + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.wep3 = self.opt_one_of('', [['Boltgun', 0, 'bgun']] + ranged, id='ac_wep3')
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift'],
                ['Icon of despair', 10, 'despair']
            ])
            self.gear = ['Power armour', 'Blight grenades', 'Krak grenades']

        def has_icon(self):
            return self.get_count() if self.opt.get('despair') else 0

        def check_rules(self):
            #any weapon can be switched for ranged, but oly single ranged weapon can be taken
            flag1 = self.wep1.get_cur() in ranged_ids
            flag2 = self.wep2.get_cur() in ranged_ids
            flag3 = self.wep3.get_cur() in ranged_ids
            self.wep1.set_active_options(ranged_ids, not(flag2 or flag3))
            self.wep2.set_active_options(ranged_ids, not(flag1 or flag3))
            self.wep3.set_active_options(ranged_ids, not(flag2 or flag1))
            Unit.check_rules(self)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(self.Champion())
        self.warriors = self.opt_units_list(self.Fatty.name, self.Fatty, 4, 19)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [['Veterans of the Long War', 1, 'lwvet']])
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def check_rules(self):
        self.warriors.update_range()
        taken = sum((it.has_spec() for it in self.warriors.get_units()))
        if taken > 2:
            self.error("Plague Marines can't have more then 2 special weapon in unit (taken: {0}).".format(taken))
        icon = sum([u.has_icon() for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Plague Marines can't take more then 1 icon (taken: {0}).".format(icon))
        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        (self.opt.points() if not self.black_legion else 1) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1


class NoiseMarines(Unit):
    name = 'Noise Marines'
    gear = ['Mark of Slaanesh']

    class Pervy(ListSubUnit):
        name = 'Noise Marine'
        base_points = 17

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun'],
                ['Close combat weapon', 0, 'ccw'],
                ['Sonic blaster', 3, 'sblast'],
                ['Blastmaster', 30, 'blastm']
            ])
            self.ccw = self.opt_options_list('', [['Close combat weapon', 1, 'ccw']])
            self.icons = self.opt_options_list('Chaos Icon', [['Icon of excess', 30, 'excess']])
            self.gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

        def has_spec(self):
            return self.get_count() if self.wep.get_cur() == 'blastm' else 0

        def has_icon(self):
            return self.get_count() if self.icons.get('excess') else 0

        def check_rules(self):
            self.wep.set_active_options(['ccw', 'sblast'], not self.ccw.get('ccw'))
            self.ccw.set_active(not(self.wep.get_cur() == 'ccw' or self.wep.get_cur() == 'sblast'))
            ListSubUnit.check_rules(self)

    class Champion(Unit):
        name = 'Noise Champion'
        base_points = 95 - 17 * 4

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Boltgun', 0, 'bgun']] + ranged)
            self.wep3 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift'],
                ['Doom siren', 15, 'dsyr'],
                ['Icon of excess', 30, 'excess'],
            ])
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades']

        def has_icon(self):
            return self.get_count() if self.opt.get('excess') else 0

        def check_rules(self):
            #any weapon can be switched for ranged, but oly single ranged weapon can be taken
            self.wep3.set_active(True)
            self.wep3.set_visible(True)
            flag1 = self.wep1.get_cur() in ranged_ids
            flag2 = self.wep2.get_cur() in ranged_ids
            flag3 = self.wep3.get_cur() in ranged_ids
            self.wep1.set_active_options(ranged_ids, not (flag2 or flag3))
            self.wep2.set_active_options(ranged_ids, not (flag1 or flag3))
            self.wep3.set_active_options(ranged_ids, not(flag2 or flag1))
            Unit.check_rules(self)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(self.Champion())
        self.warriors = self.opt_units_list(self.Pervy.name, self.Pervy, 4, 19)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [['Veterans of the Long War', 1, 'lwvet']])
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def check_rules(self):
        self.warriors.update_range()
        spec_lim = 1 if self.get_count() < 10 else 2
        taken = sum((it.has_spec() for it in self.warriors.get_units()))
        if taken > spec_lim:
            self.error("Noise Marines can't take more then {0} blastmaster (taken: {1})".format(spec_lim, taken))
        icon = sum([u.has_icon() for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Noise Marines can't take more then 1 icon (taken: {0}).".format(icon))
        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        (self.opt.points() if not self.black_legion else 1) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1
