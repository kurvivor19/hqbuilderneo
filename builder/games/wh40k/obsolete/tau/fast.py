__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.tau.armory import *
from builder.games.wh40k.obsolete.tau.troops import Devilfish


class GunDrones(Unit):
    name = 'Drone squadron'

    def __init__(self):
        Unit.__init__(self)
        self.drones = [
            self.opt_count('Gun Drone', 4, 12, 14),
            self.opt_count('Marker Drone', 0, 12, 14),
            self.opt_count('Shield Drone', 0, 12, 14),
        ]

    def get_count(self):
        return sum((dr.get() for dr in self.drones))

    def check_rules(self):
        norm_counts(4, 12, self.drones)
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)


class Pathfinders(Unit):
    name = 'Pathfinder team'

    class Shasui(Unit):
        name = 'Shas\'ui'
        base_points = 21
        base_gear = ['Pulse carbine', 'Markerlight', 'Recon armour', 'Photon grenades']

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options', [
                ['Blacksun filter', 1]
            ])
            self.drones = [self.opt_count(tp[0], 0, 2, tp[1]) for tp in drones]

        def set_grenades(self, val):
            self.gear = self.base_gear + (['EMP grenades'] if val else [])
            norm_counts(0, 2, self.drones)
            self.base_points = 21 + (2 if val else 0)
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.leader = self.opt_optional_sub_unit('Team leader', self.Shasui())
        self.team = self.opt_count('Pathfinder', 4, 10, 11)
        self.ion = self.opt_count('Ion rifle', 0, 3, 10)
        self.rail = self.opt_count('Rail rifle', 0, 3, 15)

        self.enclaves = enclaves
        if self.enclaves:
            self.opt = self.opt_options_list('Options', [
                ['EMP grenades', 2, 'emp'],
            ])
        else:
            self.opt = self.opt_options_list('Options', [
                ['EMP grenades', 2, 'emp'],
                ['Bonding knife ritual', 1, 'bk'],
            ])
        self.drones = self.opt_options_list('Drones', [
            ['Recon Drone', 28],
            ['Grav-inhibitor Drone', 15],
            ['Pulse accelerator Drone', 15],
        ])
        self.transport = self.opt_optional_sub_unit('Transport', Devilfish())

    def check_rules(self):
        leader = self.leader.get_count()
        self.team.update_range(4 - leader, 10 - leader)
        norm_counts(0, 3, [self.ion, self.rail])
        if leader:
            self.leader.get_unit().get_unit().set_grenades(self.opt.get('emp'))
        if self.enclaves:
            self.set_points(self.build_points(exclude=[self.opt.id], count=1) +
                            (self.opt.points() + 1) * self.team.get())
        else:
            self.set_points(self.build_points(exclude=[self.opt.id], count=1) + self.opt.points() * self.team.get())
        self.build_description(
            exclude=[self.team.id, self.ion.id, self.rail.id, self.transport.id, self.opt.id],
            count=1,
            gear=['Bonding knife ritual'] if self.enclaves or self.opt.get('bk') else []
        )
        desc = ModelDescriptor('Pathfinder', gear=['Recon armour', 'Photon grenades'], points=11)
        if self.opt.get('emp'):
            desc.add_gear('EMP grenades', points=2)
        self.description.add(desc.clone().add_gear('Ion rifle', points=10).build(self.ion.get()))
        self.description.add(desc.clone().add_gear('Rail rifle', points=15).build(self.rail.get()))
        self.description.add(
            desc.clone().add_gear('Pulse carbine').add_gear('Markerlight')
            .build(self.team.get() - self.rail.get() - self.ion.get())
        )
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.team.get() + self.leader.get_count()


class EnclavesPathfinders(Pathfinders):
    def __init__(self):
        Pathfinders.__init__(self, enclaves=True)


class PiranhaTeam(Unit):
    name = 'Piranhas'

    class Piranha(ListSubUnit):
        name = 'Pirahna'
        base_points = 40
        gear = ['Gun Drone', 'Gun Drone']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=5)
            self.wep = self.opt_one_of('Weapon', [
                ['Burst cannon', 0, 'bcan'],
                ['Fusion blaster', 10, 'fblast']
            ])
            self.mis = self.opt_count('Seeker missile', 0, 2, 8)
            self.opt = self.opt_options_list('Options', vehicle)

    def __init__(self):
        Unit.__init__(self)
        self.units = self.opt_units_list(self.Piranha.name, self.Piranha, 1, 5)

    def check_rules(self):
        self.units.update_range()
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.units.get_count()


class Vespids(Unit):
    name = 'Vespid Stingwings'

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_count('Vespid Stingwing', 4, 12, 18)
        self.leader = self.opt_options_list('', [['Vespid Stain Leader', 28, 'ldr']])

    def check_rules(self):
        leader = 1 if self.leader.get('ldr') else 0
        self.team.update_range(4 - leader, 12 - leader)
        self.points.set(self.build_points(count=1))
        self.build_description(count=1, options=[])
        self.description.add(ModelDescriptor(name='Vespid Stain Leader', points=28,
                                             gear=['Neutron blaster', 'Combat armour']).build(leader))
        self.description.add(ModelDescriptor(name='Vespid Stingwing', points=18,
                                             gear=['Neutron blaster', 'Combat armour']).build(self.team.get()))

    def get_count(self):
        return self.team.get() + (1 if self.leader.get('ldr') else 0)


class SunShark(Unit):
    name = 'Sun Shark Bomber'
    base_points = 160
    gear = ['Pulse bomb generator', 'Networked markerlight', 'Seeker missile', 'Seeker missile']

    def __init__(self):
        Unit.__init__(self)
        self.opt_one_of('Weapon', [
            ['Missile pod', 0],
            ['Twin-linked missile pod', 5],
        ])
        self.opt_options_list('Options', vehicle)

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Interceptor Drone', gear=['Twin-linked ion rifle'], count=2).build())


class RazorShark(Unit):
    name = 'Razorshark Strike Fighter'
    base_points = 145
    gear = ['Quad ion turret', 'Seeker missile', 'Seeker missile']

    def __init__(self):
        Unit.__init__(self)

    def __init__(self):
        Unit.__init__(self)
        self.opt_one_of('Weapon', [
            ['Burst cannon', 0],
            ['Missile pod', 5],
        ])
        self.opt_options_list('Options', vehicle)
