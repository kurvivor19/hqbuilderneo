from builder.core.options import norm_counts
from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.tau.armory import weapon, crisis_support, drones, signature, tl_ids

__author__ = 'dante'


class Bodyguards(Unit):
    name = 'XV8 Crisis bodyguard team'

    class CrisisSuit(ListSubUnit):
        base_points = 32
        name = 'Crisis Bodyguard'
        gear = ['Crisis battlesuit']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_options_list('Ranged weapon', weapon)
            self.support = self.opt_options_list('Support system', crisis_support)
            self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]
            self.signature = self.opt_options_list('Signature system', signature)

        def check_rules(self):
            norm_counts(0, 2, self.drones)
            nodes = len(self.wep.get_all()) + len(self.support.get_all())
            for tl in tl_ids:
                if self.wep.get(tl):
                    nodes += 1
            if nodes > 3:
                self.error(self.name + ' may take up to 3 items from Ranged weapon and Support systems lists. '
                                       '(taken {0})'.format(nodes))
            ListSubUnit.check_rules(self)

        def has_special(self, sp_id):
            return self.get_count() if self.signature.get(sp_id) or self.wep.get(sp_id) else 0

    def __init__(self, max_models=2):
        Unit.__init__(self)
        self.team = self.opt_units_list('Shas\'ui', self.CrisisSuit, 1, max_models)
        self.ritual = self.opt_options_list('Options', [
            ['Bonding knife ritual', 2, 'up']
        ])

    def check_rules(self):
        self.team.update_range()
        self.set_points(self.build_points(count=1, exclude=[self.ritual.id]) + self.ritual.points() * self.get_count())
        self.build_description(count=1)

    def get_count(self):
        return self.team.get_count()

    def has_special(self, sp_id):
        result = 0
        for member in self.team.get_units():
            result += member.has_special(sp_id)
        return result


class Farsight(Unit):
    unique = True
    name = 'Commander Farsight'
    base_points = 165
    gear = ['Crisis battlesuit', 'Shield generator', 'Plasma rifle', 'Dawn blade']

    def __init__(self):
        Unit.__init__(self)
        self.bodyguards = self.opt_optional_sub_unit('', Bodyguards(max_models=7))

    def has_special(self, sp_id):
        if self.bodyguards.get_unit():
            return self.bodyguards.get_unit().get_unit().has_special(sp_id)
        return 0


class Shadowsun(Unit):
    unique = True
    name = 'Commander Shadowsun'
    base_points = 135
    gear = ['XV22 Stealth battlesuit',
            'Advanced targeting system',
            'Fusion blaster',
            'Fusion blaster']

    def __init__(self):
        Unit.__init__(self)
        self.opt_count('Command link drone', 0, 1, 20)
        self.opt_count('MV52 Shield drone', 0, 2, 20)
        self.bodyguards = self.opt_optional_sub_unit('', Bodyguards(max_models=2))

    def has_special(self, sp_id):
        if self.bodyguards.get_unit():
            return self.bodyguards.get_unit().get_unit().has_special(sp_id)
        return 0


class Commander(Unit):
    base_points = 85
    name = 'Commander'
    gear = ['Crisis battlesuit']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Ranged weapon', weapon)
        self.support = self.opt_options_list('Support system', crisis_support)
        self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]
        self.signature = self.opt_options_list('Signature system', signature)
        self.bodyguards = self.opt_optional_sub_unit('', Bodyguards(max_models=2))

    def check_rules(self):
        norm_counts(0, 2, self.drones)
        nodes = len(self.wep.get_all()) + len(self.support.get_all())
        for tl in tl_ids:
            if self.wep.get(tl):
                nodes += 1
        if nodes > 4:
            self.error(self.name + ' may take up to 4 items from Ranged weapon and Support systems lists. '
                                   '(taken {0})'.format(nodes))
        Unit.check_rules(self)

    def has_special(self, sp_id):
        if self.bodyguards.get_unit():
            return self.bodyguards.get_unit().get_unit().has_special(sp_id)
        return 0


class CrisisTeam(Unit):
    name = 'XV8 Crisis team'

    class CrisisSuit(ListSubUnit):
        base_points = 22
        name = 'Shas\'ui'
        gear = ['Crisis battlesuit']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=3)
            self.wep = self.opt_options_list('Ranged weapon', weapon)
            self.support = self.opt_options_list('Support system', crisis_support)
            self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]
            self.leader = self.opt_options_list('', [
                ['Shas\'vre', 10, 'up']
            ])
            self.signature = self.opt_options_list('Signature system', signature)

        def check_rules(self):
            self.leader.set_active_options(['up'], self.get_count() <= 1)
            norm_counts(0, 2, self.drones)
            self.signature.set_visible(self.leader.get('up'))
            self.name = 'Shas\'ui' if not self.leader.get('up') else 'Shas\'vre'
            nodes = len(self.wep.get_all()) + len(self.support.get_all())
            for tl in tl_ids:
                if self.wep.get(tl):
                    nodes += 1
            if nodes > 3:
                self.error(self.name + ' may take up to 3 items from Ranged weapon and Support systems lists. '
                                       '(taken {0})'.format(nodes))
            ListSubUnit.check_rules(self)

        def count_leader(self):
            return self.get_count() if self.leader.get('up') else 0

        def has_special(self, sp_id):
            return self.get_count() if self.signature.get(sp_id) or self.wep.get(sp_id) else 0

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_units_list('Shas\'ui', self.CrisisSuit, 1, 3)
        self.ritual = self.opt_options_list('Options', [
            ['Bonding knife ritual', 1, 'up']
        ])

    def check_rules(self):
        self.team.update_range()
        leader = sum((m.count_leader() for m in self.team.get_units()))
        if leader > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(leader))
        self.set_points(self.build_points(count=1, exclude=[self.ritual.id]) + self.ritual.points() * self.get_count())
        self.build_description(count=1)

    def get_count(self):
        return self.team.get_count()

    def has_special(self, sp_id):
        result = 0
        for member in self.team.get_units():
            result += member.has_special(sp_id)
        return result