__author__ = 'Denis Romanow'

from builder.core2 import *
from .transport import Transport, TerminatorTransport
from .armory import Weapon


class Azrael(Unit):
    type_id = 'azrael_v1'
    type_name = 'Azrael'

    def __init__(self, parent):
        super(Azrael, self).__init__(parent=parent, points=215, unique=True, static=True, gear=[
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Protector'),
            Gear('Lion\'s Wrath'),
            Gear('Sword of Secrets'),
            Gear('Lion Helm'),
        ])


class Ezekiel(Unit):
    type_id = 'ezekiel_v1'
    type_name = 'Ezekiel'

    def __init__(self, parent):
        super(Ezekiel, self).__init__(parent=parent, points=145, unique=True, static=True, gear=[
            Gear('Master crafted bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Artificer armour'),
            Gear('Psychic hood'),
            Gear('Traitor\'s Bane'),
            Gear('Book of Salvation'),
        ])


class Asmodai(Unit):
    type_id = 'asmodai_v1'
    type_name = 'Asmodai'

    def __init__(self, parent):
        super(Asmodai, self).__init__(parent=parent, points=140, unique=True, static=True, gear=[
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Power armour'),
            Gear('Crozius arcanum'),
            Gear('Rosarius'),
            Gear('Blade of Reason'),
        ])


class Belial(Unit):
    type_id = 'belial_v1'
    type_name = 'Belial'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Belial.Weapon, self).__init__(parent, 'Weapon')
            self.sos = self.variant(
                name='Storm bolter and Sword of Silence',
                points=None,
                gear=[Gear('Storm bolter'), Gear('Sword of Silence')]
            )
            self.hammer = self.variant(
                name='Thunder hammer and storm shield',
                points=None,
                gear=[Gear('Thunder hammer'), Gear('Storm shield')]
            )
            self.claws = self.variant(
                name='Pair of lightning claws',
                points=None,
                gear=Gear('Lightning claw', count=2)
            )

    def __init__(self, parent):
        super(Belial, self).__init__(parent=parent, points=190, unique=True, gear=[
            Gear('Terminator armour'),
            Gear('Iron halo'),
            Gear('Teleport homer'),
        ])
        self.weapon = Belial.Weapon(self)


class Sammael(Unit):
    type_id = 'sammael_v1'
    type_name = 'Sammael'

    class Mount(OneOf):
        def __init__(self, parent):
            super(Sammael.Mount, self).__init__(parent, 'Transport')
            self.corvex = self.variant(
                name='Corvex',
                points=None,
            )
            self.sableclaw = self.variant(
                name='Sableclaw',
                points=None,
                gear=UnitDescription('Sableclaw', options=[
                    Gear('Twin-linked assault cannon'),
                    Gear('Twin-linked heavy bolter'),
                    Gear('Night Halo')
                ])
            )

    def __init__(self, parent):
        super(Sammael, self).__init__(parent=parent, points=200, unique=True, gear=[
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Teleport homer'),
            Gear('Raven Sword'),
            Gear('Adamantine Mantle'),
            Gear('Iron Halo'),
        ])
        self.weapon = Sammael.Mount(self)


class Options(OptionsList):
    def __init__(self, parent, armour=None, bike=False, jump=False, storm_shield=False):
        super(Options, self).__init__(parent, 'Options')
        self.armour = armour
        self.bike_option = bike
        self.ride = []

        self.ax = self.variant('Auspex', 5)
        self.csh = self.variant('Combat shield', 5)
        self.iv = self.variant('Infravisor', 5)
        self.mbomb = self.variant('Melta bombs', 5)
        self.dig = self.variant('Digital weapons', 10)
        self.pr = self.variant('Porta-rack', 10)
        self.cf = self.variant('Conversion field', 15)
        if jump:
            self.jpack = self.variant('Jump pack', 15)
            self.ride += [self.jpack]
        if bike:
            self.bike = self.variant('Space Marine Bike', 20)
            self.ride += [self.bike]
        self.df = self.variant('Displacer field', 25)
        self.cfg = self.variant('Power field generator', 30)
        if storm_shield:
            self.ss = self.variant('Storm Shield', 15)

    def check_rules(self):
        if self.armour:
            for opt in self.ride:
                opt.visible = opt.used = not self.armour.is_tda()
        self.process_limit(self.ride, 1)

    def has_bike(self):
        return self.bike_option and self.bike.value


class Relic(OptionsList):
    def __init__(self, parent, rel_unf=False):
        super(Relic, self).__init__(parent, 'Relic')

        if rel_unf:
            self.pru = self.variant('Perfidious Relic of the Unforgiven', 15)
        self.shroud = self.variant('Shroud of heroes', 50)

    def get_unique(self):
        return self.description


class Armour(OneOf):
    def __init__(self, parent, art=0, tda=40):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0,
                                gear=[Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')])
        if art:
            self.art = self.variant('Artificer armour', art,
                                    gear=[Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades'), ])
        self.tda = self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda


class CompanyMaster(Unit):
    type_id = 'company_master_v1'
    type_name = 'Company Master'

    def __init__(self, parent):
        super(CompanyMaster, self).__init__(parent=parent, points=90, gear=Gear('Iron halo'))

        self.armour = Armour(self, art=20, tda=40)
        self.weapon1 = Weapon(self, 'Weapon', armour=self.armour, melee=True, ranged=True, relic=True, tda_melee=True)
        self.weapon2 = Weapon(self, '', armour=self.armour, pistol=True, melee=True, ranged=True, relic=True,
                              tda_ranged=True)
        self.opt = Options(self, armour=self.armour, jump=True, storm_shield=True)
        self.relic = Relic(self, rel_unf=True)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class Librarian(Unit):
    type_id = 'librarian_v1'
    type_name = 'Librarian'

    class Level(OneOf):
        def __init__(self, parent):
            super(Librarian.Level, self).__init__(parent, 'Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 35)

    def __init__(self, parent):
        super(Librarian, self).__init__(parent=parent, points=65, gear=Gear('Psychic hood'))

        self.armour = Armour(self, tda=30)
        self.weapon1 = Weapon(self, 'Weapon', armour=self.armour, force=True, melee=True, ranged=True, relic=True)
        self.weapon2 = Weapon(self, '', armour=self.armour, pistol=True, melee=True, ranged=True, relic=True,
                              tda_ranged=True)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)
        self.relic = Relic(self, rel_unf=True)
        self.psy = self.Level(self)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class InterrogatorChaplain(Unit):
    type_id = 'interrogator_chaplain_v1'
    type_name = 'Interrogator-Chaplain'

    def __init__(self, parent):
        super(InterrogatorChaplain, self).__init__(parent=parent, points=110, gear=Gear('Rosarius'))

        self.armour = Armour(self, tda=30)
        self.weapon1 = Weapon(self, 'Weapon', armour=self.armour, crozius=True, melee=True, ranged=True, relic=True)
        self.weapon2 = Weapon(self, '', armour=self.armour, pistol=True, melee=True, ranged=True, relic=True,
                              tda_ranged=True)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)
        self.relic = Relic(self, rel_unf=True)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class Chaplain(Unit):
    type_id = 'chaplain_v1'
    type_name = 'Chaplain'

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent=parent, points=90, gear=[Gear('Rosarius'), Gear('Power armour'),
                                                                       Gear('Frag grenades'), Gear('Krak grenades')])

        self.weapon1 = Weapon(self, 'Weapon', crozius=True, melee=True, ranged=True, relic=True)
        self.weapon2 = Weapon(self, '', pistol=True, melee=True, ranged=True, relic=True)
        self.opt = Options(self, jump=True, bike=True)
        self.relic = Relic(self, rel_unf=True)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class Techmarine(Unit):
    type_id = 'techmarine_v1'
    type_name = 'Techmarine'

    class Gear(OneOf):
        def __init__(self, parent):
            super(Techmarine.Gear, self).__init__(parent, 'Gear')
            self.arm = self.variant('Servo-arm', 0)
            self.harness = self.variant('Servo-harness', 25)

    def __init__(self, parent):
        super(Techmarine, self).__init__(
            parent=parent,
            points=50,
            gear=[Gear('Artificar armour'), Gear('Frag grenades'), Gear('Krak grenades')]
        )
        self.weapon = Weapon(self, 'Weapon', pistol=True, melee=True, ranged=True)
        self.tech_gear = self.Gear(self)
        self.opt = Options(self, bike=True)


class Servitors(Unit):
    type_name = "Servitors"
    type_id = 'servitors_v1'
    model_points = 10

    def __init__(self, parent):
        super(Servitors, self).__init__(parent=parent)
        self.models = Count(self, self.name, points=self.model_points, min_limit=1, max_limit=5, per_model=True)
        self.hw = [
            Count(self, 'Heavy bolter', 0, 2, 10),
            Count(self, 'Multi-melta', 0, 2, 20),
            Count(self, 'Plasma cannon', 0, 2, 20)
        ]

    def check_rules(self):
        Count.norm_counts(0, min(2, self.get_count()), self.hw)

    def build_description(self):
        desc = UnitDescription(self.name, points=self.points, count=self.get_count())
        serv = UnitDescription('Servitor', points=10)
        arms_count = self.get_count()
        for wep in self.hw:
            hw_serv = serv.clone()
            hw_serv.add(Gear(wep.name))
            hw_serv.points += wep.option_points
            hw_serv.count = wep.cur
            desc.add(hw_serv)
            arms_count -= wep.cur
        serv.add(Gear('Servo-Arm'))
        serv.count = arms_count
        desc.add(serv)
        return desc

    def get_count(self):
        return self.models.cur


class Banners(OptionsList):
    def __init__(self, parent):
        super(Banners, self).__init__(parent, 'Banners', limit=1)
        self.sor = self.variant('Standard of Retribution', 45)
        self.sod = self.variant('Standard of Devastation', 65)
        self.sof = self.variant('Standard of Fortitude', 85)

        self.unique = [self.sor, self.sod, self.sof]

    def get_unique(self):
        if any(o.value for o in self.unique):
            return self.description
        return []


class CommandSquad(Unit):
    type_id = 'command_squad_v1'
    type_name = 'Command Squad'

    model_points = 20

    class Veteran(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent, name, ccw=False, pistol=False):
                super(CommandSquad.Veteran.Weapon, self).__init__(parent=parent, name=name)
                if ccw:
                    self.csw = self.variant('Chainsword', 0)
                if pistol:
                    self.bpist = self.variant('Bolt pistol', 0)
                    self.ppist = self.variant('Plasma pistol', 15)
                self.bgun = self.variant('Bolter', 0)
                self.sbgun = self.variant('Storm Bolter', 3)
                self.flame = self.variant('Flamer', 5)
                self.mgun = self.variant('Meltagun', 10)
                self.cmelta = self.variant('Combi-melta', 10)
                self.cflame = self.variant('Combi-flamer', 10)
                self.cplasma = self.variant('Combi-plasma', 10)
                self.psw = self.variant('Plasmagun', 15)
                self.psw = self.variant('Power weapon', 15)
                self.lclaw = self.variant('Lightning claw', 15)
                self.pfist = self.variant('Power fist', 25)
                self.ham = self.variant('Thunder hammer', 30)

        class Options(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Options, self).__init__(parent=parent, name='Options')

                self.mbomb = self.variant('Melta bombs', 5)
                self.ss = self.variant('Storm Shield', 15)

        class Banners(Banners):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Banners, self).__init__(parent=parent)

                self.comstd = self.variant('Company Standard', 15)
                self.revstd = self.variant('Revered Standard', 25)
                self.cb = self.variant('Dark Angels Chapter Banner', 45)

                self.unique += [self.cb]

        def __init__(self, parent):
            super(CommandSquad.Veteran, self).__init__(
                parent=parent, name='Veteran', points=CommandSquad.model_points,
                gear=[
                    Gear('Power Armour'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                ])

            self.weapon1 = CommandSquad.Veteran.Weapon(self, 'Weapon', ccw=True)
            self.weapon2 = CommandSquad.Veteran.Weapon(self, '', pistol=True)
            self.opt = CommandSquad.Veteran.Options(self)
            self.banners = CommandSquad.Veteran.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    class SpecModels(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.SpecModels, self).__init__(parent, '')
            cham_points = CommandSquad.model_points + 15
            medic_points = CommandSquad.model_points + 15

            self.ch = self.variant('Company Champion', cham_points, gear=UnitDescription(
                name='Company Champion',
                points=cham_points,
                options=[
                    Gear('Power Armour'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Blade of Caliban'),
                    Gear('Combat Shield'),
                    Gear('Bolt pistol'),
                ]
            ))
            self.ap = self.variant('Apothecary', medic_points, gear=UnitDescription(
                name='Apothecary',
                points=medic_points,
                options=[
                    Gear('Power Armour'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Chainsword'),
                    Gear('Narthecium'),
                ]
            ))

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=1, max_limit=5, start_value=5)
        self.up = CommandSquad.SpecModels(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.veterans.count + self.up.count

    def check_rules(self):
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Apothecary and Company champion) '
                       '(taken: {})'.format(self.get_count()))
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), []) + self.transport.get_unique_gear()


class DeathwingCommandSquad(Unit):
    type_id = 'deathwing_command_squad_v1'
    type_name = 'Deathwing Command Squad'

    model_points = 220 / 5

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingCommandSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 0)
                self.chain_fist = self.variant('Chainfist', 5)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingCommandSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 0)
                self.hf = self.variant('Heavy flamer', 10)
                self.pc = self.variant('Plasma Cannon', 15)
                self.ac = self.variant('Assault Cannon', 20)

            def enable_spec(self, value):
                self.hf.active = self.pc.active = self.ac.active = value

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(DeathwingCommandSquad.Veteran.Weapon, self).__init__(parent=parent, name='')
                self.cyc = self.variant('Cyclone missile launcher', 25)

                self.lc = self.variant('Pair of lightning claws', 0, gear=Gear('Lightning claw', count=2))
                self.thss = self.variant('Thunder hammer and storm shield', 5,
                                         gear=[Gear('Thunder hammer'), Gear('Storm shield')])

                self.both = [self.lc, self.thss]

            def has_both(self):
                return any(opt.value for opt in self.both)

            def check_rules(self):
                super(DeathwingCommandSquad.Veteran.Weapon, self).check_rules()
                self.process_limit(self.both, 1)

        class Banners(Banners):
            def __init__(self, parent):
                super(DeathwingCommandSquad.Veteran.Banners, self).__init__(parent=parent)

                self.com_banner = self.variant('Deathwing Company Banner', 45)
                self.rev_std = self.variant('Revered Standard', 25)

                self.unique += [self.com_banner]

        def __init__(self, parent):
            super(DeathwingCommandSquad.Veteran, self).__init__(
                parent=parent, name='Deathwing Terminator', points=DeathwingCommandSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)
            self.banners = self.Banners(self)

        def check_rules(self):
            super(DeathwingCommandSquad.Veteran, self).check_rules()
            for wep in [self.right_weapon, self.left_weapon]:
                wep.visible = wep.used = not self.weapon.has_both()
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

    class SpecModels(OptionsList):
        def __init__(self, parent):
            super(DeathwingCommandSquad.SpecModels, self).__init__(parent, '')
            cham_points = DeathwingCommandSquad.model_points + 5
            medic_points = DeathwingCommandSquad.model_points + 30

            self.ch = self.variant('Deathwing Champion', cham_points, gear=UnitDescription(
                name='Deathwing Champion',
                points=cham_points,
                options=[
                    Gear('Terminator Armour'),
                    Gear('Halberd of Caliban'),
                ]
            ))
            self.ap = self.variant('Deathwing Apothecary', medic_points, gear=UnitDescription(
                name='Deathwing Apothecary',
                points=medic_points,
                options=[
                    Gear('Terminator Armour'),
                    Gear('Power fist'),
                    Gear('Narthecium'),
                ]
            ))

    def __init__(self, parent):
        super(DeathwingCommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=1, max_limit=5, start_value=5)
        self.up = self.SpecModels(self)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.veterans.count + self.up.count

    def check_rules(self):
        if self.get_count() != 5:
            self.error('Deathwing Command Squad must include 5 Deathwing Terminator (include Apothecary and '
                       'Champion) (taken: {})'.format(self.get_count()))
        spec = sum(c.has_spec() for c in self.veterans.units)
        if spec > 1:
            self.error('Only one Deathwing Terminator can take a special weapon (taken: {})'.format(spec))
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Deathwing Terminator can take a banner (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), []) + self.transport.get_unique_gear()


class RavenwingCommandSquad(Unit):
    type_id = 'ravenwing_command_squad_v1'
    type_name = 'Ravenwing Command Squad'

    model_points = 40

    class Veteran(ListSubUnit):
        base_gear = [
            Gear('Power Armour'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Teleport Homer'),
        ]

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingCommandSquad.Veteran.Weapon, self).__init__(parent=parent, name='Weapon')

                self.pt = self.variant('Plasma talon', 0)
                self.rfl = self.variant('Ravenwing grenade launcher', 0)

        class Banners(Banners):
            def __init__(self, parent):
                super(RavenwingCommandSquad.Veteran.Banners, self).__init__(parent=parent)

                self.cb = self.variant('Ravenwing Company Banner', 15)
                self.revstd = self.variant('Revered Standard', 25)

                self.unique += [self.cb]

        def __init__(self, parent):
            super(RavenwingCommandSquad.Veteran, self).__init__(
                parent=parent, name='Ravenwing Black Knight',
                points=RavenwingCommandSquad.model_points,
                gear=self.base_gear)
            self.weapon = self.Weapon(self)
            self.banners = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    class SpecModels(OptionsList):
        def __init__(self, parent):
            super(RavenwingCommandSquad.SpecModels, self).__init__(parent, '')
            cham_points = RavenwingCommandSquad.model_points + 5
            medic_points = RavenwingCommandSquad.model_points + 30

            self.ch = self.variant('Ravenwing Champion', cham_points, gear=UnitDescription(
                name='Ravenwing Champion',
                points=cham_points,
                options=RavenwingCommandSquad.Veteran.base_gear + [
                    Gear('Blade of Caliban'),
                    Gear('Bolt pistol'),
                    Gear('Plasma Talon'),
                ]
            ))
            self.ap = self.variant('Ravenwing Apothecary', medic_points, gear=UnitDescription(
                name='Ravenwing Apothecary',
                points=medic_points,
                options=RavenwingCommandSquad.Veteran.base_gear + [
                    Gear('Corvus hammer'),
                    Gear('Narthecium'),
                    Gear('Plasma Talon'),
                ]
            ))

    def __init__(self, parent):
        super(RavenwingCommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=1, max_limit=5, start_value=3)
        self.up = self.SpecModels(self)

    def get_count(self):
        return self.veterans.count + self.up.count

    def check_rules(self):
        if not 3 <= self.get_count() <= 5:
            self.error('Ravenwing Command Squad must include from 3 to 5 Ravenwing Black Knights '
                       '(include Apothecary and Champion) (taken: {})'.format(self.get_count()))
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Ravenwing Black Knight can take a banner (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), [])
