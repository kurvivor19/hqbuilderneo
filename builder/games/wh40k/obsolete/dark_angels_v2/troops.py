from builder.core2 import Unit, Gear, SubUnit, Count, OptionsList, UnitDescription
from .transport import Transport
from .armory import Weapon

__author__ = 'Denis Romanov'


class TacticalSquad(Unit):
    type_name = 'Tactical Squad'
    type_id = 'tactical_squad_v1'
    model_points = 14
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(TacticalSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent, signum=False):
            super(TacticalSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * TacticalSquad.model_points,
                gear=TacticalSquad.model_gear + ([Gear('Signum')] if signum else [])
            )
            self.wep1 = Weapon(self, 'Weapon', bolt_gun=True, melee=True)
            self.wep2 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(TacticalSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    class Special(OptionsList):
        def __init__(self, parent):
            super(TacticalSquad.Special, self).__init__(parent=parent, name='Special weapon', limit=1)

            self.flame = self.variant('Flamer', 5)
            self.mgun = self.variant('Meltagun', 10)
            self.pgun = self.variant('Plasmagun', 15)

    class Heavy(OptionsList):
        def __init__(self, parent):
            super(TacticalSquad.Heavy, self).__init__(parent=parent, name='Heavy weapon')

            self.hbgun = self.variant('Heavy bolter', 10)
            self.mulmgun = self.variant('Multi-melta', 10)
            self.mlaunch = self.variant('Missile launcher', 15)
            self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
            self.pcannon = self.variant('Plasma cannon', 15)
            self.lcannon = self.variant('Lascannon', 20)
            self.heavy = [self.hbgun, self.mulmgun, self.mlaunch, self.pcannon, self.lcannon]

        def check_rules(self):
            super(TacticalSquad.Heavy, self).check_rules()
            self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
            self.process_limit(self.heavy, 1)

    def __init__(self, parent):
        super(TacticalSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.special = self.Special(parent=self)
        self.heavy = self.Heavy(parent=self)
        self.transport = Transport(parent=self)

    def check_rules(self):
        self.special.visible = self.special.used = (not self.heavy.any or self.get_count() == 10)
        self.heavy.visible = self.heavy.used = (not self.special.any or self.get_count() == 10)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=TacticalSquad.model_gear + [Gear('Bolt pistol')],
            points=TacticalSquad.model_points
        )
        count = self.marines.cur
        for o in [self.heavy, self.special]:
            if o.used and o.any:
                spec_marine = marine.clone()
                spec_marine.points += o.points
                spec_marine.add(o.description)
                desc.add(spec_marine)
                count -= 1
        marine.add(Gear('Boltgun'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class ScoutSquad(Unit):
    type_name = 'Scout Squad'
    type_id = 'scout_squad_v1'

    model_points = 12
    model_gear = [Gear('Scout armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):
        class Weapon(Weapon):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon', bolt_gun=True,
                                                                 melee=True)
                self.sniperrifle = self.variant('Sniper rifle', 0)
                self.shotgun = self.variant('Shotgun', 0)
                self.combatknife = self.variant('Combat knife', 0)

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(ScoutSquad.Sergeant, self).__init__(
                name='Scout Sergeant',
                parent=parent, points=60 - 4 * ScoutSquad.model_points,
                gear=TacticalSquad.model_gear
            )
            from .hq import Weapon
            self.wep1 = self.Weapon(self)
            self.wep2 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(ScoutSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Scout Sergeant'
            return desc

    class Heavy(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Heavy, self).__init__(parent=parent, name='Heavy weapon')

            self.hbgun = self.variant('Heavy bolter', 8)
            self.mlaunch = self.variant('Missile launcher', 15)
            self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
            self.heavy = [self.hbgun, self.mlaunch]

        def check_rules(self):
            super(ScoutSquad.Heavy, self).check_rules()
            self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
            self.process_limit(self.heavy, 1)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Options, self).__init__(parent=parent, name='Options')
            self.cloak = self.variant('Camo cloaks', 2, per_model=True)

    def __init__(self, parent):
        super(ScoutSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Scout', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.sniper = Count(parent=self, name='Sniper rifle', min_limit=0, max_limit=4, points=0)
        self.shotgun = Count(parent=self, name='Space Marine shotgun', min_limit=0, max_limit=4, points=0)
        self.knife = Count(parent=self, name='Combat knife', min_limit=0, max_limit=4, points=0)

        self.heavy = self.Heavy(parent=self)
        self.opt = self.Options(parent=self)

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        super(ScoutSquad, self).check_rules()
        max_wep = self.marines.cur
        if self.heavy.any:
            max_wep -= 1
        Count.norm_counts(0, max_wep, [self.sniper, self.shotgun, self.knife])

    def build_points(self):
        return self.sergeant.points + self.marines.points + self.heavy.points + self.opt.points * self.get_count()

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count(),
                               self.opt.description + self.sergeant.description)
        scout = UnitDescription('Scout', self.model_points, options=self.model_gear + [Gear('Bolt pistol')])
        count = self.marines.cur
        if self.heavy.any:
            spec_weapon = scout.clone().add(self.heavy.description)
            spec_weapon.points += self.heavy.points
            desc.add(spec_weapon)
            count -= 1
        for o in [self.knife, self.shotgun, self.sniper]:
            if o.cur:
                desc.add(scout.clone().add(Gear(o.name)).set_count(o.cur))
                count -= o.cur
        if count:
            scout.count = count
            desc.add(scout.add(Gear('Boltgun')))
        return desc
