__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.space_marines.hq import *
from builder.games.wh40k.obsolete.space_marines.elites import *
from builder.games.wh40k.obsolete.space_marines.troops import *
from builder.games.wh40k.obsolete.space_marines.fast import *
from builder.games.wh40k.obsolete.space_marines.heavy import *
from functools import reduce


class SpaceMarines(LegacyWh40k):
    army_id = '5deec709785f45d8bc94f21172ee1210'
    army_name = 'Space Marines'
    obsolete = True

    def __init__(self, secondary=False):

        LegacyWh40k.__init__(
            self,
            hq=[Calgar, Sicarius, Tigurius, Cassius, Khan, Vulkan, Shrike, Lysander, Kantor, HighMarshall, Grimaldus,
                Champion, ChapterMaster, {'unit': HonourGuardSquad, 'active': False}, Captain, TerminatorCaptain,
                {'unit': CommandSquad, 'active': False}, Librarian, Chaplain, ForgeMaster, Techmarine, Servitors],
            elites=[VanguardVeteranSquad, SternguardVeteranSquad, Dred, IronDred, TheDamned, TerminatorSquad,
                    TerminatorAssaultSquad, CenturionAssault],
            troops=[TacticalSquad, ScoutSquad, CrusaderSquad, {'unit': SpaceMarineBikers, 'active': False}],
            fast=[AssaultSquad, LandSpeederSquadron, Stormtalon, SpaceMarineBikers, AttackBikeSquad, ScoutBikers],
            heavy=[Devastators, CenturionDevastators, Thunderfire, Predator, Whirlwind, Vindicator, Hunter, Stalker,
                   LandRaider, LandRaiderCrusader, LandRaiderRedeemer, StormravenGunship, Chronus,
                   {'unit': Dred, 'active': False},
                   {'unit': IronDred, 'active': False}],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.SM)
        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_unit(HonourGuardSquad) - \
                self.hq.count_unit(CommandSquad) - self.hq.count_unit(Techmarine) - self.hq.count_unit(Servitors)
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

        def check_troop_limit():
            min_count = len(self.troops.get_units())
            max_count = sum([2 if tr.get_count() == 10 else 1 for tr in self.troops.get_units()])
            return self.troops.min <= max_count and min_count <= self.troops.max
        self.troops.check_limits = check_troop_limit

    def check_rules(self):

        honor_guards = self.hq.count_units([ChapterMaster, Kantor, HighMarshall]) + self.hq.count_units([Calgar]) * 3
        self.hq.set_active_types([HonourGuardSquad], honor_guards > 0)
        if honor_guards < self.hq.count_units([HonourGuardSquad]):
            self.error("You can't have more Honour Guard squads then Chapter Masters "
                       "(except for Calgar, he allows 3 of them).")

        command_squads = self.hq.count_units([Sicarius, Tigurius, Cassius, Vulkan, Lysander, Shrike, Khan, Grimaldus,
                                              Captain, Librarian, Chaplain])
        self.hq.set_active_types([CommandSquad], command_squads > 0)
        if command_squads < self.hq.count_units([CommandSquad]):
            self.error("You can't have more Command squads then Captains, Librarians and Chaplains.")

        techmarines = len(self.hq.get_units()) - self.hq.count_unit(HonourGuardSquad) - \
            self.hq.count_unit(CommandSquad) - self.hq.count_unit(Techmarine) - self.hq.count_unit(Servitors)
        self.hq.set_active_types([Techmarine], techmarines > 0)
        if techmarines < self.hq.count_units([Techmarine]):
            self.error("You can't have more Techmarines then Captains, Librarians, Chaplains and Masters of the Forge.")

        serv_units = self.hq.count_units([Techmarine, ForgeMaster])
        self.hq.set_active_types([Servitors], serv_units > 0)
        if serv_units < self.hq.count_units([Techmarine]):
            self.error("You can only have one unit of Servitors per Techmarine or Master of the Forge.")

        self.heavy.set_active_types([Dred, IronDred], self.hq.count_units([ForgeMaster]) > 0)
        if self.hq.count_units([ForgeMaster]) == 0 and self.heavy.count_units([Dred, IronDred]) > 0:
            self.error("You can only take dreadnoughts in heavy support if your army includes Master of the Forge.")

        if self.heavy.count_units([Chronus]) > 0 and self.heavy.count_units([LandRaider, LandRaiderCrusader,
                                                                             LandRaiderRedeemer, Predator, Whirlwind,
                                                                             Vindicator, Stalker, Hunter]) < 1:
            self.error("There must be a tank for Chronus to command.")

        bike_caps = self.hq.get_units([Captain, ChapterMaster, Khan])
        biker_troops = reduce(lambda val, u: val or u.has_bike(), bike_caps, False)
        self.troops.set_active_types([SpaceMarineBikers], biker_troops)
        if not biker_troops:
            if self.troops.count_units([SpaceMarineBikers]) > 0:
                self.error("Space Maine bikers cn only be taken as troops if Captain on the bike is in HQ.")
        else:
            small_squads = reduce(lambda val, u: val + 1 if u.get_bikers() < 5 else val,
                                  self.troops.get_units(SpaceMarineBikers), 0)
            if small_squads > 0:
                self.error("Space Marine bikers taken as troops must be at least 5 man strong "
                           "(not counting Attack Bikes).")

        def Ultramarines():
            units = []
            for u in [Calgar, Sicarius, Tigurius, Cassius]:
                if self.hq.count_units([u]):
                    units.append(u.name)
            if self.heavy.count_unit(Chronus):
                units.append(Chronus.name)
            for u in self.troops.get_units([ScoutSquad]):
                if u.has_telion():
                    units.append('Sergeant Telion')
                    break
            if units:
                return 'Ultramarines', units

        def WhiteScars():
            if self.hq.count_unit(Khan):
                return 'White Scars', [Khan.name]

        def Salamanders():
            if self.hq.count_unit(Vulkan):
                return 'Salamanders', [Vulkan.name]

        def RavenGuard():
            if self.hq.count_unit(Shrike):
                return 'Raven Guard', [Shrike.name]

        def ImperialFists():
            units = []
            for u in [Lysander, Kantor]:
                if self.hq.count_units([u]):
                    units.append(u.name)
            if units:
                return 'Imperial Fists', units

        def BlackTemplars():
            units = []
            for u in [HighMarshall, Grimaldus, Champion]:
                if self.hq.count_units([u]):
                    units.append(u.name)
            if self.troops.count_unit(CrusaderSquad):
                units.append(CrusaderSquad.name)
            if units:
                return 'Black Templars', units

        chapters_check = [
            Ultramarines,
            WhiteScars,
            Salamanders,
            RavenGuard,
            ImperialFists,
            BlackTemplars
        ]

        chapters = dict([_f for _f in [f() for f in chapters_check] if _f])

        if len(chapters) > 1:
            units = '; '.join([ch + ': ' + ', '.join(units) for ch, units in chapters.items()])
            self.error("You can't mix different chapter tactics in one detachment. ({0})".format(units))

        if 'Black Templars' in chapters and self.hq.count_unit(Librarian):
            self.error('Abhor the Witch! Librarians may not be included in detachments of Black Templars.')
