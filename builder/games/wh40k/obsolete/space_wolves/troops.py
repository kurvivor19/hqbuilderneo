__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.core.options import norm_counts


class DropPod(Unit):
    name = 'Drop Pod'
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ["Storm Bolter", 5, "bolt"],
            ["Deathwind Missile Launcher", 20, "missile"],
        ])


class Rhino(Unit):
    name = 'Rhino'
    base_points = 35
    gear = ["Storm Bolter", "Smoke Launchers", "Searchlight"]

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
        ])


class Razorback(Unit):
    name = 'Razorback'
    base_points = 40
    gear = ["Smoke Launchers", "Searchlight"]

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ["Twin-linked Heavy Bolter", 0, "heavy"],
            ["Twin-linked Heavy Flamer", 25, "flamer"],
            ["Twin-linked Assault Cannon", 35, "assault"],
            ["Twin-linked Lascannon", 35, "las"],
            ["Lascannon and Twin-linked Plasma Gun", 35, "plasma"],
        ])
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
        ])


class BloodClaws(Unit):
    name = "Blood Claws Pack"
    base_points = 15
    gear = ["Power Armour", "Frag and Krak grenades"]
    min = 5
    max = 15

    class Lucas(StaticUnit):
        name = "Lukas the Trickster"
        base_points = 155
        gear = ['Power Armour', 'Wolftooth Necklace', 'Wolf Tail Talisman', 'Wolf Claw', 'Plasma Pistol', 'Pelt of the Doppegangrel']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Blood Claws", self.min, self.max, self.base_points)

        self.wep1 = self.opt_one_of("Weapon", [
            ["Bolt Pistol", 0, "bolt"],
            ["Flamer", 0, "flame"],
            ["Meltagun", 5, "melta"],
            ["Plasma Gun", 10, "plasmagun"],
        ], id='w1')

        self.wep2 = self.opt_one_of("", [
            ["Bolt Pistol", 0, "bolt"],
            ["Flamer", 0, "flame"],
            ["Meltagun", 0, "melta"],
            ["Plasma Gun", 0, "plasmagun"],
        ], id='w2')

        self.wep3 = self.opt_one_of("", [
            ["Bolt Pistol", 0, "bolt"],
            ["Plasma Pistol", 15, "plasma"],
        ], id='w3')

        self.wep4 = self.opt_one_of("", [
            ["Close Combat Weapon", 0, "ccw"],
            ["Power Weapon", 15, "pw"],
            ["Power Fist", 25, "fist"],
        ], id='w4')
        self.opt = self.opt_options_list("Upgrade", [
            [self.Lucas.name, self.Lucas.base_points, "lucas"],
        ])
        self.lucas_unit = self.opt_sub_unit(self.Lucas())

        self.trans = self.opt_options_list('Transport', [
            [DropPod.name, DropPod.base_points, 'dp'],
            [Rhino.name, Rhino.base_points, 'rh'],
            [Razorback.name, Razorback.base_points, 'rz']
        ], limit=1)
        self.dp_unit = self.opt_sub_unit(DropPod())
        self.rh_unit = self.opt_sub_unit(Rhino())
        self.rz_unit = self.opt_sub_unit(Razorback())

    def check_rules(self):
        lucas = 1 if self.opt.get('lucas') else 0
        norm_counts(self.min - lucas, self.max - lucas, [self.count])

        self.lucas_unit.set_active(self.opt.get('lucas'))
        self.dp_unit.set_active(self.trans.get('dp'))
        self.rh_unit.set_active(self.trans.get('rh'))
        self.rz_unit.set_active(self.trans.get('rz'))
        self.wep2.set_visible(self.count.get() + lucas == self.max)

        self.points.set(self.count.points() + self.wep1.points() + self.wep2.points() + self.wep3.points() +
                        self.wep4.points() + self.lucas_unit.points() + self.dp_unit.points() +
                        self.rh_unit.points() + self.rz_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(["Close Combat Weapon"], self.count.get() - 1)
        self.description.add(["Bolt Pistol"], self.count.get() - 3)
        if self.count.get() + lucas < self.max:
            self.description.add(["Bolt Pistol"])
        self.description.add(self.wep1.get_selected())
        self.description.add(self.wep2.get_selected())
        self.description.add(self.wep3.get_selected())
        self.description.add(self.wep4.get_selected())
        self.description.add(self.lucas_unit.get_selected())
        self.description.add(self.dp_unit.get_selected())
        self.description.add(self.rh_unit.get_selected())
        self.description.add(self.rz_unit.get_selected())
        self.set_description(self.description)

    def get_count(self):
        return self.count.get() + self.lucas_unit.get_count()

    def get_unique(self):
        if self.opt.get('lucas'):
            return self.Lucas.name


class GreyHunters(Unit):
    name = "Grey Hunters Pack"
    base_points = 15
    gear = ["Power Armour", "Frag and Krak grenades"]

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Grey Hunters", 5, 10, self.base_points)

        self.wep1 = self.opt_one_of("Weapon", [
            ["Bolter", 0, "bolt"],
            ["Flamer", 0, "flame"],
            ["Meltagun", 5, "melta"],
            ["Plasma Gun", 10, "plasmagun"],
            ], id ='w1')

        self.wep2 = self.opt_one_of("", [
            ["Bolter", 0, "bolt"],
            ["Flamer", 0, "flame"],
            ["Meltagun", 0, "melta"],
            ["Plasma Gun", 0, "plasmagun"],
            ], id ='w2')

        self.wep3 = self.opt_one_of("", [
            ["Bolt Pistol", 0, "bolt"],
            ["Plasma Pistol", 15, "plasma"],
            ], id ='w3')

        self.wep4 = self.opt_one_of("", [
            ["Close Combat Weapon", 0, "ccw"],
            ["Power Weapon", 15, "pw"],
            ["Power Fist", 25, "fist"],
            ], id ='w4')
        self.opt = self.opt_options_list("Options", [
            ["Wolf Standard", 10, "flag"],
            ["Mark of the Wulfen", 15, "mark"],
            ])

        self.trans = self.opt_options_list('Transport', [
            [DropPod.name, DropPod.base_points, 'dp'],
            [Rhino.name, Rhino.base_points, 'rh'],
            [Razorback.name, Razorback.base_points, 'rz']
        ], limit=1)
        self.dp_unit = self.opt_sub_unit(DropPod())
        self.rh_unit = self.opt_sub_unit(Rhino())
        self.rz_unit = self.opt_sub_unit(Razorback())

    def check_rules(self):
        self.dp_unit.set_active(self.trans.get('dp'))
        self.rh_unit.set_active(self.trans.get('rh'))
        self.rz_unit.set_active(self.trans.get('rz'))
        self.wep2.set_visible(self.count.get() == 10)

        self.points.set( self.count.points() + self.wep1.points() + self.wep2.points() + self.wep3.points() + self.wep4.points() + self.opt.points() +
                         self.dp_unit.points() + self.rh_unit.points() + self.rz_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(["Close Combat Weapon", "Bolt Pistol"], self.count.get() - 1)
        self.description.add(["Bolter"], self.count.get() - 2)
        if self.count.get() < 10:
            self.description.add(["Bolter"])
        self.description.add(self.wep1.get_selected())
        self.description.add(self.wep2.get_selected())
        self.description.add(self.wep3.get_selected())
        self.description.add(self.wep4.get_selected())
        self.description.add(self.opt.get_selected())
        self.description.add(self.dp_unit.get_selected())
        self.description.add(self.rh_unit.get_selected())
        self.description.add(self.rz_unit.get_selected())
        self.set_description(self.description)
