__author__ = 'Denis Romanov'

from builder.core2 import *
from .armory import *


class CrisisTeam(Unit):
    type_name = 'Crisis Battlesuit Team'
    type_id = 'crisisbattlesuitteam_v1'

    class CrisisSuit(ListSubUnit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent):
            super(CrisisTeam.CrisisSuit, self).__init__(
                parent=parent, points=22, name='Shas\'ui',
                gear=[Gear('Crisis battlesuit'), Gear('Multi-tracker'), Gear('Blacksun filter')]
            )
            self.weapon = Weapon(self, slots=3)
            self.support = SupportSystem(self, slots=3)
            self.shasvre = self.ShasVre(self)
            self.drones = Drones(self)
            self.signature = SignatureSystem(self)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(CrisisTeam.CrisisSuit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)

                self.shasvre = self.variant('Shas\'vre', 10, gear=[])

        def check_rules(self):
            self.drones.check_rules()
            self.signature.visible = self.signature.used = self.shasvre.shasvre.value
            free_slots = 3 - self.weapon.count_slots() - self.signature.count_slots() - self.support.count_slots()
            for s in [self.weapon, self.signature, self.support]:
                s.set_free_slots(free_slots)

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return self.signature.get_unique_gear() + self.weapon.get_unique_gear()

        def build_description(self):
            desc = super(CrisisTeam.CrisisSuit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

    def __init__(self, parent):
        super(CrisisTeam, self).__init__(parent=parent)
        self.team = UnitList(self, self.CrisisSuit, min_limit=1, max_limit=3)
        self.opt = Ritual(self)

    def get_count(self):
        return self.team.count

    def check_rules(self):
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.team.units), [])


class StealthTeam(Unit):
    type_name = 'Stealth Team'
    type_id = 'stealthteam_v1'

    def __init__(self, parent):
        super(StealthTeam, self).__init__(parent=parent)
        self.team = UnitList(self, self.Suit, min_limit=3, max_limit=6)
        self.opt = Ritual(self)

    class Suit(ListSubUnit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent):
            super(StealthTeam.Suit, self).__init__(
                parent=parent, points=30,
                gear=[Gear('XV25 Stealth battlesuit'), Gear('Multi-tracker'), Gear('Blacksun filter')]
            )
            self.weapon = self.Weapon(self)
            self.support = SupportSystem(self, slots=1)
            self.shasvre = self.ShasVre(self)
            self.opt = self.Options(self)
            self.drones = Drones(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(StealthTeam.Suit.Weapon, self).__init__(parent=parent, name='Weapon')
                self.burstcannon = self.variant('Burst cannon', 0)
                self.fusionblaster = self.variant('Fusion blaster', 5)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(StealthTeam.Suit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)
                self.shasvre = self.variant('Shas\'vre', 10, gear=[])

        def check_rules(self):
            for o in [self.opt, self.drones]:
                o.visible = o.used = self.shasvre.shasvre.value
            self.drones.check_rules()

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

        @ListSubUnit.count_gear
        def count_blasters(self):
            return self.weapon.cur == self.weapon.fusionblaster

        class Options(OptionsList):
            def __init__(self, parent):
                super(StealthTeam.Suit.Options, self).__init__(parent=parent, name='Options')
                self.homingbeacon = self.variant('Homing beacon', 10)
                self.marketlightandtargetlock = self.variant('Market light and target lock', 5, gear=[
                    Gear('Market light'), Gear('Target lock')
                ])

    def check_rules(self):
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))

        blaster = sum(m.count_blasters() for m in self.team.units)
        blaster_lim = int(self.get_count() / 3)
        if blaster > blaster_lim:
            self.error('Only one blaster may be taken per 3 models in unit.')

    def get_count(self):
        return self.team.count


class Riptide(Unit):
    type_name = 'XV104 Riptide'
    type_id = 'xv104riptide_v1'

    def __init__(self, parent):
        super(Riptide, self).__init__(
            parent=parent, points=180,
            gear=[Gear('Riptide battlesuit'), Gear('Multi-tracker'), Gear('Blacksun filter'),
                  Gear('Riptide shield generator')]
        )
        self.Weapon1(self)
        self.Weapon2(self)
        SupportSystem(self, slots=2, riptide=True)
        self.shieldedmissiledrone = Count(self, 'Shielded missile drone', min_limit=0, max_limit=2, points=25)
        self.signature = SignatureSystem(self, slots=None)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Riptide.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.heavyburstcannon = self.variant('Heavy burst cannon', 0)
            self.ionaccelerator = self.variant('Ion accelerator', 5)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Riptide.Weapon2, self).__init__(parent=parent, name='')
            self.twinlinkedsmartmissilesystem = self.variant('Twin-linked smart missile system', 0)
            self.twinlinkedplasmarifle = self.variant('Twin-linked plasma rifle', 0)
            self.twinlinkedfusionblaster = self.variant('Twin-linked fusion blaster', 0)

    def check_rules(self):
        super(Riptide, self).check_rules()
        self.signature.visible = self.signature.used = self.roster.is_enclaves

    def get_unique_gear(self):
        if self.signature.used:
            return self.signature.description
        return []
