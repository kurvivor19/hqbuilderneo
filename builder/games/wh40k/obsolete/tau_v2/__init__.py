__author__ = 'maria'
__summary__ = ['Codex Tau 6tg edition', 'Farsight Enclaves supplement', 'Firebase Support Cadre dataslate']

from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment, AlliedDetachment, HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort, Formation
from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.farsight = UnitType(self, Farsight)
        self.shadowsun = UnitType(self, Shadowsun)
        self.aunva = UnitType(self, Aunva)
        self.aunshi = UnitType(self, Aunshi)
        self.darkstider = UnitType(self, Darkstider)
        self.ethereal = UnitType(self, Ethereal)
        self.cadre = UnitType(self, Cadre)
        self.commander = UnitType(self, Commander)
        self.bodyguards = UnitType(self, Bodyguards, slot=0)
        self.farsight_commanders = UnitType(self, FarsightCommanders, slot=0)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.crisis_team = UnitType(self, CrisisTeam)
        self.stealth_team = UnitType(self, StealthTeam)
        self.stealth_team = UnitType(self, Riptide)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, FireWarriors)
        UnitType(self, KrootSquad)
        self.crisis_team = UnitType(self, CrisisTeam)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        self.pathfinders = UnitType(self, Pathfinders)
        self.vespids = UnitType(self, Vespids)
        self.gun_drones = UnitType(self, GunDrones)
        self.piranha = UnitType(self, PiranhaTeam)
        self.sun_shark = UnitType(self, SunShark)
        self.razor_shark = UnitType(self, RazorShark)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.broadside_team = UnitType(self, BroadsideTeam)
        self.hammerhead = UnitType(self, Hammerhead)
        self.sky_ray = UnitType(self, SkyRay)
        self.sniper_drone_team = UnitType(self, SniperDroneTeam)


class TauV2Base(Wh40kBase):
    army_id = 'tau_v2_base'
    army_name = 'Tau Empire'

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(TauV2Base.SupplementOptions, self).__init__(parent=parent, name='Codex')
            self.base = self.variant(name=TauV2Base.army_name)
            self.enclaves = self.variant(name='Farsight Enclaves')

    def __init__(self):

        from builder.games.wh40k.escalation.tau import LordsOfWar
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(TauV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self), lords=LordsOfWar(parent=self)
        )

        self.codex = self.SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_enclaves(self):
        return self.codex.cur == self.codex.enclaves

    def check_rules(self):
        super(TauV2Base, self).check_rules()

        self.hq.shadowsun.visible = self.hq.aunva.visible = self.is_base
        if self.is_enclaves and self.hq.shadowsun.count > 0:
            self.error('A Farsight Enclaves army cannot include Commander Shadowsun')
        if self.is_enclaves and self.hq.aunva.count > 0:
            self.error('A Farsight Enclaves army cannot include Aun\'Va')

        self.hq.farsight_commanders.visible = self.is_enclaves
        if self.is_base and self.hq.farsight_commanders.count > 0:
            self.error('A Tau Empire army cannot include Farsight\'s Commander Team')

        self.troops.crisis_team.visible = self.is_enclaves
        self.elites.crisis_team.visible = self.is_base
        if self.is_enclaves:
            self.troops.move_units(self.elites.crisis_team.units)
        else:
            self.elites.move_units(self.troops.crisis_team.units)

        farsight_bodyguards = sum(u.is_farsight_guard() for u in self.hq.bodyguards.units)
        fs = self.hq.farsight.count
        if fs == 0 and farsight_bodyguards > 0:
            self.error("You can't have Farsight\'s bodyguard team without Commander Farsight.")
        if fs == 0 and self.hq.farsight_commanders.count > 0:
            self.error("You can't have Farsight\'s commander team without Commander Farsight.")
        if farsight_bodyguards > 1:
            self.error("You can't have more then one Farsight\'s bodyguard team.")
        if farsight_bodyguards > 0 and self.hq.farsight_commanders.count > 0:
            self.error("You can't have Farsight\'s commander team and Farsight\'s bodyguard team together.")

        com = sum(ut.count for ut in [self.hq.farsight, self.hq.shadowsun, self.hq.commander])
        self.hq.bodyguards.active = com > 0
        if com < self.hq.bodyguards.count + self.hq.farsight_commanders.count:
            self.error("You can't have more bodyguard teams then commanders (include Commander Farsight and "
                       "Commander Shadowsun)")

        if self.is_enclaves and sum(u.get_count() >= 3 for u in self.troops.crisis_team.units) == 0:
            self.error("You must include at least one XV8 Crisis Team consists of three models.")


class TauV2CAD(TauV2Base, CombinedArmsDetachment):
    army_id = 'tau_v2_cad'
    army_name = 'Tau Empire (Combined arms detachment)'


class TauV2AD(TauV2Base, AlliedDetachment):
    army_id = 'tau_v2_ad'
    army_name = 'Tau Empire (Allied detachment)'


class Firebase(Formation):
    army_id = 'tau_v2_firebase'
    army_name = 'Tau Firebase Support Cadre'

    @property
    def is_base(self):
        return True

    @property
    def is_enclaves(self):
        return False

    def __init__(self):
        super(Firebase, self).__init__()
        UnitType(self, Riptide, min_limit=1, max_limit=1)
        self.broadsides = UnitType(self, BroadsideTeam, min_limit=2, max_limit=2)

    def check_rules(self):
        super(Firebase, self).check_rules()
        for unit in self.broadsides.units:
            if unit.count != 3:
                self.error('Broadside team must include 3 battlesuits')

faction = 'Tau'


class TauV2(Wh40k7ed):
    army_id = 'tau_v2'
    army_name = 'Tau Empire'
    faction = faction
    obsolete = True

    def __init__(self):
        super(TauV2, self).__init__([TauV2CAD, Firebase],
                                    [TauV2AD])

    def check_rules(self):
        super(TauV2, self).check_rules()
        # enclaves may ally with empire
        # but no allying between same factions is allowed
        if any([isinstance(m.sub_roster.roster, TauV2CAD)
               for m in self.primary.units]):
            enclave_flag = self.primary.units[0].sub_roster.roster.is_enclaves
            same_allies = [m.sub_roster.roster.is_enclaves
                           for m in self.secondary.units
                           if isinstance(m.sub_roster.roster, TauV2AD)]
            if sum([f == enclave_flag for f in same_allies]) > 0:
                self.error('Alliyng between same faction is prohibited')

detachments = [TauV2AD, TauV2CAD, Firebase]
