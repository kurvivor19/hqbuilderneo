__author__ = 'Denis Romanov'

from builder.core2 import *
from .armory import *


class Bodyguards(Unit):
    type_name = 'Crisis Bodyguard Team'
    type_id = 'bodyguardteam_v1'

    class CrisisSuit(ListSubUnit):
        def __init__(self, parent):
            super(Bodyguards.CrisisSuit, self).__init__(
                parent=parent, points=32, name='Crisis Bodyguard',
                gear=[Gear('Crisis battlesuit'), Gear('Multi-tracker'), Gear('Blacksun filter')]
            )
            self.weapon = Weapon(self, slots=3)
            self.support = SupportSystem(self, slots=3)
            self.signature = SignatureSystem(self, slots=None)
            self.drones = Drones(self)

        def check_rules(self):
            self.drones.check_rules()
            free_slots = 3 - self.weapon.count_slots() - self.support.count_slots()
            for s in [self.weapon, self.support]:
                s.set_free_slots(free_slots)

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return self.signature.get_unique_gear() + self.weapon.get_unique_gear()

    def __init__(self, parent):
        super(Bodyguards, self).__init__(parent=parent)
        self.team = UnitList(self, self.CrisisSuit, min_limit=1, max_limit=7)
        self.opt = self.Options(self, ritual_points=2)

    class Options(Ritual):
        def __init__(self, parent, ritual_points):
            super(Bodyguards.Options, self).__init__(parent, ritual_points=ritual_points)
            self.farsight = self.variant('Farsight\'s Bodyguard Team')

    def check_rules(self):
        self.team.update_range(1, 7 if self.opt.farsight.value else 3)

    def get_count(self):
        return self.team.count

    def is_farsight_guard(self):
        return self.opt.farsight.value

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.team.units), [])


class FarsightCommanders(Unit):
    type_name = 'Farsight\'s Commander Team'
    type_id = 'farsight_commander_team_v1'

    class Team(OptionsList):
        class Model(OptionsList.Variant):
            def __init__(self, *args, **kwargs):
                self.unique_gear = kwargs.pop('unique_gear', [])
                super(FarsightCommanders.Team.Model, self).__init__(*args, **kwargs)

        def __init__(self, parent):
            super(FarsightCommanders.Team, self).__init__(parent, 'Commanders')
            members = [
                dict(
                    name='O\'Vesa', points=305, unique_gear=['Earth Caste Pilot Array'],
                    gear=[
                        'Riptide battlesuit', 'Ion accelerator', 'Twin-linked fusion blaster',
                        'Riptide shield generator', 'Early warning override', 'Stimulant injector',
                        'Shielded missile drone', 'Shielded missile drone', 'Earth Caste Pilot Array'
                    ]
                ),
                dict(
                    name='Commander Bravestorm', points=199, unique_gear=['XV8-02 Crisis \'Iridium\' Battlesuit',
                                                                          'Onager Gauntlet'],
                    gear=[
                        'XV8-02 Crisis \'Iridium\' Battlesuit', 'Plasma rifle', 'Flamer', 'Stimulant injector',
                        'Shield generator', 'Gun drone', 'Gun drone', 'Onager Gauntlet'
                    ]
                ),
                dict(
                    name='Commander Brightsword', points=200, unique_gear=['Fusion Blades', 'Warscaper Drone'],
                    gear=[
                        'Crisis battlesuit', 'Twin-linked fusion blaster', 'Advanced targeting system',
                        'Stimulant injector', 'Shield drone', 'Fusion Blades', 'Warscaper Drone'
                    ]
                ),
                dict(
                    name='Shas\'O Sha\'vastos', points=174, unique_gear=['Puretide Engram Neurochip'],
                    gear=[
                        'Crisis battlesuit', 'Plasma rifle', 'Flamer', 'Shield generator', 'Vectored retro-thrusters',
                        'Gun drone', 'Gun drone', 'Puretide Engram Neurochip'
                    ]
                ),
                dict(
                    name='Shas\'O Arra\'kon', points=169, unique_gear=['Cyclic ion blaster',
                                                                       'Airbursting fragmentation projector',
                                                                       'Repulsor Impact Field'],
                    gear=[
                        'Crisis battlesuit', 'Plasma rifle', 'Cyclic ion blaster',
                        'Airbursting fragmentation projector', 'Counterfire defence system', 'Gun drone', 'Gun drone',
                        'Repulsor Impact Field'
                    ]),
                dict(
                    name='Broadside Shas\'vre Ob\'lotai 9-o', points=129, unique_gear=[],
                    gear=[
                        'Broadside battlesuit', 'Twinlinked high-yield missile pod', 'Twin-linked smart missile system',
                        'Velocity tracker', 'Missile drone', 'Missile drone', 'Seeker missile'
                    ]),
                dict(
                    name='Sub-commander Torchstar', points=154, unique_gear=['Multi-Spectrum Sensor Suite',
                                                                             'Neuroweb System Jammer'],
                    gear=[
                        'Crisis battlesuit', 'Flamer', 'Flamer', 'Target lock', 'Drone controller', 'Marker drone',
                        'Marker drone', 'Multi-Spectrum Sensor Suite', 'Neuroweb System Jammer'
                    ]
                ),
            ]

            def to_gear(g):
                return [Gear(s) for s in g]

            for m in members:
                self.Model(self, name=m['name'], points=m['points'], unique_gear=to_gear(m['unique_gear']),
                           gear=[UnitDescription(name=m['name'], points=m['points'], options=to_gear(m['gear']))])

    def __init__(self, parent):
        super(FarsightCommanders, self).__init__(parent, unique=True)
        self.team = self.Team(self)

    def check_rules(self):
        if self.get_count() == 0:
            self.error('Farsight\'s commander team must include at least one member')

    def get_count(self):
        return self.team.count

    def get_unique_gear(self):
        return sum((o.unique_gear for o in self.team.options), [])


class Commander(Unit):
    type_name = 'Commander'
    type_id = 'commander_v1'
    system_slots = 4

    def __init__(self, parent):
        super(Commander, self).__init__(
            parent=parent, points=85, gear=[Gear('Crisis battlesuit'), Gear('Multi-tracker'), Gear('Blacksun filter')]
        )

        self.weapon = Weapon(self, slots=self.system_slots)
        self.support = SupportSystem(self, slots=self.system_slots)
        self.signature = SignatureSystem(self, slots=None, commander=True)
        self.drones = Drones(self)

    def check_rules(self):
        self.drones.check_rules()
        free_slots = self.system_slots - self.weapon.count_slots() - self.support.count_slots()
        for s in [self.weapon, self.support]:
            s.set_free_slots(free_slots)

    def get_unique_gear(self):
        return self.signature.get_unique_gear() + self.weapon.get_unique_gear()


class Farsight(Unit):
    type_name = 'Commander Farsight'
    type_id = 'commanderfarsight_v1'

    def __init__(self, parent):
        super(Farsight, self).__init__(
            parent=parent, points=165, unique=True, gear=[
                Gear('Crisis battlesuit'), Gear('Multi-tracker'), Gear('Blacksun filter'), Gear('Shield generator'),
                Gear('Plasma rifle'), Gear('Dawn blade')
            ]
        )


class Shadowsun(Unit):
    type_name = 'Commander Shadowsun'
    type_id = 'commandershadowsun_v1'

    def __init__(self, parent):
        super(Shadowsun, self).__init__(
            parent=parent, points=135, unique=True, gear=[
                Gear('XV22 Stealth battlesuit'), Gear('Advanced targeting system'), Gear('Fusion blaster', count=2)
            ]
        )
        self.commandlinkdrone = Count(self, 'Command link drone', min_limit=0, max_limit=1, points=20)
        self.mv52shielddrone = Count(self, 'MV52 Shield drone', min_limit=0, max_limit=2, points=20)


class Aunva(Unit):
    type_name = 'Aun\'va'
    type_id = 'aunva_v1'

    def __init__(self, parent):
        super(Aunva, self).__init__(parent=parent, points=100, unique=True, gear=[
            UnitDescription(self.type_name, options=[Gear('The Paradox of Duality'), Gear('Recon armour')]),
            UnitDescription(name='Honour Guard', options=[Gear('Honour blade'), Gear('Recon armour'),
                                                          Gear('Photon grenades')], count=2)])


class Ethereal(Unit):
    type_name = 'Ethereal'
    type_id = 'ethereal_v1'

    def __init__(self, parent):
        super(Ethereal, self).__init__(parent=parent, points=50)
        self.Weapon(self)
        self.Options(self)
        self.drones = Drones(self)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Ethereal.Weapon, self).__init__(parent=parent, name='Weapon', limit=1)
            self.honourblade = self.variant('Honour blade', 5)
            self.twoequalisers = self.variant('Two equalisers', 10, gear=Gear('Equaliser', count=2))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ethereal.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.blacksunfilter = self.variant('Blacksun filter', 5)
            self.homingbeacon = self.variant('Homing beacon', 5)

    def check_rules(self):
        self.drones.check_rules()


class Aunshi(Unit):
    type_name = 'Aun\'shi'
    type_id = 'aunshi_v1'

    def __init__(self, parent):
        super(Aunshi, self).__init__(
            parent=parent, points=110, unique=True,
            gear=[Gear('Honour blade'), Gear('EMP Grenades'), Gear('Photon grenades'), Gear('Shield generator')]
        )


class Darkstider(Unit):
    type_name = 'Darkstider'
    type_id = 'darkstider_v1'

    def __init__(self, parent):
        super(Darkstider, self).__init__(
            parent=parent, points=100, unique=True,
            gear=[Gear('Recon armour'), Gear('Pulse carabine'), Gear('Photon greandes'), Gear('Blacksun filter'),
                  Gear('Markerlight')]
        )


class Cadre(Unit):
    type_name = 'Cadre Fireblade'
    type_id = 'cadrefireblade_v1'

    def __init__(self, parent):
        super(Cadre, self).__init__(
            parent=parent, points=60,
            gear=[Gear('Combat armour'), Gear('Pulse rifle'), Gear('Photon grenades'), Gear('Markerlight')])
        self.drones = Drones(self)

    def check_rules(self):
        self.drones.check_rules()
