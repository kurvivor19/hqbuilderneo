from builder.core2 import Gear, OneOf, UnitList, ListSubUnit, OptionsList
from builder.games.wh40k.unit import Unit


class SilentSistersSquad(Unit):
    type_name = 'Sisters of Silence Squad'
    type_id = 'silence_squad_v1'

    class Sister(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(SilentSistersSquad.Sister.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Boltgun')
                self.variant('Executioner greatblade')
                self.variant('Flamer', 2)

        class Promotion(OptionsList):
            def __init__(self, parent):
                super(SilentSistersSquad.Sister.Promotion, self).__init__(parent, 'Promote to')
                self.variant('Sister Superior', 10, gear=[])

        def __init__(self, parent):
            super(SilentSistersSquad.Sister, self).__init__(parent, 'Sister of Silence', 15, gear=[
                Gear('Close combat weapon'), Gear('Psyk-out grenades')
            ])
            self.Weapon(self)
            self.up = self.Promotion(self)

        def build_description(self):
            res = super(SilentSistersSquad.Sister, self).build_description()
            if self.up.any:
                res.name = 'Sister Superior'
            return res

        @ListSubUnit.count_gear
        def is_boss(self):
            return self.up.any

    def __init__(self, parent):
        super(SilentSistersSquad, self).__init__(parent)
        self.models = UnitList(self, self.Sister, 5, 10)

    def check_rules(self):
        super(SilentSistersSquad, self).check_rules()
        if sum(sis.is_boss() for sis in self.models.units) > 1:
            self.error('Only one Sister may be upgraded to Sister Superior')

    def get_count(self):
        return self.models.count
