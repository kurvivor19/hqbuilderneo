from builder.core2 import *

__author__ = 'Denis Romanov'

ia_id = ' (IA vol.11)'


class Lynx(Unit):
    clear_name = 'Lynx'
    type_id = 'ia11_lynx_v1'
    type_name = clear_name + ia_id

    def __init__(self, parent):
        super(Lynx, self).__init__(parent=parent, points=320, name=self.clear_name)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Lynx.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.pulsar = self.variant('Pulsar', 0)
            self.soniclance = self.variant('Sonic Lance', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Lynx.Weapon2, self).__init__(parent=parent, name='')
            self.shurikencannon = self.variant('Shuriken Cannon', 0)
            self.scatterlaser = self.variant('Scatter Laser', 10)
            self.starcannon = self.variant('Starcannon', 25)
            self.brightlance = self.variant('Bright Lance', 30)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Lynx.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.vectorengines = self.variant('Vector Engines', 40)
            self.starengines = self.variant('Star Engines', 30)


class Scorpion(Unit):
    clear_name = 'Scorpion'
    type_name = clear_name + ia_id
    type_id = 'ia11_scorpion_v1'

    def __init__(self, parent):
        super(Scorpion, self).__init__(parent=parent, points=500, gear=[Gear('Twin-linked Pulsars')],
                                       name=self.clear_name)
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Scorpion.Weapon, self).__init__(parent=parent, name='Weapon')

            self.shurikencannon = self.variant('Shuriken Cannon', 0)
            self.scatterlaser = self.variant('Scatter Laser', 10)
            self.starcannon = self.variant('Starcannon', 20)
            self.brightlance = self.variant('Bright Lance', 30)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Scorpion.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.vectorengines = self.variant('Vector Engines', 40)
            self.starengines = self.variant('Star Engines', 30)


class Cobra(Unit):
    clear_name = 'Cobra'
    type_name = clear_name + ia_id
    type_id = 'ia11_cobra_v1'

    def __init__(self, parent):
        super(Cobra, self).__init__(parent=parent, points=600, gear=[Gear('D-Cannon')], name=self.clear_name)
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Cobra.Weapon, self).__init__(parent=parent, name='Weapon')

            self.shurikencannon = self.variant('Shuriken Cannon', 0)
            self.scatterlaser = self.variant('Scatter Laser', 10)
            self.eldarmissilelauncher = self.variant('Eldar Missile Launcher', 15)
            self.starcannon = self.variant('Starcannon', 20)
            self.brightlance = self.variant('Bright Lance', 30)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Cobra.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.vectorengines = self.variant('Vector Engines', 40)
            self.starengines = self.variant('Star Engines', 30)


class VampireRaider(Unit):
    clear_name = 'Vampire'
    type_name = clear_name + ia_id
    type_id = 'ia11_vampire_v1'

    def __init__(self, parent):
        super(VampireRaider, self).__init__(parent=parent, points=730, gear=[Gear('Scatter Laser')],
                                            name=self.clear_name)
        self.Weapons(self)

    class Weapons(OneOf):
        def __init__(self, parent):
            super(VampireRaider.Weapons, self).__init__(parent=parent, name='Weapons')
            self.variant('Twin-linked Pulse Lasers', 0, gear=[Gear('Twin-linked Pulse Laser', count=2)])
            self.variant('Twin-linked Phoenix Missile Launchers', 0,
                         gear=[Gear('Twin-linked Phoenix Missile Launcher', count=2)])
            self.pulsar = self.variant('Pulsar', 0)


class VampireHunter(Unit):
    clear_name = 'Vampire Hunter'
    type_name = clear_name + ia_id
    type_id = 'ia11_vampirehunter_v1'

    def __init__(self, parent):
        super(VampireHunter, self).__init__(parent=parent, points=730, name=self.clear_name,
                                            gear=[Gear('Twin-linked Pulsar'),
                                                  Gear('Twin-linked Phoenix Missile Launcher'),
                                                  Gear('Scatter Laser')])


class Revenant(Unit):
    clear_name = 'Revenant Titan'
    type_name = clear_name + ia_id
    type_id = 'ia11_revenanttitan_v1'

    def __init__(self, parent):
        super(Revenant, self).__init__(parent=parent, points=800, gear=[Gear('Revenant Missile Launcher')],
                                       name=self.clear_name)
        self.Weapons(self)

    class Weapons(OneOf):
        def __init__(self, parent):
            super(Revenant.Weapons, self).__init__(parent=parent, name='Weapons')
            self.variant('Pulsars', 0, gear=[Gear('Pulsar', count=2)])
            self.variant('Sonic Lances', 0, gear=[Gear('Sonic Lance', count=2)])


class Phantom(Unit):
    clear_name = 'Phantom Titan'
    type_name = clear_name + ia_id
    type_id = 'ia11_phantomtitan_v1'

    def __init__(self, parent):
        super(Phantom, self).__init__(parent=parent, points=2500, name=self.clear_name,
                                      gear=[Gear('Phantom Titan Missile Launcher'),
                                            Gear('Phantom Titan AA Launcher on Anti-Aircraft Mount')])
        self.Weapon1(self)
        self.Weapon2(self, name='Arm mounted')
        self.Weapon3(self, name='')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Phantom.Weapon1, self).__init__(parent=parent, name='Caparace Mounted Weapon')
            self.variant('Phantom Titan Starcannon', 0)
            self.variant('Pulse Laser', 0)

    class Weapon2(OneOf):
        def __init__(self, parent, name):
            super(Phantom.Weapon2, self).__init__(parent=parent, name=name)
            self.variant('Phantom Titan Pulsar', 0)
            self.variant('Phantom Titan Distortion Cannon', 0)

    class Weapon3(Weapon2):
        def __init__(self, parent, name):
            super(Phantom.Weapon3, self).__init__(parent=parent, name=name)
            self.variant('Titan Close Combat Weapon with Twin-linked Phantom Starcannon', 0, gear=[
                Gear('Titan Close Combat Weapon'),
                Gear('Twin-linked Phantom Starcannon'),
            ])
