__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, StaticUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_counts
from functools import reduce


class Techmarine(Unit):
    name = 'Techmarine'
    base_points = 90
    gear = ['Artificer armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

    def __init__(self):
        Unit.__init__(self)
        self.rng = self.opt_one_of('Weapon', [
            ['Boltgun', 0, 'bgun'],
            ['Storm bolter', 3, 'sbgun'],
            ['Conversion beamer', 20, 'cbeam']
        ])
        self.mlw = self.opt_one_of('', [
            ['Power weapon', 0, 'psw'],
            ['Nemesis force sword', 5, 'nsw'],
            ['Nemesis force halberd', 10, 'nhal'],
            ['Nemesis Daemon hammer', 10, 'nham'],
            ['Pair of Nemesis falcions', 15, 'nfalc'],
            ['Nemesis warding stave', 25, 'nws']
        ])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 3)
        self.opt = self.opt_options_list('Options', [
            ['Blind grenades', 5, 'bg'],
            ['Meltabombs', 5, 'mb'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Empyrean brain mines', 10, 'ebm'],
            ['Rad grenades', 10, 'rg'],
            ['Psychotroke grenades', 15, 'pg'],
            ['Digital weapons', 15, 'dgw'],
            ['Orbital strike relay', 50, 'psr']
        ])

    def check_rules(self):
        Unit.check_rules(self)
        if self.rng.get_cur() != 'cbeam':
            self.description.add('Servo-harness')


class Rhino(Unit):
    name = "Rhino"
    base_points = 40
    gear = ['Storm bolter', 'Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ["Dozer blade", 5, 'dblade'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Warp stabilisation field', 5, 'wsf'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Storm bolter", 10, 'sbgun'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])


class Razorback(Unit):
    name = "Razorback"
    base_points = 45
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Twin-linked heavy bolter", 0, 'tlhbgun'],
            ["Twin-linked heavy flamer", 25, 'tlhflame'],
            ["Twin-linked assault cannon", 35, 'tlasscan'],
            ["Twin-linked lascannon", 35, 'tllcannon'],
            ["Lascannon and twin-linked plasma gun", 35, 'lascplasgun']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ["Dozer blade", 5, 'dblade'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Psyflame ammunition', 5, 'pfl'],
            ['Warp stabilisation field', 5, 'wsf'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Storm bolter", 10, 'sbgun'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])


class Purifiers(Unit):
    name = 'Purifier squad'

    class Knight(Unit):
        name = "Knight of the Flame"
        base_points = 24
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            Unit.__init__(self)
            self.urwep = self.opt_options_list('Weapon', [
                ['Master-crafted Storm bolter', 5, 'uwep']
            ])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 2, 'nfhlb'],
                ['Nemesis Daemon Hammer', 5, 'nham'],
                ['Pair of Nemesis falcions', 5, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])
            self.uwep = self.opt_options_list('', [
                ['Make master-crafted', 5, 'uwep']
            ])
            self.opt = self.opt_options_list('Options', [['Digital weapons', 5, 'dwep']])

        def has_stave(self):
            return self.wep.get_cur() == 'stave'

        def allow_stave(self, val):
            self.wep.set_active_options(['stave'], self.has_stave() or val)

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(exclude=[self.urwep.id, self.uwep.id, self.wep.id])
            if self.urwep.get('uwep'):
                self.description.add('Master-crafted Storm bolter')
            else:
                self.description.add('Storm bolter')
            if self.uwep.get('uwep'):
                self.description.add('Master-crafted ' + self.wep.get_selected())
            else:
                self.description.add(self.wep.get_selected())

    class Purifier(ListSubUnit):
        name = 'Purifier'
        base_points = 24
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.rng = self.opt_one_of('Weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Incinerator', 0, 'inc'],
                ['Psilencer', 0, 'psil'],
                ['Psycannon', 10, 'pcan']
            ])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 2, 'nfhlb'],
                ['Nemesis Daemon Hammer', 5, 'nham'],
                ['Pair of Nemesis falcions', 5, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])

        def has_stave(self):
            return self.wep.get_cur() == 'stave'

        def allow_stave(self, val):
            self.wep.set_active_options(['stave'], (self.has_stave() or val) and self.count == 1)

        def has_hvy(self):
            return self.rng.get_cur() != 'sbgun'

        def allow_hvy(self, val):
            self.rng.set_active_options(['inc', 'psil', 'pcan'], self.has_hvy() or val)

        def check_rules(self):
            self.wep.set_active(not self.has_hvy())
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Knight())
        self.squad = self.opt_units_list(self.Purifier.name, self.Purifier, 4, 9)
        self.opt = self.opt_options_list('Options', [['Psybolt ammunition', 20, 'pba']])
        self.ride = self.opt_optional_sub_unit('Transport', [Rhino(), Razorback()])

    def check_rules(self):
        self.squad.update_range()
        fullunit = self.squad.get_units() + [self.leader.get_unit()]
        stave_cnt = reduce(lambda res, cur: (res + 1) if cur.has_stave() else res, fullunit, 0)
        if stave_cnt > 1:
            self.error('Only one Warding Stave may be present in unit. Taken: ' + stave_cnt)
        for gk in fullunit:
            gk.allow_stave(stave_cnt < 1)
        hvy_lim = 2 * int(self.get_count() / 5)
        hvy_cnt = reduce(lambda res, cur: (res + 1) if cur.has_hvy() else res, self.squad.get_units(), 0)
        if hvy_cnt > hvy_lim:
            self.error("In squad of {0} models only {1} special weapons are allowed. "
                       "Taken: {2}".format(self.get_count(), hvy_lim, hvy_cnt))
        for gk in self.squad.get_units():
            gk.allow_hvy(hvy_cnt < hvy_lim)
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return 1 + self.squad.get_count()


class VenDred(Unit):
    name = 'Venerable Dreadnought'
    base_points = 175
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Multi-melta', 0, 'mmelta'],
            ['Twin-linked heavy flamer', 0, 'tlhflame'],
            ['Twin-linked heavy bolter', 5, 'tlhbgun'],
            ['Twin-linked autocannon', 10, 'tlacan'],
            ['Plasma cannon', 10, 'pcan'],
            ['Assault cannon', 10, 'asscan'],
            ['Twin-linked lascannon', 30, 'tllcan']
        ])
        self.wep2 = self.opt_one_of('', [
            ['Nemesis doomfist', 0, 'dccw'],
            ['Twin-linked autocannon', 5, 'tlacan'],
            ['Missile launcher', 5, 'mlnch']
        ])
        self.bin = self.opt_one_of('', [
            ['Built-in Storm bolter', 0, 'sbgun'],
            ['Built-in Heavy flamer', 10, 'hflame']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Psyflame ammunition', 5, 'pfl'],
            ['Warp stabilisation field', 5, 'wsf'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        Unit.check_rules(self)


class Paladins(Unit):
    name = 'Paladin squad'

    class Paladin(ListSubUnit):
        name = 'Paladin'
        base_points = 55
        gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.prom = self.opt_options_list('Upgrade', [['Upgrade to Apothecary', 75, 'up']])
            self.rng = self.opt_one_of('Weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Incinerator', 5, 'inc'],
                ['Psilencer', 10, 'psil'],
                ['Psycannon', 20, 'pcan']
            ])
            self.rngup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 0, 'nfhlb'],
                ['Nemesis Daemon Hammer', 0, 'nham'],
                ['Pair of Nemesis falcions', 5, 'nfalc'],
                ['Nemesis warding stave', 20, 'stave']
            ])
            self.mlwup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
            self.ban = self.opt_options_list('Options', [['Brotherhood banner', 25, 'ban']])

        def is_doctor(self):
            return self.get_count() if self.prom.get('up') else 0

        def has_stave(self):
            return self.get_count() if self.wep.get_cur() == 'stave' else 0

        def has_hvy(self):
            return self.get_count() if self.rng.is_used() and self.rng.get_cur() != 'sbgun' else 0

        def has_banner(self):
            return self.get_count() if self.ban.get('ban') else 0

        def check_rules(self):
            self.rng.set_visible(not self.is_doctor())
            self.rngup.set_visible(not self.is_doctor())
            self.wep.set_visible(not self.has_banner())
            self.mlwup.set_visible(not self.has_banner())
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.set_points(single_points * self.get_count())
            if self.is_doctor():
                self.build_description(name='Apothecary', exclude=[self.wep.id, self.mlwup.id, self.prom.id,
                                                                   self.count.id], count=1)
                self.description.add('Narthecium')
            else:
                self.build_description(exclude=[self.wep.id, self.mlwup.id, self.rng.id, self.rngup.id,
                                                self.count.id], count=1)
                if self.rngup.get('wup'):
                    self.description.add('Master-crafted ' + self.rng.get_selected())
                else:
                    self.description.add(self.rng.get_selected())
            if not self.has_banner():
                if self.mlwup.get('wup'):
                    self.description.add('Master-crafted ' + self.wep.get_selected())
                else:
                    self.description.add(self.wep.get_selected())

    def __init__(self):
        Unit.__init__(self)
        self.squad = self.opt_units_list(self.Paladin.name, self.Paladin, 1, 10)
        self.opt = self.opt_options_list('Options', [['Psybolt ammunition', 20, 'pba']])

    def check_rules(self):
        self.squad.update_range()
        stave_cnt = sum((cur.has_stave() for cur in self.squad.get_units()))
        if stave_cnt > 1:
            self.error('Only one Warding Stave may be present in unit. Taken: {0}'.format(stave_cnt))
        hvy_lim = 2 * int(self.squad.get_count() / 5)
        hvy_cnt = sum((cur.has_hvy() for cur in self.squad.get_units()))
        if hvy_cnt > hvy_lim:
            self.error("In squad of {0} models only {1} special weapons are allowed. "
                       "Taken: {2}".format(self.squad.get_count(), str(hvy_lim), hvy_cnt))
        ban_cnt = sum((cur.has_banner() for cur in self.squad.get_units()))
        if ban_cnt > 1:
            self.error('Only one Brotherhood Banner may be present in unit. Taken: {0}'.format(ban_cnt))
        doc_cnt = sum((cur.is_doctor() for cur in self.squad.get_units()))
        if doc_cnt > 1:
            self.error('Only one Paladin may be upgraded to Apothecary')
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.squad.get_count()


class Callidus(StaticUnit):
    name = 'Callidus Assassin'
    base_points = 145
    gear = ['Synskin', 'Neural shredder', 'C\'tan phase sword', 'Frag grenades', 'Polymorphine']


class Eversor(StaticUnit):
    name = 'Eversor Assassin'
    base_points = 130
    gear = ['Synskin', 'Executioner pistol', 'Neuro gauntlet', 'Frag grenades', 'Melta bombs', 'Frenzon']


class Culexus(StaticUnit):
    name = 'Culexus Assassin'
    base_points = 135
    gear = ['Synskin', 'Animus speculum', 'Frag grenades', 'Psyk-out grenades', 'Etherium', 'Psyocculum']


class Vindicare(StaticUnit):
    name = 'Vindicare Assassin'
    base_points = 145
    gear = ['Synskin', 'Exitus pistol', 'Exitus rifle', 'Frag grenades', 'Blind grenades']


class Chimera(Unit):
    name = "Inquisitorial Chimera"
    base_points = 55
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ["Multi-laser", 0, 'mlas'],
            ["Heavy flamer", 0, 'hflame'],
            ["Heavy bolter", 0, 'hbgun']
        ])
        self.wep2 = self.opt_one_of('', [
            ["Heavy flamer", 0, 'hflame'],
            ["Heavy bolter", 0, 'hbgun']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ["Dozer blade", 5, 'dblade'],
            ['Warp stabilisation field', 5, 'wsf'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Storm bolter", 10, 'sbgun'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])


class Warband(Unit):
    name = 'Inquisitorial Henchmen Warband'

    class Acolyte(ListSubUnit):
        base_points = 4
        name = 'Warrior Acolyte'
        spec = [
            ['Combi-flamer', 10, 'cflame'],
            ['Combi-melta', 10, 'cmelta'],
            ['Combi-plasma', 10, 'cplasma'],
            ['Plasma gun', 10, 'pgun'],
            ['Meltagun', 10, 'mgun'],
            ['Flamer', 10, 'flame'],
            ['Power sword', 15, 'psw'],
            ['Plasma pistol', 15, 'ppist'],
            ['Storm shield', 20, 'ss'],
            ['Power fist', 25, 'pfist']
        ]
        generic = [
            ['Bolter', 1, 'bgun'],
            ['Storm bolter', 3, 'sbgun'],
            ['Hot-shot lasgun', 5, 'hslgun']
        ]

        def __init__(self):
            ListSubUnit.__init__(self, max_models=12)
            self.spec_ids = [v[2] for v in self.spec]
            self.wep1 = self.opt_one_of('Weapon', [['Laspistol', 0, 'lpist']] + self.generic + self.spec)
            self.wep2 = self.opt_one_of('', [['Chainsword', 0, 'csw']] + self.generic + self.spec)
            self.armour = self.opt_one_of('Armour', [
                ['Flak armour', 0, 'flak'],
                ['Caparace armour', 4, 'cap'],
                ['Power armour', 10, 'pwr']
            ])
            self.opt = self.opt_options_list('Options', [['Melta bombs', 5, 'mbomb']])

        def has_spec(self):
            return self.get_count() \
                if self.wep1.get_cur() in self.spec_ids or self.wep2.get_cur() in self.spec_ids else 0

    def __init__(self):
        Unit.__init__(self)
        self.acolyte = self.opt_options_list('', [[self.Acolyte.name, self.Acolyte.base_points]])
        self.acolyte_unit = self.opt_units_list('', self.Acolyte, 1, 12)
        self.arc = self.opt_count('Arco-flagellant', 0, 12, 15)
        self.ban = self.opt_count('Banisher', 0, 12, 15)
        self.ban_wep = self.opt_count('Evisecrator', 0, 0, 15)
        self.crus = self.opt_count('Crusader', 0, 12, 15)
        self.host = self.opt_count('Daemonhost', 0, 12, 12)
        self.cult = self.opt_count('Assassin', 0, 12, 15)
        self.serv = self.opt_count('Servitor', 0, 12, 10)
        self.serv_hb = self.opt_count('Heavy bolter', 0, 0, 0)
        self.serv_mm = self.opt_count('Multi-melta', 0, 0, 0)
        self.serv_pc = self.opt_count('Plasma cannon', 0, 0, 10)
        self.jok = self.opt_count('Jokaero', 0, 12, 35)
        self.mys = self.opt_count('Mystic', 0, 12, 10)
        self.psy = self.opt_count('Psyker', 0, 12, 10)
        self.ride = self.opt_optional_sub_unit('Transport', [Rhino(), Razorback(), Chimera()])
        self.counts = [self.arc, self.ban, self.crus, self.host, self.cult, self.serv, self.jok,
                       self.mys, self.psy]
        self.wep = [self.ban_wep, self.serv_hb, self.serv_mm, self.serv_pc]

    def get_count(self):
        return sum((lst.get() for lst in self.counts)) + self.acolyte_unit.get_count()

    def check_rules(self):
        self.acolyte_unit.set_active(self.acolyte.get_all() != [])
        self.acolyte_unit.update_range()
        spec = sum((u.has_spec() for u in self.acolyte_unit.get_units()))
        if spec > 3:
            self.error('Only 3 Warrior Acolyte can take special weapon. Taken: {0}'.format(spec))
        self.ban_wep.set_active(self.ban.get() > 0)
        self.ban_wep.update_range(0, self.ban.get())
        self.serv_hb.set_active(self.serv.get() > 0)
        self.serv_mm.set_active(self.serv.get() > 0)
        self.serv_pc.set_active(self.serv.get() > 0)
        norm_counts(0, min(3, self.serv.get()), [self.serv_hb, self.serv_mm, self.serv_pc])
        self.set_points(self.build_points(count=1, exclude=[self.acolyte.id]))
        self.build_description(exclude=[self.ride.id] + [u.id for u in self.counts + self.wep + [self.acolyte]])
        self.description.add(ModelDescriptor(name='Arco-flagellant', gear=['Arco-flails'], points=15)
            .build(self.arc.get()))
        ban = ModelDescriptor(points=15, gear=['Flak armour', 'Laspistol'], name='Banisher')
        self.description.add(ban.clone().add_gear('Chainsword').build(self.ban.get() - self.ban_wep.get()))
        self.description.add(ban.clone().add_gear('Evisecrator', points=15).build(self.ban_wep.get()))
        self.description.add(ModelDescriptor(points=15, gear=['Flak armour', 'Power weapon', 'Storm shield'],
                                             name='Crusader').build(self.crus.get()))
        self.description.add(ModelDescriptor(points=12, gear=['Fearsome claws & runic chains'],
                                             name='Daemonhost').build(self.host.get()))
        self.description.add(ModelDescriptor(points=15, gear=['Flak armour', 'Power weapon', 'Power weapon'],
                                             name='Death Cult Assassin').build(self.cult.get()))
        serv = ModelDescriptor(points=10, gear=['Flak armour'], name='Servitor')
        self.description.add(serv.clone().add_gear('Heavy bolter').build(self.serv_hb.get()))
        self.description.add(serv.clone().add_gear('Multi-melta').build(self.serv_mm.get()))
        self.description.add(serv.clone().add_gear('Plasma cannon', points=10).build(self.serv_pc.get()))
        self.description.add(serv.clone().add_gear('Servo-arm').build(self.serv.get() - self.serv_hb.get() -
                                                                      self.serv_mm.get() - self.serv_pc.get()))
        self.description.add(ModelDescriptor(points=35, name='Jokaero weaponsmith',
                                             gear=['Defense orbs', 'Digital weapons']).build(self.jok.get()))
        self.description.add(ModelDescriptor(points=10,
                                             gear=['Flak armour', 'Laspistol'], name='Mystic').build(self.mys.get()))
        self.description.add(ModelDescriptor(points=10,
                                             gear=['Flak armour', 'Laspistol'], name='Psyker').build(self.psy.get()))
        self.description.add(self.ride.get_selected())
        if not 3 <= self.get_count() <= 12:
            self.error(self.name + ' must include 3-12 henchmen. Taken: {0}'.format(self.get_count()))
