__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.grey_knights.hq import *
from builder.games.wh40k.obsolete.grey_knights.elites import *
from builder.games.wh40k.obsolete.grey_knights.troops import *
from builder.games.wh40k.obsolete.grey_knights.fast import *
from builder.games.wh40k.obsolete.grey_knights.heavy import *
from functools import reduce

class GreyKnights(LegacyWh40k):
    army_name = 'Grey Knights'
    army_id = 'bf17f5c0d738421aa1603a4bb715dbff'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[Draigo, Mordrak, Stern, Crowe, GrandMaster, Captain, Champion, Librarian,Karamazov,Coteaz,Valeria,Xenos,Hereticus,Malleus],
            elites = [Techmarine,Purifiers,VenDred,Paladins,Callidus,Eversor,Culexus,Vindicare,{'unit': Warband, 'active': True}],
            troops = [Terminators,Strikes,{'unit': Warband, 'active': False},{'unit': Purifiers, 'active': False},{'unit': Paladins, 'active': False}],
            fast = [StormravenGunship,Interceptors],
            heavy = [Purgations,Dred,Dreadknight,LandRaider,LRCrusader,LRRedeemer],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.GK)
        )
        def check_elite_limit():
            count = len(self.hq.get_units()) - self.hq.count_unit(Warband)
            return self.elites.min <= count <= self.elites.max
        self.elites.check_limits = check_elite_limit

        def check_troop_limit():
            min_count = len(self.troops.get_units())
            max_count = reduce(lambda val,tr: val + (2 if tr.get_count() == 10 and not isinstance(tr,Warband)else 1),self.troops.get_units(),0)
            return self.troops.min <= max_count and min_count <= self.troops.max
        self.troops.check_limits = check_troop_limit

    def check_rules(self):
        has_draigo = self.hq.count_unit(Draigo) > 0
        self.elites.set_active_types([Paladins],not has_draigo)
        self.troops.set_active_types([Paladins],has_draigo)
        if self.troops.count_unit(Paladins) > 0 and not has_draigo:
            self.error("Paladins can only be troop choices in army including Lord Caldor Draigo.")
        elif self.elites.count_unit(Paladins) > 0 and has_draigo:
            self.error("Paladins are troop choice in army including Caldor Draigo instead of elites.")
        has_crowe = self.hq.count_unit(Crowe) > 0
        self.elites.set_active_types([Purifiers],not has_crowe)
        self.troops.set_active_types([Purifiers],has_crowe)
        if self.troops.count_unit(Purifiers) > 0 and not has_crowe:
            self.error("Purifiers can only be troop choices in army including Castellan Crowe.")
        elif self.elites.count_unit(Purifiers) > 0 and has_crowe:
            self.error("Purifiers are troop choice in army including Castellan Crowe instead of elites.")
        if self.hq.count_unit(Coteaz) > 0:
            self.elites.set_active_types([Warband],False)
            self.troops.set_active_types([Warband],True)
            if self.elites.count_unit(Warband) > 0:
                self.error("Inquisitorial warbands are troop choice in army including Inquisitor Coteaz instead of elites.")
        else:
            self.troops.set_active_types([Warband],False)
            if self.troops.count_unit(Warband) > 0:
                self.error("Inquisitorial warbands can only be troop choices in army including Inquisitor Coteaz.")
            inq_cnt = self.hq.count_units([Karamazov,Valeria,Malleus,Xenos,Hereticus])
            self.elites.set_active_types([Warband],inq_cnt > 0)
            if self.elites.count_unit(Warband) > inq_cnt:
                self.error("Only one Inquisitorial warband can be taken per inquisitor.")

