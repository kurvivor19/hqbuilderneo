__author__ = 'Denis Romanov'

from builder.core2 import *
from .armory import *
from .troops import RhinoTransport, ChaosSpaceMarines
from builder.games.wh40k.imperial_armour.volume13.transport import IATransportedUnit
from builder.games.wh40k.imperial_armour.volume13.options import ChaosSpaceMarinesBaseVehicle,\
    ChaosSpaceMarinesBaseSquadron
from builder.games.wh40k.unit import Unit


class Havoks(IconicUnit, IATransportedUnit):
    type_name = 'Havoks'
    type_id = 'havoks_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Havocs')

    model_points = 13

    class Marine(IconBearerGroup):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(Havoks.Marine.Weapon, self).__init__(parent=parent, name='Weapon')

                self.bg = self.variant('Boltgun', 0)
                self.variant('Flamer', 5)
                self.variant('Heavy bolter', 10)
                self.variant('Autocannon', 10)
                self.variant('Meltagun', 10)
                self.variant('Plasma gun', 15)
                self.ml = self.variant('Missile launcher', 15)
                self.variant('Lascannon', 20)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Havoks.Marine.Options, self).__init__(parent=parent, name='')
                self.variant('Close combat weapon', 2)
                self.flakkmissiles = self.variant('Flakk missiles', 10)

        def __init__(self, parent):
            super(Havoks.Marine, self).__init__(
                parent, name='Havoc', points=Havoks.model_points, gear=Armour.power_armour_set + [Gear('Bolt pistol')],
                wrath=20, flame=15, despair=10, excess=30, vengeance=25
            )
            self.weapon = self.Weapon(self)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon.cur != self.weapon.bg

        def check_rules(self):
            super(Havoks.Marine, self).check_rules()
            self.opt.flakkmissiles.visible = self.opt.flakkmissiles.used = self.weapon.cur == self.weapon.ml

    def __init__(self, parent):
        super(Havoks, self).__init__(parent)
        self.marks = Marks(self, khorne=2, tzeentch=2, nurgle=3, slaanesh=2, per_model=True)
        self.champion = SubUnit(self, ChaosSpaceMarines.Champion(None))
        self.models = UnitList(self, self.Marine, min_limit=4, max_limit=9)

        self.veteran = Veterans(self, points=1, per_model=True)
        self.transport = RhinoTransport(self)

    def check_rules(self):
        super(Havoks, self).check_rules()
        heavy = sum(o.count_heavy() for o in self.models.units)
        if heavy > 4:
            self.error("Havoks can't have more then 4 heavy weapon (taken: {0})".format(heavy))


class Obliterators(Unit):
    type_name = "Obliterators"
    type_id = "obliterators_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Obliterator')

    def __init__(self, parent):
        Unit.__init__(self, parent)
        self.models = Count(
            self, 'Obliterators', 1, 3, 70,
            gear=UnitDescription('Obliterator', points=55,
                                 options=[Gear('Fleshmetal'), Gear('Power fist'), Gear('Obliterator weapons')])
        )
        self.marks = Marks(self, khorne=4, tzeentch=8, nurgle=6, slaanesh=1, per_model=True)
        Veterans(self, 3, per_model=True)

    def get_count(self):
        return self.models.cur

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Defiler(Unit):
    type_name = 'Defiler'
    type_id = 'defiler_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Defiler')

    def __init__(self, parent):
        super(Defiler, self).__init__(
            parent=parent, points=195,
            gear=[Gear('Battle cannon'), Gear('Power fist', count=2), Gear('Searchlight'), Gear('Daemonic posession'),
                  Gear('Smoke launchers')])
        self.Weapon1(self)
        self.Weapon2(self)
        Vehicle(self, tank=False)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Defiler.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Twin-linked heavy flamer', 0)
            self.variant('Havoc launcher', 5)
            self.variant('Power scourge', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Defiler.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Reaper autocannon', 0)
            self.variant('Power fist', 0)
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked lascannon', 20)


class Predator(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Predator'
    type_id = 'chaospredator_v1'

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=75, gear=[Gear('Smoke launchers'), Gear('Searchlight')],
                                       tank=True)
        self.Weapon(self)
        self.Sponsons(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Predator.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Autocannon', 0)
            self.variant('Twin-linked lascannon', 25)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='', limit=1)
            self.variant('Sponsons with heavy bolters', 20, gear=Gear('Heavy Bolter', count=2))
            self.variant('Sponsons with lascannons', 40, gear=Gear('Lascannon', count=2))


class ChaosPredators(ChaosSpaceMarinesBaseSquadron):
    type_name = 'Chaos Predators'
    type_id = 'chaospredator_sq_v1'
    unit_class = Predator
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Predator')


class Forgefiend(Unit):
    type_name = 'Forgefiend'
    type_id = 'forgefiend_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Forgefiend')

    def __init__(self, parent):
        super(Forgefiend, self).__init__(parent=parent, points=175, gear=[Gear('Daemonic posession'), ])
        self.Weapon1(self)
        self.Weapon2(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Forgefiend.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Hades autocannons', 0, gear=Gear('Hades autocannon', count=2))
            self.variant('Ectoplasma cannons', 0, gear=Gear('Ectoplasma cannon', count=2))

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(Forgefiend.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Ectoplasma cannon', 25)


class Maulerfiend(Unit):
    type_name = 'Maulerfiend'
    type_id = 'maulerfiend_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Maulerfiend')

    def __init__(self, parent):
        super(Maulerfiend, self).__init__(parent=parent, points=125, gear=[Gear('Daemonic posession'),
                                                                           Gear('Power fists', count=2)])
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Maulerfiend.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Magma cutters', 0,
                         gear=Gear('Magma cutter', count=2))
            self.variant('Lasher tendrils', 10,
                         gear=Gear('Lasher tendrils', count=2))


class Vindicator(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Vindicator'
    type_id = 'chaosvindicator_v1'

    class Vehicle(Vehicle):
        def __init__(self, parent):
            super(Vindicator.Vehicle, self).__init__(parent)
            self.variant('Siege shield', 10)

    def __init__(self, parent):
        super(Vindicator, self).__init__(
            parent=parent, points=120, gear=[Gear('Demolisher cannon'), Gear('Smoke launchers'), Gear('Searchlight')],
            tank=True)
        self.Vehicle(self)


class ChaosVindicators(ChaosSpaceMarinesBaseSquadron):
    type_name = 'Chaos Vindicators'
    type_id = 'chaosvindicator_sq_v1'
    unit_class = Vindicator
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Vindicator')


class LandRaider(ChaosSpaceMarinesBaseVehicle):
    type_name = "Chaos Land Raider"
    type_id = "chaos_land_raider_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Land-Raider')

    def __init__(self, parent):
        super(LandRaider, self).__init__(
            parent, points=230,
            gear=[Gear('Twin-linked heavy bolter'), Gear('Twin-linked lascannon', count=2),
                  Gear('Smoke launchers'), Gear('Searchlight')],
            tank=True, transport=True)
        Vehicle(self)


from builder.games.wh40k.imperial_armour.dataslates.space_marines import BaseDeimos


class DeimosVindicator(BaseDeimos, ChaosSpaceMarinesBaseVehicle):
    def get_options(self):
        class Options(Vehicle):
            def __init__(self, parent):
                super(Options, self).__init__(parent)
                self.variant('Siege shield', 10)
        return Options
