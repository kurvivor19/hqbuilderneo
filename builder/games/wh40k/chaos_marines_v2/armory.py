__author__ = 'Denis Romanov'

from builder.core2 import OneOf, OptionsList, Gear, ListSubUnit
from builder.games.wh40k.roster import Unit


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.marks = kwargs.pop('marks', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.artefact_weapon = []

        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.artefact_weapon:
            return self.description
        return []


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class InfernoBoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(InfernoBoltPistol, self).__init__(*args, **kwargs)
        self.ibp = self.variant('Inferno bolt pistol', 0)
        self.pwr_weapon += [self.ibp]


class PowerAxe(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerAxe, self).__init__(*args, **kwargs)
        self.poweraxe = self.variant('Power axe', 0)
        self.pwr_weapon += [self.poweraxe]


class PowerMaul(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerMaul, self).__init__(*args, **kwargs)
        self.powermaul = self.variant('Power maul', 0)
        self.pwr_weapon += [self.powermaul]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Ccw(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Ccw, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Close combat weapon', 0)
        self.pwr_weapon += [self.chainsword]


class ForceWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ForceWeapon, self).__init__(*args, **kwargs)
        self.fw = self.variant('Force weapon', 0)
        self.pwr_weapon += [self.fw]


class ForceStave(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ForceStave, self).__init__(*args, **kwargs)
        self.fs = self.variant('Force stave', 0)
        self.pwr_weapon += [self.fs]


class Melee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Melee, self).__init__(*args, **kwargs)
        self.pwr_weapon += [
            self.variant('Chainaxe', 8),
            self.variant('Lightning claw', 15),
            self.variant('Power weapon', 15),
            self.variant('Power fist', 25),
        ]


class Ranged(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Ranged, self).__init__(*args, **kwargs)
        self.unique_ranged = [
            self.variant('Combi-bolter', 3),
            self.variant('Combi-melta', 10),
            self.variant('Combi-flamer', 10),
            self.variant('Combi-plasma', 10),
            self.variant('Plasma pistol', 15),
        ]
        self.pwr_weapon += self.unique_ranged

    def has_unique_ranged(self):
        return self.cur in self.unique_ranged

    def set_unique_ranged(self, val):
        for o in self.unique_ranged:
            o.active = val


class TerminatorBolter(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorBolter, self).__init__(*args, **kwargs)
        self.tda_weapon += [
            self.variant('Combi-bolter', 0),
        ]


class TerminatorPower(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorPower, self).__init__(*args, **kwargs)
        self.tda_weapon += [
            self.variant('Power weapon', 0),
        ]


class TerminatorForce(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorForce, self).__init__(*args, **kwargs)
        self.tda_weapon += [
            self.variant('Force weapon', 0),
        ]


class TerminatorMelee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorMelee, self).__init__(*args, **kwargs)
        self.tda_weapon += [
            self.variant('Lightning claw', 5),
            self.variant('Power fist', 10),
            self.variant('Chainfist', 15),
        ]


class TerminatorRanged(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorRanged, self).__init__(*args, **kwargs)
        self.tda_weapon += [
            self.variant('Combi-melta', 7),
            self.variant('Combi-flamer', 7),
            self.variant('Combi-plasma', 7),
            self.variant('Power weapon', 12),
            self.variant('Lightning claw', 17),
            self.variant('Power fist', 22),
            self.variant('Chainfist', 27),
        ]


class ArtefactWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        self.crozius = kwargs.pop('crozius', False)
        melee = kwargs.pop('melee', True)
        super(ArtefactWeapon, self).__init__(*args, **kwargs)
        self.burningbrandofskalathrax = self.variant('Burning Brand of Skalathrax', 30)
        self.axeofblindfury = self.variant('Axe of Blind Fury', 35)
        self.themurdersword = self.variant('The Murder Sword', 35)
        self.theblackmace = self.variant('The Black Mace', 45)
        self.csm = [self.burningbrandofskalathrax, self.axeofblindfury, self.theblackmace, self.themurdersword]

        self.crimson = []
        if self.crozius:
            self.crimson.append(self.variant('Crozius of the Dark Covenant', 30))

        self.thousand = []
        self.alpha = []
        self.iron = []
        self.night = []
        self.blackclaws = melee and self.variant('Claws of the Black Hunt', 40)
        self.word = []
        self.eater = []
        self.guard = []
        self.child = []
        if melee:
            self.thousand += [self.variant("Seer's Bane", 40),
                              self.variant('Staff of Arcane Compulsion', 10)]
            self.alpha.append(self.variant('Blade of the Hydra', 30))
            self.iron += [self.variant('Axe of the Forgemaster', 25),
                          self.variant('Siegebreaker Mace', 25)]
            self.night.append(self.blackclaws)
            self.word.append(self.variant('The Curzed Crozius', 35))
            self.eater += [self.variant('The Berserker Glaive', 30),
                           self.variant('Gorefather', 30),
                           self.variant('Bloodfeeder', 30)]
            self.guard += [self.variant('Puscleaver', 10),
                           self.variant('The Pandemic Staff', 30),
                           self.variant('Plaguebringer', 35)]
            self.child += [self.variant('The Soulsnare Lash', 20),
                           self.variant('Blissgiver', 30)]
        else:
            self.thousand.append(self.variant('Coruscator', 20))
            self.alpha.append(self.variant("Viper's Bite", 15))
            self.eater.append(self.variant('The Crimson Killer', 20))

        self.artefact_weapon = self.csm + self.crimson + self.thousand +\
                               self.alpha + self.iron + self.night +\
                               self.guard + self.child + self.eater

    @staticmethod
    def _hide_options(opt, val):
        for o in opt:
            o.visible = o.used = val

    def check_rules(self):
        super(ArtefactWeapon, self).check_rules()
        # self._hide_options(self.csm, self.roster.is_base or self.roster.is_black)
        self._hide_options(self.crimson, self.roster.is_crimson)
        self._hide_options(self.thousand, self.roster.is_thousand)
        self._hide_options(self.alpha, self.roster.is_alpha)
        self._hide_options(self.iron, self.roster.is_iron)
        self._hide_options(self.night, self.roster.is_night)
        self._hide_options(self.word, self.roster.is_word)
        self._hide_options(self.eater, self.roster.is_eater)
        self._hide_options(self.guard, self.roster.is_guard)
        self._hide_options(self.child, self.roster.is_child)
        # if self.roster.is_base:
        self.axeofblindfury.active = self.marks.khorne

    @staticmethod
    def process_artefacts(weapon1, weapon2):
        # for r1, r2 in zip(weapon1.artefact_weapon, weapon2.artefact_weapon):
        #     r2.active = not weapon1.cur == r1
        #     r1.active = not weapon2.cur == r2

        def check_mark(weapon):
            if weapon.marks:
                weapon.axeofblindfury.active = weapon.marks.khorne and weapon.axeofblindfury.active

        check_mark(weapon1)
        check_mark(weapon2)


class Marks(OptionsList):
    def __init__(self, parent, khorne=10, tzeentch=15, nurgle=15, slaanesh=15, per_model=False):
        super(Marks, self).__init__(parent, name='Mark of Chaos', limit=1)

        self._khorne = khorne and self.variant('Mark of Khorne', khorne, per_model=per_model)
        self._tzeentch = self.variant('Mark of Tzeentch', tzeentch, per_model=per_model)
        self._nurgle = self.variant('Mark of Nurgle', nurgle, per_model=per_model)
        self._slaanesh = self.variant('Mark of Slaanesh', slaanesh, per_model=per_model)

    @property
    def khorne(self):
        return self._khorne and (self._khorne.value or self.parent.roster.is_eater)

    @property
    def tzeentch(self):
        return self._tzeentch.value or self.parent.roster.is_thousand

    @property
    def nurgle(self):
        return self._nurgle.value or self.parent.roster.is_guard

    @property
    def slaanesh(self):
        return self._slaanesh.value or self.parent.roster.is_child

    def set_mark_only(self, mark, mark_cost=0):
        for v in [self._khorne, self._tzeentch, self._nurgle, self._slaanesh]:
            if v == mark or (not v):
                continue
            v.value = v.active = False
        mark.points = mark_cost
        mark.active = False
        mark.value = True

    @property
    def points(self):
        if self.parent.roster.is_thousand:
            return self.parent.get_count() * self._tzeentch.points
        if self.parent.roster.is_eater and self._khorne:
            return self.parent.get_count() * self._khorne.points
        if self.parent.roster.is_guard:
            return self.parent.get_count() * self._nurgle.points
        if self.parent.roster.is_child:
            return self.parent.get_count() * self._slaanesh.points
        return self.parent.get_count() * super(Marks, self).points

    @property
    def description(self):
        if self.parent.roster.is_thousand:
            return [self._tzeentch.gear]
        if self.parent.roster.is_eater and self._khorne:
            return [self._khorne.gear]
        if self.parent.roster.is_guard:
            return [self._nurgle.gear]
        if self.parent.roster.is_child:
            return [self._slaanesh.gear]
        return super(Marks, self).description

    def check_rules(self):
        super(Marks, self).check_rules()
        self.visible = True
        if self.parent.roster.is_alpha or\
           self.parent.roster.is_iron or\
           self.parent.roster.is_night:
            self.used = self.visible = False
        if self.parent.roster.is_thousand or\
           self.parent.roster.is_eater or\
           self.parent.roster.is_guard or\
           self.parent.roster.is_child:
            self.visible = False


class ArtefactGear(OptionsList):
    def __init__(self, parent, marks, is_psy=None, lord=False,
                 sorc=False, prince=False, smith=False):
        super(ArtefactGear, self).__init__(parent, name='Chaos Artefact', limit=1)
        self.marks = marks
        self.is_psy = is_psy
        self.smith = smith

        self.key = self.variant('Dimensional Key', 25)
        self.magnus = self.variant('Scrolls of Magnus', 45)

        self.csm = [self.magnus, self.key]

        self.thespineshiverblade = self.variant('The Spineshiver Blade', 30)
        self.thecrucibleoflies = self.variant('The Crucible of Lies', 25)
        self.theeyeofnight = self.variant('The Eye of Night', 75)
        self.theskullofkerngar = self.variant('The Skull of Ker\'ngar', 40)
        self.thehandofdarkness = self.variant('The Hand of Darkness', 50)
        self.lastmemoryoftheyuranthos = self.variant('Last Memory of the Yuranthos', 30)
        self.black = [self.thespineshiverblade, self.thecrucibleoflies, self.theeyeofnight,
                      self.theskullofkerngar,
                      self.thehandofdarkness, self.lastmemoryoftheyuranthos]

        self.crimson = []
        if lord:
            self.crimson.append(self.variant('Blade of the Relentless', 30))
        self.crimson.append(self.variant('The Slaughterer\'s Horns', 15))
        if sorc:
            self.crimson.append(self.variant('The Balestar of Mannon', 25))
        if not prince:
            self.crimson.append(self.variant('Daemonheart', 30))
        self.crimson.append(self.variant('Prophet of the Voices', 30))

        self.thousand = [self.variant('Astral Grimoire', 30),
                         self.variant('Helm of the Third Eye', 20),
                         self.variant('Athenaean Scrolls', 20)]
        self.alpha = [self.variant('The Mindveil', 30),
                      self.variant('The Drakescale Plate', 25),
                      self.variant('Icon of Insurrection', 25),
                      self.variant("Hydra's Teeth", 15)]
        self.iron = []
        if self.smith:
            self.iron += [self.variant('Warpbreacher', 25),
                          self.variant('Nest of Mechaserpents', 20)]
        self.iron += [self.variant('Fleshmetal exosceleton', 30),
                      self.variant('Cranium Malevolis', 30)]

        self.night = [self.variant('Scourging Chains', 10),
                      self.variant('Talons of the Night Terror', 20),
                      self.variant('Vox Daemonicus', 30),
                      self.variant("Curze's Orb", 20),
                      self.variant('Stormbolt Plate', 20)]

        self.word = [self.variant('The Skull of Monarchia', 15),
                     self.variant('Crown of the Blasphemer', 30),
                     self.variant('The Scripts of Erebus', 15),
                     self.variant('Baleful Icon', 15)]
        self.maltome = self.variant('The Malefic Tome', 10)
        self.word.append(self.maltome)
        self.eater = [self.variant('Talisman of Burning Blood', 25),
                      self.variant('Brass collar of Bhorghaster', 20)]
        self.guard = [self.variant('Plague Skull of Glothila', 15),
                      self.variant('Dolorous Knell', 25),
                      self.variant('Poxwalker Hive', 20)]
        self.child = [self.variant('Intoxicating Elixir', 25),
                      self.variant('Shriekwave', 20),
                      self.variant('The Endless Grin', 10),
                      self.variant('Bolts of Ecstatic Vexation', 15)]

    @staticmethod
    def _hide_options(opt, val):
        for o in opt:
            o.visible = o.used = val

    def check_rules(self):
        super(ArtefactGear, self).check_rules()
        # self._hide_options(self.csm, self.roster.is_base or self.roster.is_black)
        self._hide_options(self.black, self.roster.is_black)
        self._hide_options(self.crimson, self.roster.is_crimson)
        self._hide_options(self.thousand, self.roster.is_thousand)
        self._hide_options(self.alpha, self.roster.is_alpha)
        self._hide_options(self.iron, self.roster.is_iron)
        self._hide_options(self.night, self.roster.is_night)
        self._hide_options(self.word, self.roster.is_word)
        self._hide_options(self.eater, self.roster.is_eater)
        self._hide_options(self.guard, self.roster.is_guard)
        self._hide_options(self.child, self.roster.is_child)
        # if self.roster.is_base:
        self.magnus.visible = self.magnus.used = self.marks.tzeentch
        if self.roster.is_black:
            self.lastmemoryoftheyuranthos.visible = self.lastmemoryoftheyuranthos.used = self.is_psy and self.is_psy()
        if self.roster.is_word:
            self.maltome.used = self.maltome.visible = self.is_psy and self.is_psy()

    def get_unique(self):
        if self.used:
            return self.description
        return []

    def count_charges(self):
        if self.lastmemoryoftheyuranthos.used and self.lastmemoryoftheyuranthos.value:
            return 1
        return 0


class Rewards(OptionsList):
    def __init__(self, parent, armour=None, marks=None, spec=True, ride=True, apostle=False):
        super(Rewards, self).__init__(parent, name='Chaos Rewards')
        self.marks = marks
        self.armour = armour
        self.spec = spec
        self.ride = ride
        self.ride_opt = []
        if self.spec:
            self.meltabombs = self.variant('Melta bombs', 5)
            self.blightgrenades = self.variant('Blight grenades', 5)
            if self.ride:
                self.jumppack = self.variant('Jump pack', 15)
                self.bike = self.variant('Chaos bike', 20)
                self.ride_opt += [self.jumppack, self.bike]
            if not apostle:
                self.sigilofcorruption = self.variant('Sigil of corruption', 25)

        self.ichorblood = self.variant('Ichor blood', 5)
        self.giftofmutation = self.variant('Gift of mutation', 10)
        self.auraofdarkglory = self.variant('Aura of dark glory', 15)
        self.combatfamiliar = self.variant('Combat familiar', 15)
        self.spellfamiliar = self.variant('Spell familiar', 15)

        if self.ride:
            self.juggernaut = self.variant('Juggernaut of Khorne', 35)
            self.disk = self.variant('Disk of Tzeentch', 30)
            self.palanquin = self.variant('Palanquin of Nurgle', 40)
            self.steed = self.variant('Steed of Slaanesh', 20)

            self.ride_opt += [self.palanquin, self.disk, self.juggernaut, self.steed]

    def check_rules(self):
        super(Rewards, self).check_rules()
        if self.spec:
            self.blightgrenades.visible = self.blightgrenades.used = self.marks.nurgle

        if self.ride:
            if not self.armour or (self.armour and not self.armour.is_tda()):
                self.palanquin.visible = self.palanquin.used = self.marks.nurgle
                self.steed.visible = self.steed.used = self.marks.slaanesh
                self.disk.visible = self.disk.used = self.marks.tzeentch
                self.juggernaut.visible = self.juggernaut.used = self.marks.khorne
                if self.spec:
                    self.bike.visible = self.bike.used = True
                    self.jumppack.visible = self.jumppack.used = True
                self.process_limit(self.ride_opt, 1)
            else:
                for o in self.ride_opt:
                    o.visible = o.used = False

    def pick_free_ride(self, ride):
        if self.ride:
            for r in self.ride_opt:
                if r == ride:
                    r.points = 0
                    r.active = False
                    r.value = True
                else:
                    r.used = r.visible = False


class Armour(OneOf):
    power_armour_set = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent, tda=0):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0, gear=self.power_armour_set)
        self.tda = tda and self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda

    def take_free_tda(self):
        self.visible = False
        self.tda.points = 0
        self.cur = self.tda


class Vehicle(OptionsList):
    def __init__(self, parent, tank=True):
        super(Vehicle, self).__init__(parent, name='Options')
        self.variant('Combi-bolter', 5)
        self.variant('Dirge caster', 5)
        if tank:
            self.variant('Dozer blade', 5)
        self.variant('Warpflame gargoyles', 5)
        self.comby = [
            self.variant('Combi-melta', 10),
            self.variant('Combi-flamer', 10),
            self.variant('Combi-plasma', 10),
        ]
        self.variant('Extra armour', 10)
        self.variant('Havok launcher', 12)
        if tank:
            self.variant('Destroyer blades', 15)
            self.variant('Daemonic possession', 15)

    def check_rules(self):
        super(Vehicle, self).check_rules()
        self.process_limit(self.comby, 1)


class Veterans(OptionsList):
    def __init__(self, parent, points=5, per_model=False, crimson=False):
        super(Veterans, self).__init__(parent, name='')
        self.crimson = crimson
        if hasattr(self.roster, 'death_to_false_emperor') and self.roster.death_to_false_emperor is True:
            points = 0
        self.vet = self.variant('Veterans of the Long War', points, per_model=per_model)

    @property
    def description(self):
        if self.roster.is_black or\
           self.roster.is_thousand or\
           self.roster.is_alpha or\
           self.roster.is_iron or\
           self.roster.is_night or\
           self.roster.is_word or\
           self.roster.is_eater or\
           self.roster.is_guard or\
           self.roster.is_child:
            return self.vet.gear
        return super(Veterans, self).description

    @property
    def points(self):
        if self.roster.is_thousand or\
           self.roster.is_black or\
           self.roster.is_alpha or\
           self.roster.is_iron or\
           self.roster.is_night or\
           self.roster.is_word or\
           self.roster.is_eater or\
           self.roster.is_guard or\
           self.roster.is_child:
            points = 0
        else:
            points = super(Veterans, self).points
        return points * self.parent.get_count()

    def check_rules(self):
        super(Veterans, self).check_rules()
        self.visible = self.roster.is_base or (self.roster.is_crimson and self.crimson)
        self.used = not self.roster.is_crimson or not self.crimson


class Icon(OptionsList):
    def __init__(self, parent, vengeance=0, wrath=0, flame=0, despair=0, excess=0):
        super(Icon, self).__init__(parent, name='Chaos Icon', limit=1, order=1000)
        self.vengeance = self.variant('Icon of vengeance', vengeance)
        self.wrath = self.variant('Icon of wrath', wrath)
        self.flame = self.variant('Icon of flame', flame)
        self.despair = self.variant('Icon of despair', despair)
        self.excess = self.variant('Icon of excess', excess)

    def check_marks(self, marks):
        self.wrath.visible = self.wrath.used = marks and marks.khorne
        self.flame.visible = self.flame.used = marks and marks.tzeentch
        self.despair.visible = self.despair.used = marks and marks.nurgle
        self.excess.visible = self.excess.used = marks and marks.slaanesh


class IconBearer(Unit):

    def __init__(self, parent, *args, **kwargs):
        self.vengeance = kwargs.pop('vengeance', 0)
        self.wrath = kwargs.pop('wrath', 0)
        self.flame = kwargs.pop('flame', 0)
        self.despair = kwargs.pop('despair', 0)
        self.excess = kwargs.pop('excess', 0)
        super(IconBearer, self).__init__(parent, *args, **kwargs)
        self.icon = Icon(self, vengeance=self.vengeance, wrath=self.wrath, flame=self.flame, despair=self.despair,
                         excess=self.excess)

    def mark_icon(self, marks):
        self.icon.check_marks(marks)

    def count_icon(self):
        return 1 if self.icon.any else 0


class IconBearerGroup(IconBearer, ListSubUnit):
    def count_icon(self):
        return super(IconBearerGroup, self).count_icon() * self.get_count()


class IconicUnit(Unit):

    def __init__(self, *args, **kwargs):
        super(IconicUnit, self).__init__(*args, **kwargs)
        self.champion = None
        self.models = None
        self.marks = None

    def check_rules(self):
        super(IconicUnit, self).check_rules()
        count = 0
        for u in [self.champion.unit] + self.models.units:
            u.mark_icon(self.marks)
            count += u.count_icon()
        if count > 1:
            self.error("{0} can't take more then 1 icon (taken: {1}).".format(self.name, count))

    def get_count(self):
        return self.models.count + 1

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle
