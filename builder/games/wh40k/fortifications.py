__author__ = 'Ivan Truskov'
__summary__ = 'Stronghold assault (2013)'


from builder.core2 import OptionsList, Unit, Gear, UnitType, Count,\
    ListSubUnit, UnitList, OneOf, OptionalSubUnit, SubUnit, UnitDescription
from .section import Section


class FortUnit(Unit):
    def build_statistics(self):
        return {}


class BuildingUpgrades(OptionsList):
    def __init__(self, parent, limit=1, location=None):
        """@param limit: 1 for a small building,
        2 for a medium, 3 for large"""
        name = 'Upgrades ({})'.format(location) if location is not None\
               else 'Upgrades'
        super(BuildingUpgrades, self).__init__(parent, name, limit=limit)
        self.variant('Ammo Store', 15)
        self.variant('Booby Traps', 20)
        self.variant('Escape Hatch', 25)
        self.variant('Magos Machine Spirit', 30)
        self.variant('Searchlights', 5)
        self.variant('Void shield', 25)


class Obstacles(object):
    total_limit = 6

    class Obstacle(Count):
        def __init__(self, *args, **kwargs):
            self.group = kwargs.pop('group', [])
            super(Obstacles.Obstacle, self).__init__(*args, **kwargs)
            self.group.append(self)

        def check_rules_chain(self):
            # import pdb; pdb.set_trace()
            super(Obstacles.Obstacle, self).check_rules_chain()
            Count.norm_counts(0, Obstacles.total_limit, self.group)

    def __init__(self, parent):
        tangle = Count(parent, 'Tanglewire', 0, 6, 5)
        barricade = Count(parent, 'Barricades', 0, 6, 10)
        self.Obstacle(parent, 'Tank Traps', 0, 6, 15, group=[tangle, barricade])


class BattlementBattlefield(OptionsList):
    def __init__(self, parent):
        super(BattlementBattlefield, self).__init__(
            parent, 'Battlements and Battlefield', limit=1)
        self.ammo_dump = self.variant('Ammunition Dump', 20)
        self.commsrelay = self.variant('Comms relay', 20)
        self.icaruslascannon = self.variant('Gun Emplacement with Icarus lascannon', 35,
                                            gear=Gear('Icarus lascannon'))
        self.quadgun = self.variant('Gun emplacement with quad-gun', 50, gear=Gear('Quad-gun'))


class Aegis(FortUnit):
    type_name = 'Aegis Defense Line'
    type_id = 'aegis_defense_line_v2'

    def __init__(self, parent):
        super(Aegis, self).__init__(parent=parent, points=50)
        BattlementBattlefield(self)
        Obstacles(self)


class Bastion(FortUnit):
    type_name = 'Imperial Bastion'
    type_id = 'imperial_bastion_v2'

    def __init__(self, parent):
        super(Bastion, self).__init__(parent=parent, points=75, gear=Gear('Heavy bolter', count=4))
        BattlementBattlefield(self)
        Obstacles(self)
        BuildingUpgrades(self, 2)


class Honored(FortUnit):
    type_name = 'Honored Imperium'
    type_id = 'honored_v2'

    def __init__(self, parent):
        super(Honored, self).__init__(parent=parent, points=40)
        Obstacles(self)


class WallOfMartyrsLine(FortUnit):
    type_name = 'Wall of Martyrs Imperial Defence Line'
    type_id = 'martyr_defense_line_v2'

    def __init__(self, parent):
        super(WallOfMartyrsLine, self).__init__(parent=parent, points=80)
        Obstacles(self)


class WallOfMartyrsEmplacement(FortUnit):
    type_name = 'Wall of Martyrs Imperial Defence Emplacement'
    type_id = 'martyr_defense_emplacement_v2'

    def __init__(self, parent):
        super(WallOfMartyrsEmplacement, self).__init__(parent=parent, points=40)
        Obstacles(self)


class WallOfMartyrsBunker(FortUnit):
    type_name = 'Wall of Martyrs Imperial Bunker'
    type_id = 'martyr_bunker_v2'

    def __init__(self, parent):
        super(WallOfMartyrsBunker, self).__init__(parent=parent, points=55)
        BattlementBattlefield(self)
        Obstacles(self)
        BuildingUpgrades(self, 2)


class WallOfMartyrsRedoubt(FortUnit):
    type_name = 'Wall of Martyrs Redoubt'
    type_id = 'martyr_redoubt_v2'

    class Weapon(OneOf):
        def __init__(self, parent, name='Weapon'):
            super(WallOfMartyrsRedoubt.Weapon, self).__init__(parent, name=name)
            self.variant('Quad Icarus Lascannon', 0)
            self.variant('Punisher gatling cannon', 0)
            self.variant('Battle cannon', 10)

    def __init__(self, parent):
        super(WallOfMartyrsRedoubt, self).__init__(parent=parent, points=200)
        self.Weapon(self)
        self.Weapon(self, '')
        BattlementBattlefield(self)
        Obstacles(self)
        BuildingUpgrades(self, 2)


class WallOfMartyrsBattery(FortUnit):
    type_name = 'Wall of Martyrs Vengeance Weapon Battery'
    type_id = 'martyr_barrety_v2'

    class Battery(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(WallOfMartyrsBattery.Battery.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Punisher gatling cannon', 0)
                self.variant('Quad Icarus Lascannon', 0)
                self.variant('Battle cannon', 10)

        def __init__(self, parent):
            super(WallOfMartyrsBattery.Battery, self).__init__(parent, 'Vengeance Weapon Battery', 75)
            self.Weapon(self)
            Obstacles(self)

    def __init__(self, parent):
        super(WallOfMartyrsBattery, self).__init__(parent=parent, points=75)
        UnitList(self, self.Battery, 1, 2)

    def build_points(self):
        return super(WallOfMartyrsBattery, self).build_points() - 75


class PromethiumRelayPipes(FortUnit):
    type_name = 'Promethium Relay Pipes'
    type_id = 'relay_pipes_v2'

    def __init__(self, parent):
        super(PromethiumRelayPipes, self).__init__(parent=parent, points=40)
        Obstacles(self)


class VoidShieldGenerator(FortUnit):
    type_name = 'Void Shield Generator'
    type_id = 'void_shield_v2'

    def __init__(self, parent):
        super(VoidShieldGenerator, self).__init__(parent=parent, points=50,
                                                  gear=[Gear('Projected Void Shield')])
        Count(self, 'Additional Void Shield', 0, 2, 25,
              gear=Gear('Projected Void Shield'))
        Obstacles(self)


class Skyshield(FortUnit):
    type_name = 'Skyshield Landing Pad'
    type_id = 'skyshield_landing_pad_v2'

    class Ready(OptionsList):
        def __init__(self, parent):
            super(Skyshield.Ready, self).__init__(parent, 'Upgrade')
            self.variant('"Ready for Takeoff"', 5)

    def __init__(self, parent):
        super(Skyshield, self).__init__(parent=parent, points=75)
        self.Ready(self)
        Obstacles(self)


class Fortress(FortUnit):
    type_name = 'Fortress of Redemtion'
    type_id = 'fortress_of_redemtion_v2'
    base_points = 220

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Fortress.Weapon, self).__init__(parent=parent, name='Options')
            self.krak = self.variant('Krakstorm missiles', 30)
            self.variant('Remote fire', 10)

    def __init__(self, parent):
        super(Fortress, self).__init__(
            parent=parent, points=220,
            gear=[Gear('Twin-linked Icarus lascannon'), Gear('Fragstorm missiles')])
        self.opt = self.Weapon(self)
        self.emplacements = Count(self, name='Emplaced heavy bolters', min_limit=0, max_limit=4,
                                  points=10, gear=Gear('Heavy bolter'))
        Obstacles(self)
        BuildingUpgrades(self, 2, 'Main tower')
        BuildingUpgrades(self, 1, 'Bunker annex')
        BuildingUpgrades(self, 1, 'Bunker annex')
        BuildingUpgrades(self, 1, 'Connecting walkway')


class CannonAquilaStrongpoint(FortUnit):
    type_name = 'Macro-cannon Aquila Strongpoint'
    type_id = 'aquila_cannon_v2'

    def __init__(self, parent):
        super(CannonAquilaStrongpoint, self).__init__(parent=parent, points=535,
                                                gear=[Gear('Aquila macro-cannon')])
        self.emplacements = Count(self, name='Emplaced heavy bolters', min_limit=0, max_limit=4,
                                  points=10, gear=Gear('Heavy bolter'))
        Obstacles(self)
        BuildingUpgrades(self, 3, 'Main strongpoint')
        BuildingUpgrades(self, 2, 'Bunker annex')


class MissileAquilaStrongpoint(FortUnit):
    type_name = 'Vortex Missile Aquila Strongpoint'
    type_id = 'aquila_missile_v2'

    def __init__(self, parent):
        super(MissileAquilaStrongpoint, self).__init__(parent=parent, points=535,
                                                gear=[Gear('Vortex missile battery')])
        self.emplacements = Count(self, name='Emplaced heavy bolters', min_limit=0, max_limit=4,
                                  points=10, gear=Gear('Heavy bolter'))
        Obstacles(self)
        BuildingUpgrades(self, 3, 'Main strongpoint')
        BuildingUpgrades(self, 2, 'Bunker annex')


class ImperialStrongpoint(FortUnit):
    type_name = 'Imperial Strongpoint'
    type_id = 'imperial_strongpoint_v2'

    class Bastion(Bastion, ListSubUnit):
        pass

    class Aegis(Aegis, ListSubUnit):
        pass

    class Options(OptionalSubUnit):
        def __init__(self, parent):
            super(ImperialStrongpoint.Options, self).__init__(parent, '')
            SubUnit(self, Honored(parent=None))
            SubUnit(self, Skyshield(parent=None))

    def __init__(self, parent):
        super(ImperialStrongpoint, self).__init__(parent=parent)
        UnitList(self, self.Bastion, 1, 3)
        UnitList(self, self.Aegis, 1, 5)
        self.Options(self)


class WallOfMartyrsNetwork(FortUnit):
    type_name = 'Wall of Martyrs Imperial Defence Network'
    type_id = 'martyr_network_v2'

    class Bunker(WallOfMartyrsBunker, ListSubUnit):
        pass

    class Line(WallOfMartyrsLine, ListSubUnit):
        pass

    class Emplacement(WallOfMartyrsEmplacement, ListSubUnit):
        pass

    class Redoubt(WallOfMartyrsRedoubt, ListSubUnit):
        pass

    class Battery(WallOfMartyrsBattery, ListSubUnit):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(WallOfMartyrsNetwork.Options, self).__init__(parent, '')
            self.redoubt = self.variant('Redoubt')
            self.redoubt.used = False
            self.redoubt_units = UnitList(parent, WallOfMartyrsNetwork.Redoubt, 1, 2)
            self.battery = self.variant('Battery')
            self.battery.used = False
            self.battery_units = UnitList(parent, WallOfMartyrsNetwork.Battery, 1, 2)

        def check_rules(self):
            super(WallOfMartyrsNetwork.Options, self).check_rules()
            self.redoubt_units.used = self.redoubt_units.visible = self.redoubt.value
            self.battery_units.used = self.battery_units.visible = self.battery.value

    def __init__(self, parent):
        super(WallOfMartyrsNetwork, self).__init__(parent=parent)
        UnitList(self, self.Bunker, 1, 3)
        UnitList(self, self.Line, 1, 4)
        UnitList(self, self.Emplacement, 1, 3)
        self.Options(self)


class VoidRelayNetwork(FortUnit):
    type_name = 'Void Relay Network'
    type_id = 'void_relay_v2'

    class Shield(VoidShieldGenerator, ListSubUnit):
        pass

    class Pipe(PromethiumRelayPipes, ListSubUnit):
        pass

    class Options(OptionalSubUnit):
        def __init__(self, parent):
            super(VoidRelayNetwork.Options, self).__init__(parent, '')
            SubUnit(self, Honored(parent=None))

    def __init__(self, parent):
        super(VoidRelayNetwork, self).__init__(parent=parent)
        UnitList(self, self.Pipe, 1, 3)
        UnitList(self, self.Shield, 1, 3)
        self.Options(self)


class PlasmaObliterator(FortUnit):
    type_name = 'Plasma Obliterator'
    type_id = 'plasma_obliterator'

    def __init__(self, parent):
        super(PlasmaObliterator, self).__init__(parent, points=230, gear=[Gear('Plasma obliterator')])
        BuildingUpgrades(self, limit=2)
        Obstacles(self)


class Container(FortUnit):
    type_name = "Munitorum Armoured Container Cache"
    type_id = 'containers'

    def __init__(self, parent):
        super(Container, self).__init__(parent)
        self.models = Count(self, 'Munitorum Armoured Container Cache', 1, 3, 40, True,
                            gear=UnitDescription('Munitorum Armoured Container Cache', 40, options=[
                                Gear('Munitorum armoured container'),
                                Gear('Munitorum Supply Crates', count=4),
                                Gear('Munitorum Propethium barrels', count=3)
                            ]))

    def get_count(self):
        return self.models.cur


class HaemotropeReactors(FortUnit):
    type_name = 'Haemotrope reactors'
    type_id = 'haemotrope'

    def __init__(self, parent):
        super(HaemotropeReactors, self).__init__(parent)
        self.models = Count(self, 'Haemotrope reactors pair', 1, 3, 40, True)

    def get_count(self):
        return self.models.cur * 2

    def build_description(self):
        res = UnitDescription('Haemotrope reactors', self.points, self.get_count())
        res.add(UnitDescription('Reactor', count=self.models.cur * 2))
        return res


class Primus(FortUnit):
    type_name = 'Primus Redoubt'
    type_id = 'primus_fort_v1'
    imperial_armour = True

    class Mount(OptionsList):
        def __init__(self, parent):
            super(Primus.Mount, self).__init__(parent, 'Mounted weapon', limit=1)
            self.variant('Twin-linked heavy bolter', 10)
            self.variant('Twin-linked heavy flamer', 10)
            self.variant('Twin-linked lascannon', 20)
            self.variant('Multi-melta & searchlight', 10, gear=[
                Gear('Multi-melta'), Gear('Searchlight')
            ])
            self.variant('Hyperios missile launcher', 30)
            self.variant('Whirlwind launcher', 35)
            self.variant('Icarus lascannon', 35)
            self.variant('Quad-gun', 50)
            self.variant('Battle cannon turret', 50)
            self.variant('Icarus quad lascannon', 75)

    def __init__(self, parent):
        super(Primus, self).__init__(parent, gear=[
            Gear('Turbo laser destroyer'), Gear('Force dome')
        ])
        self.Mount(self)
        Count(self, 'Heavy bolter', 0, 4, 10)
        BuildingUpgrades(self, 3)
        Obstacles(self)


class Fort(Section):
    ia_units = []

    def __init__(self, parent):
        super(Fort, self).__init__(parent, 'fort', 'Fortification', min_limit=0, max_limit=1)
        self.aegis = UnitType(self, Aegis)
        self.skyshield = UnitType(self, Skyshield)
        self.bastion = UnitType(self, Bastion)
        UnitType(self, Honored)
        UnitType(self, ImperialStrongpoint)
        UnitType(self, WallOfMartyrsLine)
        UnitType(self, WallOfMartyrsEmplacement)
        UnitType(self, WallOfMartyrsBunker)
        UnitType(self, WallOfMartyrsRedoubt)
        UnitType(self, WallOfMartyrsBattery)
        UnitType(self, WallOfMartyrsNetwork)
        UnitType(self, PromethiumRelayPipes)
        UnitType(self, VoidShieldGenerator)
        UnitType(self, VoidRelayNetwork)
        self.fortress = UnitType(self, Fortress)
        UnitType(self, CannonAquilaStrongpoint)
        UnitType(self, MissileAquilaStrongpoint)
        UnitType(self, PlasmaObliterator)
        UnitType(self, Container)
        UnitType(self, HaemotropeReactors)
