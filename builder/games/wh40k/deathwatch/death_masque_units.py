__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription
from builder.games.wh40k.roster import Unit


class Artemis(Unit):
    type_name = 'Watch Captain Artemis'
    type_id = 'dw_v1_artemis'

    def __init__(self, parent):
        super(Artemis, self).__init__(parent, points=145, gear=[
            Gear('Power sword'), Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Iron halo'), Gear('Special issue ammunition'),
            Gear('Hellfire Extremis'), Gear('Stasis Bomb')
        ], static=True, unique=True)


class SquadCrull(Unit):
    type_name = 'Squad Crull'
    type_id = 'dw_v1_crull'

    def __init__(self, parent):
        grenades = [Gear('Frag grenades'), Gear('Krak grenades'), Gear('Special issue ammunition')]
        super(SquadCrull, self).__init__(parent, points=205, gear=[
            UnitDescription('Watch Sergeant Crull', options=grenades + [
                Gear('Combi-melta'), Gear('Xenophase blade')]),
            UnitDescription('Veteran', count=2, options=grenades + [
                Gear('Boltgun'), Gear('Power sword')]),
            UnitDescription('Veteran', options=grenades + [
                Gear('Stalker pattern boltgun'), Gear('Close combat weapon')]),
            UnitDescription('Veteran', options=grenades + [
                Gear('Infernus heavy bolter'), Gear('Hellfire shells'), Gear('Close combat weapon')])
        ], unique=True, static=True)

    def get_count(self):
        return 5

    def build_description(self):
        result = super(SquadCrull, self).build_description()
        result.count = 1
        return result

    def furor_weapons(self):
        return 1

    def hammers(self):
        return 0

    def stalkers(self):
        return 1


class SquadGalatael(Unit):
    type_name = 'Squad Galatael'
    type_id = 'dw_v1_galatael'

    def __init__(self, parent):
        grenades = [Gear('Frag grenades'), Gear('Krak grenades')]
        super(SquadGalatael, self).__init__(parent, points=205, gear=[
            UnitDescription('Brother Galatael', options=grenades + [
                Gear('Plasma pistol'), Gear('Power sword')]),
            UnitDescription('Veteran', count=2, options=grenades + [
                Gear('heavy thunder hammer')]),
            UnitDescription('Veteran', options=grenades + [
                Gear('Power sword'), Gear('Storm shield')]),
            UnitDescription('Veteran', options=grenades + [
                Gear('Power maul'), Gear('Storm shield')])
        ], unique=True, static=True)

    def get_count(self):
        return 5

    def build_description(self):
        result = super(SquadGalatael, self).build_description()
        result.count = 1
        return result


class Nihilus(Unit):
    type_name = 'Venerable Dreadnought Nihilus'
    type_id = 'dw_v1_nihilus'

    def __init__(self, parent):
        super(Nihilus, self).__init__(parent, 'Nihilus', 135, gear=[
            Gear('Plasma cannon'), Gear('Power fist'), Gear('Storm bolter'),
            Gear('Searchlight'), Gear('Smoke launchers')], static=True,
                                      unique=True)
