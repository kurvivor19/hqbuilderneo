__author__ = 'Ivan Truskov'
from builder.games.wh40k.section import UnitType, Formation,\
    Detachment

from .hq import WatchCaptain, WatchMaster,\
    Chaplain, Librarian
from .fast import Corvus, Bikers, Razorback
from .elites import Terminators, Vanguard
from .troops import Veterans
from .overkill_units import SquadDonatus, Setorax,\
    Delassio, Garran, Suberei, Cassius, Jensus
from .death_masque_units import Artemis, SquadCrull,\
    SquadGalatael, Nihilus
from itertools import chain


class KillTeam(Formation):

    def __init__(self, optional, mandatory=None):
        super(KillTeam, self).__init__()
        # to ensure
        self.vets = UnitType(self, Veterans, min_limit=1)
        self.vets.min_limit = 0
        self.crull = UnitType(self, SquadCrull)
        self.add_type_restriction([self.vets, self.crull], 1, 1)
        self.mand = mandatory and UnitType(self, mandatory, min_limit=1, max_limit=1)
        lower_limit = 1 if mandatory is None else 0
        self.optional = [UnitType(self, ut) for ut in optional]
        if lower_limit > 0 and len(self.optional) > 0:
            self.add_type_restriction(self.optional, lower_limit, 5)

    def check_rules(self):
        super(KillTeam, self).check_rules()
        total_models = sum(u.count for u in self.units)
        if total_models > 10:
            self.error('The formation may not consist of more then ten models (excluding Dedicated Transport). Currently taken: {}'.format(total_models))


class Aquila(KillTeam):
    army_name = 'Aquila Kill Team'
    army_id = 'dw_v1_aquila'

    def __init__(self):
        super(Aquila, self).__init__([Librarian, Terminators,
                                      Vanguard, Bikers, SquadGalatael])


class Furor(KillTeam):
    army_name = 'Furor Kill Team'
    army_id = 'dw_v1_furor'

    def __init__(self):
        super(Furor, self).__init__([Librarian, Vanguard,
                                     Bikers], Terminators)

    def check_rules(self):
        super(Furor, self).check_rules()
        if not any(u.furor_weapons() for u in (self.vets.units + self.crull.units)):
            self.error('At least one Veteran must be armed with either a Deathwatch frag cannon or infernus heavy bolter')


class Venator(KillTeam):
    army_name = 'Venator Kill Team'
    army_id = 'dw_v1_venator'

    def __init__(self):
        super(Venator, self).__init__([Librarian, Terminators,
                                       Vanguard], Bikers)

    def check_rules(self):
        super(Venator, self).check_rules()
        biker_count = sum(u.count for u in self.mand.units)
        if biker_count < 2:
            self.error('The formation must include at least two Bikers')


class Dominatus(KillTeam):
    army_name = 'Dominatus Kill Team'
    army_id = 'dw_v1_dominatus'

    def __init__(self):
        super(Dominatus, self).__init__([Librarian, Terminators,
                                         Bikers], Vanguard)
        self.mand.min_limit = 0
        self.gal = UnitType(self, SquadGalatael)
        self.add_type_restriction([self.mand, self.gal], 1, 1)

    def check_rules(self):
        super(Dominatus, self).check_rules()
        vanguard_count = sum(u.count for u in (self.mand.units + self.gal.units))
        if vanguard_count < 2:
            self.error('The formation must include at least two Vanguard Veterans')


class Malleus(KillTeam):
    army_name = 'Malleus Kill Team'
    army_id = 'dw_v1_malleus'

    def __init__(self):
        super(Malleus, self).__init__([Librarian, Vanguard,
                                       Bikers], Terminators)
        self.terms = self.mand
        self.vanguard = self.optional[1]

    def check_rules(self):
        super(Malleus, self).check_rules()
        hammers = sum(u.hammers() for u in chain(*(ut.units for ut in [self.vets,
                                                                       self.terms,
                                                                       self.vanguard])))
        if hammers < 2:
            self.error("At least 2 models must be armed with thunder hammers or heavy thunder hammers")


class Purgatus(KillTeam):
    army_name = 'Purgatus Kill Team'
    army_id = 'dw_v1_purgatus'

    def __init__(self):
        super(Purgatus, self).__init__([Librarian, Vanguard,
                                        Bikers], Terminators)
        self.add_type_restriction(self.optional[0:1], 1, 5)

    def check_rules(self):
        super(Purgatus, self).check_rules()
        if not any(u.stalkers() for u in (self.crull.units + self.vets.units)):
            self.error('At least one Veteran must be armed with Stalker pattern boltgun')


class Strategium(Formation):
    army_name = 'Strategium Command Team'
    army_id = 'dw_v1_strategium'

    def __init__(self):
        super(Strategium, self).__init__()
        commanders = [
            UnitType(self, WatchCaptain),
            UnitType(self, Chaplain),
            UnitType(self, Librarian),
            UnitType(self, Artemis)
        ]
        self.add_type_restriction(commanders, 1, 1)
        teams = [UnitType(self, Veterans), UnitType(self, SquadCrull)] +\
                [Detachment.build_detach(self, unittype) for unittype in
                 [Aquila, Furor, Venator, Dominatus, Malleus, Purgatus]]
        self.add_type_restriction(teams, 1, 1)


class Company(Formation):
    army_name = 'Watch Company'
    army_id = 'dw_v1_company'

    def __init__(self):
        super(Company, self).__init__()
        captains = [UnitType(self, WatchCaptain),
                    UnitType(self, Artemis)]
        self.add_type_restriction(captains, 1, 1)
        teams = [UnitType(self, Veterans), UnitType(self, SquadCrull)] +\
                [Detachment.build_detach(self, unittype) for unittype in
                 [Aquila, Furor, Venator, Dominatus, Malleus, Purgatus]]
        self.add_type_restriction(teams, 4, 4)


class Dropships(Formation):
    army_name = 'Corvus Dropship Wing'
    army_id = 'dw_v1_dropships'

    def __init__(self):
        super(Dropships, self).__init__()
        UnitType(self, Corvus, min_limit=3, max_limit=3)


class TeamCassius(Formation):
    army_name = 'Kill Team Cassius'
    army_id = 'dw_v1_team_cassius'

    def __init__(self):
        super(TeamCassius, self).__init__()
        for _type in [Cassius, Jensus, SquadDonatus, Setorax,
                      Delassio, Garran, Suberei]:
            UnitType(self, _type, min_limit=1, max_limit=1)


class WatchbladeTaskForce(Formation):
    army_name = 'Watchblade Taskforce'
    army_id = 'dw_v1_watchblade'

    def __init__(self):
        super(WatchbladeTaskForce, self).__init__()
        UnitType(self, WatchMaster, min_limit=1, max_limit=1)
        UnitType(self, Veterans, min_limit=3, max_limit=3)
        UnitType(self, Terminators, min_limit=1, max_limit=1)
        UnitType(self, Bikers, min_limit=1, max_limit=1)
        UnitType(self, Razorback, min_limit=1, max_limit=1)


class WatchForceArtemis(Formation):
    army_name = 'Watch Force Artemis'
    army_id = 'dw_v1_force_artemis'

    def __init__(self):
        super(WatchForceArtemis, self).__init__()
        UnitType(self, Artemis, min_limit=1, max_limit=1)
        UnitType(self, SquadCrull, min_limit=1, max_limit=1)
        UnitType(self, SquadDonatus, min_limit=1, max_limit=1)
        UnitType(self, Nihilus, min_limit=1, max_limit=1)
