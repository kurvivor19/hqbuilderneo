from builder.games.wh40k.adepta_sororitas_v4.armory import VehicleOptions
from builder.core2 import Gear, Count, OptionsList, UnitDescription, SubUnit, OptionalSubUnit, OneOf
from .armory import BoltPistol, Boltgun, Melee, Ranged
from builder.games.wh40k.roster import Unit


class Seraphims(Unit):
    type_name = 'Seraphim Squad'
    type_id = 'seraphim_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Seraphim-Squad')

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades'), Gear('Jump pack')]
    model_points = 15

    class Superior(Unit):
        name = 'Seraphim Superior'

        class Weapon1(BoltPistol):
            def __init__(self, parent):
                super(Seraphims.Superior.Weapon1, self).__init__(parent)
                self.variant('Chainsword', 0)
                self.variant('Power sword', 15)

        class Weapon2(BoltPistol):
            def __init__(self, parent):
                super(Seraphims.Superior.Weapon2, self).__init__(parent, '')
                self.variant('Plasma pistol', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Seraphims.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(Seraphims.Superior, self).__init__(parent, self.name, Seraphims.model_points + 10,
                                                     Seraphims.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Seraphims.Leader, self).__init__(parent, 'Leader')
            SubUnit(self, Seraphims.Superior(parent=None))

    def __init__(self, parent):
        super(Seraphims, self).__init__(parent)
        self.sup = self.Leader(self)
        self.squad = Count(self, 'Seraphim', 5, 10, self.model_points, True)
        self.fl = Count(self, 'Hand flamers', 0, 2, 10)
        self.inf = Count(self, 'Inferno pistols', 0, 2, 30)

    def check_rules(self):
        Count.norm_counts(0, 2, [self.fl, self.inf])
        self.squad.min = 5 - self.sup.count
        self.squad.max = 10 - self.sup.count

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.sup.description)
        desc = UnitDescription('Seraphim', points=self.model_points, options=self.common_gear)
        if self.fl.cur:
            res.add(desc.clone().add(Gear('Hand flamer', count=2)).add_points(
                self.fl.option_points).set_count(self.fl.cur)
                    )
        if self.inf.cur:
            res.add(desc.clone().add(Gear('Inferno Pistol', count=2)).add_points(
                self.inf.option_points).set_count(self.inf.cur)
                    )
        if self.squad.cur - self.fl.cur - self.inf.cur:
            res.add(desc.clone().add(Gear('Bolt pistol', count=2)).set_count(
                self.squad.cur - self.fl.cur - self.inf.cur)
            )
        return res

    def get_count(self):
        return self.squad.cur + self.sup.count


class Dominions(Unit):
    type_name = 'Dominion Squad'
    type_id = 'dominions_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Dominion-Squad')

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 13

    class Superior(Unit):
        name = 'Dominion Superior'

        class Weapon1(Ranged, Melee, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Dominions.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.vet = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(Dominions.Superior, self).__init__(parent, self.name, Dominions.model_points,
                                                     Dominions.common_gear)

            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(Dominions.Superior, self).check_rules()
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

        def build_description(self):
            res = super(Dominions.Superior, self).build_description()
            if self.opt.vet.value:
                res.name = 'Veteran Dominion Superior'
            return res

    class Simulacrum(OptionsList):
        def __init__(self, parent):
            super(Dominions.Simulacrum, self).__init__(parent, 'Icon of Faith')
            self.variant('Simulacrum imperialis', 10)

    def __init__(self, parent):
        super(Dominions, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=None))
        self.squad = Count(self, 'Dominion', 4, 9, self.model_points, True)
        self.opt = self.Simulacrum(self)
        self.sb = Count(self, 'Storm Bolter', 0, 4, 5)
        self.fl = Count(self, 'Flamer', 0, 4, 5)
        self.mg = Count(self, 'Meltagun', 0, 4, 10)
        self.transport = Transport(self)

    def check_rules(self):
        super(Dominions, self).check_rules()
        free = min(4, self.squad.cur - self.opt.any)
        Count.norm_counts(0, free, [self.fl, self.sb, self.mg])

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.sup.description)
        desc = UnitDescription('Dominion', points=self.model_points, options=self.common_gear + [Gear('Bolt Pistol')])
        count = self.squad.cur
        if self.opt.any:
            res.add(desc.clone().add(Gear('Boltgun')).add(self.opt.description).add_points(self.opt.points))
            count -= 1
        for cnt in [self.fl, self.sb, self.mg]:
            if cnt.cur:
                res.add(desc.clone().add(cnt.gear).add_points(cnt.option_points).set_count(cnt.cur))
                count -= cnt.cur
        if count:
            res.add(desc.clone().add(Gear('Boltgun')).set_count(count))
        res.add(self.transport.description)
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Dominions, self).build_statistics())


class Rhino(Unit):
    type_name = "Sororitas Rhino"

    type_id = 'rhino_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Sororitas-Rhino')

    gear = []

    def __init__(self, parent):
        super(Rhino, self).__init__(parent, points=40, gear=[
            Gear('Storm bolter'), Gear('Smoke launchers'), Gear('Searchlight')
        ])
        self.opt = VehicleOptions(self)


class Immolator(Unit):
    type_name = "Immolator"
    type_id = 'immolator_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Immolator')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Immolator.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked heavy flamer', 0)
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked multi-melta', 0)

    def __init__(self, parent):
        super(Immolator, self).__init__(parent, points=60, gear=[
            Gear('Smoke launchers'), Gear('Searchlight')
        ])
        self.wep = self.Weapon(self)
        self.opt = VehicleOptions(self)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent, 'Transport', limit=1)
        SubUnit(self, Rhino(parent=None))
        SubUnit(self, Immolator(parent=None))
