from builder.core2 import Gear, OptionsList
from builder.games.wh40k.unit import Unit


class Rhino(Unit):
    type_name = 'Null-Maiden Rhino'
    type_id = 'null_rhino_v2'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Rhino.Options, self).__init__(parent, 'Options')
            self.variant('Dozer blade', 5)
            self.variant('Storm bolter', 5)
            self.variant('Extra armour', 10)
            self.variant('Hunter-killer missile', 10)

    def __init__(self, parent):
        super(Rhino, self).__init__(parent, points=45, gear=[
            Gear('Storm bolter'), Gear('Searchlight'), Gear('Smoke lunchers')
        ])
        self.Options(self)
