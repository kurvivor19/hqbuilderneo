from builder.core2 import OptionalSubUnit, OptionsList, OneOf,\
    Gear, Count, SubUnit, UnitDescription, ListSubUnit, UnitList
from builder.games.wh40k.roster import Unit
from .armory import *
from .fast import Trukk

__author__ = 'Denis Romanow'


class Boyz(Unit):
    type_id = 'boyz_v1'
    type_name = 'Boyz'

    class Nob(Unit):
        class NobMelee(Melee, Choppa):
            pass

        class NobRanged(Ranged, Slugga):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Boyz.Nob.Options, self).__init__(parent, 'Options')
                self.ha = self.variant('Bosspole', 5)

        def __init__(self, parent):
            super(Boyz.Nob, self).__init__(parent, name='Boss Nob', points=6 + 10)
            self.NobMelee(self, 'Melee weapon')
            self.rng = self.NobRanged(self, 'Ranged weapon')
            self.Options(self)

        def check_rules(self):
            super(Boyz.Nob, self).check_rules()
            self.rng.slg.visible = self.parent.parent.parent.ranged.cur ==\
                                   self.parent.parent.parent.ranged.sl

        def build_description(self):
            res = super(Boyz.Nob, self).build_description()
            root = self.parent.parent.parent
            res.add_points(root.opt.gear_points).add_points(root.ranged.gear_points)
            res.add(root.opt.description)
            return res

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Boyz.Boss, self).__init__(parent=parent, name='Boss', limit=1)
            SubUnit(self, Boyz.Nob(parent=None))

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Boyz.Ranged, self).__init__(parent, 'Ranged weapon')
            self.sl = self.variant('Slugga', 0)
            self.sh = self.variant('Shoota', 1, per_model=True)

        @property
        def points(self):
            return self.gear_points * (self.parent.get_count() - self.parent.bs.cur - self.parent.rl.cur)

        @property
        def gear_points(self):
            return super(Boyz.Ranged, self).points

    class Options(OptionsList):
        def __init__(self, parent):
            super(Boyz.Options, self).__init__(parent, 'Options')
            self.ha = self.variant('\'eavy armour', 4, per_model=True)

        @property
        def points(self):
            return self.gear_points * self.parent.get_count()

        @property
        def gear_points(self):
            return super(Boyz.Options, self).points

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Boyz.Transport, self).__init__(parent, 'Transport')
            self.trukk = SubUnit(self, Trukk(parent=None))

    def __init__(self, parent):
        super(Boyz, self).__init__(parent)
        self.models = Count(self, 'Ork Boy', 10, 30, 6)
        self.ranged = self.Ranged(self)
        self.opt = self.Options(self)
        self.bs = Count(self, 'Big shoota', 0, 1, 5)
        self.rl = Count(self, 'Rokkit launcha', 0, 1, 5)
        self.boss = self.Boss(self)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.models.cur + self.boss.count

    def check_rules(self):
        self.models.min = 10 - self.boss.count
        self.models.max = 30 - self.boss.count
        Count.norm_counts(0, int(self.get_count() / 10), [self.bs, self.rl])

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        model = UnitDescription(name='Ork Boy', points=6, options=[Gear('Choppa'), Gear('Stikkbombs')])
        model.add(self.opt.description).add_points(self.opt.gear_points)
        common = model.clone().add(self.ranged.description).add_points(self.ranged.gear_points)
        desc.add(common.set_count(self.models.cur - self.bs.cur - self.rl.cur))
        desc.add(model.clone().add(Gear('Big shoota')).add_points(5).set_count(self.bs.cur))
        desc.add(model.clone().add(Gear('Rokkit launcha')).add_points(5).set_count(self.rl.cur))
        desc.add(self.boss.description)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Boyz, self).build_statistics())


class Gretchin(Unit):
    """Cheap disposable infantry"""
    type_id = 'gretchin_v1'
    type_name = 'Gretchin'

    class Runtherd(ListSubUnit):
        """Unit leader"""

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(Gretchin.Runtherd.Weapon, self).__init__(parent, name=name)
                self.variant('Grabba stikk', 0)
                self.variant('Grot-prod', 5)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Gretchin.Runtherd.Options, self).__init__(parent, 'Options')
                self.hound = self.variant('Squig hound', 5)

        def __init__(self, parent):
            super(Gretchin.Runtherd, self).__init__(parent=parent, name='Runtherd', points=10, gear=[Gear('Slugga'), Gear('Stikkbombs')])
            self.weapon = self.Weapon(self, 'Melee weapon')
            self.opts = self.Options(self)

    class Runtherds(UnitList):
        def __init__(self, parent):
            super(Gretchin.Runtherds, self).__init__(parent, Gretchin.Runtherd, 1, 3, 1)

        @property
        def points(self):
            return super(Gretchin.Runtherds, self).points - 5

    def __init__(self, parent):
        super(Gretchin, self).__init__(name='Gretchin', parent=parent)
        self.grots = Count(name='Gretchin', parent=self, min_limit=10, max_limit=30, points=3, per_model=True, gear=UnitDescription('Gretchin', 3, options=[Gear('Grot blasta')]))
        self.runtherds = self.Runtherds(self)

    def get_count(self):
        return self.grots.cur + self.runtherds.count

    def check_rules(self):
        super(Gretchin, self).check_rules()
        if self.runtherds.count != self.grots.cur // 10:
            self.error('For every 10 Gretchin there must be a runtherd')


class RustgobRunts(Unit):
    type_name = "Rustgob's Runts"
    type_id = 'rustgob_v1'

    def __init__(self, parent):
        super(RustgobRunts, self).__init__(parent, points=45, static=True, gear=[
            UnitDescription('Gretchin', options=[Gear('Grot blasta')], count=10),
            UnitDescription('Rustgob', options=[Gear('Slugga'), Gear('Grabba stick'), Gear('Stikkbombs'), Gear('Squig hound')])
        ])

    def get_count(self):
        return 11

    def get_unique(self):
        return 'Rustgob'
