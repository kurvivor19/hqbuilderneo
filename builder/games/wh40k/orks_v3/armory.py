from builder.core2 import OneOf, Count, OptionsList


__author__ = 'Denis Romanow'


class Shoota(OneOf):
    def __init__(self, parent, name):
        super(Shoota, self).__init__(parent, name=name)
        self.variant('Shoota', 0)


class Ranged(Shoota):
    def __init__(self, parent, name):
        super(Ranged, self).__init__(parent, name=name)
        self.variant('Twin-linked shoota', 3)
        self.variant('Kombi-weapon with rokkit launcha', 5)
        self.variant('Kombi-weapon with skorcha', 10)


class MekWeapon(OneOf):
    def __init__(self, parent, name):
        super(MekWeapon, self).__init__(parent, name=name)
        self.variant('Kombi-weapon with rokkit launcha', 5)
        self.variant('Kustom mega-blasta', 5)
        self.variant('Rokkit launcha', 5)
        self.variant('Kombi-weapon with skorcha', 10)
        self.variant('Kustom mega-slugga', 10)


class TlShoota(OneOf):
    def __init__(self, parent, name):
        super(TlShoota, self).__init__(parent, name=name)
        self.variant('Twin-linked shoota', 0)


class MegaCustomRanged(OneOf):
    def __init__(self, parent, name):
        super(MegaCustomRanged, self).__init__(parent, name=name)
        self.variant('Kombi-weapon with rokkit launcha', 5)
        self.variant('Kombi-weapon with skorcha', 10)


class MegaRanged(MegaCustomRanged, Shoota, TlShoota):
    pass


class Melee(OneOf):
    def __init__(self, parent, name):
        super(Melee, self).__init__(parent, name=name)
        self.variant('Big choppa', 5)
        self.variant('Power klaw', 25)


class MegaMelee(OneOf):
    def __init__(self, parent, name):
        super(MegaMelee, self).__init__(parent, name=name)
        self.variant('Power klaw', 0)
        self.variant('Big choppa', 5)


class Gifts(OptionsList):
    def __init__(self, parent, mek=False, armour=None):

        self.flag = parent.roster.is_ghazkull()

        if self.flag:
            name = 'Gifts of Gork and Mork & Orkimedes\' kustom gubbinz'
        else:
            name = 'Gifts of Gork and Mork'
        super(Gifts, self).__init__(parent, name=name)
        ddss = self.variant('Da Dead Shiny Shoota', 5)
        dfk = self.variant('Da Finkin\' Kap', 10)
        dls = self.variant('Da Lucky Stikk', 25)
        hk = self.variant('Headwoppa\'s Killchoppa', 20)
        self.bike = self.variant('Warboss Gazbag\'s Blitzbike', 35)
        self.armour = armour

        base_gifts = [ddss, dfk, dls, hk, self.bike]
        # Orkimedes' kustom gubbinz
        cr = self.variant('Choppa of da Ragnarok', 20)
        bb = self.variant('Big Bosspole', 20)
        dsc = self.variant('Da Supa-Cybork', 50)
        dkk = self.variant('Da Killa Klaw', 40)
        kd = self.variant('Kill-dakka', 30)

        supp_gifts = [cr, bb, dsc, dkk, kd]
        if mek:
            dfu = self.variant('Da Fixer Upperz', 15)
            mff = self.variant('Mega Force Field', 75)
            base_gifts.append(dfu)
            supp_gifts.append(mff)

        # hide unused
        if not self.flag:
            for gift in supp_gifts:
                gift.visible = gift.used = False

    def check_rules(self):
        super(Gifts, self).check_rules()
        if self.armour:
            self.set_bike(not self.armour.is_mega())

    def set_bike(self, val):
        if not self.flag:
            self.bike.visible = self.bike.used = val


class KnowWots(OptionsList):
    def __init__(self, parent, armour=None):
        super(KnowWots, self).__init__(parent, name='Orky Know-wots')
        self.variant('Bosspole', 5)
        self.variant('Cybork body', 5)
        self.variant('Gitfinda', 5)
        self.bike = self.variant('Warbike', 25)
        self.armour = armour

    def check_rules(self):
        super(KnowWots, self).check_rules()
        if self.armour:
            self.set_bike(not self.armour.is_mega())

    def set_bike(self, val):
        self.bike.visible = self.bike.used = val


class RuntsSquigs(object):
    class Single(OptionsList):
        def __init__(self, parent, doc):
            super(RuntsSquigs.Single, self).__init__(parent, '')
            if doc:
                self.variant('Grot orderly', 5)
            self.variant('Attack squig', 15)

    def __init__(self, parent, mek=False, doc=False):
        super(RuntsSquigs, self).__init__()
        Count(parent, 'Ammo runt', 0, 3, points=3)
        if mek:
            Count(parent, 'Grot oiler', 0, 3, points=5)
        self.Single(parent=parent, doc=doc)


class Slugga(OneOf):
    def __init__(self, parent, name):
        super(Slugga, self).__init__(parent, name=name)
        self.slg = self.variant('Slugga', 0)


class Choppa(OneOf):
    def __init__(self, parent, name):
        super(Choppa, self).__init__(parent, name=name)
        self.variant('Choppa', 0)


class VehicleEquipment(OptionsList):
    def __init__(self, parent):
        super(VehicleEquipment, self).__init__(parent,
                                               name='Ork Vehicle Equipment')
        self.variant('Red paint job', 5)
        self.ram = self.variant('Reinforced ram', 5)
        self.variant('Stikkbomb chukka', 5)
        self.variant('Extra armour', 10)
        self.variant('Grot riggers', 10)
        self.variant('Wreckin\' ball', 10)
        self.variant('Boarding plank', 15)

    def has_deffrolla(self):
        return False

    def check_rules(self):
        self.ram.used = self.ram.visible = not self.has_deffrolla()
        super(VehicleEquipment, self).check_rules()
