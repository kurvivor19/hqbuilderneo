from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, ElitesSection,\
    FastSection, HeavySection, TroopsSection, Formation, Wh40kImperial, Wh40kKillTeam,\
    HQSection, LordsOfWarSection, Fort, CombinedArmsDetachment, AlliedDetachment
from builder.core2 import UnitType
from .troops import Troupe
from .elites import Shadowseer, DeathJester, Solitare
from .fast import Starweaver, Skyweavers
from .heavy import Voidweavers
from .death_masque_units import DeathsCompanions, CompanyOfTheThreefoldStranger, Inriam, TheBladesOfFate, SerpentsBreath
from builder.games.wh40k.eldar_v3.hq import Eldrad
from builder.games.wh40k.ynnari import Yvraine, Visarch, Yncarne

__author__ = 'Ivan Truskov'


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent, 3, 3)
        UnitType(self, Troupe)
        UnitType(self, DeathsCompanions)
        UnitType(self, CompanyOfTheThreefoldStranger)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent, 0, 7)
        UnitType(self, DeathJester)
        UnitType(self, Shadowseer)
        UnitType(self, Solitare)
        UnitType(self, Inriam)


class Fast(FastSection):
    def __init__(self, parent):
        super(Fast, self).__init__(parent, 2, 2)
        UnitType(self, Skyweavers)
        UnitType(self, Starweaver)
        UnitType(self, TheBladesOfFate)


class Heavy(HeavySection):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent, 1, 1)
        UnitType(self, Voidweavers)
        UnitType(self, SerpentsBreath)


class HarlequinDetachment(Wh40kBase):
    army_id = 'harlequin_det'
    army_name = 'Harlequin detachment'

    def __init__(self):
        super(HarlequinDetachment, self).__init__(
            troops=Troops(parent=self),
            elites=Elites(parent=self),
            fast=Fast(parent=self),
            heavy=Heavy(parent=self)
        )


class Hq(HQSection):
    def __init__(self, parent):
        super(Hq, self).__init__(parent)
        UnitType(self, Yvraine)
        UnitType(self, Visarch)


class Lord(LordsOfWarSection):
    def __init__(self, parent):
        super(Lord, self).__init__(parent)
        UnitType(self, Yncarne)


class HarlequinBase(Wh40kBase):

    def __init__(self):
        super(HarlequinBase, self).__init__(
            hq=Hq(parent=self), elites=Elites(parent=self), troops=Troops(parent=self),
            fast=Fast(parent=self), heavy=Heavy(parent=self),
            fort=Fort(parent=self),
            lords=Lord(parent=self)
        )


class HarlequinCAD(HarlequinBase, CombinedArmsDetachment):
    army_id = 'harlequins_cad'
    army_name = 'Harlequins (Combined arms detachment)'


class HarlequinAD(HarlequinBase, AlliedDetachment):
    army_id = 'harlequins_ad'
    army_name = 'Harlequins (Allied detachment)'


class HarlequinKillTeam(Wh40kKillTeam):
    army_id = 'harlequins_kt'
    army_name = 'Harlequins'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(HarlequinKillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Troupe])
            UnitType(self, DeathsCompanions)
            UnitType(self, CompanyOfTheThreefoldStranger)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(HarlequinKillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [Starweaver])
            UnitType(self, Skyweavers)
            UnitType(self, TheBladesOfFate)

    def __init__(self):
        super(HarlequinKillTeam, self).__init__(
            self.KTTroops(self), Elites(self), self.KTFast(self)
        )


class Revenge(Formation):
    army_id = 'harlequins_revenge'
    army_name = 'Cegorach\'s Revenge'

    def __init__(self, parent=None):
        super(Revenge, self).__init__(parent)
        troupse = [
            UnitType(self, Troupe),
            UnitType(self, DeathsCompanions),
            UnitType(self, CompanyOfTheThreefoldStranger),
        ]
        self.add_type_restriction(troupse, 3, 3)
        jesters = [
            UnitType(self, DeathJester),
            UnitType(self, Inriam),
        ]
        self.add_type_restriction(jesters, 3, 3)
        UnitType(self, Shadowseer, min_limit=3, max_limit=3)
        UnitType(self, Solitare, min_limit=1, max_limit=1)
        skyweavers = [
            UnitType(self, Skyweavers),
            UnitType(self, TheBladesOfFate),
        ]
        self.add_type_restriction(skyweavers, 2, 2)
        voidweavers = [
            UnitType(self, Voidweavers),
            UnitType(self, SerpentsBreath),
        ]
        self.add_type_restriction(voidweavers, 1, 1)


class Brood(Formation):
    army_id = 'harlequins_brood'
    army_name = 'The Serpent\'s Brood'

    class Troupe(Troupe):
        transport_allowed = False

    def __init__(self, parent=None):
        super(Brood, self).__init__(parent)
        troupse = [
            UnitType(self, Troupe),
            UnitType(self, DeathsCompanions),
            UnitType(self, CompanyOfTheThreefoldStranger),
        ]
        self.add_type_restriction(troupse, 3, 3)
        UnitType(self, Starweaver, min_limit=3, max_limit=3)
        voidweavers = [
            UnitType(self, Voidweavers),
            UnitType(self, SerpentsBreath),
        ]
        self.add_type_restriction(voidweavers, 1, 1)


class Cast(Formation):
    army_id = 'harlequins_cast'
    army_name = 'Cast of Players'

    def __init__(self, parent=None):
        super(Cast, self).__init__(parent)
        troupse = [
            UnitType(self, Troupe),
            UnitType(self, DeathsCompanions),
            UnitType(self, CompanyOfTheThreefoldStranger),
        ]
        self.add_type_restriction(troupse, 1, 1)
        jesters = [
            UnitType(self, DeathJester),
            UnitType(self, Inriam),
        ]
        self.add_type_restriction(jesters, 1, 1)
        UnitType(self, Shadowseer, min_limit=1, max_limit=1)


class Jest(Formation):
    army_id = 'harlequins_jest'
    army_name = 'Cegorach\'s Jest'

    def __init__(self, parent=None):
        super(Jest, self).__init__(parent)
        troupse = [
            UnitType(self, Troupe),
            UnitType(self, DeathsCompanions),
            UnitType(self, CompanyOfTheThreefoldStranger),
        ]
        self.add_type_restriction(troupse, 1, 1)
        skyweavers = [
            UnitType(self, Skyweavers),
            UnitType(self, TheBladesOfFate),
        ]
        self.add_type_restriction(skyweavers, 1, 1)
        voidweavers = [
            UnitType(self, Voidweavers),
            UnitType(self, SerpentsBreath),
        ]
        self.add_type_restriction(voidweavers, 1, 1)


class Path(Formation):
    army_id = 'harlequins_path'
    army_name = 'The Heroes\' Path'

    def __init__(self, parent=None):
        super(Path, self).__init__(parent)
        jesters = [
            UnitType(self, DeathJester),
            UnitType(self, Inriam),
        ]
        self.add_type_restriction(jesters, 1, 1)
        UnitType(self, Shadowseer, min_limit=1, max_limit=1)
        UnitType(self, Solitare, min_limit=1, max_limit=1)


class Blade(Formation):
    army_id = 'harlequins_blade'
    army_name = 'Faolchu\'s Blade'

    def __init__(self, parent=None):
        super(Blade, self).__init__(parent)
        skyweavers = [
            UnitType(self, Skyweavers),
            UnitType(self, TheBladesOfFate),
        ]
        self.add_type_restriction(skyweavers, 2, 2)
        voidweavers = [
            UnitType(self, Voidweavers),
            UnitType(self, SerpentsBreath),
        ]
        self.add_type_restriction(voidweavers, 1, 1)


class CoherianHost(Formation):
    army_name = 'The Coherian Host'
    army_id = 'coherian_host_v1'

    def __init__(self):
        super(CoherianHost, self).__init__()
        UnitType(self, Eldrad, min_limit=1, max_limit=1)
        UnitType(self, DeathsCompanions, min_limit=1, max_limit=1)
        UnitType(self, CompanyOfTheThreefoldStranger, min_limit=1, max_limit=1)
        UnitType(self, Inriam, min_limit=1, max_limit=1)
        UnitType(self, TheBladesOfFate, min_limit=1, max_limit=1)
        UnitType(self, SerpentsBreath, min_limit=1, max_limit=1)


faction = 'Harlequins'


class Harlequins(Wh40kImperial, Wh40k7ed):
    army_id = 'harlequins_7ed'
    army_name = 'Harlequins'
    # development = True
    faction = faction

    def __init__(self):
        super(Harlequins, self).__init__([
            HarlequinDetachment, HarlequinCAD, Revenge, Brood, Cast, Jest, Path, Blade, CoherianHost
        ])


detachments = [HarlequinDetachment, HarlequinCAD, HarlequinAD,
               Revenge, Brood, Cast, Jest, Path, Blade, CoherianHost]


unit_types = [
    Troupe,
    Shadowseer, DeathJester, Solitare,
    Starweaver, Skyweavers,
    Voidweavers
]
