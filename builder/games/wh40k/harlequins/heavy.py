from builder.core2 import Gear, ListSubUnit, UnitList, OneOf
from builder.games.wh40k.roster import Unit

__author__ = 'Ivan Truskov'


class Voidweavers(Unit):
    type_name = 'Voidweavers'
    type_id = 'voidweaver_v1'

    class Voidweaver(Unit):
        class Cannon(OneOf):
            def __init__(self, parent):
                super(Voidweavers.Voidweaver.Cannon, self).__init__(parent,
                                                                    'Weapon')
                self.variant('Haywire cannon', 0)
                self.variant('Prismatic cannon', 5)

        def __init__(self, parent):
            super(Voidweavers.Voidweaver, self).__init__(parent, 'Voidweaver',
                                                         75, gear=[
                                                             Gear('Shuriken cannon', count=2),
                                                             Gear('Holo-fields'),
                                                             Gear('Mirage launchers')
                                                         ])
            self.Cannon(self)

    class SubVoidweaver(Voidweaver, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Voidweavers, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.SubVoidweaver, 1, 3)

    def get_count(self):
        return self.models.count
