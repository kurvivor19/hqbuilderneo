__author__ = 'Denis Romanow'

from builder.core2 import *
from .fast import Transport
from builder.games.wh40k.roster import Unit


class Options(OptionsList):
    def __init__(self, parent):
        super(Options, self).__init__(parent, 'Options')
        self.mbomb = self.variant('Melta bombs', 5)


class CCW(OneOf):
    def __init__(self, parent, name):
        super(CCW, self).__init__(parent, name=name)
        self.variant('Close combat weapon', 0)
        self.variant('Power weapon', 15)
        self.variant('Power fist', 25)


class Armour(OneOf):
    grenades = [Gear('Frag grenades'), Gear('Krak grenades')]
    carapace = [Gear('Carapace armour')] + grenades

    def __init__(self, parent):
        super(Armour, self).__init__(parent, 'Armour')
        self.flak = self.variant('Flak armour', 0, gear=[Gear('Flak armour')] + self.grenades)
        self.car = self.variant('Carapace armour', 5, gear=self.carapace)


class Commissar(Unit):
    type_id = 'commissar_v1'
    type_name = 'Commissar'

    class Lord(OptionsList):
        def __init__(self, parent):
            super(Commissar.Lord, self).__init__(parent, name='')
            self.lord = self.variant('Lord Commissar', 40, gear=[Gear('Refractor field')])

        @property
        def is_lord(self):
            return self.lord.value

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(Commissar.Pistol, self).__init__(parent, name=name)
            self.variant('Bolt pistol', 0)
            self.variant('Boltgun', 0)
            self.variant('Plasma pistol', 15)

    def __init__(self, parent):
        super(Commissar, self).__init__(parent=parent, points=25)

        CCW(self, 'Weapon')
        self.Pistol(self, '')
        self.armour = Armour(self)
        self.opt = Options(self)
        self.lord = self.Lord(self)

    def check_rules(self):
        self.opt.active = self.opt.visible = self.armour.car.active = self.lord.is_lord

    def build_description(self):
        desc = super(Commissar, self).build_description()
        if self.lord.is_lord:
            desc.name = 'Lord Commissar'
        return desc


class TempestorPrime(Unit):

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(TempestorPrime.Pistol, self).__init__(parent, name=name)
            self.variant('Hot-shot laspistol', 0)
            self.variant('Bolt pistol', 0)
            self.variant('Plasma pistol', 15)

    def __init__(self, parent):
        super(TempestorPrime, self).__init__(parent, name='Tempestor Prime', gear=Armour.carapace)
        CCW(self, 'Weapon')
        self.Pistol(self, '')


class CommandSquad(Unit):
    type_name = 'Militarum Tempestus Command Squad'
    type_id = 'command_squad_v1'

    scion = UnitDescription('Tempestus Scion', options=Armour.carapace)

    class Options(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.Options, self).__init__(parent, name='Options')
            spec = CommandSquad.scion.clone().add(Gear('Hot-shot lasgun'))
            self.caster = self.variant('Vox-caster', 5, gear=[])
            self.caster_weapon = OneOf(parent, 'Vox operator weapon', visible=False, used=False)
            self.caster_weapon.variant('Hot-shot lasgun', gear=[spec.clone().add(Gear('Vox-caster'))])
            self.caster_weapon.variant('Hot-shot laspistol', gear=[CommandSquad.scion.clone().add(
                [Gear('Hot-shot laspistol'), Gear('Vox-caster')])])
            self.variant('Platoon standard', 10, gear=[spec.clone().add(Gear('Platoon standard'))])
            self.variant('Medi-pack', 15, gear=[spec.clone().add(Gear('Medi-pack'))])

        def check_rules(self):
            super(CommandSquad.Options, self).check_rules()
            self.caster_weapon.used = self.caster_weapon.visible = self.caster.value

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent, points=85)
        SubUnit(self, TempestorPrime(None))
        self.opt = self.Options(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=1, points=g['points'],
                  gear=CommandSquad.scion.clone().add(Gear(g['name'])))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Hot-shot volley gun', points=10),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
            ]
        ]
        self.transport = Transport(self)

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        Count.norm_counts(0, 4 - self.opt.count, self.spec)

    def build_description(self):
        desc = super(CommandSquad, self).build_description()
        desc.add(self.scion.clone().add(Gear('Hot-shot lasgun')).set_count(4 - self.opt.count -
                                                                           sum(o.cur for o in self.spec)))
        return desc

    def get_count(self):
        return 5

    def build_statistics(self):
        return self.note_transport(super(CommandSquad, self).build_statistics())
