__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.ogre_kingdoms.heroes import *
from builder.games.whfb.ogre_kingdoms.lords import *
from builder.games.whfb.ogre_kingdoms.core import *
from builder.games.whfb.ogre_kingdoms.special import *
from builder.games.whfb.ogre_kingdoms.rare import *


class OgreKingdoms(LegacyFantasy):
    army_name = 'Ogre Kingdoms'
    army_id = '9f49df0bfeea4c09a5e5c9a3affd5033'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Greasus, Skrag, Tyrant, Slaughtermaster],
            heroes=[Golgfag, Bragg, Bruiser, Hunter, Butcher, Firebelly],
            core=[Ogres, Ironguts, Gnoblars],
            special=[Leadbelchers, Maneaters, Sabretusk, Yhetees, Mournfang, Gorger],
            rare=[Scraplauncher, Ironblaster, Giant, Stonehorn, Thundertusk]
        )

    def has_goldfag(self):
        return self.heroes.count_unit(Golgfag) > 0

    def has_skrag(self):
        return self.lords.count_unit(Skrag) > 0

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        ig_flags = sum((1 for ig in self.core.get_units([Ironguts]) if ig.has_magic_flag()))
        if ig_flags > 1:
            self.error('Only one units of Ironguts may take Magic Standard')
        golgfag = sum((1 for ig in self.special.get_units([Maneaters]) if ig.is_golgfag_up()))
        if golgfag > 1:
            self.error('Only one units of Maneaters may take Golgfag\'s Maneaters upgrade')

    def check_special_limit(self, name, group, limit, mul):
        if name == Gorger.name and self.lords.count_unit(Skrag) > 0:
            return True
        return LegacyFantasy.check_special_limit(self, name, group, limit, mul)