__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

skaven_weapon = [
    ['The Fellblade', 100, 'skaven_fb'],
    ['Warpforged Blade', 50, 'skaven_wfb'],
    ['Warlock-augment Weapon', 45, 'skaven_waw'],
    ['Blade of Corruption', 35, 'skaven_boc'],
    ['Weeping Blade', 30, 'skaven_wb'],
    ['Dwarfbane', 25, 'skaven_db'],
    ['Blade of Nurglitch', 10, 'skaven_bon'],
] + magic_weapons

skaven_armour = [
    ['Warpstone Armour', 30, 'skaven_woa'],
    ['World\'s Edge Armour', 25, 'skaven_edge'],
    ['Shield of Distruction', 15, 'skaven_sod'],
] + magic_armours

skaven_armour_suits_ids = magic_armour_suits_ids + ['skaven_woa', 'skaven_edge']
skaven_shields_ids = magic_shields_ids + ['skaven_sod']

skaven_talisman = [
    ['Foul Pendant', 30, 'skaven_foul'],
    ['Shadow Magnet Trinket', 30, 'skaven_magnet'],
    ['Rival Hide Ralisman', 15, 'skaven_hide']
] + talismans

skaven_enchanted = [
    ['Skavenbrew', 50, 'skaven_brew'],
    ['Skalm', 30, 'skaven_skalm'],
    ['Pipes of Piebald', 25, 'skaven_pipes'],
    ['Portents of Verminomus Doom', 10, 'skaven_doom'],
] + enchanted_items

skaven_arcane = [
    ['Warpstorm Scroll', 50, 'skaven_wss'],
    ['Warp-energy Condenser', 20, 'skaven_wec'],
    ['Scrying Stone', 15, 'skaven_ss'],
] + arcane_items

skaven_standard = [
    ['Sacred Banner of Horned Rat', 70, 'skaven_sacred'],
    ['Storm Banner', 50, 'skaven_storm'],
    ['Grand Banner of Clan Superiority', 30, 'skaven_super'],
    ['Shroud of Dripping Death', 30, 'skaven_shroud'],
    ['Banner of Under-Empire', 25, 'skaven_under'],
    ['Dwarf-hide Banner', 15, 'skaven_hide'],
    ['Banner of Verminous Scurrying', 10, 'skaven_scur'],
] + magic_standards

skaven_pile = [
    ['Warpmusket', 15, 'mus'],
    ['Poisoned Attacks', 15, 'att'],
    ['Tail Weapon', 8, 'tail'],
    ['Warplock Pistol', 8, 'pistol'],
    ['Rat Hound Bodyguard', 5, 'bg'],
]

heroes_weapon = filter_items(skaven_weapon, 50)
heroes_armour = filter_items(skaven_armour, 50)
heroes_talisman = filter_items(skaven_talisman, 50)
heroes_arcane = filter_items(skaven_arcane, 50)
heroes_enchanted = filter_items(skaven_enchanted, 50)

eshin_weapon = [
    ['Warpstone stars', 50, 'eshin_stars']
]

eshin_enchanted = [
    ['Infernal Bomb', 30, 'eshin_inf'],
    ['Smoke Bomb', 10, 'eshin_smoke']
]

skrire_weapon = [
    ['Doomrocket', 30, 'skrire_rocket'],
    ['Death Globe', 25, 'skrire_globe'],
]

skrire_enchanted = [
    ['Brass Orb', 50, 'skrire_orb'],
    ['Warplock Optics', 20, 'skrire_opt'],
]

pest_arcane = [
    ['Warp Scroll', 35, 'pest_scroll'],
]

pest_standard = [
    ['Plague Banner', 30, 'pest_ban'],
]

moulder_weapon = [
    ['Shock-prod', 25, 'mol_prod'],
    ['Electro-whip', 15, 'mol_whip'],
]
