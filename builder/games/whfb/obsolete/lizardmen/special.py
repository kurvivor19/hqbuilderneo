__author__ = 'Denis Romanov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from .armory import *
from builder.core.options import norm_point_limits


class ChameleonSkins(FCGUnit):
    name = 'Chameleon Skins'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Chameleon', model_price=12, min_models=5, max_model=10,
            champion_name='Stalker', champion_price=6, base_gear=['Hand weapon', 'Blowpipe']
        )


class TerradonRaiders(FCGUnit):
    name = 'Terradon Raiders'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Terradon Raider', model_price=30, min_models=3,
            champion_name='Terradon Raider Brave', champion_price=16, base_gear=['Hand weapon', 'Javelin']
        )


class TempleGuard(Unit):
    name = 'Temple Guard'
    base_gear = ['Halberd', 'Shield', 'Light armour', 'Hand weapon']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Temple Guard', 10, 100, 16)
        fcg = [
            ['Revered Guardian', 14, 'chmp'],
            ['Standard bearer', 14, 'sb'],
            ['Musician', 7, 'mus']
        ]
        self.command_group = self.opt_options_list('Command group', fcg)
        self.magic_wep = self.opt_options_list('Magic weapon', filter_items(heroes_liz_magic_weapons, 25), 1)
        self.magic_arm = self.opt_options_list('Magic armour', filter_items(heroes_saurus_magic_armour, 25), 1)
        self.magic_tal = self.opt_options_list('Talisman', filter_items(heroes_liz_talismans, 25), 1)
        self.magic_enc = self.opt_options_list('Enchanted item', filter_items(heroes_saurus_enchanted_items, 25), 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
        self.ban = self.opt_options_list('Magic Standard', filter_items(liz_magic_standards, 50), limit=1)

    def get_unique_gear(self):
        if self.ban:
            return self.ban.get_selected()

    def check_rules(self):
        self.ban.set_visible(self.command_group.get('sb'))
        for opt in self.magic:
            opt.set_visible(self.command_group.get('chmp'))
        norm_point_limits(25, self.magic)
        self.points.set(self.build_points(count=1))
        self.build_description(options=[])
        d = ModelDescriptor('Temple Guard', gear=self.base_gear, points=16)
        if self.command_group.get('chmp'):
            rg = ModelDescriptor('Revered Guardian', points=30, gear=['Halberd', 'Hand weapon'])
            for opt in self.magic:
                rg.add_gear_opt(opt)
            if not self.magic_arm.is_selected(liz_magic_shields_ids):
                rg.add_gear('Shield')
            if not self.magic_arm.is_selected(liz_magic_armour_suits_ids):
                rg.add_gear('Light armour')
            self.description.add(rg.build(1))
        if self.command_group.get('sb'):
            self.description.add(d.clone().add_gear('Standard bearer', 14).add_gear_opt(self.ban).build(1))
        if self.command_group.get('mus'):
            self.description.add(d.clone().add_gear('Musician', 7).build(1))
        command_count = len(self.command_group.get_all())
        self.description.add(d.build(self.count.get() - command_count))

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.ban] if m.get_selected()), [])


class ColdOnes(FCGUnit):
    name = 'Cold One Cavalry'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Cold One Raider', model_price=35, min_models=5,
            musician_price=10, standard_price=20, champion_name='Cold One Raider Champion', champion_price=20,
            base_gear=['Spear', 'Shield', 'Hand weapon'],
            magic_banners=filter_items(liz_magic_standards, 50),
        )


class Kroxigor(FCGUnit):
    name = 'Kroxigor'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Kroxigor', model_price=55, min_models=3,
            champion_name='Kroxigor Ancient', champion_price=20,
            base_gear=['Great weapon'],
        )


class Stegadon(Unit):
    name = 'Stegadon'
    static = True
    gear = ['Giant Bow']
    base_points = 235

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.boss = boss

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        crew = 5 if not self.boss else 4
        self.description.add(ModelDescriptor(name='Skink Crew', count=crew, gear=['Hand weapon', 'Javelin']).build())