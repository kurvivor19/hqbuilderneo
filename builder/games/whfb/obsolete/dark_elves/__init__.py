__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.obsolete.dark_elves.heroes import *
from builder.games.whfb.obsolete.dark_elves.lords import *
from builder.games.whfb.obsolete.dark_elves.core import *
from builder.games.whfb.obsolete.dark_elves.special import *
from builder.games.whfb.obsolete.dark_elves.rare import *


class DarkElves(LegacyFantasy):
    army_name = 'Dark Elves'
    army_id = '02860a3931d04f87816f4694d9452401'
    obsolete = True

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Malekith, Morathi, Crone, Dreadlord, SupremeSorceress],
            heroes=[Malus, Shadowblade, Lokhir, Sorceress, Master, DeathHag],
            core=[Warriors, Crossbowmen, Corsairs, DarkRaiders, Harpies, dict(unit=WitchElves, active=False)],
            special=[WitchElves, Shades, Executioners, ColdOneKnights, ColdOneChariot, BlackGuard],
            rare=[BoltThrower, Hydra]
        )

    def check_rare_limit(self, name, group, limit, mul):
        if name == BoltThrower.name:
            limit *= 2
        return LegacyFantasy.check_rare_limit(self, name, group, limit, mul)

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        crone = self.lords.count_unit(Crone) > 0
        self.core.set_active_types([WitchElves], crone)
        self.special.set_active_types([WitchElves], not crone)
        if not crone and self.core.count_unit(WitchElves) > 0:
            self.error("You can't have Witch Elves in core without {0}.".format(Crone.name))
