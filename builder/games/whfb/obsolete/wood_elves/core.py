__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.obsolete.wood_elves.armory import *


class GladeGuard(FCGUnit):
    name = 'Glade Guard'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Glade Guard', model_price=12, min_models=10,
            musician_price=6, standard_price=12, champion_name='Lord\'s Bowman', champion_price=6,
            base_gear=['Long bow'],
            magic_banners=filter_items(we_standard, 25)
        )


class EternalGuard(FCGUnit):
    name = 'Eternal Guard'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Eternal Guard', model_price=12, min_models=10,
            musician_price=6, standard_price=12, champion_name='Guardian', champion_price=12,
            base_gear=['Eternal guard weapons and armour'],
            magic_banners=filter_items(we_standard, 50)
        )


class Dryads(FCGUnit):
    name = 'Dryads'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Dryad', model_price=12, min_models=8, max_model=20,
            champion_name='Branch Nymph', champion_price=12,
            base_gear=['Razor sharp talons'],
        )


class Scouts(FCGUnit):
    name = 'Scouts'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Glade Guard', model_price=17, min_models=5, max_model=10,
            musician_price=6, standard_price=12, champion_name='Lord\'s Bowman', champion_price=6,
            base_gear=['Long bow'],
            magic_banners=filter_items(we_standard, 25)
        )


class GladeRaiders(FCGUnit):
    name = 'Glade Raiders'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Glade Raider', model_price=24, min_models=5, max_model=10,
            musician_price=9, standard_price=18, champion_name='Horsemaster', champion_price=9,
            base_gear=['Long bow', 'Spear'],
        )
