__author__ = 'Denis Romanov'

from builder.games.whfb.obsolete.wood_elves.mounts import *
from builder.games.whfb.obsolete.wood_elves.armory import *
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Sisters(Unit):
    name = 'Naestra and Arahan - Sisters of Twilight'
    base_points = 275

    def __init__(self):
        Unit.__init__(self)
        self.mount = self.opt_one_of('Mount', [
            ['Ceithin-Har', 320, 'ch'],
            ['Gwindalor', 50, 'gdl'],
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Naestra', gear=['Bow of Naestra']).build(1))
        self.description.add(ModelDescriptor('Arahan', gear=['Bow of Arahan']).build(1))
        if self.mount.get_cur() == 'ch':
            self.description.add(ModelDescriptor('Ceithin-Har', points=320).build(1))
        else:
            self.description.add(ModelDescriptor('Gwindalor', points=50).build(1))



class Drycha(StaticUnit):
    name = 'Drycha'
    base_points = 350
    gear = ['Level 2 Wizard', 'A Blight of Terrors', 'A Cluster of Radiants', 'A Cluster of Radiants']


class Orion(Unit):
    name = 'Orion, the King in the Woods'
    base_points = 575
    gear = ['Horn of the Wild Hunt', 'Spear of Kurnous', 'Cloak of Isha', 'The Hawk\'s Talon']

    def __init__(self):
        Unit.__init__(self)
        self.hounds = self.opt_count('Hound', 0, 2, 15)


class Highborn(Unit):
    name = 'Wood Elf Highborn'
    gear = ['Long Bow', 'Hand Weapon']
    base_points = 145

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 6, 'hw'],
            ['Spear', 3, 'sp'],
            ['Great weapon', 6, 'gw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
            ['Light armour', 3, 'la'],
        ])
        self.kind = self.opt_options_list('Kindreds', [
            ['Wardancer', 50, 'wd'],
            ['Wild Raider', 50, 'wr'],
            ['Waywacher', 45, 'ww'],
            ['Alter', 35, 'al'],
            ['Scout', 25, 'sc'],
            ['Eternal', 10, 'et'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(), GreatEagle(), GreatStag(), ForestDragon()])
        self.magic_wep = self.opt_options_list('Magic weapon', we_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', we_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', we_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', we_enchanted, 1)
        self.spites = self.opt_options_list('Spites', spites, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc, self.spites]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(we_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(we_armour_suits_ids))
        self.magic_arm.set_visible_options(['we_bh'], self.steed.get_count() == 0)
        self.magic_tal.set_visible_options(['we_amber'], self.steed.get_count() == 0)
        self.magic_wep.set_visible_options(['we_bol'], self.kind.get('wd'))
        self.steed.set_visible_options([GreatStag.name], self.kind.get('wr'))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Spellweaver(Unit):
    name = 'Spellweaver'
    gear = ['Long bow']
    base_points = 215

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.kind = self.opt_options_list('Kindreds', [
            ['Glamourweave', 20, 'gw'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(), GreatEagle(), Unicorn()])
        self.magic_wep = self.opt_options_list('Magic weapon', we_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', we_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', we_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', we_arcane, 1)
        self.spites = self.opt_options_list('Spites', spites, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Athel Loren', 0, 'loren'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc, self.spites]

    def check_rules(self):
        self.magic_tal.set_visible_options(['we_amber'], self.steed.get_count() == 0)
        self.magic_wep.set_visible_options(['we_bol'], False)
        self.steed.set_visible_options([Unicorn.name], self.kind.get('gw'))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Ancient(Unit):
    name = 'Treeman Ancient'
    base_points = 325

    def __init__(self):
        Unit.__init__(self)
        self.spites = self.opt_options_list('Spites', spites, points_limit=100)

    def get_unique_gear(self):
        return self.spites.get_selected()
#
#
# class HoethLoremaster(Unit):
#     name = 'Loremaster of Hoeth'
#     base_gear = ['Level 2 Wizard', 'Great weapon']
#     base_points = 230
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.magic_wep = self.opt_options_list('Magic weapon', he_weapon, 1)
#         self.magic_arm = self.opt_options_list('Magic armour', he_armour, 1)
#         self.magic_tal = self.opt_options_list('Talisman', he_talisman, 1)
#         self.magic_enc = self.opt_options_list('Enchanted item', he_enchanted, 1)
#         self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
#
#     def check_rules(self):
#         self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(he_armour_suits_ids) else [])
#         self.magic_wep.set_visible_options(['he_star'], False)
#         norm_point_limits(100, self.magic)
#         Unit.check_rules(self)
#
#     def get_unique_gear(self):
#         return sum((m.get_selected() for m in self.magic), [])
#
#
