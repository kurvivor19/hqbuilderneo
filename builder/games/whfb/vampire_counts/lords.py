from builder.games.whfb.vampire_counts import build_magic_levels

__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.vampire_counts.mounts import *
from builder.games.whfb.legacy_armory import *
from builder.games.whfb.vampire_counts.armory import *
from builder.games.whfb.vampire_counts.special import CorpseCart
from builder.games.whfb.vampire_counts.rare import Terrorgheist
from builder.core.options import norm_point_limits


class Vlad(StaticUnit):
    name = 'Vlad von Carstein'
    base_points = 495
    gear = ['Blood Drinker', 'The Carstein Ring', 'Heavy armour', 'Level 3 Wizard', 'The Lore of the Vampires',
            'Aura of Dark Majesty', 'Beguile', 'Supernatural Horror']


class MannfredC(Unit):
    name = 'Count Mannfred'
    base_points = 530
    gear = ['Sword of Unholy Power', 'Armour of Templehof', 'Level 4 Wizard', 'The Lore of Death',
            'The Lore of the Vampires', 'Dark Acolyte', 'Master of the Black Arts', 'Summon Creatures of the Night']

    def get_unique(self):
        return 'Mannfred'

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [Nightmare(), Hellsteed(), AbyssalTerror()])


class Kemmler(StaticUnit):
    name = 'Heinrich Kemmler'
    base_points = 350
    gear = ['Chaos Tomb Blade', 'Cloak of Mists and Shadows', 'Scull Staff', 'Level 4 Wizard',
            'The Lore of the Vampires', 'Master of the Dead']

    def get_unique(self):
        return [self.name]


class VampireLord(Unit):
    name = 'Vampire Lord'
    gear = ['Hand weapon']
    base_points = 220

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic', build_magic_levels(1, 4, 35))
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 4, 'hw2'],
            ['Great weapon', 10, 'gw'],
            ['Lance', 10, 'lnc']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
            ['Heavy armour', 6, 'harm']
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Nightmare(), Hellsteed(), AbyssalTerror(), ZombieDragon(),
                                                          CovenThrone()])
        self.powers = self.opt_options_list('Vampiric powers', vampire_powers, points_limit=100)
        self.magic_wep = self.opt_options_list('Magic weapon', vampire_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', vampire_armours, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_arc = self.opt_options_list('Arcane item', vampire_arcane_items, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', vampire_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_arc, self.magic_enc]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
            ['The Lore of the Vampires', 0, 'vampires'],
        ], limit=1)

    def check_rules(self):
        self.wep.set_active_options(['lnc'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.eq.set_active_options(['harm'], not self.magic_arm.is_selected(magic_armour_suits_ids))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class MasterNecromancer(Unit):
    name = 'Master Necromancer'
    gear = ['Hand weapon']
    base_points = 165

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))

        self.steed = self.opt_optional_sub_unit('Steed', [Nightmare(4), Hellsteed(), AbyssalTerror(), CorpseCart(True)])
        self.opt = self.opt_options_list('Upgrade', [['Master of the Dead', 20, 'motd']])
        self.magic = [
            self.opt_options_list('Magic weapon', vampire_magic_weapons, 1),
            self.opt_options_list('Magic armour', necromancer_armours, 1),
            self.opt_options_list('Talisman', talismans, 1),
            self.opt_options_list('Arcane item', vampire_arcane_items, 1),
            self.opt_options_list('Enchanted item', vampire_enchanted_items, 1)
        ]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Death', 0, 'death'],
            ['The Lore of the Vampires', 0, 'vampires'],
        ], limit=1)


    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])

    def is_master(self):
        return self.opt.get('motd')


class StrigoiKing(Unit):
    name = 'Strigoi Ghoul King'
    gear = ['Hand weapon', 'Level 1 Wizard', 'The Lore of the Vampires']
    base_points = 260

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [Terrorgheist()])
        self.powers = self.opt_options_list('Vampiric powers', vampire_powers, points_limit=100)
        self.magic = [
            self.opt_options_list('Magic weapon', vampire_magic_weapons, 1),
            self.opt_options_list('Talisman', talismans, 1),
            self.opt_options_list('Arcane item', vampire_arcane_items, 1),
            self.opt_options_list('Enchanted item', vampire_enchanted_items, 1),
        ]

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
