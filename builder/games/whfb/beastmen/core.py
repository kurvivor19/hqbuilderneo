__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit, Unit
from builder.core.unit import OptionsList
from builder.core.model_descriptor import ModelDescriptor


class GorHerd(FCGUnit):
    name = 'Gor Herd'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Gor', model_price=7, min_models=10,
            musician_price=5, standard_price=10, champion_name='For-render', champion_price=10,
            base_gear=['Hand weapon'],
            options=[
                OptionsList('arm', 'Options', [
                    ['Hand weapon', 1, 'hw'],
                    ['Shield', 1, 'sh']
                ], limit=1)
            ]
        )


class Warhounds(FCGUnit):
    name = 'Chaos Warhounds'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Chaos Warhound', model_price=6, min_models=5,
            options=[
                OptionsList('arm', 'Options', [
                    ['Poisoned Attacks', 3, 'pa'],
                    ['Scaly Skin', 1, 'ss']
                ])
            ]
        )


class UngorRaiders(FCGUnit):
    name = 'Ungor Raiders'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Ungor Raider ', model_price=6, min_models=5, max_model=10,
            musician_price=3, champion_name='Raider Halfhorn', champion_price=6,
            base_gear=['Hand weapon', 'Short bow'],
        )


class UngorHerd(Unit):
    name = 'Ungor Herd'

    base_gear = ['Shield', 'Hand weapon']
    model_price = 5

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ungor', 10, 100, self.model_price)
        fcg = [
            ['For-render', 6, 'chmp'],
            ['Standard bearer', 6, 'sb'],
            ['Musician', 3, 'mus']
        ]
        self.command_group = self.opt_options_list('Command group', fcg)
        self.boss = self.opt_options_list('', [['Ungrol Four-Horn', 75, 'ungrol']])
        self.opt = self.opt_options_list('Weapon', [['Spear', 1, 'sp']])

    def get_unique(self):
        return self.boss.get_selected()

    def check_rules(self):
        self.command_group.set_active_options(['chmp'], not self.boss.get('ungrol'))
        self.boss.set_active_options(['ungrol'], not self.command_group.get('chmp'))
        self.points.set(self.build_points(count=1, exclude=[self.opt.id]) + self.count.get() * self.opt.points())
        self.build_description(options=[])
        d = ModelDescriptor('Ungor', gear=self.base_gear, points=self.model_price).add_gear_opt(self.opt)
        if self.command_group.get('chmp'):
            self.description.add(d.clone().set_name('For-render').add_points(6).build(1))
        if self.boss.get('ungrol'):
            self.description.add(ModelDescriptor('Ungrol Four-Horn', points=75,
                                                 gear=['Hand weapon', 'Hand weapon', 'The Stolen Crown']).build(1))
        if self.command_group.get('sb'):
            self.description.add(d.clone().add_gear('Standard bearer', 6).build(1))
        if self.command_group.get('mus'):
            self.description.add(d.clone().add_gear('Musician', 3).build(1))
        command_count = len(self.command_group.get_all())
        self.description.add(d.build(self.count.get() - command_count))

    def get_count(self):
        return self.count.get() + (1 if self.boss.get('ungrol') else 0)


class TuskgorChariot(Unit):
    name = 'Tuskgor Chariot'
    base_points = 80
    static = True

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.boss = boss

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        if not self.boss:
            self.description.add(ModelDescriptor(name='Bestigor', count=1, gear=['Great weapon']).build())
        self.description.add(ModelDescriptor(name='Gor', count=1, gear=['Spear']).build())
        self.description.add(ModelDescriptor(name='Tuskgor', count=2).build())
