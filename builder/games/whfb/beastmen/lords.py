__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.beastmen.armory import *
from builder.games.whfb.beastmen.special import RazorgorChariot
from builder.games.whfb.beastmen.core import TuskgorChariot
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Khazrak(StaticUnit):
    name = 'Khazrak the One-Eye'
    base_points = 270
    gear = ['Scourge', 'The Dark Mail']


class Gorgoth(Unit):
    name = 'Gorgoth the Beastlord'
    base_points = 350
    gear = ['The Impaler', 'Skull of Mugrar', 'Cloak of the Beastlord']

    def __init__(self):
        Unit.__init__(self)
        self.eq = self.opt_options_list('Steed', [
            ['Razorgor Chariot', 65, 'ch'],
        ])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(options=[])
        if self.eq.get('ch'):
            char = ModelDescriptor(name='Razorgor Chariot', count=1, points=65)
            char.add_sub_unit(ModelDescriptor(name='Bagrar', gear=['Spear']).build(count=1))
            char.add_sub_unit(ModelDescriptor(name='Razorgor').build(count=2))
            self.description.add(char.build())


class Malagor(Unit):
    name = 'Malagor, the Dark Omen'
    base_points = 350
    gear = ['Icon of Vilification', 'Braystaff']

    def __init__(self):
        Unit.__init__(self)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Wild', 0, 'wild'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)


class Taurox(StaticUnit):
    name = 'Taurox, the Brass Bull'
    base_points = 335
    gear = ['Rune-tortured Axes']


class Beastlord(Unit):
    name = 'Beastlord'
    gear = ['Hand Weapon']
    base_points = 145

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 12, 'gw'],
            ['Hand weapon', 8, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
        ])
        self.arm = self.opt_options_list('', [
            ['Light armour', 3, 'la'],
            ['Heavy armour', 6, 'ha'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [TuskgorChariot(boss=True), RazorgorChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', beast_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', beast_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', beast_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', beast_enchanted, 1)
        self.magic_gifts = self.opt_options_list('Gifts of Chaos', gifts)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(beast_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(beast_armour_suits_ids))
        self.magic_enc.set_visible_options(['beast_skin'], False)
        norm_point_limits(100, self.magic + [self.magic_gifts])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Doombull(Unit):
    name = 'Doombull'
    gear = ['Hand Weapon']
    base_points = 235

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 15, 'gw'],
            ['Hand weapon', 8, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 6, 'sh'],
        ])
        self.arm = self.opt_options_list('', [
            ['Light armour', 6, 'la'],
            ['Heavy armour', 12, 'ha'],
        ], limit=1)
        self.magic_wep = self.opt_options_list('Magic weapon', beast_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', beast_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', beast_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', beast_enchanted, 1)
        self.magic_gifts = self.opt_options_list('Gifts of Chaos', gifts)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(beast_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(beast_armour_suits_ids))
        self.magic_enc.set_visible_options(['beast_skin', 'beast_hunt', 'beast_first'], False)
        norm_point_limits(100, self.magic + [self.magic_gifts])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class GreatBrayShaman(Unit):
    name = 'Great Bray-Shaman'
    gear = ['Hand Weapon']
    base_points = 200

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 4, 'hw'],
        ], limit=1)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [TuskgorChariot(boss=True), RazorgorChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', beast_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', beast_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', beast_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', beast_arcane, 1)
        self.magic_gifts = self.opt_options_list('Gifts of Chaos', gifts)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Wild', 0, 'wild'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_enc.set_visible_options(['beast_hunt', 'beast_first'], False)
        norm_point_limits(100, self.magic + [self.magic_gifts])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
