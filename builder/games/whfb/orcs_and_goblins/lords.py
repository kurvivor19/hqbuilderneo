__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.orcs_and_goblins.mounts import *
from builder.games.whfb.orcs_and_goblins.armory import *
from builder.games.whfb.orcs_and_goblins.special import BoarChariot, WolfChariot
from builder.games.whfb.orcs_and_goblins.rare import Arachnarok
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Gorbad(StaticUnit):
    name = 'Gorbad Ironclaw'
    base_points = 375
    gear = ['Morgrol the Mangler', 'Heavy armour']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Gnarla the War Boar', count=1).build())


class Azhag(StaticUnit):
    name = 'Azhag the Slaughterer'
    base_points = 550
    gear = ['Slagga\'s Slashas', 'Azhag\' \'Ard Armour', 'The Crown of Sorcery']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Skullmuncha', count=1).build())


class Skarsnik(StaticUnit):
    name = 'Skarsnik, Warlord of the Eight Peaks'
    base_points = 275
    gear = ['Skarsnik\'s Prodder', 'Light armour']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Gobbla', count=1).build())


class Grimgor(StaticUnit):
    name = 'Grimgor Ironhide'
    base_points = 355
    gear = ['Gitsnik', 'Blood-Forged Armour']


class Wurrzag(Unit):
    name = 'Wurrzag Da Great Green Prophet'
    base_points = 350
    gear = ['Bonewood Staff', 'Baleful Mask', 'Squiggly Beast', 'Level 4 Wizard', 'Spells of do Big Waaagh!']

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar()])


class Grom(StaticUnit):
    name = 'Grom the Paunch of Misty Mountain'
    base_points = 285
    gear = ['Axe of Grom', 'Light armour']

    def __init__(self):
        StaticUnit.__init__(self)
        char = ModelDescriptor(name='Grom\'s Chariot', count=1)
        char.add_sub_unit(ModelDescriptor(name='Niblet', gear=['Lucky banner', 'Hand weapon']).build(count=1))
        char.add_sub_unit(ModelDescriptor(name='Giant Wolf').build(count=3))
        self.description.add(char.build())


class OrcWarboss(Unit):
    name = 'Orc Warboss'
    base_gear = ['Hand weapon']
    base_points = 115

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Spear', 3, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(), Wyvern(), BoarChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.gear = self.base_gear + (['Light Armour'] if not self.magic_arm.is_selected(orcs_armour_suits_ids) else [])
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class BlackOrcWarboss(Unit):
    name = 'Black Orc Warboss'
    base_gear = ['Armed to da Teef']
    base_points = 160

    def __init__(self):
        Unit.__init__(self)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(), Wyvern(), BoarChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(orcs_armour_suits_ids) else [])
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class SavageOrcWarboss(Unit):
    name = 'Savage Orc Warboss'
    gear = ['Hand weapon']
    base_points = 150

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Spear', 3, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(), Wyvern()])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', magic_shields, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class GoblinWarboss(Unit):
    name = 'Goblin Warboss'
    gear = ['Hand weapon']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Spear', 3, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
            ['Light armour', 3, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [GiantWolf(), GiantSpider(), GiganticSpider(),
                                                          WolfChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(orcs_armour_suits_ids))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class NightGoblinWarboss(Unit):
    name = 'Night Goblin Warboss'
    gear = ['Hand weapon']
    base_points = 55

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Spear', 3, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
            ['Light armour', 3, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [GreatCaveSquig()])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(orcs_armour_suits_ids))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class OrcGreatShaman(Unit):
    name = 'Orc Great Shaman'
    base_gear = ['Hand weapon', 'Spells of do Big Waaagh!']
    base_points = 165

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(), Wyvern(), BoarChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class SavageOrcGreatShaman(Unit):
    name = 'Savage Orc Great Shaman'
    base_gear = ['Hand weapon', 'Spells of do Big Waaagh!']
    base_points = 170

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(), Wyvern()])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class GoblinGreatShaman(Unit):
    name = 'Goblin Great Shaman'
    base_gear = ['Hand weapon', 'Spells of do Little Waaagh!']
    base_points = 145

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [GiantWolf(), WolfChariot(), Arachnarok(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class NightGoblinGreatShaman(Unit):
    name = 'Night Goblin Great Shaman'
    base_gear = ['Hand weapon', 'Magic Mushrooms', 'Spells of do Little Waaagh!']
    base_points = 140

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
