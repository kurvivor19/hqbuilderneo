__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.empire.heroes import *
from builder.games.whfb.empire.lords import *
from builder.games.whfb.empire.core import *
from builder.games.whfb.empire.special import *
from builder.games.whfb.empire.rare import *


class Empire(LegacyFantasy):
    army_name = 'Empire'
    army_id = '9ed2faf9ad9b4d95aff2594f53a87dd4'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[KarlFranz, KurtHelborg, Balthasar, Volkmar, MariusLeitdorf, General, BattleWizard, Lector, Master],
            heroes=[Ludwig, Luthor, Wulfart, Captain, Wizard, Priest, Engineer, WitchHunter],
            core=[Halberdiers, Spearmen, Swordsmen, Crossbowmen, Handgunners, Archers, Militia, Knights],
            special=[Greatswords, Demigryph, Reiksguard, Huntsmen, Pistoliers, Outriders, Cannon, Mortar, Flagellants],
            rare=[Helblaster, Helstorm, Tank, Hurricanum, Luminark]
        )
