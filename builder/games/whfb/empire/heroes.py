__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.empire.mounts import *
from builder.games.whfb.empire.armory import *
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


heroes_weapon = filter_items(empire_weapon, 50)
heroes_armour = filter_items(empire_armour, 50)
heroes_talisman = filter_items(empire_talisman, 50)
heroes_arcane = filter_items(empire_arcane, 50)
heroes_enchanted = filter_items(empire_enchanted, 50)


class Captain(Unit):
    name = 'Captain'
    gear = ['Hand Weapon']
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Lance', 7, 'ln'],
            ['Handgun', 6, 'hg'],
            ['Long Bow', 5, 'lb'],
            ['Pistol', 5, 'ps'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
        ])
        self.arm = self.opt_one_of('', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 2, 'ha'],
            ['Full plate armour', 6, 'pa'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Warhorse(12, 4), Pegasus()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', empire_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(empire_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(empire_armour_suits_ids))
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['ln'], self.steed.get_count() > 0)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class Priest(Unit):
    name = 'Warrior Priest'
    gear = ['Hand Weapon']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 5, 'gw'],
            ['Hand weapon', 2, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
        ])
        self.arm = self.opt_one_of('', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 2, 'ha'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Warhorse(12, 4)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(empire_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(empire_armour_suits_ids))
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Engineer(Unit):
    name = 'Master Engineer'
    gear = ['Hand Weapon']
    base_points = 65

    class MechanicalSteed(StaticUnit):
        name = 'Mechanical Steed'
        base_points = 25

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Grenade launching blunderbuss', 10, 'gl'],
            ['Hochland long rifle', 20, 'hr'],
            ['Pigeon bombs', 20, 'pb'],
            ['Repeater handgun', 10, 'rh'],
            ['Repeater pistol', 10, 'rp'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Light armour', 1, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Warhorse(12, 2), self.MechanicalSteed()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_visible(not self.magic_arm.is_selected(empire_armour_suits_ids))
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class WitchHunter(Unit):
    name = 'Witch Hunter'
    base_gear = ['Hand Weapon']
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Pistol', 0, 'rp'],
            ['Brace of Pistol', 5, 'rp'],
        ])
        self.eq = self.opt_options_list('', [
            ['Great weapon', 5, 'gw'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.gear = self.base_gear
        if not self.magic_arm.is_selected(empire_armour_suits_ids):
            self.gear = self.gear + ['Light armour']
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Wizard(Unit):
    name = 'Battle Wizard'
    gear = ['Hand Weapon']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [Warhorse(12, 4)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Life', 0, 'life'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Ludwig(Unit):
    base_points = 185
    name = 'Ludwig Schwarzhelm, The Emperor\'s Champion'
    gear = ['Full palate armour', 'Sword of Justice', 'Battle Standard Bearer', 'The Emperor\'s Standard']
    static = True

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Warhorse', count=1, gear=['Barding']).build())


class Luthor(Unit):
    base_points = 155
    name = 'Luthor Huss, Prophet of Sigmar'
    gear = ['Massive Warhammer', 'Heavy armour']
    static = True

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Warhorse', count=1, gear=['Barding']).build())


class Wulfart(Unit):
    base_points = 140
    name = 'Markus Wulfhart, Huntsman of the Empire'
    gear = ['Hand weapon', 'The Amber Bow']
    static = True
    unique = True

