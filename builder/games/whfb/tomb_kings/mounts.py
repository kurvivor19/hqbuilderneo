__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.core.model_descriptor import ModelDescriptor


class SkeletalSteed(Unit):
    name = 'Skeletal Steed'
    base_points = 15
    static = True

    def __init__(self, points=15):
        self.base_points = points
        Unit.__init__(self)


class KingChariot(Unit):
    name = 'Skeleton Chariot'
    base_points = 55
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Skeletal Steed', count=2).build())


# class GreatEagle(Unit):
#     name = 'Great Eagle'
#     base_points = 50
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.opt = self.opt_options_list('Options', [
#             ['Swiftsense', 10, 'sw'],
#             ['Shredding Talons', 5, 'st'],
#         ])
#
#
# class Griffon(Unit):
#     name = 'Griffon'
#     base_points = 150
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.opt = self.opt_options_list('Options', [
#             ['Swooping Strike', 25, 'ss'],
#             ['Swiftsense', 20, 'sw'],
#         ])
#
#
# class SunDragon(StaticUnit):
#     name = 'Sun Dragon'
#     base_points = 235
#
#
# class MoonDragon(StaticUnit):
#     name = 'Moon Dragon'
#     base_points = 300
#
#
# class StarDragon(StaticUnit):
#     name = 'Star Dragon'
#     base_points = 390
