__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.high_elves.heroes import *
from builder.games.whfb.high_elves.lords import *
from builder.games.whfb.high_elves.core import *
from builder.games.whfb.high_elves.special import *
from builder.games.whfb.high_elves.rare import *


class HighElves(LegacyFantasy):
    army_name = 'High Elves'
    army_id = '1d954e0c5f0d4e9db9223bc2dff4563e'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Tyrion, Teclis, Eltharion, Alith, Alarielle, Prince, Archmage, Anointed, HoethLoremaster],
            heroes=[Korhil, Caradryan, Noble, Mage, DragonMage, SeaHelm, Handmaiden],
            core=[Spearmen, Archers, SeaGuard, SilverHelm, Reavers],
            special=[LionChariot, WhiteLions, Swordmasters, Shadow, PhoenixGuard, DragonPrinces, Skycutter,
                     TiranocChariot],
            rare=[BoltThrower, GreatEagles, FrostPhoenix, FlamePhoenix, Sisters]
        )

    def check_rare_limit(self, name, group, limit, mul):
        if name == BoltThrower.name:
            limit = 4
        return LegacyFantasy.check_rare_limit(self, name, group, limit, mul)

    def has_alarielle(self):
        return self.lords.count_unit(Alarielle) > 0
