__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit


class ElvenSteed(Unit):
    name = 'Elven Steed'
    base_points = 20

    def __init__(self, points=20, barding=7):
        self.base_points = points
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Ithilmar barding', barding, 'brd']
        ])


class GreatEagle(Unit):
    name = 'Great Eagle'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Swiftsense', 10, 'sw'],
            ['Shredding Talons', 5, 'st'],
        ])


class Griffon(Unit):
    name = 'Griffon'
    base_points = 150

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Swooping Strike', 25, 'ss'],
            ['Swiftsense', 20, 'sw'],
        ])


class SunDragon(StaticUnit):
    name = 'Sun Dragon'
    base_points = 235


class MoonDragon(StaticUnit):
    name = 'Moon Dragon'
    base_points = 300


class StarDragon(StaticUnit):
    name = 'Star Dragon'
    base_points = 390
