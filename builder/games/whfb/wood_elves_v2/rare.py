__author__ = 'Denis Romanov'


from builder.core2 import *
from builder.games.whfb.roster import FCGUnit


class Waywatchers(FCGUnit):
    type_name = 'Waywatchers'
    type_id = 'waywatchers_v1'

    def __init__(self, parent):
        super(Waywatchers, self).__init__(
            parent, model_name='Waywatcher', model_price=20, min_models=5,
            champion_name='Waywatcher Sentinel', champion_price=10,
            gear=[Gear('Hand weapon', count=2), Gear('Asrai longbow')]
        )


class Eagles(Unit):
    type_id = 'eagles_v1'
    type_name = 'Great Eagles'

    def __init__(self, parent):
        super(Eagles, self).__init__(parent)
        Count(self, 'Great Eagle', 1, 100, 50, gear=UnitDescription('Great Eagle', points=50))


class Treeman(Unit):
    type_id = 'treeman_v1'
    type_name = 'Treeman'

    class Opt(OptionsList):
        def __init__(self, parent):
            super(Treeman.Opt, self).__init__(parent, 'Options')
            self.variant('Strangleroots', 20)

    def __init__(self, parent):
        super(Treeman, self).__init__(parent, points=225)
        self.Opt(self)
