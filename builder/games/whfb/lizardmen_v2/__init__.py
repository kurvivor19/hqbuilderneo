from builder.games.whfb.roster import (
    LordsSection as BaseLordsSection,
    HeroesSection as BaseHeroesSection,
    CoreSection as BaseCoreSection,
    SpecialSection as BaseSpecialSection,
    RareSection as BaseRareSection,
    FantasyRoster
)

from .lords import *
from .heroes import *
from .core import *
from .special import *
from .rare import *

__author__ = 'Denis Romanov'


class LordsSection(BaseLordsSection):
    def __init__(self, parent):
        super(LordsSection, self).__init__(parent=parent)
        UnitType(self, Mazdamundi)
        UnitType(self, Kroak)
        UnitType(self, KroqGar)
        self.teh = UnitType(self, Tehenhauin)
        UnitType(self, Slann)
        UnitType(self, Oldblood)


class HeroesSection(BaseHeroesSection):
    def __init__(self, parent):
        super(HeroesSection, self).__init__(parent=parent)
        UnitType(self, Chakax)
        UnitType(self, GorRok)
        UnitType(self, TettoEko)
        self.tik = UnitType(self, TictaqTo)
        UnitType(self, Oxiotl)
        UnitType(self, Priest)
        UnitType(self, Scar)
        UnitType(self, Chief)


class CoreSection(BaseCoreSection):
    def __init__(self, parent):
        super(CoreSection, self).__init__(parent=parent)
        UnitType(self, SaurusWarriors)
        UnitType(self, Skinks)
        UnitType(self, SkinksSkirmishers)


class SpecialSection(BaseSpecialSection):
    def __init__(self, parent):
        super(SpecialSection, self).__init__(parent=parent)
        UnitType(self, TempleGuard)
        UnitType(self, Swarms)
        UnitType(self, ChameleonSkins)
        UnitType(self, ColdOneRaiders)
        UnitType(self, Kroxigor)
        self.terr = UnitType(self, TerradonRaiders)
        UnitType(self, Stegadon)
        UnitType(self, Bastiladon)
        UnitType(self, RipperdactylRaiders)


class RareSection(BaseRareSection):
    def __init__(self, parent):
        super(RareSection, self).__init__(parent=parent)
        UnitType(self, AncientStegadon)
        UnitType(self, SalamanderPack)
        UnitType(self, RazordonPack)
        UnitType(self, Triglodon)


class LizardmenV2(FantasyRoster):
    army_name = 'Lizardmen'
    army_id = 'lizardmen_v2'

    def __init__(self):
        super(LizardmenV2, self).__init__(
            lords=LordsSection(parent=self),
            heroes=HeroesSection(parent=self),
            core=CoreSection(parent=self),
            special=SpecialSection(parent=self),
            rare=RareSection(parent=self)
        )

    @property
    def has_teh(self):
        return self.lords.teh.count > 0

    @property
    def has_tik(self):
        return self.heroes.tik.count > 0

    def check_rules(self):
        super(LizardmenV2, self).check_rules()
        amb = sum(u.has_amb() for u in self.special.terr.units)
        if amb > 1:
            self.error('Only one unit of Terradon Raiders may be upgraded to have Ambushers special rule '
                       '(taken: {})'.format(amb))
