__author__ = 'Denis Romanov'


from builder.core2 import *
from builder.games.whfb.roster import FCGUnit
from .armory import Standards, Weapon


class Warlocks(FCGUnit):
    type_name = 'Doomfire Warlocks'
    type_id = 'warlocks_v1'

    def __init__(self, parent):
        super(Warlocks, self).__init__(
            parent, model_name='Doomfire Warlock', model_price=25, min_models=5,
            champion_name='Master of Warlocks', champion_price=10,
            gear=[Gear('Hand weapon')]
        )


class Medusa(Unit):
    type_id = 'medusa_v1'
    type_name = 'Bloodwrack Medusa'

    def __init__(self, parent):
        super(Medusa, self).__init__(parent, points=90)


class Kharibdyss(Unit):
    type_id = 'kharibdyss_v1'
    type_name = 'Kharibdyss'

    def __init__(self, parent):
        super(Kharibdyss, self).__init__(parent, points=160)


class Shrine(Unit):
    type_name = 'Bloodwrack Shrine'
    type_id = 'bloodwrack_shrine_v1'

    def __init__(self, parent):
        super(Shrine, self).__init__(parent, points=175, gear=[
            UnitDescription('Bloodwrack Medusa', options=[Gear('Spear')]),
            UnitDescription('Shrinekeeper', options=[Gear('Spear')], count=2),
            Gear('Scythes')
        ])


class SistersOfSlaughter(FCGUnit):
    type_name = 'Sisters of Slaughter'
    type_id = 'sisters_of_slaughter_v1'

    def __init__(self, parent):
        super(SistersOfSlaughter, self).__init__(
            parent, model_name='Sister of Slaughter', model_price=15, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Handmaiden of Shards', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Shield')]
        )
        self.magic_banners = [Standards(self, limit=50)]
