__author__ = 'Denis Romanov'

from itertools import groupby


class Games(object):

    def __init__(self, debug, logger):
        super(Games, self).__init__()
        self.debug = debug
        self.logger = logger
        from . import wh40k
        from . import whfb
        from . import aos
        from . import swag
        from . import wh40k8ed
        from . import killteam
        self.roster_types = wh40k8ed.armies + killteam.armies\
            + wh40k.armies + whfb.armies\
            + swag.armies + aos.armies
        self.game_list = self._build_game_list()
        self.game_map = self._build_game_map()

    def _build_game_list(self):
        check = []
        for army in self.roster_types:
            army.post_process()
            if army.army_id in check:
                print('Double army_id for {0}'.format(army.army_name))
            check.append(army.army_id)

        army_map = {}

        for army in self.roster_types:
            if army.obsolete:
                continue
            if army.development:
                if self.debug:
                    army.game_name = '__development__'
                    if self.logger:
                        self.logger.warning('Development army {0} added'.format(army.army_name))
                else:
                    continue
            if army.game_name not in army_map:
                army_map[army.game_name] = []
            army_map[army.game_name].append({
                'name': army.army_name,
                'id': army.army_id,
                'type': army.roster_type if army.roster_type else '---'
            })
        game_order = [
            'Warhammer 40k',
            'Kill Team',
            'Warhammer Fantasy Battle',
            'Shadow War: Armmageddon',
            'Warhammer 40k (7th edition)',
            'Warhammer 40k (7th edition): Imperial armour',
            '__development__'
        ]
        games = [{
            'name': game,
            'armies': [(gn, [ge for ge in gi])
                       for gn, gi in
                       groupby(sorted(army_map[game], key=lambda a: a['name']),
                               key=lambda a: a['name'])]
        } for game in game_order if game in army_map]

        return games[:]

    def _build_game_map(self):
        return {army.army_id: army for army in self.roster_types}

    def get_roster_name(self, army_id):
        army = self.game_map.get(army_id, None)
        if army:
            return army.game_name, army.army_name, army.base_tags
        return 'Undefined', 'Undefined', []

    def get_roster_object(self, army_id):
        if army_id in self.game_map:
            return self.game_map[army_id]()

    def load_roster(self, roster_id, roster_data):
        roster = self.get_roster_object(roster_data['army_id'])
        roster.load(roster_data)
        roster.id = roster_id
        roster.check_rules_chain()
        roster.flush()
        return roster
