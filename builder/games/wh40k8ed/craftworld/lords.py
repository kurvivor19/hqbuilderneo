from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count
from builder.games.wh40k8ed.utils import get_name, points_price, get_cost, get_costs
from . import armory
from . import units
from . import melee
from . import wargear
from . import ranged

__author__ = 'Ivan Truskov'


class Wraithknight(LordsUnit, armory.CraftworldUnit):
    type_name = get_name(units.Wraithknight)
    type_id = 'wraithknight_v1'
    faction = ['Spirit host']

    keywords = ['Monster', 'Titanic']

    power = 27

    class MainWeapons(OneOf):
        def __init__(self, parent):
            super(Wraithknight.MainWeapons, self).__init__(parent, 'Main weapons')
            self.variant('Two heavy wraithcannons', gear=[Gear('Heavy wraithcannon', count=2)],
                         points=2 * get_cost(ranged.HeavyWraithcannon))
            self.variant('Titanic ghostglaive and scattershield', gear=[
                Gear('Titanic ghostglaive'), Gear('Scattershield')
            ], points=get_costs(*[melee.TitanicGhostglaive, wargear.Scattershield]))
            self.variant('Suncannon and scattershield', gear=[
                Gear('Suncannon'), Gear('Scattershield')
            ], points=get_costs(*[ranged.Suncannon, wargear.Scattershield]))

    def __init__(self, parent):
        points = points_price(get_cost(units.Wraithknight), *[melee.TitanicWraithboneFists, melee.TitanicFeet])
        super(Wraithknight, self).__init__(parent, gear=[
            Gear('Titanic wraithbone fists'), Gear('Titanic wraithbone feet')], points=points)
        self.MainWeapons(self)
        self.shoulder = [
            Count(self, 'Scatter laser', 0, 2, points=get_cost(ranged.ScatterLaser)),
            Count(self, 'Shuriken cannon', 0, 2, points=get_cost(ranged.ShurikenCannon)),
            Count(self, 'Starcannon', 0, 2, points=get_cost(ranged.Starcannon))
        ]

    def check_rules(self):
        super(Wraithknight, self).check_rules()
        Count.norm_counts(0, 2, self.shoulder)
