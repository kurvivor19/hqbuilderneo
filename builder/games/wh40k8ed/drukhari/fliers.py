__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit, Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, SubUnit, UnitDescription, Count, ListSubUnit, UnitList
from . import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Razorwing(FlierUnit, armory.NonCovenUnit):
    type_name = 'Razorwing Jetfighter'
    type_id = 'razorwing_v1'

    keywords = ['Vehicle', 'Fly']
    power = 8

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(Razorwing.MainWeapon, self).__init__(parent, 'Main weapom')
            self.variant('Two disintegrator cannons', get_cost(ranged.DisintegratorCannon)*2,
                         gear=[Gear('Disintegrator cannon', count=2)])
            self.variant('Two dark lances', get_cost(ranged.DarkLance)*2, gear=[Gear('Dark lance', count=2)])

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(Razorwing.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant(*ranged.TwinSplinterRifle)
            self.variant(*ranged.SplinterCannon)

    def __init__(self, parent):
        super(Razorwing, self).__init__(parent, points=get_cost(units.RazorwingJetfighter),
                                        gear=[Gear('Razorwing missiles')])
        self.MainWeapon(self)
        self.SecondaryWeapon(self)


class Voidraven(FlierUnit, armory.NonCovenUnit):
    type_name = get_name(units.Voidraven)
    type_id = 'voidraven_v1'

    keywords = ['Vehicle', 'Fly']
    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Voidraven.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two void lances', 2 * get_cost(ranged.VoidLance),
                         gear=[Gear('Void lance', count=2)])
            self.variant('Two dark scythes', 2 * get_cost(ranged.DarkScythe),
                         gear=[Gear('Dark scythe', count=2)])

    class Payload(OptionsList):
        def __init__(self, parent):
            super(Voidraven.Payload, self).__init__(parent, 'Payload')
            self.variant(*ranged.VoidravenMissiles)

    def __init__(self, parent):
        super(Voidraven, self).__init__(parent, points=get_cost(units.Voidraven))
        self.Weapon(self)
        self.Payload(self)
