from .hq import Patriarch, Magus, Primus, Iconward, Abominant, JackalAlphus
from .troops import AcolyteHybrids, NeophyteHybrids, BroodInfantry
from .elites import HybridMetamorphs, Aberrants, PurestrainGenestealers,\
    Clamavus, Sanctus, Kellermorph, Biophagus, Nexos, Locus
from .transport import GoliathTruck, CultChimera
from .fast import CultScoutSentinelSquad, CultArmouredSentinelSquad, RidgerunnerSquadron,\
    AtalanJackals
from .heavy import CultLemanRuss, GoliathRockgrinder, BroodHeavyWeaponSquad
from .fort import TectonicFragdrill

from builder.games.wh40k8ed.sections import HQSection, ElitesSection, UnitType


class CultHQ(HQSection):
    def check_rules(self):
        super(CultHQ, self).check_rules()
        for ut in self.types:
            if not 'CHARACTER' in ut.unit_class.keywords:
                continue
            if ut.count > 1:
                self.error('Cannot take more then 1 {} in the same detachment; taken: {}'.format(ut.name, ut.count))


class CultElites(ElitesSection):
    def check_rules(self):
        super(CultElites, self).check_rules()
        for ut in self.types:
            if not 'CHARACTER' in ut.unit_class.keywords:
                continue
            if ut.count > 1:
                self.error('Cannot take more then 1 {} in the same detachment; taken: {}'.format(ut.name, ut.count))

    
unit_types = [Patriarch, Magus, Primus, Iconward, AcolyteHybrids,
              NeophyteHybrids, HybridMetamorphs, Aberrants,
              PurestrainGenestealers, GoliathTruck, CultChimera,
              CultScoutSentinelSquad, CultArmouredSentinelSquad,
              CultLemanRuss, GoliathRockgrinder, Abominant,
              JackalAlphus, BroodInfantry, Clamavus, Sanctus,
              Kellermorph, Biophagus, Nexos, Locus,
              RidgerunnerSquadron, AtalanJackals,
              BroodHeavyWeaponSquad, TectonicFragdrill]
