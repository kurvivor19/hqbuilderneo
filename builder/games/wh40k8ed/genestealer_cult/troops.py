__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList, Count, ListSubUnit,\
    UnitList, OptionalSubUnit, SubUnit
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class AcolyteHybrids(TroopsUnit, armory.CultUnit):
    type_name = get_name(units.AcolyteHybrids)
    type_id = 'gs_acolyte_hybrids_v1'
    power = 3
    keywords = ['Infantry']

    class Acolyte(ListSubUnit):
        type_name = 'Acolyte Hybrid'

        class Claws(OneOf):
            def __init__(self, parent):
                super(AcolyteHybrids.Acolyte.Claws, self).__init__(parent, 'Melee weapons')
                self.claws = self.variant('Rending claw and cultist knife', 0,
                                          gear=[Gear('Rending claw'), Gear('Cultist knife')])
                self.variant(*melee.HeavyRockDrill)
                self.variant(*melee.HeavyRockCutter)
                self.variant(*melee.HeavyRockSaw)
                self.charge = self.variant(*ranged.DemolitionCharges)

        class Pistols(OneOf):
            def __init__(self, parent):
                super(AcolyteHybrids.Acolyte.Pistols, self).__init__(parent, 'Pistol')
                self.variant(*ranged.Autopistol)
                self.variant(*ranged.HandFlamer)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(AcolyteHybrids.Acolyte.Banners, self).__init__(parent, "Icon", limit=1)
                self.variant(*wargear.CultIcon)

        def __init__(self, parent):
            super(AcolyteHybrids.Acolyte, self).__init__(
                parent=parent, points=get_cost(units.AcolyteHybrids),
                gear=create_gears(ranged.BlastingCharge)
            )
            self.claws = self.Claws(self)
            self.pistols = self.Pistols(self)
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

        @ListSubUnit.count_gear
        def count_special(self):
            return self.claws.cur != self.claws.claws

        @ListSubUnit.count_gear
        def count_charges(self):
            return self.claws.cur == self.claws.charge

    class AcolyteLeader(Unit):
        type_name = 'Acolyte Leader'

        class CloseCombatWeapon(OneOf):
            def __init__(self, parent):
                super(AcolyteHybrids.AcolyteLeader.CloseCombatWeapon, self).__init__(parent, 'Close combat weapon')
                self.ccw = self.variant(*melee.CultistKnife)
                # self.variant(*melee.Bonesword)
                self.whip = self.variant(*melee.LashWhipAndBonesword, gear=[Gear('Lash whip'),
                                                                            Gear('Bonesword')])

        class Pistols(OneOf):
            def __init__(self, parent, name, *args, **kwargs):
                super(AcolyteHybrids.AcolyteLeader.Pistols, self).__init__(parent, name=name, *args, **kwargs)
                self.variant(*ranged.Autopistol)
                self.variant(*ranged.HandFlamer)

        def __init__(self, parent):
            super(AcolyteHybrids.AcolyteLeader, self).__init__(parent, points=get_cost(units.AcolyteHybrids),
                                                               gear=[Gear('Rending claw'), Gear('Blasting charges')])
            self.ccw = self.CloseCombatWeapon(self)
            self.pistols = self.Pistols(self, 'Pistols')

        def check_rules(self):
            super(AcolyteHybrids.AcolyteLeader, self).check_rules()
            self.pistols.used = self.pistols.visible = not self.ccw.cur == self.ccw.whip

    def __init__(self, parent):
        super(AcolyteHybrids, self).__init__(parent=parent)
        self.leader = SubUnit(self, self.AcolyteLeader(self))
        self.acolytes = UnitList(self, self.Acolyte, 4, 19)

    def get_count(self):
        return self.acolytes.count + 1

    def check_rules(self):
        super(AcolyteHybrids, self).check_rules()

        spec_total = sum(acolyte.count_special() for acolyte in self.acolytes.units)
        if 5 * spec_total > 2 * self.get_count():
            self.error('In Acolyte squad only 2 special weapon per 5 models may be taken (taken: {0})'.format(spec_total))
        flags = sum(pal.count_banners() for pal in self.acolytes.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

    def build_power(self):
        return self.power * ((self.get_count() + 4) / 5)


class NeophyteHybrids(TroopsUnit, armory.CultUnit):
    type_name = get_name(units.NeophyteHybrids)
    type_id = 'gs_neophyte_hybrids_v1'
    keywords = ['Infantry']
    power = 4

    class Hybrid(ListSubUnit):

        class Weapon(OneOf):
            def __init__(self, parent):
                super(NeophyteHybrids.Hybrid.Weapon, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Autogun)
                self.variant(*ranged.Shotgun)
                # self.variant(*ranged.Lasgun)
                armory.add_special_weapons(self)
                armory.add_mining_weapons(self)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(NeophyteHybrids.Hybrid.Banners, self).__init__(parent, "Icon", limit=1)
                self.variant(*wargear.CultIcon)

        def __init__(self, parent):
            super(NeophyteHybrids.Hybrid, self).__init__(
                parent=parent, points=get_cost(units.NeophyteHybrids), name='Neophyte Hybrid',
                gear=[Gear('Blasting charges'), Gear('Autopistol')]
            )
            self.wep = self.Weapon(self)
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_heavy_weapon(self):
            return self.wep.cur in self.wep.mining

        @ListSubUnit.count_gear
        def count_special_weapon(self):
            return self.wep.cur in self.wep.special

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

    class NeophyteLeader(Unit):

        class Pistol(OneOf):
            def __init__(self, parent):
                super(NeophyteHybrids.NeophyteLeader.Pistol, self).__init__(parent, 'Pistol')
                self.variant(*ranged.Autopistol)
                armory.add_pistols(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(NeophyteHybrids.NeophyteLeader.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.Autogun)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(NeophyteHybrids.NeophyteLeader, self).__init__(parent, name='Neophyte Leader',
                                                                 points=get_cost(units.NeophyteHybrids),
                                                                 gear=create_gears(ranged.BlastingCharge))
            self.ccw = self.Weapon(self)
            self.pistols = self.Pistol(self)

    def __init__(self, parent):
        super(NeophyteHybrids, self).__init__(parent)
        self.leader = SubUnit(self, self.NeophyteLeader(parent=self))
        self.hybrids = UnitList(self, self.Hybrid, 7, 19, start_value=9)

    def check_rules(self):
        heavy_total = sum(u.count_heavy_weapon() for u in self.hybrids.units)
        if 2 < heavy_total:
            self.error('Neophyte Hybrid unit can take only 2 heavy weapon (taken: {0})'.format(heavy_total))

        special_total = sum(u.count_special_weapon() for u in self.hybrids.units)
        if 2 < special_total:
            self.error('Neophyte Hybrid unit can take only 2 special weapon (taken: {0})'.format(special_total))

        flags = sum(pal.count_banners() for pal in self.hybrids.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

    def get_count(self):
        return self.hybrids.count + 1

    def build_power(self):
        return self.power + (2 if self.get_count() > 10 else 0)


class BroodWeaponTeam(Unit):
    type_name = 'Brood brothers Weapon Team'

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(BroodWeaponTeam.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            armory.add_heavy_weapons(self)

    def __init__(self, parent, root_unit):
        super(BroodWeaponTeam, self).__init__(parent, gear=[Gear('Lasgun'), Gear('Frag grenades')])
        self.base_unit = root_unit
        self.wep = self.HeavyWeapon(self)


class BroodInfantry(TroopsUnit, armory.BroodUnit):
    type_name = get_name(units.BroodBrotherInfantry)
    type_id= 'gs_brood_infantry_v1'
    power = 3
    keywords = ['Infantry']

    class Special(OneOf):
        def __init__(self, parent):
            super(BroodInfantry.Special, self).__init__(parent, 'Special weapon')
            self.las = self.variant(*ranged.Lasgun)
            self.variant(*ranged.Flamer)
            self.variant(*ranged.GrenadeLauncher)

        def taken(self):
            return self.cur != self.las

        @property
        def description(self):
            return []

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(BroodInfantry.Members, self).__init__(parent, name='', limit=1)
            self.heavy = SubUnit(self, BroodWeaponTeam(self, parent))

    class Caster(OptionsList):
        def __init__(self, parent):
            super(BroodInfantry.Caster, self).__init__(parent, 'Squad comms')
            self.variant(*wargear.CultVoxCaster)

        @property
        def description(self):
            if self.any:
                return [UnitDescription('Brood brother', options=create_gears(ranged.Lasgun, ranged.FragGrenades, wargear.CultVoxCaster))]
            else:
                return []

    class Squad(Count):
        @property
        def description(self):
            cnt = self.cur - self.parent.comm.any - 2 * self.parent.team.count - sum(spec.taken() for spec in self.parent.spec)
            res = [self.gear.clone().set_count(cnt)]
            return res
    
    def __init__(self, parent):
        super(BroodInfantry, self).__init__(parent, points=get_cost(units.BroodBrotherInfantry),
                                            gear=UnitDescription('Brood Brother Leader',
                                                                 options=[
                                                                     Gear('Laspistol'),
                                                                     Gear('Chainsaword'),
                                                                     Gear('Frag grenades')
                                                                 ]))
        self.comm = self.Caster(self)
        self.team = self.Members(self)
        self.squad = self.Squad(self, 'Brood Brother', 9, 19, get_cost(units.BroodBrotherInfantry), per_model=True,
                                gear=UnitDescription('Brood brother', options=[Gear('Lasgun'), Gear('Frag grenades')]))
        self.spec = [self.Special(self), self.Special(self)]

    def get_count(self):
        return 1 + self.squad.cur

    def build_power(self):
        return self.power + (2 if self.get_count() > 10 else 0)

    def build_description(self):
        res = super(BroodInfantry, self).build_description()
        for spec in self.spec:
            if spec.taken():
                res.add_dup(UnitDescription('Brood Brother', options=[Gear(spec.cur.title), Gear('Frag grenades')]))
        return res
