__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, FortUnit
from . import armory, units
from builder.games.wh40k8ed.utils import *


class TectonicFragdrill(FortUnit, armory.Unit):
    type_name = get_name(units.TectonicFragdrill)
    type_id = 'gs_da_drill_v1'
    keywords = ['Sector Mechanicus']
    faction = ['Genestealer Cults']
    power = 4

    def __init__(self, parent):
        super(TectonicFragdrill, self).__init__(parent, points=get_cost(units.TectonicFragdrill),
                                                static=True)
