from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList
from . import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class BADevastators(HeavyUnit, armory.BAUnit):
    type_name = 'Blood Angels Devastator Squad'
    type_id = 'ba_devastators_v1'
    keywords = ['Infantry']
    power = 8

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(BADevastators.Sergeant, self).__init__(
                parent, gear=armory.create_gears(*BADevastators.model_gear),
                points=armory.get_cost(units.DevastatorSquad)
            )
            self.wep1 = armory.SergeantWeapon(self)

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(BADevastators.HeavyWeapon, self).__init__(parent, 'Heavy Weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_heavy_weapons(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(BADevastators.Options, self).__init__(parent, 'Options')
            self.variant(*units.ArmoriumCherub,
                         gear=[UnitDescription('Armorium Cherub')])

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(BADevastators, self).__init__(parent=parent, name=armory.get_name(units.DevastatorSquad))
        self.sergeant = SubUnit(parent=self, unit=self.get_leader()(self))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9,
                             points=armory.get_cost(units.DevastatorSquad),
                             per_model=True)
        self.heavy = [self.HeavyWeapon(self) for i in range(0, 4)]
        self.opt = self.Options(self)

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=armory.create_gears(*BADevastators.model_gear),
        )
        for o in self.heavy:
            spec_marine = marine.clone()
            spec_marine.add(o.description)
            desc.add_dup(spec_marine)

        marine.add(Gear('Boltgun')).set_count(self.marines.cur - 4)
        desc.add_dup(marine)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power + 3 * (self.marines.cur > 4)


class BaalPredator(HeavyUnit, armory.BAUnit):
    type_name = get_name(units.BaalPredator)
    type_id = "ba_predator_v1"
    keywords = ['Vehicle', 'Predator']
    power = 8

    class Turret(OneOf):
        def __init__(self, parent):
            super(BaalPredator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.TwinAssaultCannon)
            self.twinlinkedlascannon = self.variant(*ranged.FlamestormCannon)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Two heavy bolters', 2 * get_cost(ranged.HeavyBolter),
                                                         gear=Gear('Heavy bolter', count=2))
            self.sponsonswithlascannons = self.variant('Two heavy flamers', 2 * get_cost(ranged.HeavyFlamer),
                                                       gear=Gear('Heavy flamer', count=2))

    class Options(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        super(BaalPredator, self).__init__(parent=parent, points=get_cost(units.BaalPredator))
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.Options(self)


class BAEliminators(HeavyUnit, armory.BAUnit):
    type_name = 'Blood Anglels ' + get_name(units.EliminatorSquad)
    type_id = 'ba_eliminators_v1'

    keywords = ['Infantry', 'Primaris', 'Phobos']
    model_cost = get_cost(units.EliminatorSquad)
    gearlist = [ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades, wargear.CamoCloak]
    power = 4

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(BAEliminators.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.InstigatorBoltCarbine)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator Sergeant', options=armory.create_gears(*BAEliminators.gearlist))
                    .add(super(BAEliminators.Sergeant, self).description)]

    class SquadWeapons(OneOf):
        def __init__(self, parent):
            super(BAEliminators.SquadWeapons, self).__init__(parent, 'Squad weapons')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator', count=2, options=armory.create_gears(*BAEliminators.gearlist))
                    .add(super(BAEliminators.SquadWeapons, self).description)]

    def __init__(self, parent):
        cost = points_price(get_cost(units.EliminatorSquad), *self.gearlist)
        super(BAEliminators, self).__init__(parent, points=3 * cost)
        self.Sergeant(self)
        self.sqw = self.SquadWeapons(self)

    def build_points(self):
        return super(BAEliminators, self).build_points() + self.sqw.points

    def get_count(self):
        return 3
