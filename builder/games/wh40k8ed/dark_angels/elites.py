from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit, \
    SubUnit, UnitList
from . import armory
from . import units
from . import ranged
from . import wargear
from . import melee
from builder.games.wh40k8ed.utils import get_name, create_gears, get_cost, get_costs, points_price


class DACompanyChampion(ElitesUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.CompanyChampion)
    type_id = 'da_company_champion_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        gear = [ranged.BoltPistol, melee.BladeOfCaliban, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.CompanyChampion), *gear)
        super(DACompanyChampion, self).__init__(parent, name=get_name(units.CompanyChampion),
                                                points=cost, gear=create_gears(*gear),
                                                static=True)


class DACompanyVeterans(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.CompanyVeterans)
    type_id = 'da_company_veterans_v1'
    keywords = ['Infantry']

    armory = armory
    power = 3

    class Options(OptionsList):
        def __init__(self, parent):
            super(DACompanyVeterans.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.CombatShield)

    class Sergeant(armory.ClawUser):
        type_name = 'Company Veteran Sergeant'

        def __init__(self, parent):
            super(DACompanyVeterans.Sergeant, self).__init__(parent, points=get_cost(units.CompanyVeterans),
                                                             gear=[Gear('Frag grenades'), Gear('Krak grenades')])

            class Sword(parent.armory.SergeantWeapon):
                sword = False

                def add_double(self):
                    sword = [self.variant(*melee.Chainsword)]
                    super(Sword, self).add_double()
                    self.double += sword

            self.wep1 = parent.armory.SergeantWeapon(self, double=False)
            self.wep2 = Sword(self, double=True)
            DACompanyVeterans.Options(self)

        def check_rules(self):
            super(DACompanyVeterans.Sergeant, self).check_rules()
            self.parent.parent.armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Veteran(ListSubUnit):
        type_name = 'Space Marine Veteran'

        class Heavy(OptionsList):
            def __init__(self, parent):
                super(DACompanyVeterans.Veteran.Heavy, self).__init__(parent, 'Heavy weapon')
                armory.add_heavy_weapons(self)

        class Pistols(OneOf):
            def __init__(self, parent):
                super(DACompanyVeterans.Veteran.Pistols, self).__init__(parent, 'Pistols')
                self.parent.root_unit.armory.add_pistol_options(self)
                self.variant(*wargear.StormShield)
                self.parent.root_unit.armory.add_melee_weapons(self)

        class Melee(OneOf):
            def __init__(self, parent):
                super(DACompanyVeterans.Veteran.Melee, self).__init__(parent, 'Melee weapon')
                self.parent.root_unit.armory.add_melee_weapons(self)
                self.variant(*wargear.StormShield)
                self.variant(*ranged.Boltgun)
                self.parent.root_unit.armory.add_pistol_options(self)
                self.parent.root_unit.armory.add_combi_weapons(self)
                self.parent.root_unit.armory.add_special_weapons(self)

        def __init__(self, parent):
            super(DACompanyVeterans.Veteran, self).__init__(parent, points=get_cost(units.CompanyVeterans),
                                                            gear=[Gear('Frag grenades'), Gear('Krak grenades')])
            self.pistols = self.Pistols(self)
            self.mle = self.Melee(self)
            self.heavy = self.Heavy(self)
            DACompanyVeterans.Options(self)

        def build_points(self):
            res = super(DACompanyVeterans.Veteran, self).build_points()
            if self.pistols.cur == self.pistols.claw and self.mle.cur == self.mle.claw:
                res -= 5
            return res

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.heavy.any

    def get_leader(self):
        return self.Sergeant

    def get_member(self):
        return self.Veteran

    def __init__(self, parent):
        super(DACompanyVeterans, self).__init__(parent)
        self.ldr = SubUnit(self, self.get_leader()(parent=self))
        self.models = UnitList(self, self.get_member(), 1, 4)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 5 * (self.models.count > 1)

    def check_rules(self):
        super(DACompanyVeterans, self).check_rules()
        hcnt = sum(u.has_heavy() for u in self.models.units)
        if hcnt > 1:
            self.error('Only one Veteran may take heavy weapon')


class DWApothecary(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingApothecary)
    type_id = 'dwing_apothecary_v1'
    faction = ['Deathwing']
    kwname = 'Apothecary'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 4

    def __init__(self, parent):
        gear = [ranged.StormBolter]
        cost = points_price(get_cost(units.DeathwingApothecary), *gear)
        super(DWApothecary, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)


class DWAncient(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingAncient)
    type_id = 'dwing_ancient_v1'
    faction = ['Deathwing']
    kwname = 'Ancient'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    class Weapons(OneOf):
        def __init__(self, parent):
            super(DWAncient.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Power fist and storm bolter', get_cost(melee.PowerFist) + get_cost(ranged.StormBolter), gear=[
                Gear('Power fist'), Gear('Storm bolter')
            ])
            self.variant('Chainfist and storm bolter', get_cost(melee.Chainfist) + get_cost(ranged.StormBolter), gear=[
                Gear('Chainfist'), Gear('Storm bolter')
            ])
            self.variant('Two lightning claws', get_cost(melee.PairLightningClaws), gear=[
                Gear('Lightning claw', count=2)
            ])
            self.variant('Thunder hammer and storm shield',
                         get_cost(melee.CharacterThunderHammer) + get_cost(ranged.StormBolter), gear=[
                    Gear('Thunder hammer'), Gear('Storm shield')
                ])

    def __init__(self, parent):
        super(DWAncient, self).__init__(parent, points=get_cost(units.DeathwingAncient), unique=True)
        self.Weapons(self)


class DWChampion(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingChampion)
    type_id = 'dwing_champion_v1'
    faction = ['Deathwing']

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    def __init__(self, parent):
        gear = [melee.HalberdOfCaliban]
        cost = points_price(get_cost(units.DeathwingChampion), *gear)
        super(DWChampion, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True, unique=True)


class DeathwingTerminatorSquad(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingTerminatorSquad)
    type_id = 'deathwing_terminator_squad_v1'
    faction = ['Deathwing']
    keywords = ['Infantry', 'Trminator']

    class Veteran(ListSubUnit):
        type_name = 'Deathwing Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.chain_fist = self.variant(*melee.Chainfist)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.StormBolter)
                self.pc = self.variant(*ranged.PlasmaCannon)
                armory.add_term_heavy_weapons(self)
                self.variant('Cyclone missile launcher and storm bolter',
                             get_costs(ranged.CycloneMissileLauncher, ranged.StormBolter),
                             gear=create_gears(ranged.CycloneMissileLauncher, ranged.StormBolter))

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.Weapon, self).__init__(parent=parent, name='', limit=1)
                self.lc = self.variant('Pair of lightning claws', get_cost(melee.PairLightningClaws),
                                       gear=Gear('Lightning claw', count=2))
                self.thss = self.variant('Thunder hammer and storm shield',
                                         get_cost(melee.ThunderHammer) + get_cost(wargear.StormShield),
                                         gear=[Gear('Thunder hammer'), Gear('Storm shield')])

        def __init__(self, parent):
            super(DeathwingTerminatorSquad.Veteran, self).__init__(parent=parent,
                                                                   points=get_cost(units.DeathwingTerminatorSquad))

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)

        def check_rules(self):
            super(DeathwingTerminatorSquad.Veteran, self).check_rules()
            for wep in [self.right_weapon, self.left_weapon]:
                wep.visible = wep.used = not self.weapon.any

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.right_weapon.has_spec()

    class TerminatorSergeant(Unit):
        type_name = 'Deathwing Sergeant'

        class Weapons(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.TerminatorSergeant.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Storm bolter and power sword', get_cost(ranged.StormBolter) + get_costs(melee.PowerSword),
                             gear=[Gear('Storm bolter'), Gear('Power sword')])
                self.variant('Two lightning claws', get_costs(melee.PairLightningClaws),
                             gear=[Gear('Lightning claw', count=2)])
                self.variant('Thunder hammer and storm shield',
                             get_cost(melee.ThunderHammer) + get_cost(wargear.StormShield),
                             gear=[Gear('Thunder hammer'), Gear('Storm shield')])

        def __init__(self, parent):
            super(DeathwingTerminatorSquad.TerminatorSergeant, self).__init__(parent, points=get_cost(
                units.DeathwingTerminatorSquad))
            self.Weapons(self)

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(DeathwingTerminatorSquad.SquadOptions, self).__init__(parent, 'Options')
            self.variant('Watcher in the Dark', 5)

    def __init__(self, parent):
        super(DeathwingTerminatorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.TerminatorSergeant(parent=self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.opt = self.SquadOptions(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        super(DeathwingTerminatorSquad, self).check_rules()
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Deathwing Terminators may take only {0} heavy weapon (taken {1})'.format(spec_limit, spec))

    def build_power(self):
        return 13 + 12 * int(self.terms.count > 4)


class DeathwingKnights(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingKnights)
    type_id = 'deathwing_knights_v1'
    faction = ['Deathwing']
    knight_gear = [melee.MaceOfAbsolution, wargear.StormShield]
    knights_points = points_price(get_cost(units.DeathwingKnights), *knight_gear)

    def __init__(self, parent):
        serg_gear = [melee.FlailOfTheUnforgiven, wargear.StormShield]
        super(DeathwingKnights, self).__init__(parent=parent,
                                               points=points_price(get_cost(units.DeathwingKnights), *serg_gear),
                                               gear=[
                                                   UnitDescription('Knight master', options=create_gears(*serg_gear))
                                               ])
        self.terms = Count(self, name='Deathwing Knight', min_limit=4, max_limit=9, points=self.knights_points,
                           per_model=True, gear=UnitDescription('Deathwing Knight', points=self.knights_points,
                                                                options=create_gears(*self.knight_gear)))
        self.watcher = DeathwingTerminatorSquad.SquadOptions(self)

    def get_count(self):
        return self.terms.cur + 1

    def build_power(self):
        return 12 * (1 + (self.terms.cur > 4))


class RWAncient(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingAncient)
    type_id = 'rwing_ancient_v1'
    faction = ['Ravenwing']
    kwname = 'Ancient'
    keywords = ['Character', 'Biker']
    power = 6

    class Weapons(OneOf):
        def __init__(self, parent):
            super(RWAncient.Weapons, self).__init__(parent, 'Bike weapon')
            self.variant(*ranged.PlasmaTalon)
            self.variant(*ranged.RavenwingGrenadeLauncher)

    def __init__(self, parent):
        gear = [ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.RavenwingAncient), *gear)
        super(RWAncient, self).__init__(parent, points=cost, unique=True, gear=create_gears(*gear))
        self.Weapons(self)


class RWApothecary(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingApothecary)
    type_id = 'rwing_apothecary_v1'
    faction = ['Ravenwing']
    kwname = 'Apothecary'
    keywords = ['Character', 'Biker']
    power = 5

    class Weapons(OneOf):
        def __init__(self, parent):
            super(RWApothecary.Weapons, self).__init__(parent, 'Bike weapon')
            self.variant(*ranged.PlasmaTalon)
            self.variant(*ranged.RavenwingGrenadeLauncher)

    def __init__(self, parent):
        gear = [ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.RavenwingApothecary), *gear)
        super(RWApothecary, self).__init__(parent, points=cost, gear=create_gears(*gear))
        RWApothecary.Weapons(self)


class RWChampion(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingChampion)
    type_id = 'rwing_champion_v1'
    faction = ['Ravenwing']
    keywords = ['Character', 'Biker']
    power = 6

    def __init__(self, parent):
        gear = [melee.BladeOfCaliban, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades, ranged.PlasmaTalon]
        cost = points_price(get_cost(units.RavenwingChampion), *gear)
        super(RWChampion, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True, unique=True)


class DAPrimarisApothecary(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.PrimarisApothecary)
    type_id = 'da_primaris_apothecary_v1'
    kwname = 'Apothecary'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 4

    def __init__(self, parent):
        gear = [ranged.ReductorPistol, ranged.AbsolvorBoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.PrimarisApothecary), *gear)
        super(DAPrimarisApothecary, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                   static=True)


class DAApothecary(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.Apothecary)
    type_id = 'da_apothecary_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(DAApothecary, self).__init__(parent, points=get_cost(units.Apothecary),
                                           gear=create_gears(ranged.FragGrenades, ranged.KrakGrenades,
                                                           ranged.BoltPistol, melee.Chainsword), static=True)


class DACodexCompanyAncient(ElitesUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.CompanyAncient)
    type_id = 'da_company_ancient_v2'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DACodexCompanyAncient.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            self.variant(ranged.Boltgun)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(DACodexCompanyAncient, self).__init__(parent, get_name(units.CompanyAncient),
                                                    points=get_cost(units.CompanyAncient),
                                                    gear=create_gears(ranged.KrakGrenades, ranged.FragGrenades))
        self.Weapon1(self)


class DAPrimarisAncient(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.PrimarisAncient)
    type_id = 'da_primaris_ancient_v1'

    keywords = ['Character', 'Infantry', 'Primaris']
    power = 4

    def __init__(self, parent):
        gear = [ranged.BoltRifle, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.PrimarisAncient), *gear)
        super(DAPrimarisAncient, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                static=True)


class DAChapterAncient(ElitesUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.ChapterAncient)
    type_id = 'da_chapter_ancient_v1'
    keywords = ['Character', 'Infantry', 'Ancient']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DAChapterAncient.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(DAChapterAncient, self).__init__(parent, get_name(units.ChapterAncient),
                                               points=get_cost(units.ChapterAncient),
                                               gear=create_gears(ranged.KrakGrenades, ranged.FragGrenades))
        self.Weapon1(self)


class DACataphractiiTerminatorSquad(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingCataphractiiTerminatorSquad)
    type_id = 'da_cataphractii_terminator_squad_v1'
    keywords = ['Infantry', 'Terminator']
    faction = ['Deathwing']
    power = 12

    model_points = get_cost(units.DeathwingCataphractiiTerminatorSquad)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = 'Cataphractii Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DACataphractiiTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.claw = self.variant(*melee.SingleLightningClaw)
                self.chain_fist = self.variant(*melee.Chainfist)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DACataphractiiTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.claw = self.variant(*melee.SingleLightningClaw)
                self.hf = self.variant(*ranged.HeavyFlamer)

            def has_spec(self):
                return self.cur == self.hf

        def __init__(self, parent):
            super(DACataphractiiTerminatorSquad.Veteran, self).__init__(
                parent=parent, points=DACataphractiiTerminatorSquad.model_points
            )

            self.wep1 = self.LeftWeapon(self)
            self.wep2 = self.RightWeapon(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep2.has_spec()

    class Sergeant(armory.ClawUser):
        type_name = 'Cataphractii Sergeant'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DACataphractiiTerminatorSquad.Sergeant.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.sword = self.variant(*melee.PowerSword)
                self.power_fist = self.variant(*melee.PowerFist)
                self.claw = self.variant(*melee.SingleLightningClaw)
                self.chain_fist = self.variant(*melee.Chainfist)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DACataphractiiTerminatorSquad.Sergeant.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.claw = self.variant(*melee.SingleLightningClaw)

            def has_claw(self):
                return self.cur == self.claw

        def __init__(self, parent):
            super(DACataphractiiTerminatorSquad.Sergeant, self).__init__(
                parent=parent, points=DACataphractiiTerminatorSquad.model_points
            )

            self.wep1 = self.LeftWeapon(self)
            self.wep2 = self.RightWeapon(self)
            self.opt = self.Options(self)

        class Options(OptionsList):
            def __init__(self, parent):
                super(DACataphractiiTerminatorSquad.Sergeant.Options, self).__init__(parent=parent, name='Options',
                                                                                     limit=None)
                self.harness = self.variant

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(DACataphractiiTerminatorSquad, self).__init__(parent)
        self.leader = SubUnit(self, self.get_leader()(self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Cataphractii Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def build_power(self):
        return self.power * (1 + (self.terms.count > 4))


class DATartarosTerminatorSquad(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.DeathwingTartarosTerminatorSquad)
    type_id = 'da_tartaros_terminator_squad_v1'
    keywords = ['Infantry', 'Terminator']
    faction = ['Deathwing']

    model_points = get_cost(units.DeathwingTartarosTerminatorSquad)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(DATartarosTerminatorSquad.Harness, self).__init__(parent, '')
            self.variant(*ranged.GrenadeHarness)

    class Veteran(ListSubUnit):
        type_name = 'Tartaros Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DATartarosTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.claws = self.variant('Two lightning claws', get_costs(melee.PairLightningClaws),
                                          gear=[Gear('Lightning claw', count=2)])
                self.chain_fist = self.variant(*melee.Chainfist)

            def has_claws(self):
                return self.cur == self.claws

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DATartarosTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.hf = self.variant(*ranged.HeavyFlamer)
                self.variant(*ranged.ReaperAutocannon)

            def has_spec(self):
                return self.used and self.cur != self.bolt

        def __init__(self, parent):
            super(DATartarosTerminatorSquad.Veteran, self).__init__(
                parent=parent, points=DATartarosTerminatorSquad.model_points
            )
            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.grenade = DATartarosTerminatorSquad.Harness(self)

        def check_rules(self):
            super(DATartarosTerminatorSquad.Veteran, self).check_rules()
            self.right_weapon.used = self.right_weapon.visible = not self.left_weapon.has_claws()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_harness(self):
            return self.grenade.any

    class Sergeant(Unit):
        type_name = 'Tartaros Sergeant'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DATartarosTerminatorSquad.Sergeant.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.sword = self.variant(*melee.PowerSword)
                self.power_fist = self.variant(*melee.PowerFist)
                self.claws = self.variant('Two lightning claws', get_costs(melee.PairLightningClaws),
                                          gear=[Gear('Lightning claw', count=2)])
                self.chain_fist = self.variant(*melee.Chainfist)

            def has_claws(self):
                return self.cur == self.claws

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DATartarosTerminatorSquad.Sergeant.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.variant(*ranged.PlasmaBlaster)
                self.variant(*ranged.VolkiteCharger)

        def __init__(self, parent):
            super(DATartarosTerminatorSquad.Sergeant, self).__init__(
                parent=parent, points=DATartarosTerminatorSquad.model_points
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.grenade = DATartarosTerminatorSquad.Harness(self)

        def check_rules(self):
            super(DATartarosTerminatorSquad.Sergeant, self).check_rules()
            self.right_weapon.used = self.right_weapon.visible = not self.left_weapon.has_claws()

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(DATartarosTerminatorSquad, self).__init__(parent)
        self.leader = SubUnit(self, self.get_leader()(self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        super(DATartarosTerminatorSquad, self).check_rules()
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Tartaros Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))
        gren = sum(c.has_harness() for c in self.terms.units) + self.leader.unit.grenade.any
        if gren > spec_limit:
            self.error('Tartaros Terminators may take only {0} grenade harnesses (taken {1})'.format(spec_limit, gren))

    def build_power(self):
        return 13 * (1 + (self.terms.count > 4))


class DACodexDreadnought(ElitesUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.Dreadnought)
    type_id = 'da_dreadnought_v1'
    keywords = ['Vehicle']
    power = 7

    model_points = get_cost(units.Dreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(DACodexDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DACodexDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DACodexDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtCombatWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

    def __init__(self, parent):
        super(DACodexDreadnought, self).__init__(parent=parent, name=get_name(units.Dreadnought),
                                                 points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(DACodexDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class DACodexVenDreadnought(ElitesUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.VenerableDreadnought)
    type_id = 'da_ven_dreadnought_v2'
    keywords = ['Vehicle']
    power = 8

    model_points = get_cost(units.VenerableDreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(DACodexVenDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DACodexVenDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DACodexVenDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtCombatWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

    def __init__(self, parent):
        super(DACodexVenDreadnought, self).__init__(parent=parent,
                                                    name=get_name(units.VenerableDreadnought),
                                                    points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(DACodexVenDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class DAContDreadnought(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.ContemptorDreadnought)
    type_id = 'da_contemptor_dreadnought_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DAContDreadnought.Weapon, self).__init__(parent=parent, name='Weapon')
            self.melta = self.variant(*ranged.MultiMelta)
            self.cannon = self.variant(*ranged.KheresPatternAssaultCannon)

    def __init__(self, parent):
        gear = [ranged.CombiBolter, melee.DreadnoughtCombatWeapon]
        cost = points_price(get_cost(units.ContemptorDreadnought), *gear)
        super(DAContDreadnought, self).__init__(parent=parent, points=cost,
                                                gear=create_gears(*gear))
        self.wep = self.Weapon(self)


class DARedemptorDreadnought(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.RedemptorDreadnought)
    type_id = 'da_redemptor_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 10

    gears = [ranged.IcarusRocketPod, melee.RedemptorFist]

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DARedemptorDreadnought.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DARedemptorDreadnought.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.MacroPlasmaIncinerator)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(DARedemptorDreadnought.Weapon3, self).__init__(parent, 'Secondary weapon')
            self.variant('Two fragstorm grenade launchers', 2 * get_cost(ranged.FragstormGrenadeLauncher),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))
            self.variant('Two storm bolters', 2 * get_cost(ranged.StormBolter),
                         gear=create_gears(*[ranged.StormBolter] * 2))

    def __init__(self, parent):
        cost = points_price(get_cost(units.RedemptorDreadnought), *self.gears)
        gear = create_gears(*self.gears)
        super(DARedemptorDreadnought, self).__init__(parent, points=cost, gear=gear)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)


class DAAgressors(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.AggressorSquad)
    type_id = 'da_agressors_v1'

    keywords = ['Infantry', 'Primaris', 'Mk X gravis']

    power = 6

    class Weapons(OneOf):
        def __init__(self, parent):
            super(DAAgressors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Auto boltstorm gauntlets and fragstorm grenade launcher',
                         get_costs(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher),
                         gear=create_gears(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher))
            self.variant(*ranged.FlamestormGauntlets)

        @property
        def description(self):
            return [UnitDescription('Aggressor Sergeant').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Aggressor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(DAAgressors, self).__init__(parent, points=get_cost(units.AggressorSquad))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Agressors', 2, 5, points=get_cost(units.AggressorSquad),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(DAAgressors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class DAReivers(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.ReiverSquad)
    type_id = 'da_reivers_v1'
    keywords = ['Infantry', 'Primaris']

    power = 5

    model_gear = [ranged.HeavyBoltPistol, ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.ShockGrenades]

    class Wargear(OptionsList):
        def __init__(self, parent):
            super(DAReivers.Wargear, self).__init__(parent, 'Squad wargear', used=False)
            self.variant(*wargear.GravChute, per_model=True)
            self.variant(*wargear.GrapnelLauncher, per_model=True)

    class SergeantKnife(OneOf):
        def __init__(self, parent):
            super(DAReivers.SergeantKnife, self).__init__(parent, 'Sergeant weapons')
            self.variant('Bolt carbine and heavy bolt pistol',
                         points=get_costs(ranged.BoltCarbine, ranged.HeavyBoltPistol),
                         gear=create_gears(ranged.BoltCarbine, ranged.HeavyBoltPistol))
            self.variant('Bolt carbine and combat knife',
                         points=get_costs(ranged.BoltCarbine, melee.CombatKnife),
                         gear=create_gears(ranged.BoltCarbine, melee.CombatKnife))
            self.variant('Heavy bolt pistol',
                         points=get_costs(ranged.HeavyBoltPistol, melee.CombatKnife),
                         gear=create_gears(ranged.HeavyBoltPistol, melee.CombatKnife))

        @property
        def description(self):
            return [UnitDescription('Reiver Sergeant', options=create_gears(*DAReivers.model_gear[1:])). \
                        add(super(DAReivers.SergeantKnife, self).description).add(self.parent.wgear.description)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Reiver', options=create_gears(*DAReivers.model_gear)).
                        add(self.parent.wep.cur.gear).
                        add(self.parent.wgear.description).set_count(self.cur)]

    class SquadWeapon(OneOf):
        def __init__(self, parent):
            super(DAReivers.SquadWeapon, self).__init__(parent, 'Squad weapons', used=False)
            self.variant(*ranged.BoltCarbine)
            self.variant(*melee.CombatKnife)

    def __init__(self, parent):
        sarge_cost = points_price(get_cost(units.ReiverSquad), *DAReivers.model_gear[1:])
        super(DAReivers, self).__init__(parent, points=sarge_cost)
        self.wgear = self.Wargear(self)
        self.wep = self.SquadWeapon(self)
        self.sarge = self.SergeantKnife(self)
        self.models = self.Marines(self, 'Reivers', 4, 9, sarge_cost + get_cost(DAReivers.model_gear[0]),
                                   per_model=True)

    def get_count(self):
        return self.models.cur + 1

    def build_points(self):
        return super(DAReivers, self).build_points() + \
               (self.wgear.points + self.wep.points) * (1 + self.models.cur)

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class DAServitors(ElitesUnit, armory.DAUnit):
    type_name = get_name(units.Servitors)
    type_id = 'da_servitors_v1'
    keywords = ['Infantry']

    power = 3
    model = UnitDescription('Servitor')

    class Servitor(Count):
        @property
        def description(self):
            return DAServitors.model.clone().add(create_gears(melee.ServoArm)). \
                set_count(self.cur - sum(c.cur for c in self.parent.wep.counts))

    class SpecialWeapons(object):
        def variant(self, name, point=None):
            self.counts.append(Count(
                self.parent, name, 0, 4, point, gear=DAServitors.model.clone().add(Gear(name))))

        def __init__(self, parent):
            self.parent = parent
            self.counts = []
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.PlasmaCannon)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        super(DAServitors, self).__init__(parent)
        self.squad = self.Servitor(self, 'Servitor', 4, 4,
                                   per_model=True, points=get_cost(units.Servitors))
        self.squad.visible = False
        self.wep = self.SpecialWeapons(self)

    def check_rules(self):
        super(DAServitors, self).check_rules()
        Count.norm_counts(0, 2, self.wep.counts)

    def build_points(self):
        return super(DAServitors, self).build_points() + \
               get_cost(melee.ServoArm) * (4 - sum(c.cur for c in self.wep.counts))
