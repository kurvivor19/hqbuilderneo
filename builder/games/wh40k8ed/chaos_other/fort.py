__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FortUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList
from  builder.games.wh40k8ed.heretic_astartes import units, ranged
from builder.games.wh40k8ed.utils import *


class ChaosBastion(FortUnit, Unit):
    type_name = 'Chaos Bastion'
    type_id = 'cbastion_v1'
    faction = ['Chaos']

    keywords = ['Building', 'Vehicle', 'Transport']

    power = 10

    class Emplacement(OptionsList):
        def __init__(self, parent):
            super(ChaosBastion.Emplacement, self).__init__(parent, 'Gun emplacement',
                                                           limit=1)
            self.variant('Icarus lascannon', 25)
            self.variant('Quad-gun', 30)

    def __init__(self, parent):
        super(ChaosBastion, self).__init__(parent, points=160 + 8 * 4,
                                           gear=Gear('Heavy bolter', count=4))
        self.Emplacement(self)


class NoctilithCrown(FortUnit, Unit):
    type_name = get_name(units.NoctilithCrown)
    type_id = 'noctilith_crown_v1'
    faction = ['Chaos']

    keywords = ['Building', 'Vehicle']

    power = 5

    def __init__(self, parent):
        super(NoctilithCrown, self).__init__(parent, points=get_cost(units.NoctilithCrown),
                                             gear=create_gears(ranged.LashingWarpEnergies), static=True)

