from .lords import KnightCrusader, KnightErrant, KnightGallant,\
     KnightPaladin, KnightWarden, ArmigerWarglaives, ArmigerHelverins,\
     KnightPreceptor, KnightCastellan, CanisRex, KnightValiant
from .ia_lords import KnightPorphyron, KnightAcheron, KnightAtropos,\
    KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix
from .fort import SacristanForgeshrine
from builder.games.wh40k8ed.rosters import DetachSuperHeavy


class KnightSuperHeavy(DetachSuperHeavy):
    def check_rules(self):
        super(KnightSuperHeavy, self).check_rules()
        tit_cnt = sum((ut.count * ('TITANIC' in ut.unit_class.keywords)
                       for ut in self.lords.types))
        if tit_cnt < 1:
            self.command = 0
        elif tit_cnt < 3:
            self.command = 3
        else:
            self.command = 6


ia_units = [KnightPorphyron, KnightAcheron, KnightAtropos,
            KnightCastigator, KnightLancer, KnightMagaera,
            KnightStyrix]

unit_types = [KnightCrusader, KnightErrant, KnightGallant,
              KnightPaladin, KnightWarden, ArmigerWarglaives, ArmigerHelverins,
              KnightPreceptor, KnightCastellan, CanisRex, KnightValiant,
              SacristanForgeshrine] + ia_units
