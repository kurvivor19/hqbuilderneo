from . import armory
from . import ia_melee
from . import ia_ranged
from . import ia_units
from builder.core2 import UnitList
from builder.games.wh40k8ed.options import OneOf, OptionsList, ListSubUnit
from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.utils import *


class KnightPorphyron(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.AcastusKnightPorphyrion) + ' (Imperial Armour)'
    type_id = 'kn_porphyron_v1'

    power = 40

    class CaparaceWeapons(OneOf):
        def __init__(self, parent):
            super(KnightPorphyron.CaparaceWeapons, self).__init__(parent, 'Caparace Weapon')
            self.variant(*ia_ranged.IronstormMissilePod)
            self.variant(*ia_ranged.HeliosDefenceMissiles)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(KnightPorphyron.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ia_ranged.Autocannon)
            self.variant(*ia_ranged.Lascannon)

    def __init__(self, parent):
        gear = [ia_ranged.TwinMagnaLascannon, ia_ranged.TwinMagnaLascannon,
                ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.AcastusKnightPorphyrion), *gear)
        super(KnightPorphyron, self).__init__(parent, get_name(ia_units.AcastusKnightPorphyrion), points=cost,
                                              gear=create_gears(*gear))
        self.Weapon(self)
        self.Weapon(self)
        self.CaparaceWeapons(self)


class KnightAcheron(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.CerastusKnightAcheron) + ' (Imperial Armour)'
    type_id = 'kn_acheron_v1'

    power = 25

    def __init__(self, parent):
        gear = [ia_ranged.AcheronFlameCannon, ia_melee.ReaperChainfist,
                ia_ranged.TwinHeavyBolter, ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.CerastusKnightAcheron), *gear)
        super(KnightAcheron, self).__init__(parent, get_name(ia_units.CerastusKnightAcheron), points=cost,
                                            gear=create_gears(*gear), static=True)


class KnightAtropos(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.CerastusKnightAtropos) + ' (Imperial Armour)'
    type_id = 'kn_atropos_v1'

    power = 29

    def __init__(self, parent):
        gear = [ia_ranged.AtroposLascutter,
                ia_ranged.GravitonSingularityCannon, ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.CerastusKnightAtropos), *gear)
        super(KnightAtropos, self).__init__(parent, get_name(ia_units.CerastusKnightAtropos), points=cost,
                                            gear=create_gears(*gear), static=True)


class KnightCastigator(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.CerastusKnightCastigator) + ' (Imperial Armour)'
    type_id = 'kn_castigator_v1'

    power = 25

    def __init__(self, parent):
        gear = [ia_ranged.CastigatorBoltCannon, ia_melee.TempestWarblade,
                ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.CerastusKnightCastigator), *gear)
        super(KnightCastigator, self).__init__(parent, get_name(ia_units.CerastusKnightCastigator), points=cost,
                                               gear=create_gears(*gear), static=True)


class KnightLancer(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.CerastusKnightLancer) + ' (Imperial Armour)'
    type_id = 'kn_lancer_v1'

    power = 24

    def __init__(self, parent):
        gear = [ia_melee.CerastusShockLance, ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.CerastusKnightLancer), *gear)
        super(KnightLancer, self).__init__(parent, get_name(ia_units.CerastusKnightLancer), points=cost,
                                            gear=create_gears(*gear), static=True)


class KnightMagaera(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.QuestorisKnightMagaera) + ' (Imperial Armour)'
    type_id = 'kn_magaera_v1'

    power = 28

    class Weapon(OneOf):
        def __init__(self, parent):
            super(KnightMagaera.Weapon, self).__init__(parent, 'Melee weapon')
            self.sword = self.variant(*ia_melee.ReaperChainsword)
            self.variant(join_and(ia_melee.HekatonSiegeClaw, ia_ranged.TwinRadCleanser),
                         get_costs(ia_melee.HekatonSiegeClaw, ia_ranged.TwinRadCleanser),
                         gear=create_gears(ia_melee.HekatonSiegeClaw, ia_ranged.TwinRadCleanser))

    def __init__(self, parent):
        gear = [ia_ranged.LightningCannon, ia_ranged.PhasedPlasmaFusil,
                ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.QuestorisKnightMagaera), *gear)
        super(KnightMagaera, self).__init__(parent, get_name(ia_units.QuestorisKnightMagaera), points=cost,
                                            gear=create_gears(*gear))
        self.wep = self.Weapon(self)

    def build_power(self):
        return self.power + (self.wep.cur != self.wep.sword)


class KnightStyrix(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(ia_units.QuestorisKnightStyrix) + ' (Imperial Armour)'
    type_id = 'kn_styrix_v1'

    power = 24
    def __init__(self, parent):
        gear = [ia_ranged.VolkiteChieorovile, ia_ranged.GravitonCrusher,
                ia_melee.TitanicFeet]
        cost = points_price(get_cost(ia_units.QuestorisKnightStyrix), *gear)
        super(KnightStyrix, self).__init__(parent, get_name(ia_units.QuestorisKnightStyrix), points=cost,
                                           gear=create_gears(*gear))
        self.wep = KnightMagaera.Weapon(self)

    def build_power(self):
        return self.power + (self.wep.cur != self.wep.sword)
