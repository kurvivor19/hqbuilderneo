from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from . import armory, units, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Rhino(TransportUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Rhino)
    type_id = 'rhino_aa_v1'

    keywords = ['Vehicle', 'Transport']
    power = 5

    @classmethod
    def calc_faction(cls):
        return super(Rhino, cls).calc_faction() + ['DEATHWATCH']

    class Options(OptionsList):
        def __init__(self, parent, blade=False):
            super(Rhino.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKiller)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        gear = [ranged.StormBolter]
        super(Rhino, self).__init__(
            parent, gear=armory.create_gears(*gear),
            points=armory.points_price(armory.get_cost(units.Rhino), *gear))
        self.Options(self)


class Razorback(TransportUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Razorback) + ' (Index)' 
    type_id = 'razorback_v1'

    keywords = ['Vehicle', 'Transport']
    power = 5
    obsolete = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)
            self.variant(*ranged.TwinAssault)
            self.variant('Twin Heavy Flamer', 34)
            self.variant('Lascannon and Twin Plasma Gun', 25 + 20,
                         gear=[Gear('Lascannon'), Gear('Twin plasma gun')])

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent,
                                        points=65)
        self.Weapon(self)
        Rhino.Options(self)


class CodexRazorback(TransportUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Razorback)
    type_id = 'razorback_v2'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CodexRazorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)
            self.variant(*ranged.TwinAssault)

    def __init__(self, parent):
        super(CodexRazorback, self).__init__(parent=parent,
                                             points=armory.get_cost(units.Razorback))
        self.Weapon(self)
        Rhino.Options(self)


class DropPod(TransportUnit, armory.CommonSMUnit):
    type_id = 'drop_pod_v1'
    type_name = armory.get_name(units.DropPod) + ' (Index)'

    keywords = ['Vehicle', 'Transport']
    power = 5
    obsolete = True

    @classmethod
    def calc_faction(cls):
        return super(DropPod, cls).calc_faction() + ['DEATHWATCH']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant(*ranged.StormBolter)
            self.dwind = self.variant(*ranged.DeathwindLauncher)

    def __init__(self, parent):
        super(DropPod, self).__init__(parent=parent, name=armory.get_name(units.DropPod),
                                      points=103)
        self.weapon = DropPod.Weapon(self)


class CodexDropPod(TransportUnit, armory.CommonSMUnit):
    type_id = 'drop_pod_v2'
    type_name = armory.get_name(units.DropPod)

    keywords = ['Vehicle', 'Transport']
    power = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CodexDropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant(*ranged.StormBolter)
            self.dwind = self.variant(*ranged.DeathwindLauncher)

    def __init__(self, parent):
        super(CodexDropPod, self).__init__(parent=parent, name=armory.get_name(units.DropPod),
                                      points=armory.get_cost(units.DropPod))
        self.Weapon(self)


class LandSpeederStorm(TransportUnit, armory.SMUnit):
    type_name = armory.get_name(units.LandSpeederStorm)
    model_points = armory.get_cost(units.LandSpeederStorm)
    type_id = 'land_speeder_storm_v1'
    keywords = ['Vehicle', 'Transport', 'Land Speeder', 'Scout']
    power = 3

    def __init__(self, parent):
        gear = [ranged.CerberusLauncher, ranged.HeavyBolter]
        super(LandSpeederStorm, self).__init__(
            parent=parent, points=armory.points_price(self.model_points, *gear),
            gear=armory.create_gears(*gear), static=True
        )
        # self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LandSpeederStorm.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.multimelta = self.variant(*ranged.MultiMelta)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.assaultcannon = self.variant(*ranged.AssaultCannon)


class Repulsor(TransportUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Repulsor)
    type_id = 'repulsor_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 15

    @classmethod
    def calc_faction(cls):
        return super(Repulsor, cls).calc_faction() + ['DEATHWATCH']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Repulsor.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Repulsor.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.LasTalon)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(Repulsor.Weapon3, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon4(OneOf):
        def __init__(self, parent):
            super(Repulsor.Weapon4, self).__init__(parent, '')
            self.variant('Two stormbolters', points=armory.get_costs(*[ranged.StormBolter] * 2),
                         gear=armory.create_gears(*[ranged.StormBolter] * 2))
            self.variant('Two fragstorm grenade launchers', points=armory.get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=armory.create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon5(OneOf):
        def __init__(self, parent):
            super(Repulsor.Weapon5, self).__init__(parent, 'AA turret')
            self.variant(*ranged.IcarusIronhailHeavyStubber)
            self.variant(*ranged.IcarusRocketPod)
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.FragstormGrenadeLauncher)

    class Weapon6(OneOf):
        def __init__(self, parent):
            super(Repulsor.Weapon6, self).__init__(parent, 'Launchers')
            self.variant(*wargear.AutoLaunchers)
            self.variant('Two fragstorm grenade launchers', points=armory.get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=armory.create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon7(OptionsList):
        def __init__(self, parent):
            super(Repulsor.Weapon7, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)

    def __init__(self, parent):
        gear = [ranged.KrakstormGrenadeLauncher] * 2
        super(Repulsor, self).__init__(parent, points=armory.points_price(armory.get_cost(units.Repulsor), *gear),
                                       gear=armory.create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Weapon7(self)
        self.Weapon4(self)
        self.Weapon5(self)
        self.Weapon6(self)


class Impulsor(TransportUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Impulsor)
    type_id = 'impulsor_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Impulsor.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Two storm bolters', get_costs(ranged.StormBolter, ranged.StormBolter),
                         gear=create_gears(ranged.StormBolter, ranged.StormBolter))
            self.variant('Two fragstorm grenade launchers',
                         get_costs(ranged.FragstormGrenadeLauncher, ranged.FragstormGrenadeLauncher),
                         gear=create_gears(ranged.FragstormGrenadeLauncher, ranged.FragstormGrenadeLauncher))

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(Impulsor.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Impulsor.Options, self).__init__(parent, 'Options', limit=1)
            self.variant(*wargear.ShieldDome)
            self.variant(*wargear.OrbitalCommsArray)
            self.variant(*ranged.BellicatusMissileArray)
            self.variant(*ranged.IronhailSkytalonArray)

    def __init__(self, parent):
        super(Impulsor, self).__init__(parent, points=get_cost(units.Impulsor))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)
