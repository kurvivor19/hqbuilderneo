from .lords import WarDog, KnightDespoiler, KnightDesecrator, KnightRampager, KnightTyrant
from builder.games.wh40k8ed.rosters import DetachSuperHeavy


class KnightSuperHeavy(DetachSuperHeavy):
    def check_rules(self):
        super(KnightSuperHeavy, self).check_rules()
        tit_cnt = sum((ut.count * ('TITANIC' in ut.unit_class.keywords)
                       for ut in self.lords.types))
        if tit_cnt < 1:
            self.command = 0
        elif tit_cnt < 3:
            self.command = 3
        else:
            self.command = 6


unit_types = [WarDog, KnightDespoiler, KnightDesecrator, KnightRampager, KnightTyrant]
