__author__ = 'Truskov Ivan'

from builder.core2 import Unit as CoreUnit, ListSubUnit as CoreListSubUnit
from itertools import chain


class MetaUnit(type):
    def __init__(cls, name, bases, cldict):
        super(MetaUnit, cls).__init__(name, bases, cldict)
        # keywords are uppercase
        cls.keywords = [kw.upper() for kw in cls.keywords]
        cls.faction = [kw.upper() for kw in cls.faction]
        # simplified keywords agregation during inheritance
        for base in bases:
            if hasattr(base, 'keywords'):
                for kw in base.keywords:
                    if kw not in cls.keywords:
                        cls.keywords.append(kw)
            if hasattr(base, 'faction') and base.faction is not None:
                for fac in base.faction:
                    if fac not in cls.faction:
                        cls.faction.append(fac)
        for sub_cls in chain([cls], bases):
            if hasattr(sub_cls, 'faction_substitution'):
                for new_fac, old_fac in sub_cls.faction_substitution.items():
                    if old_fac.upper() in cls.faction:
                        cls.faction.remove(old_fac.upper())
                    if new_fac.upper() not in cls.faction:
                        cls.faction.append(new_fac.upper())
        # probably not needed?
        if 'kwname' in cldict:
            cls.keywords.append(cls.kwname.upper())
        elif cls.type_name:
            cls.keywords.append(cls.type_name.upper())


class Unit(CoreUnit, metaclass=MetaUnit):
    roles = []
    keywords = []
    faction = []

    wiki_faction = ''
    obsolete = False
    always_common = False

    def lookup_faction(self):
        return '8ed_' + self.wiki_faction

    def lookup_name(self):
        res = self._name
        if res.startswith(self.wiki_faction):
            res = res[len(self.wiki_faction):]
        if res.find('(') > 0:
            res = res[:res.find('(')]
        return res.strip()

    # def lookup_wikilink(self):
    #     pass

    use_power = True
    power = 0

    def __init__(self, parent, *args, **kwargs):
        super(Unit, self).__init__(parent, *args, **kwargs)
        self.use_power = parent.root.use_power

    @classmethod
    def check_faction(cls, fac):
        return fac in cls.faction or fac in cls.calc_faction()

    @classmethod
    def calc_faction(cls):
        return []

    def build_power(self):
        return self.power

    def check_rules_chain(self):
        super(Unit, self).check_rules_chain()
        if self.use_power:
            self.points = self.build_power()
            self.description = self.build_description().dump()

    def build_statistics(self):
        return {'Units': 1,
                'Models': self.get_count()}


class ListSubUnit(CoreListSubUnit):
    use_power = True
    power = 0

    def __init__(self, parent, *args, **kwargs):
        super(ListSubUnit, self).__init__(parent, *args, **kwargs)
        self.use_power = parent.root.use_power

    def lookup_wikilink(self):
        pass

    def build_power(self):
        return self.power


class WarlordUnit(CoreUnit):
    starrable = True

    def update(self, data):
        super(WarlordUnit, self).update(data)
        if 'star' in data and self.star:
            self.root.known_warlord = self

    def check_rules(self):
        super(WarlordUnit, self).check_rules()
        if self.star == 2 and self.root.known_warlord is not None:
            self.star = 1 + (self == self.root.known_warlord)

    @property
    def name(self):
        name = super(WarlordUnit, self).name
        if self.star == 2:
            name = name + ' - Warlord'
        return name


class ElitesUnit(WarlordUnit):
    roles = ['elite']


class FastUnit(WarlordUnit):
    roles = ['fast']


class TroopsUnit(WarlordUnit):
    roles = ['troops']


class FlierUnit(WarlordUnit):
    roles = ['fliers']


class HeavyUnit(WarlordUnit):
    roles = ['heavy']


class LordsUnit(WarlordUnit):
    roles = ['lords']


class HqUnit(WarlordUnit):
    roles = ['hq']


class TransportUnit(WarlordUnit):
    roles = ['transports']


class FortUnit(WarlordUnit):
    roles = ['fort']
