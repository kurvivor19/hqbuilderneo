__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, UnitDescription, OptionsList
from . import armory
from . import units
from . import wargear
from . import ranged
from . import melee
from builder.games.wh40k8ed.utils import *


class Valoris(HqUnit, armory.CustodesUnit):
    type_name = get_name(units.TrajanValoris)
    type_id = 'valoris_v1'

    keywords = ['Character', 'Infantry']
    power = 10

    def __init__(self, parent):
        super(Valoris, self).__init__(parent, points=get_cost(units.TrajanValoris),
                                      gear=[
                                          Gear("Watcher's axe"), Gear('Misericordia')
        ], unique=True, static=True)


class ShieldCaptain(HqUnit, armory.CustodesUnit):
    type_name = get_name(units.ShieldCaptain)
    type_id = 'shield_captain_v1'

    keywords = ['Character', 'Infantry']
    power = 7

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ShieldCaptain.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.GuardianSpear)
            self.blade = self.variant(*ranged.SentinelBlade)
            self.variant(*ranged.CastellanAxe)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ShieldCaptain.Options, self).__init__(parent, 'Options')
            self.variant(*melee.Misericordia)
            self.shield = self.variant(*wargear.StormShieldChar)

        def check_rules(self):
            super(ShieldCaptain.Options, self).check_rules()
            self.shield.used = self.shield.visible = self.parent.wep.cur == self.parent.wep.blade

    def __init__(self, parent):
        super(ShieldCaptain, self).__init__(parent, points=get_cost(units.ShieldCaptain))
        self.wep = self.Weapon(self)
        self.Options(self)


class AllarusShieldCaptain(HqUnit, armory.CustodesUnit):
    type_name = get_name(units.AllarusShieldCaptain)
    type_id = 'allarus_shield_captain_v1'
    kwname = 'Shield-Captain'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(AllarusShieldCaptain.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.GuardianSpear)
            self.variant(*ranged.CastellanAxe)

    def __init__(self, parent):
        super(AllarusShieldCaptain, self).__init__(
            parent, points=points_price(get_cost(units.AllarusShieldCaptain), ranged.BalistusGrenadeLauncher),
            gear=create_gears(ranged.BalistusGrenadeLauncher))
        self.wep = self.Weapon(self)
        armory.Misericordia(self)


class BikeShieldCaptain(HqUnit, armory.CustodesUnit):
    type_name = get_name(units.BikeShieldCaptain)
    type_id = 'bike_shield_captain_v1'
    kwname = 'Shield-Captain'
    keywords = ['Character', 'Biker', 'Fly']
    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(BikeShieldCaptain.Weapon, self).__init__(parent, 'Bike Weapon')
            self.variant(*ranged.HurricaneBolter)
            self.variant(*ranged.SalvoLauncher)

    def __init__(self, parent):
        super(BikeShieldCaptain, self).__init__(
            parent, points=points_price(get_cost(units.BikeShieldCaptain), melee.InterceptorLance),
            gear=create_gears(melee.InterceptorLance))
        self.wep = self.Weapon(self)
        armory.Misericordia(self)
