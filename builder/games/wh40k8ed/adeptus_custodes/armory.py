__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList
from . import melee


class CustodesUnit(Unit):
    faction = ['Imperium', 'Adeptus Custodes']
    wiki_faction = 'Adeptus Custodes'


class Misericordia(OptionsList):
    def __init__(self, parent):
        super(Misericordia, self).__init__(parent, 'Options')
        self.variant(*melee.Misericordia)
