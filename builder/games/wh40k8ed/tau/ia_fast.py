__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit,\
    OptionalSubUnit
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name,\
    create_gears
from . import armory
from . import ranged
from . import ia_units
from . import ia_ranged
from . import ia_wargear
from . import units


class Yvahra(FastUnit, armory.SeptUnit):
    type_name = get_name(ia_units.YVahraBattlesuit) + ' (Imperial Armour)'
    type_id = 'xv109yvahra_v1'
    keywords = ['Battlesuit', 'Monster', 'Jetpack', 'Fly']
    power = 20

    class SupportSystem(OptionsList):
        def __init__(self, parent, slots=1):
            super(Yvahra.SupportSystem, self).__init__(parent=parent, name='Support system', limit=slots)
            self.variant(*ia_wargear.CounterfireDefenceSystem)
            self.variant(*ia_wargear.DroneController)
            self.variant(*ia_wargear.MultiTrackerYVahra)
            self.variant(*ia_wargear.AdvancedTargetingSystem)
            self.variant(*ia_wargear.TargetLockYVahra)
            self.variant(*ia_wargear.VelocityTrackerYVahra)
            self.variant(*ia_wargear.ShieldGeneratorYVahra)
            self.variant(*ia_wargear.StimulantInjector)

    def __init__(self, parent):
        gear = [ia_ranged.IonicDischargeCannon, ia_ranged.PhasedPlasmaFlamer, ia_ranged.FletchettePod]
        super(Yvahra, self).__init__(parent, name=get_name(ia_units.YVahraBattlesuit),
                                     points=points_price(get_cost(ia_units.YVahraBattlesuit), *gear),
                                     gear=create_gears(*gear))
        self.supp = self.SupportSystem(self, slots=2)
        self.drones = [Count(self, get_name(units.MV84ShieldedMissileDrone), min_limit=0, max_limit=2,
                             points=points_price(get_cost(units.MV84ShieldedMissileDrone), ranged.MissilePod),
                             per_model=True, gear=UnitDescription(get_name(units.MV84ShieldedMissileDrone),
                                                                  options=[Gear('Missile pod')])),
                       Count(self, get_name(units.MV52ShieldDrone), min_limit=0, max_limit=2,
                             points=get_cost(units.MV52ShieldDrone),
                             per_model=True, gear=UnitDescription(get_name(units.MV52ShieldDrone)))]

    def build_power(self):
        return self.power + 2 * (self.drones[0].cur > 0 or self.drones[1].cur > 0)


class Tetras(FastUnit, armory.SeptUnit):
    type_name = 'Tetra Scout Speeder Team' + ' (Imperial Armour)'
    type_id = 'tetras_v1'
    keywords = ['Vehicle', 'Fly']
    power = 4

    def __init__(self, parent):
        super(Tetras, self).__init__(parent, 'Tetra Scout Speeder Team')
        gear = [ia_ranged.PulseRifle, ia_ranged.PulseRifle, ia_ranged.Markerlight]
        desc = UnitDescription(get_name(ia_units.TetraScoutSpeeder), options=create_gears(*gear))
        cost = points_price(get_cost(ia_units.TetraScoutSpeeder), *gear)
        self.models = Count(self, 'Tetra Scout Speeders', 2, 4, cost, per_model=True, gear=desc.clone())

    def build_power(self):
        return self.power + 3 * (self.models.cur - 2)

    def get_count(self):
        return self.models.cur


class PiranhaTX42Team(FastUnit, armory.SeptUnit):
    type_name = get_name(ia_units.PiranhaTx42) + ' (Imperial Armour)'
    type_id = 'pirahna_tx42_v1'
    keywords = ['Vehicle', 'Fly']
    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PiranhaTX42Team.Weapon, self).__init__(parent, '')
            self.variant('Two fusion blasters', get_cost(ia_ranged.FusionBlaster) * 2,
                         gear=[Gear(get_name(ia_ranged.FusionBlaster), count=2)])
            self.variant('Two missile pods', get_cost(ia_ranged.MissilePod) * 2,
                         gear=[Gear(get_name(ia_ranged.MissilePod), count=2)])
            self.variant('Two plasma rifles',
                         get_cost(ia_ranged.PlasmaRifle) * 2,
                         gear=[Gear(get_name(ia_ranged.PlasmaRifle), count=2)])
            self.variant('Two rail rifles',
                         get_cost(ia_ranged.RailRifle) * 2,
                         gear=[Gear(get_name(ia_ranged.RailRifle), count=2)])

    class Pirahna(ListSubUnit):
        type_name = get_name(ia_units.PiranhaTx42)

        def __init__(self, parent):
            super(PiranhaTX42Team.Pirahna, self).__init__(parent, points=get_cost(ia_units.PiranhaTx42))
            PiranhaTX42Team.Weapon(self)

    def __init__(self, parent):
        super(PiranhaTX42Team, self).__init__(parent, get_name(ia_units.PiranhaTx42))
        self.models = UnitList(self, self.Pirahna, 1, 6)

    def build_power(self):
        return self.power + (self.get_count() - 1) * 5

    def get_count(self):
        return self.models.count
