__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit,\
    OptionalSubUnit
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name
from . import armory
from . import units
from . import ranged
from . import melee


class KroothoundSquad(FastUnit, armory.TauUnit):
    type_name = get_name(units.KrootHounds)
    type_id = 'kroothound_v1'
    faction = ['Kroot']
    keywords = ['Beasts']

    def __init__(self, parent):
        super(KroothoundSquad, self).__init__(parent=parent)
        gear = [melee.RippingFangs]
        cost = points_price(get_cost(units.KrootHounds), *gear)
        self.kroothound = Count(
            self, 'Kroot Hounds', min_limit=4, max_limit=12, points=cost,
            gear=UnitDescription('Kroot Hound', options=[Gear('Ripping fangs')])
        )

    def get_count(self):
        return self.kroothound.cur

    def build_power(self):
        return (self.kroothound.cur + 3) / 4


class Pathfinders(FastUnit, armory.SeptUnit):
    type_name = get_name(units.PathfinderTeam)
    type_id = 'pathfinderteam_v1'
    keywords = ['Infantry']

    model_points = get_cost(units.PathfinderTeam) + get_cost(ranged.Markerlight) + \
                   get_cost(ranged.PhotonGrenades)+ get_cost(ranged.PulseCarbine)
    model_gear = [Gear('Photon grenades')]
    model_min = 5
    model_max = 10
    model_name = 'Pathfinder'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class Shasui(Unit):
        type_name = 'Pathfinder Shas\'ui'
        type_id = 'shasui_v1'

        class Options(OptionsList):
            def __init__(self, parent):
                super(Pathfinders.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.variant(*ranged.PulsePistol)

        def __init__(self, parent=None):
            super(Pathfinders.Shasui, self).__init__(parent, points=Pathfinders.model_points, gear=[
                Gear('Photon grenades'), Gear('Markerlight'),
                Gear('Pulse carbine')
            ])
            self.opt = self.Options(self)

    class ModelCount(Count):
        @property
        def description(self):
            return [Pathfinders.model.clone().add([Gear('Markerlight'), Gear('Pulse carbine')]).
                    set_count(self.cur - self.parent.ionrifle.cur - self.parent.railrifle.cur)]

        @property
        def points(self):
            return super(Pathfinders.ModelCount, self).points - 3 * (self.parent.ionrifle.cur + self.parent.railrifle.cur)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Pathfinders.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, Pathfinders.Shasui(parent=self))

    class Drones(OptionsList):
        def __init__(self, parent):
            super(Pathfinders.Drones, self).__init__(parent=parent, name='Drones', limit=None)
            self.recondrone = self.variant('Recon Drone', points=get_cost(units.MB3ReconDrone),
                                           gear=[UnitDescription('MB3 Recon Drone',
                                                                 options=[Gear('Burst cannon')])])
            self.grav = self.variant('Grav-inhibitor Drone', points=get_cost(units.MV33GravInhibitorDrone),
                                                   gear=[UnitDescription('MV33 Grav-inhibitor Drone')])
            self.pulse = self.variant('Pulse accelerator Drone', points=get_cost(units.MV31PulseAcceleratorDrone),
                                                      gear=[UnitDescription('MV31 Pulse accelerator Drone')])

    def __init__(self, parent):
        super(Pathfinders, self).__init__(parent=parent, points=0, gear=[])
        self.leader = self.Leader(self)
        self.pathfinder = self.ModelCount(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                                          points=self.model_points)
        self.ionrifle = Count(self, 'Ion rifle', min_limit=0, max_limit=3, points=get_cost(ranged.IonRifle),
                              gear=self.model.clone().add(Gear('Ion rifle')))
        self.railrifle = Count(self, 'Rail rifle', min_limit=0, max_limit=3, points=get_cost(ranged.RailRifle),
                               gear=self.model.clone().add(Gear('Rail rifle')))
        self.sup_drones = self.Drones(self)
        armory.add_drones(self)

    def check_rules(self):
        super(Pathfinders, self).check_rules()
        self.pathfinder.min = self.model_min - self.leader.count
        self.pathfinder.max = self.model_max - self.leader.count
        Count.norm_counts(0, 3, [self.ionrifle, self.railrifle])
        Count.norm_counts(0, 2 - self.sup_drones.recondrone.value, self.drones)

    def get_count(self):
        return self.pathfinder.cur + self.leader.count

    def build_statistics(self):
        res = super(Pathfinders, self).build_statistics()
        res['Models'] += self.sup_drones.count + sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return 3 + 3 * (self.count > 5) +\
            (sum(c.cur for c in self.drones) > 0 or self.sup_drones.recondrone.value) +\
            (self.sup_drones.grav.value or self.sup_drones.pulse.value)


class PiranhaTeam(FastUnit, armory.SeptUnit):
    type_name = get_name(units.TX4Piranhas)
    type_id = 'piranha_team_v1'
    keywords = ['Vehicle', 'Fly']

    class Piranha(ListSubUnit):
        type_name = 'TX4 Pirahna'

        def __init__(self, parent):
            gear = [units.MV1GunDrone, units.MV1GunDrone]
            cost = points_price(get_cost(units.TX4Piranhas), *gear)
            super(PiranhaTeam.Piranha, self).__init__(parent, name='TX4 Piranha', points=cost,
                                                      gear=[Gear('MV1 Gun Drone', count=2)])
            self.Weapon(self)
            Count(self, 'Seeker missile', min_limit=0, max_limit=2, points=get_cost(ranged.SeekerMissile))

        class Weapon(OneOf):
            def __init__(self, parent):
                super(PiranhaTeam.Piranha.Weapon, self).__init__(parent=parent, name='Weapon')
                self.variant(*ranged.BurstCannon)
                self.variant(*ranged.FusionBlaster)

    def __init__(self, parent):
        super(PiranhaTeam, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Piranha, min_limit=1, max_limit=5)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        res = super(PiranhaTeam, self).build_statistics()
        res['Models'] *= 3
        return res

    def build_power(self):
        return 4 * self.models.count


class Drones(FastUnit, armory.SeptUnit):
    type_name = 'Tactical drones'
    type_id = 'drones_tac_v1'
    keywords = ['Drone', 'Fly']

    def __init__(self, parent):
        super(Drones, self).__init__(parent)
        armory.add_drones(self, 12)

    def check_rules(self):
        super(Drones, self).check_rules()
        Count.norm_counts(4, 12, self.drones)

    def get_count(self):
        return sum(c.cur for c in self.drones)

    def build_power(self):
        return 2 * ((self.count + 3) / 4)


class Vespids(FastUnit, armory.TauUnit):
    type_name = get_name(units.VespidStingwings)
    type_id = 'vespidstingwings_v1'
    faction = ['Vespid']
    keywords = ['Infantry', 'Fly']
    model_points = get_cost(units.VespidStingwings) + get_cost(ranged.NeutronBlaster)
    model_gear = [Gear('Neutron blaster')]

    class Leader(OptionsList):
        def __init__(self, parent):
            super(Vespids.Leader, self).__init__(parent=parent, name='')
            self.vespidstainleader = self.variant(
                'Vespid Stain Leader', get_cost(units.VespidStingwings),
                gear=UnitDescription(name='Vespid Stain Leader',
                                     options=Vespids.model_gear)
            )

    def __init__(self, parent):
        super(Vespids, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.vespidstingwing = Count(
            self, 'Vespid Stingwing', min_limit=4, max_limit=12, points=self.model_points,
            gear=UnitDescription(name='Vespid Stingwing', options=Vespids.model_gear)
        )

    def check_rules(self):
        self.vespidstingwing.min = 4 - self.leader.any
        self.vespidstingwing.max = 12 - self.leader.any

    def get_count(self):
        return self.vespidstingwing.cur + self.leader.vespidstainleader.value

    def build_power(self):
        return 3 * ((self.count + 3) / 4)
