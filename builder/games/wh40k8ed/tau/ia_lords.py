__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitDescription
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import ia_units
from . import ia_ranged
from . import ranged


class Taunar(LordsUnit, armory.SeptUnit):
    type_name = get_name(ia_units.TaUnarSupremacyArmour) + ' (Imperial Armour)'
    type_id = 'taunar_v1'
    keywords = ['Vehicle', 'Titanic', 'Monster', 'Battlesuit']
    power = 55

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Taunar.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ia_ranged.TriAxisIonCannon)
            self.variant(*ia_ranged.FusionEradicator)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Taunar.Weapon2, self).__init__(parent, '')
            self.variant(*ia_ranged.PulseOrdnanceMultiDriver)
            self.variant(*ia_ranged.NexusMeteorMissileSystem)
            self.variant(*ia_ranged.HeavyRailCannon)
            self.variant('Cluster shells', 0)

    def __init__(self, parent):
        gear = [ranged.SmartMissileSystem, ranged.SmartMissileSystem, ranged.SmartMissileSystem,
                ranged.BurstCannon, ranged.BurstCannon, ranged.BurstCannon, ranged.BurstCannon,
                ('Crushing feet', 0)]
        cost = points_price(get_cost(ia_units.TaUnarSupremacyArmour), *gear)
        super(Taunar, self).__init__(
            parent, get_name(ia_units.TaUnarSupremacyArmour),
            points=cost, gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon1(self)
        self.Weapon2(self)


class Manta(LordsUnit, armory.SeptUnit):
    type_name = get_name(ia_units.MantaSuperHeavyDropship) + ' (Imperial Armour)'
    type_id = 'manta_v1'
    keywords = ['Vehicle', 'Fly']
    power = 100

    def __init__(self, parent):
        gear = [ia_ranged.HeavyRailCannon, ia_ranged.MissilePod] * 2 + \
            [ia_ranged.LongBarrelledIonCannon] * 6 + \
            [ia_ranged.LongBarrelledBurstCannon] * 6 + \
            [ia_ranged.SeekerMissile] * 10
        cost = points_price(get_cost(ia_units.MantaSuperHeavyDropship), *gear)
        super(Manta, self).__init__(
            parent=parent, points=cost, name=get_name(ia_units.MantaSuperHeavyDropship),
            gear=create_gears(*gear), static=True
        )
