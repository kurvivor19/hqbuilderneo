__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import units
from . import ranged


class RazorShark(FlierUnit, armory.SeptUnit):
    type_name = get_name(units.AX3RazorsharkStrikeFighter)
    type_id = 'razorsharkstrikefighter_v1'
    keywords = ['Vehicle', 'Fly']
    power = 8

    def __init__(self, parent):
        gear = [ranged.QuadIonTurret, ranged.SeekerMissile, ranged.SeekerMissile]
        cost = points_price(get_cost(units.AX3RazorsharkStrikeFighter), *gear)
        super(RazorShark, self).__init__(
            parent=parent, points=cost,
            gear=create_gears(*gear)
        )
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RazorShark.Weapon, self).__init__(parent=parent, name='Weapon')
            self.burstcannon = self.variant(*ranged.BurstCannon)
            self.missilepod = self.variant(*ranged.MissilePod)


class SunShark(FlierUnit, armory.SeptUnit):
    type_name = get_name(units.AX39SunSharkBomber)
    type_id = 'sunsharkbomber_v3'
    keywords = ['Vehicle', 'Fly']
    power = 9

    def __init__(self, parent):
        gear = [ranged.Markerlight, ranged.MissilePod, ranged.SeekerMissile, ranged.SeekerMissile]
        cost = points_price(get_cost(units.AX39SunSharkBomber), *gear)

        super(SunShark, self).__init__(
            parent=parent, points=cost + get_cost(units.MV17InterceptorDrone) * 2,
            gear=[Gear('Markerlight'), Gear('Missle Pod'), Gear('Seeker missile', count=2),
                  UnitDescription(name='MV17 Interceptor Drone', options=[Gear('Ion rifle')], count=2)]
        )
        Count(self, get_name(ranged.MissilePod), 1, 2, points=get_cost(ranged.MissilePod))

    def build_statistics(self):
        res = super(SunShark, self).build_statistics()
        res['Models'] += 2
        return res
