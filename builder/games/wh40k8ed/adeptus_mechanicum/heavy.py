__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class KastelanManiple(HeavyUnit, armory.CultUnit):
    type_id = 'kastellan_v1'
    type_name = get_name(units.KastelanRobots)
    keywords = ['Vehicle']
    power = 12

    class Robot(ListSubUnit):
        type_name = 'Kastelan Robot'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(KastelanManiple.Robot.Weapon1, self).__init__(parent, 'Hand weapons')
                self.variant(*melee.KastelanFists)
                self.variant('Two heavy phosphor blaster', 2 * get_cost(ranged.HeavyPhosphorBlaster),
                             gear=create_gears(ranged.HeavyPhosphorBlaster, ranged.HeavyPhosphorBlaster))

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(KastelanManiple.Robot.Weapon2, self).__init__(parent, 'Caparace weapon')
                self.variant(*ranged.IncendineCombustor)
                self.variant(*ranged.HeavyPhosphorBlaster)

        def __init__(self, parent):
            super(KastelanManiple.Robot, self).__init__(parent, points=get_cost(units.KastelanRobots))
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(KastelanManiple, self).__init__(parent)
        self.robots = UnitList(self, self.Robot, 2, 6)

    def get_count(self):
        return self.robots.count

    def build_power(self):
        return self.power * ((self.robots.count + 1) / 2)


class Dunecrawler(HeavyUnit, armory.SkitariiUnit):
    type_name = get_name(units.OnagerDunecrawler)
    type_id = 'crawler_v1'
    keywords = ['Vehicle']
    power = 7

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(Dunecrawler.MainWeapon, self).__init__(
                parent, 'Main weapon')
            self.variant(*ranged.EradicationBeamer)
            self.variant(join_and(ranged.NeutronLaser, ranged.CognisHeavyStubber),
                         get_costs(ranged.NeutronLaser, ranged.CognisHeavyStubber),
                         gear=create_gears(ranged.NeutronLaser, ranged.CognisHeavyStubber))
            self.variant(*ranged.TwinHeavyPhosphorBlaster)
            self.array = self.variant(*ranged.IcarusArray)

    class VehicleEquipment(OptionsList):
        def __init__(self, parent):
            super(Dunecrawler.VehicleEquipment, self).__init__(parent, 'Options')
            self.variant(*ranged.CognisHeavyStubber)
            self.alt = [self.variant(*wargear.BroadSpectrumDataTether),
                        self.variant(*wargear.SmokeLaunchers)]

        def check_rules(self):
            super(Dunecrawler.VehicleEquipment, self).check_rules()
            OptionsList.process_limit(self.alt, 1)

    def __init__(self, parent):
        super(Dunecrawler, self).__init__(parent, points=get_cost(units.OnagerDunecrawler))
        self.wep = self.MainWeapon(self)
        self.opt = self.VehicleEquipment(self)
