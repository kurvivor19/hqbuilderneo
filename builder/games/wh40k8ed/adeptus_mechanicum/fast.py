__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Dragoons(FastUnit, armory.SkitariiUnit):
    type_name = get_name(units.SydonianDragoons)
    type_id = 'dragoons_v1'
    keywords = ['Vehicle']
    power = 3

    class Dragoon(ListSubUnit):
        type_name = 'Sydonian Dragoon'

        class MainWeapon(OneOf):
            def __init__(self, parent):
                super(Dragoons.Dragoon.MainWeapon, self).__init__(
                    parent, 'Main weapon')
                self.variant(*melee.TaserLance)
                self.variant(*ranged.RadiumJezzail)

        class OptionalWeapon(OptionsList):
            def __init__(self, parent):
                super(Dragoons.Dragoon.OptionalWeapon, self).__init__(
                    parent, 'Optional weapon')
                self.variant(*ranged.PhosphorSerpenta)

        def __init__(self, parent):
            super(Dragoons.Dragoon, self).__init__(parent, points=get_cost(units.SydonianDragoons),
                                                   gear=create_gears(wargear.BroadSpectrumDataTether))
            self.MainWeapon(self)
            self.OptionalWeapon(self)

    def __init__(self, parent):
        super(Dragoons, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Dragoon, 1, 6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class Ironstriders(FastUnit, armory.SkitariiUnit):
    type_name = get_name(units.IronstriderBallistarii)
    type_id = 'striders_v1'
    keywords = ['Vehicle']
    power = 4

    class Ironstrider(ListSubUnit):
        type_name = 'Ironstrider Ballistarius'

        class MainWeapon(OneOf):
            def __init__(self, parent):
                super(Ironstriders.Ironstrider.MainWeapon, self).__init__(
                    parent, 'Main weapon')
                self.variant(*ranged.TwinCognisAutocannon)
                self.variant(*ranged.TwinCognisLascannon)

        def __init__(self, parent):
            super(Ironstriders.Ironstrider, self).__init__(parent, points=get_cost(units.IronstriderBallistarii),
                                                           gear=create_gears(wargear.BroadSpectrumDataTether))
            self.MainWeapon(self)

    def __init__(self, parent):
        super(Ironstriders, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Ironstrider, 1, 6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count
