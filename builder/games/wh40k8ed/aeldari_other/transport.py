__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, TransportUnit
from builder.games.wh40k8ed.options import Gear
from . import armory, units, ranged
from builder.games.wh40k8ed.utils import *


class Starweaver(TransportUnit, armory.HarlequinUnit):
    type_name = 'Starweaver'
    type_id = 'starweaver_v1'

    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 5

    def __init__(self, parent):
        gear = [ranged.ShurikenCannon, ranged.ShurikenCannon]
        points = points_price(get_cost(units.Starweaver), *gear)
        super(Starweaver, self).__init__(parent, points=points, static=True,
                                         gear=create_gears(*gear))
